#ifndef INCLUDE_OTTERS_CUDA_PRIMITIVES_RANDOM_CUH
#define INCLUDE_OTTERS_CUDA_PRIMITIVES_RANDOM_CUH

#include <otters/common/containers/span.h>
#include <curand_kernel.h>

namespace otters {

__global__ void generate_uniform(double* data, ssize_t size, unsigned long long seed) {
    auto idx = threadIdx.x + blockIdx.x * blockDim.x;
    auto block_size = gridDim.x * blockDim.x;
    curandState_t state;
    curand_init(seed, idx, 0, &state);
    for(auto i = idx; i < size; i += block_size) {
        data[i] = curand_uniform_double(&state);
        // if(blockIdx.x == 0 && threadIdx.x == 1)
            // printf("generated data[%d] = %lf", i, data[i]);
    }
}

__global__ void generate_normal(double* data, ssize_t size, unsigned long long seed) {
    auto idx = threadIdx.x + blockIdx.x * blockDim.x;
    auto block_size = gridDim.x * blockDim.x;
    curandState_t state;
    curand_init(seed, idx, 0, &state);
    for(auto i = idx; i < size; i += block_size) {
        data[i] = curand_normal_double(&state);
    }
}

__global__ void generate_log_normal(double* data, ssize_t size, unsigned long long seed) {
    auto idx = threadIdx.x + blockIdx.x * blockDim.x;
    auto block_size = gridDim.x * blockDim.x;
    curandState_t state;
    curand_init(seed, idx, 0, &state);
    for(auto i = idx; i < size; i += block_size) {
        data[i] = curand_log_normal_double(&state, 0.0, 1.0);
    }
}


}

#endif
