#pragma once

#ifndef OTTERS_CUDA_PRIMITIVES_SHFL_CUH
#define OTTERS_CUDA_PRIMITIVES_SHFL_CUH

#include <type_traits>
#include <otters/common/utils/void_t.h>

namespace otters {

    /*
template<typename T>
inline __device__ T shfl_sync(unsigned mask, T var, int srcLane, int width = warpSize);


template<typename T>
inline __device__ T shfl_xor_sync(unsigned mask, T var, int laneMask, int width = warpSize);

template<typename T>
inline __device__ T shfl_up_sync(unsigned mask, T var, unsigned int delta, int width = warpSize);
*/

template<typename T, class = void_t<>>
struct has_can_reinterpret_as : std::false_type {};

template<typename T>
struct has_can_reinterpret_as<T, void_t<typename T::can_reinterpret_as_t>> : std::true_type {};

template<typename T>
constexpr bool has_can_reinterpret_as_v = has_can_reinterpret_as<T>();

template<typename T>
constexpr bool has_default_shfl_v =
    std::is_same<T,int32_t>::value || std::is_same<T, int64_t>::value ||
    std::is_same<T,uint32_t>::value || std::is_same<T, uint64_t>::value ||
    std::is_same<T, float>::value  || std::is_same<T, double>::value;

template<typename T>
inline __device__ typename std::enable_if<has_default_shfl_v<T>, T>::type
shfl_sync(unsigned mask, T var, int srcLane, int width = warpSize) {
    return __shfl_sync(mask, var, srcLane, width);
}

template<typename T>
inline __device__ typename std::enable_if<has_default_shfl_v<T>, T>::type
shfl_xor_sync(unsigned mask, T var, int laneMask, int width = warpSize) {
    return __shfl_xor_sync(mask, var, laneMask, width);
}

template<typename T>
inline __device__ typename std::enable_if<has_default_shfl_v<T>, T>::type
shfl_up_sync(unsigned mask, T var, unsigned int delta, int width = warpSize) {
    return __shfl_up_sync(mask, var, delta, width);
}

template<typename T>
inline __device__ typename std::enable_if<has_default_shfl_v<T>, T>::type
shfl_down_sync(unsigned mask, T var, unsigned int delta, int width = warpSize) {
    return __shfl_down_sync(mask, var, delta, width);
}


template<typename T>
inline __device__ typename std::enable_if<has_can_reinterpret_as_v<T>, T>::type
shfl_sync(unsigned mask, T var, int srcLane, int width = warpSize) {
    using TT = typename T::can_reinterpret_as_t;
    TT* pvar = reinterpret_cast<TT*>(&var);
    TT result = __shfl_sync(mask, *pvar, srcLane, width);
    T* pres = reinterpret_cast<T*>(&result);
    return *pres;
}
template<typename T>
inline __device__ typename std::enable_if<has_can_reinterpret_as_v<T>, T>::type
shfl_xor_sync(unsigned mask, T var, int laneMask, int width = warpSize) {
    using TT = typename T::can_reinterpret_as_t;
    TT* pvar = reinterpret_cast<TT*>(&var);
    TT result = __shfl_xor_sync(mask, *pvar, laneMask, width);
    T* pres = reinterpret_cast<T*>(&result);
    return *pres;
}

template<typename T>
inline __device__ typename std::enable_if<has_can_reinterpret_as_v<T>, T>::type
shfl_up_sync(unsigned mask, T var, unsigned int delta, int width = warpSize) {
    using TT = typename T::can_reinterpret_as_t;
    TT* pvar = reinterpret_cast<TT*>(&var);
    TT result = __shfl_up_sync(mask, *pvar, delta, width);
    T* pres = reinterpret_cast<T*>(&result);
    return *pres;
}

template<typename T>
inline __device__ typename std::enable_if<has_can_reinterpret_as_v<T>, T>::type
shfl_down_sync(unsigned mask, T var, unsigned int delta, int width = warpSize) {
    using TT = typename T::can_reinterpret_as_t;
    TT* pvar = reinterpret_cast<TT*>(&var);
    TT result = __shfl_down_sync(mask, *pvar, delta, width);
    T* pres = reinterpret_cast<T*>(&result);
    return *pres;
}


}

#endif
