#pragma once

#ifndef OTTERS_CUDA_PRIMITIVES_HISTOGRAM_H
#define OTTERS_CUDA_PRIMITIVES_HISTOGRAM_H

#include <otters/cuda/primitives/functional.cuh>
#include <otters/cuda/primitives/warp_algorithms.cuh>
#include <otters/common/primitives/histogram.h>

namespace otters {

namespace detail {

/*
 * This function build a histogram at final_histogram* for
 * each block collectively.
 * assuming all input keys are in range [0..total_bucket-1),
 * each block is calling using the same parameters.
 * 
 */    
template<size_t total_buckets, typename Count, typename Iter, typename Check = NoCheck>
__device__ inline void build_histograms_fixed_len
        (Count* final_histogram, Count* local_histogram, Iter first, Iter last) {
    auto thread_id = threadIdx.x;
    auto block_size = blockDim.x;
    auto warp_id = threadIdx.x % warpSize;
    build_local_histograms_with_bucket_id<Count, Iter, Check>(local_histogram, total_buckets,
        first + thread_id, last, block_size);

    for(auto i = 0u; i < total_buckets; ++i) {
        Count sum_warp = warp_reduction<otters::plus<Count>>(local_histogram[i]);
        if(warp_id == 0)
            atomicAdd(&final_histogram[i], sum_warp);
    }
}

}

}


#endif
