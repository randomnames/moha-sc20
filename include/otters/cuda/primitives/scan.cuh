#pragma once

#ifndef OTTERS_CUDA_PRIMITIVES_SCAN_CUH
#define OTTERS_CUDA_PRIMITIVES_SCAN_CUH

#include <otters/cuda/primitives/functional.cuh>
#include <otters/cuda/container/iterator.cuh>
#include <otters/cuda/primitives/warp_algorithms.cuh>
#include <assert.h>

namespace otters {

/*
struct RunInfo {
    uint16_t _run_id;
    uint16_t _run_length;
}

template<typename Iter>
struct RunInfoComputeFunc {
public:
    RunInfo operator(Iter& iter) {
        RunInfo result{0,0,false};
        result._run_length = (_bucket_id == *iter) ? 1 : 0;
        if(_head == iter || *iter != *(iter-1)) {
            result._run_id = 1;
        }
        return result;
    }
public:
    Iter _head;
    BucketID _bucket_id;
}

struct RunInfoOPlus {
    RunInfo operator()(const RunInfoOPlus& lhs, const RunInfoOPlus& rhs) {
        RunInfo result;
        result._run_id = lhs._run_id + rhs._run_id;
        if(rhs._run_id > 0) {
            result._run_length = rhs._run_length;
        } else {
            result._run_length = lhs._run_length + rhs._run_length;
        }
    }
}

struct RunInfoWriter {
    void operator=(RunInfo& info) {
        if(info._head_flag) {
            _device[info._run_id * 2] = info.run_position;
            _device[info._run_id * 2 + 1] = info.run_length;
        }
    }
public:
    uint16_t _device;
}
*/

template<ScanType kind, typename BinaryOp, typename Iter, typename OIter, typename OBuffer, typename T>
__device__ void scan(OBuffer buffer, OIter out, Iter begin, Iter end, T id) {
    using detail::WARP_SIZE;
    using detail::warp_reduction_multiple;
    using detail::warp_scan_multiple;

    assert(blockDim.x <= WARP_SIZE * WARP_SIZE);
    //do a wrap reduce
    auto total_size = end - begin;
    auto size_per_warp = static_cast<int>(total_size / blockDim.x * WARP_SIZE);
    auto warp_id = threadIdx.x / WARP_SIZE;
    // auto idx_per_warp = threadIdx.x % WARP_SIZE;
    auto warp_offset = static_cast<int>(warp_id * size_per_warp);
    auto reduce_result = detail::warp_reduction_multiple_noncommutative<BinaryOp>
            (begin + warp_offset, begin + warp_offset + size_per_warp);
    if(threadIdx.x % WARP_SIZE == 0) {
        buffer[warp_id] = reduce_result;
    //    printf("blockIdx.x = %d, threadIdx.x = %d, buffer[%d] = %d\n", blockIdx.x, threadIdx.x, warp_id, reduce_result);
    }
           
    __syncthreads();
    //do a wrap scan on scan_buffer
    if(warp_id == 0) {
        auto total_warps = blockDim.x / WARP_SIZE;
        assert(threadIdx.x < WARP_SIZE);
        // TODO: this requires the buffer is 32-wide, which is unnecessary.
        // Probably wanna templatize on WIDTH parameter
        auto result = detail::warp_scan<ScanType::Exclusive, BinaryOp>(buffer[warp_id], id);
        if(threadIdx.x < total_warps)
            buffer[threadIdx.x] = result;
    }
    //if (threadIdx.x % WARP_SIZE == 0) {
    //    printf("blockIdx.x = %d, threadIdx.x = %d, buffer[%d] = %d\n",
    //            blockIdx.x,
    //            threadIdx.x,
    //            warp_id,
    //            buffer[warp_id]);
    //}

    __syncthreads();
    //do a wrap scan with the id = scan_buffer[warp_id]
    detail::warp_scan_multiple<kind, BinaryOp>(out + warp_offset,
        begin + warp_offset, begin + warp_offset + size_per_warp, buffer[warp_id]);
} 

template<ScanType kind, typename BinaryOp, typename OBuffer, typename T>
__device__ T block_scan_commutative(OBuffer buffer, T t, T id) {
    using detail::WARP_SIZE;

    assert(blockDim.x < WARP_SIZE * WARP_SIZE);
    //do a wrap reduce
    auto warp_id = threadIdx.x / WARP_SIZE;
    // auto idx_per_warp = threadIdx.x % WARP_SIZE;
    auto reduce_result = detail::warp_reduction<BinaryOp>(t);
    if(threadIdx.x % WARP_SIZE == 0) {
        buffer[warp_id] = reduce_result;
        // printf("[reduction] blockIdx.x = %d, threadIdx.x = %d, buffer[%d] = %d\n", blockIdx.x, threadIdx.x, warp_id, reduce_result);
    }
           
    __syncthreads();
    //do a wrap scan on scan_buffer
    if(warp_id == 0) {
        auto total_warps = blockDim.x / WARP_SIZE;
        assert(threadIdx.x < WARP_SIZE);
        // TODO: this requires the buffer is 32-wide, which is unnecessary.
        // Probably wanna templatize on WIDTH parameter
        auto result = detail::warp_scan<ScanType::Exclusive, BinaryOp>(buffer[threadIdx.x], id);
        if(threadIdx.x < total_warps) {
            buffer[threadIdx.x] = result;
            //     printf("[after scan1] blockIdx.x = %d, threadIdx.x = %d, buffer[%d] = %d id = %d\n",
            //             blockIdx.x,
            //             threadIdx.x,
            //             threadIdx.x,
            //             buffer[threadIdx.x],
            //             id);
        }
    }
    // __syncthreads();
    // if (threadIdx.x % WARP_SIZE == 0) {
    //     printf("[after scan] blockIdx.x = %d, threadIdx.x = %d, buffer[%d] = %d id = %d\n",
    //             blockIdx.x,
    //             threadIdx.x,
    //             warp_id,
    //             buffer[warp_id],
    //             id);
    // }
    __syncthreads();

    //do a wrap scan with the id = scan_buffer[warp_id]
    return detail::warp_scan<kind, BinaryOp>(t, buffer[warp_id]);
} 


}

#endif 
