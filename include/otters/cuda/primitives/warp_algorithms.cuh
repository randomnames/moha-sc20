#pragma once

#ifndef OTTERS_CUDA_PRIMITIES_WARP_ALGORITHMS_H
#define OTTERS_CUDA_PRIMITIES_WARP_ALGORITHMS_H

#include <otters/cuda/primitives/shfl.cuh>
#include <otters/cuda/clion_helper.h>

namespace otters {

enum class ScanType {
    Inclusive,
    Exclusive
};

template<typename Iter>
struct iterator_traits {
    using value_t = typename Iter::value_t;
};

template<typename T>
struct iterator_traits<T*> {
    using value_t = T;
};

namespace detail {

const static int WARP_MASK_FULL = 0xFFFFFFFF;
const static int WARP_SIZE = 32;
const static int LAST_WARP = WARP_SIZE - 1;

template<typename BinaryOp, typename T>
__device__ T warp_reduction(T t) {
    BinaryOp op;
    auto result = t;
    //auto thread_id = threadIdx.x;
#pragma unroll
    for(auto i = WARP_SIZE / 2; i >= 1; i /= 2) {
        result = op(result, shfl_xor_sync(WARP_MASK_FULL, result, i));
    }
    return result;
}

template<typename BinaryOp, typename Iter, typename T = typename iterator_traits<Iter>::value_t>
__device__ T warp_reduction_multiple(Iter begin, Iter end) {
    BinaryOp op;
    auto result = T();
    auto idx_in_warp = threadIdx.x % WARP_SIZE;
    for(auto it = begin + idx_in_warp; it < end; it += WARP_SIZE) {
        result = op(result, *it);
    }
    result = warp_reduction<BinaryOp, T>(result);
    return result;
}

template<typename BinaryOp, typename T>
__device__ T warp_reduction_noncommutative(T t) {
    BinaryOp op;
    auto result = t;
    //auto thread_id = threadIdx.x;
#pragma unroll
    for(auto i = 1; i < WARP_SIZE; i *= 2) {
        result = op(result, shfl_down_sync(WARP_MASK_FULL, result, i));
    }
    return result;
}


template<typename BinaryOp, typename Iter, typename T = typename iterator_traits<Iter>::value_t>
__device__ T warp_reduction_multiple_noncommutative(Iter begin, Iter end) {
    BinaryOp op;
    auto result = T();
    auto idx_in_warp = threadIdx.x % WARP_SIZE;
    for(auto it = begin + idx_in_warp; it < end; it += WARP_SIZE) {
        auto partial_result = warp_reduction_noncommutative<BinaryOp>(*it);
        if(idx_in_warp == 0) {
            result = op(result, partial_result);
        }
    }
    return result;
}


template<ScanType kind, typename BinaryOp, typename T>
__device__ T warp_scan(T t, T id) {
    BinaryOp op;
    auto result = t;
    auto idx_in_warp = threadIdx.x % WARP_SIZE;
#pragma unroll
    for(auto delta = 1u; delta <= WARP_SIZE / 2; delta *= 2) {
        auto lhs = shfl_up_sync(WARP_MASK_FULL, result, delta);
        if(idx_in_warp >= delta)
            result = op(lhs, result);
        //todo: add a test to capture the bug
    }
    result = op(id, result);
    if(kind == ScanType::Exclusive) {
        result = shfl_up_sync(WARP_MASK_FULL, result, 1);
        if(idx_in_warp == 0) {
            result = id;
        }
    }
    
    // printf("threadIdx = %d result = %d\n", threadIdx.x, result);
    return result;
}

template<ScanType kind, typename BinaryOp, typename Iter, typename OIter, typename T = typename Iter::value_t>
__device__ void warp_scan_multiple(OIter out, Iter begin, Iter end, T id) {
    auto idx_in_warp = threadIdx.x % WARP_SIZE;
    auto it = begin + idx_in_warp;
    auto oit = out + idx_in_warp;
    for( ; it < end; it += WARP_SIZE, oit += WARP_SIZE) {
        auto result = warp_scan<ScanType::Inclusive, BinaryOp>(*it, id);
        //  printf("threadIdx = %d result = %d\n", threadIdx.x, result);
        auto old_id = id;
        id = shfl_sync(WARP_MASK_FULL, result, LAST_WARP);
        if (kind == ScanType::Exclusive) {
            result = shfl_up_sync(WARP_MASK_FULL, result, 1);
            if (idx_in_warp == 0) {
                result = old_id;
            }
        }
        *oit = result;

        // if(threadIdx.x % WARP_SIZE == 0) {
        //   printf("threadIdx.x = %d, i= %d\n", threadIdx.x, id);
        // }
    }
}

}

}

#endif
