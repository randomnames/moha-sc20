#pragma once

#ifndef OTTERS_CUDA_PRIMITIVES_FUNCTIONAL_CUH
#define OTTERS_CUDA_PRIMITIVES_FUNCTIONAL_CUH

namespace otters {

template<typename T>
struct plus {
    __host__ __device__ T operator()(T a, T b) {
        return a + b;
    }
};
}

#endif
