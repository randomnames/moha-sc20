#ifndef OTTERS_MULTI_ATTRIBUTE_BUFFER_CUH
#define OTTERS_MULTI_ATTRIBUTE_BUFFER_CUH

#include <otters/common/containers/span.h>
#include <otters/common/io/serialization.h>
#include <otters/cuda/container/buffer.cuh>

namespace otters {

template<typename Buffer, size_t num_attributes>
class MultiAttributeBuffer {
public:
    Buffer& attribute(size_t i) {
        return _attributes[i];
    }

    void clear() {
        for(auto i = 0u; i < num_attributes; ++i) {
            _attributes[i].clear();
        }
    }

    template<typename CPUBuffer>
    void copy_to_host_async_stream(MultiAttributeBuffer<CPUBuffer, num_attributes>& cpu_buffer, cudaStream_t stream) {
        for(size_t i = 0; i < num_attributes; ++i) {
            attribute(i).copy_to_host_async_stream(cpu_buffer.attribute(i), stream);
        }
    }

    template<typename CPUBuffer>
    void resize_for_copy(MultiAttributeBuffer<CPUBuffer, num_attributes>& cpu_buffer) {
        for(size_t i = 0; i < num_attributes; ++i) {
            attribute(i).resize_for_copy(cpu_buffer.attribute(i));
        }
    }

    ssize_t serialize_size() const {
        ssize_t result = 0;
        for(auto i = 0u; i < num_attributes; ++i) {
            result += otters::serialize_size(_attributes[i]);
        }
        return result;
    }

    span<char> serialize(span<char> buffer) const {
        assert(buffer.size() >= serialize_size());
        auto output_buffer = buffer;
        for(auto i = 0u; i < num_attributes; ++i) {
            output_buffer = otters::serialize(output_buffer, _attributes[i]);
        }
        return output_buffer;
    }

    span<char> deserialize(span<char> buffer) {
        // assert(buffer.size() > serialize_size());
        auto output_buffer = buffer;
        for(auto i = 0u; i < num_attributes; ++i) {
            output_buffer = otters::deserialize(_attributes[i], output_buffer);
        }
        return output_buffer;
    }

private:
    std::array<Buffer, num_attributes> _attributes;
};

}

#endif
