#pragma once

#ifndef INLCUDE_OTTERS_CUDA_CONTAINER_ITERATOR_CUH
#define INLCUDE_OTTERS_CUDA_CONTAINER_ITERATOR_CUH

namespace otters {

namespace detail {
/*
 * run count iterator. Returns 1 if an number is the first element
 * or if is different from the last element.
 */
template<typename T, typename Out = T>
struct run_count_iterator {
    using value_t = T;
    using output_t = Out;
    __device__ T operator*() {
        if (_idx == 0 || _head[_idx] != _head[_idx - 1])
            return _head[_idx];
        else
            return -1;
    }
    __host__ __device__ bool operator<(const run_count_iterator& iter) {
        return _idx < iter._idx;
    }
    __host__ __device__ bool operator==(const run_count_iterator& iter) {
        return _idx == iter._idx;
    }
    __host__ __device__ run_count_iterator& operator+=(T skip) {
        _idx += skip;
        return *this;
    }
    __host__ __device__ run_count_iterator operator+(T skip) {
        return { _head, _idx + skip };
    }
    __host__ __device__ run_count_iterator& operator++() {
        ++_idx;
        return *this;
    }
    T* _head;
    int32_t _idx;
};

} // namespace detail

template<typename Iter, typename Func>
struct iterator_adaptor {
public:
    Iter _iter;
    Func _func;

public:
    using value_t = decltype(_func(_iter));
    using underlying_t = decltype(*_iter);

public:
    __host__ __device__ iterator_adaptor(Iter iter, Func func = Func()) : _iter(iter), _func(func) {
    }
    __host__ __device__ value_t operator*() {
        return _func(_iter);
    }
    __host__ __device__ underlying_t original_value() {
        return *_iter;
    }
    __host__ __device__ bool operator<(const iterator_adaptor& other) {
        return _iter < other._iter;
    }
    __host__ __device__ bool operator==(const iterator_adaptor& other) {
        return _iter == other._iter;
    }
    __host__ __device__ bool operator!=(const iterator_adaptor& other) {
        return _iter != other._iter;
    }
    __host__ __device__ iterator_adaptor& operator++() {
        _iter++;
        return *this;
    }
    __host__ __device__ iterator_adaptor& operator+=(int skip) {
        _iter += skip;
        return *this;
    }
    __host__ __device__ iterator_adaptor& operator-=(int skip) {
        _iter -= skip;
        return *this;
    }
    __host__ __device__ iterator_adaptor operator+(int skip) {
        auto result = *this;
        result += skip;
        return result;
    }

    __host__ __device__ iterator_adaptor operator-(int skip) {
        auto result = *this;
        result -= skip;
        return result;
    }
    __host__ __device__ ptrdiff_t operator-(const iterator_adaptor& other) {
        return _iter - other._iter;
    }


};

template<typename OIter, typename IIter, typename Info, typename OutputFunc>
struct gather_output_iterator {
public:
    OIter _oiter;
    IIter _iiter;
    OutputFunc _output_func;
    using output_t = typename std::decay<decltype(*_oiter)>::type;

public:
    struct output_delegate {
        __host__ __device__ output_t& operator=(const Info& info) {
            // printf("blockIdx.x = %d, threadIdx.x = %d, info  = %d", blockIdx.x, threadIdx.x, info);
            auto result = _self._output_func(_self._iiter, info);
            if (result._flag) {
                //FIXME: Add a test if oiter and iter is different type;
                _self._oiter[result._index] = result._result;
            }
            return *_self._oiter;
        }

    public:
        gather_output_iterator _self;
    };

public:
    __host__ __device__ gather_output_iterator(
        OIter oiter, IIter iiter, OutputFunc func = OutputFunc())
        : _oiter(oiter), _iiter(iiter), _output_func(func) {
    }

    __host__ __device__ output_delegate operator*() {
        return output_delegate{ *this };
    }

    __host__ __device__ bool operator==(const gather_output_iterator& iter) {
        return _iiter == iter._iiter;
    }
    __host__ __device__ output_delegate operator[](int index) {
        return output_delegate{ *this + index };
    }

    __host__ __device__ gather_output_iterator& operator+=(int skip) {
        _iiter += skip;
        return *this;
    }
    __host__ __device__ gather_output_iterator operator+(int skip) {
        auto new_iter = *this;
        new_iter += skip;
        return new_iter;
    }
    __host__ __device__ gather_output_iterator& operator++() {
        ++_iiter;
        return *this;
    }
};

} // namespace otters

#endif
