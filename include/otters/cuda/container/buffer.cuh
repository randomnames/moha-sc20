#pragma once

#ifndef OTTERS_CUDA_CONTAINER_BUFFER_CUH
#define OTTERS_CUDA_CONTAINER_BUFFER_CUH

#include <otters/cuda/exception/cuda_assert.h>
#include <otters/common/containers/buffer.h>
#include <memory>
#include <future>
#include <fstream>
#include <iostream>


#define BOOST_STACKTRACE_USE_ADDR2LINE
#include <boost/stacktrace.hpp>

namespace otters {

template<typename T>
struct CUDAManagedAllocator {
    static T* alloc(ssize_t size) {
        T* data;
        OTTERS_CUDA_ERRORCHECK( cudaMallocManaged(&data, size * sizeof(T)) );
        return data;
    }

    static void free(T* data) {
        OTTERS_CUDA_ERRORCHECK( cudaFree(data) );
    }

    static void memcpy(T* target, T* src, ssize_t size) {
        std::copy(src, src + size, target);
    }
};

template<typename T>
struct CUDADeviceAllocator {
    static T* alloc(ssize_t size) {
        T* data;
        OTTERS_CUDA_ERRORCHECK( cudaMalloc(&data, size * sizeof(T)) );
        // fmt::print("Allocated {0} bytes at {1:x}\n", size * sizeof(T), (uint64_t) data);
        return data;
    }

    static void free(T* data) {
        // fmt::print("Freed bytes at {0:x}\n", (uint64_t) data);
        OTTERS_CUDA_ERRORCHECK( cudaFree(data) );
    }

    static void memcpy(T* target, T* src, ssize_t size) {
        (void) target;
        (void) src;
        (void) size;
        // OTTERS_CUDA_ERRORCHECK( cudaMemcpy(target, src, sizeof(T) * size, cudaMemcpyDeviceToDevice) );
    }
};

template<typename T>
struct CUDAHostAllocator {
    static T* alloc(ssize_t size) {
        T* data;
        OTTERS_CUDA_ERRORCHECK( cudaMallocHost(&data, size * sizeof(T)) );
        return data;
    }

    static void free(T* data) {
        OTTERS_CUDA_ERRORCHECK( cudaFreeHost(data) );
    }

    static void memcpy(T* target, T* src, ssize_t size) {
        std::copy(src, src + size, target);
    }
};


template<typename T>
using cuda_managed_buffer = resizable_buffer<T, CUDAManagedAllocator<T>>;

template<typename T>
using cuda_host_buffer = resizable_buffer<T, CUDAHostAllocator<T>>;

template<typename T>
class cuda_device_buffer : public resizable_buffer<T, CUDADeviceAllocator<T>> {
public:
    using base = resizable_buffer<T, CUDADeviceAllocator<T>>;
    using base::base;
public:
    void resize_for_copy(cuda_host_buffer<T>& host_buffer) {
        host_buffer.clear();
        host_buffer.resize(this->size());
    }
    void copy_to_host_async_stream(cuda_host_buffer<T>& host_buffer, cudaStream_t stream) {
        OTTERS_CUDA_ERRORCHECK( cudaMemcpyAsync(host_buffer.get(),this->get(), this->size() * sizeof(T), cudaMemcpyDeviceToHost, stream) );
    }
};


} // namespace otters

#endif
