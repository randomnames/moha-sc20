#ifndef OTTERS_CUDA_QUERY_DRIVERS_MPI_BITMAP_CSM_CUH
#define OTTERS_CUDA_QUERY_DRIVERS_MPI_BITMAP_CSM_CUH

#include <otters/common/bitmap/binning.h>
#include <otters/cuda/bitmap/binning.cuh>
#include <otters/cuda/bitmap/query_driver.h>
#include <otters/cuda/bitmap/roaring.cuh>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/buffer_pool.cuh>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/query/ops/bitmap_csm.cuh>
#include <otters/cuda/query/ops/consume_operator.cuh>
#include <otters/cuda/query/ops/gpu_bitmap_input.cuh>
#include <otters/cuda/query/ops/gpu_plain_input.cuh>
#include <otters/cuda/query/ops/gpu_send_recv_operator.cuh>
#include <otters/cuda/query/ops/local_send_recv_operator.cuh>
#include <otters/cuda/query/ops/mpi_send_recv.cuh>
#include <otters/cuda/query/ops/save_op.cuh>
#include <otters/cuda/query/ops/scan_op.cuh>
#include <otters/cuda/query/timestep.cuh>
#include <otters/cuda/query/ops/best_csm_result.cuh>
#include <thread>
#include <vector>

namespace otters {

template<typename T, size_t num_dimensions, size_t num_attr, typename GPUBitmap, typename GPUBinner, typename CPUBinner>
class MPIBitmapCSMDriver : public QueryDriver<T, num_attr> {
public:
//    using GPUBitmap = GPURoaringBitmap<64, DefaultRoaringTraits>;
    using GPUBuffer = MultiAttributeBuffer<GPUBitmap, num_attr>;
//    using GPUBinner = GPUEqualWidthBinner<64, T>;
    using cpu_binner_type = CPUBinner;
    using CPUBitmap = ParaRoaringBitmap<CPUBinner, cuda_host_buffer>;
    using CPUBuffer = MultiAttributeBuffer<CPUBitmap, num_attr>;
    using Slice = TimeSlice<2, CPUBuffer>;

    struct MergeSelector {
        auto operator()(Slice& slice) {
            auto coord = slice.position();
            Coordinates<1> result(coord[0]);
            return std::make_pair(result, coord[1]);
        }
    };

    static const int best_to_keep = 10;
    using CSMResult = BestContrastSets<T, num_attr, num_dimensions, best_to_keep>;
    using CSMResultSlice = TimeSlice<2, CSMResult>;

    struct NodeSelector {
        bool operator()(CSMResultSlice& slice) {
            auto i = slice.timestep_id();
            auto selected_node = (i % _info._total_nodes) * 2 + 1;
            log(OTTERS_LOG_DEBUG, "Selected node: {}, {}", i, selected_node);
            return selected_node;
        }
        MPINodeInfo _info;
    };

    using ConsumeOp = BestCSMResultOperator<T, 2, num_attr, num_dimensions, best_to_keep>;
    using MPISendOp = MPISendOperator<CSMResultSlice, NodeSelector>;
    using MPIRecvOp = MPIRecvOperator<ConsumeOp, CSMResultSlice, BufferPool<CSMResult>>;
    using BitmapCSMOp = CSMOperator<MPISendOp, Slice, CPUBinner, T, num_dimensions, num_attr, best_to_keep>;
    using CSMDispatchOp = LocalSendRecvOperator<BitmapCSMOp>;
    using MergeOp = MergeTimeSliceOperator2<CSMDispatchOp, Slice, MergeSelector, 1>;
    // using LocalSendRecvOp = LocalSendRecvOperator<MergeOp>;
    using GPUSendRecvOp = GPUSendRecvOperator<MergeOp, BufferPool<CPUBuffer>, TimeSlice<2, GPUBuffer>>;
    using GPUInputOp = GPUBitmapInputOperator<GPUSendRecvOp, 2, T, num_attr, GPUBitmap, GPUBinner, BufferPool<GPUBuffer>>;
    using ScanOp = ScanOperator<MergeOp, Slice, BufferPool<CPUBuffer>>;

public:
    MPIBitmapCSMDriver(MPINodeInfo mpi_info,
            int cpu_worker_amount,
            std::array<int64_t, num_dimensions> dimension_sizes,
            std::array<CPUBinner, num_attr> cpu_binners,
            int dimension_bucket,
            int attribute_bucket,
            std::string prefix,
            std::array<GPUBinner, num_attr> binners
        ) {
        log(OTTERS_LOG_DEBUG, "rank = {}, total_nodes = {}", mpi_info._rank, mpi_info._total_nodes);
        if (mpi_info._rank % 2 == 0) {
            initialize_buffer_pool(_gpu_buffer_pool, 4);
            initialize_buffer_pool(_cpu_buffer_pool, cpu_worker_amount * 2);
            initialize_buffer_pool(_scan_buffer_pool, cpu_worker_amount * 2);
            NodeSelector selector;
            selector._info = mpi_info;
            auto total_nodes = mpi_info._total_nodes;
            _mpi_send_op = std::make_unique<MPISendOp>(mpi_info, selector, cpu_worker_amount, mpi_info._total_nodes);


            for (auto i = 0; i < cpu_worker_amount; ++i) {
                _bitmap_csm_op.emplace_back(
                        _mpi_send_op.get(), dimension_sizes, cpu_binners, dimension_bucket, attribute_bucket);
            }

            std::vector<BitmapCSMOp*> csm_op_ptrs;
            for (auto i = 0; i < cpu_worker_amount; ++i) {
                csm_op_ptrs.push_back(&_bitmap_csm_op[i]);
            }

            _csm_dispatch_op = std::make_unique<CSMDispatchOp>(csm_op_ptrs, 1, cpu_worker_amount * 2);
            _merge_op = std::make_unique<MergeOp>(_csm_dispatch_op.get(), MergeSelector());

            std::vector<MergeOp*> merge_op_ptrs;
            merge_op_ptrs.push_back(_merge_op.get());

            // _local_send_recv_op = std::make_unique<LocalSendRecvOp>(merge_op_ptrs, 2, 4);
            // std::vector<LocalSendRecvOp*> local_send_recv_op_ptrs;
            // local_send_recv_op_ptrs.push_back(_local_send_recv_op.get());
            _gpu_send_recv_op = std::make_unique<GPUSendRecvOp>(merge_op_ptrs, &_cpu_buffer_pool);
            _gpu_input_op =
                    std::make_unique<GPUInputOp>(std::move(binners), _gpu_send_recv_op.get(), &_gpu_buffer_pool);
            _scan_op = std::make_unique<ScanOp>(_merge_op.get(), prefix, &_scan_buffer_pool);

            _threads.emplace_back([this](){
                try {
                    _mpi_send_op->send_loop();
                    log(OTTERS_LOG_DEBUG, "MPI send loop ended");
                } catch (std::exception& e) {
                    log(OTTERS_LOG_DEBUG, "MPI send loop error: {}", e.what());
                    std::terminate();
                }
            });

            for (auto i = 0; i < cpu_worker_amount; ++i)
                _threads.emplace_back([i, this]() { /* cpu workers */
                    try {
                        _csm_dispatch_op->drive(i);
                        _mpi_send_op->end_input();
                        log(OTTERS_LOG_DEBUG, "worker thread {} ended", i);
                    } catch (std::exception& e) {
                        log(OTTERS_LOG_DEBUG, "worker thread {} error: {}", e.what());
                        std::terminate();
                    }
                });

            _threads.emplace_back([this]() { /* merge thread */
                try {
                    _merge_op->drive();
                    _csm_dispatch_op->end_input();
                    log(OTTERS_LOG_DEBUG, "merge thread ended");
                } catch (std::exception& e) {
                    log(OTTERS_LOG_DEBUG, "merge thread error: {}", e.what());
                    std::terminate();
                }
            });

            _threads.emplace_back([this]() { /* gpu polling */
                try {
                    _gpu_send_recv_op->drive(0);
                    _merge_op->end_input();
                    log(OTTERS_LOG_DEBUG, "polling thread ended");
                } catch (std::exception& e) {
                    log(OTTERS_LOG_DEBUG, "polling thread error: {}", e.what());
                    std::terminate();
                }
            });

            _threads.emplace_back([this]() { /* scan */
                try {
                    _scan_op->drive();
                    // _local_send_recv_op->end_input();
                    log(OTTERS_LOG_DEBUG, "scan thread ended");
                } catch (std::exception& e) {
                    log(OTTERS_LOG_DEBUG, "scan thread error : {}", e.what());
                    std::terminate();
                }
            });
        } else {
            initialize_buffer_pool(_recv_csm_pool, 4);
            _consume_op = std::make_unique<ConsumeOp>(mpi_info._total_nodes);
            std::vector<ConsumeOp*> consume_ops;
            consume_ops.push_back(_consume_op.get());
            _mpi_recv_op = std::make_unique<MPIRecvOp>(consume_ops, mpi_info._total_nodes, 1, &_recv_csm_pool);

            _threads.emplace_back([this]() {
                try {
                    _mpi_recv_op->recv_loop();
                    log(OTTERS_LOG_DEBUG, "MPI recv op: Ended loop thread.");
                } catch (std::exception& e) {
                    log(OTTERS_LOG_DEBUG, "MPI recv op: loop thread error: {}.", e.what());
                    std::terminate();
                }
            });
            
            _threads.emplace_back([this]() {
                try {
                    log(OTTERS_LOG_DEBUG, "MPI recv op: Started drive thread.");
                    _mpi_recv_op->drive(0);
                    log(OTTERS_LOG_DEBUG, "MPI recv op: Ended drive thread.");
                } catch (std::exception& e) {
                    log(OTTERS_LOG_DEBUG, "MPI recv op: drive thread error: {}.", e.what());
                    std::terminate();
                }
            });
        }
    }

    ~MPIBitmapCSMDriver() {
        for(auto i = 0; i < _threads.size(); ++i) {
            _threads[i].join();
        }
    }
    virtual void add_timestep_data(int timestep_id, std::array<T*, num_attr> data, std::array<ssize_t, num_attr> sizes) override {
        // log(OTTERS_LOG_DEBUG, "adding timestep {}", timestep_id);
        Coordinates<2> coord;
        coord[0] = timestep_id;
        coord[1] = 0;
        _gpu_input_op->add_input(coord, data, sizes);
        // log(OTTERS_LOG_DEBUG, "added timestep {}", timestep_id);
    }
    virtual void end_input() override {
        _gpu_input_op->end_input();
    }
private:
    BufferPool<GPUBuffer> _gpu_buffer_pool;
    BufferPool<CPUBuffer> _cpu_buffer_pool;
    BufferPool<CPUBuffer> _scan_buffer_pool;
    BufferPool<CSMResult> _recv_csm_pool;

    // std::vector<ConsumeOp> _consume_ops;
    std::unique_ptr<ConsumeOp> _consume_op;
    std::unique_ptr<MPIRecvOp> _mpi_recv_op;

    std::unique_ptr<MPISendOp> _mpi_send_op;
    std::vector<BitmapCSMOp> _bitmap_csm_op;
    std::unique_ptr<CSMDispatchOp> _csm_dispatch_op;
    std::unique_ptr<MergeOp> _merge_op;
    // std::unique_ptr<LocalSendRecvOp> _local_send_recv_op;
    std::unique_ptr<GPUSendRecvOp> _gpu_send_recv_op;
    std::unique_ptr<GPUInputOp> _gpu_input_op;
    std::unique_ptr<ScanOp> _scan_op;

    std::vector<std::thread> _threads;
};


}

#endif
