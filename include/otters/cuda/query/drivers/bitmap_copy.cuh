#ifndef OTTERS_CUDA_QUERY_DRIVERS_BITMAP_COPY_CUH
#define OTTERS_CUDA_QUERY_DRIVERS_BITMAP_COPY_CUH

#include <otters/common/bitmap/binning.h>
#include <otters/cuda/bitmap/binning.cuh>
#include <otters/cuda/bitmap/query_driver.h>
#include <otters/cuda/bitmap/roaring.cuh>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/buffer_pool.cuh>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/query/ops/consume_operator.cuh>
#include <otters/cuda/query/ops/gpu_bitmap_input.cuh>
#include <otters/cuda/query/ops/gpu_plain_input.cuh>
#include <otters/cuda/query/ops/gpu_send_recv_operator.cuh>
#include <otters/cuda/query/ops/save_op.cuh>
#include <otters/cuda/query/ops/scan_op.cuh>
#include <otters/cuda/query/timestep.cuh>
#include <thread>
#include <vector>

namespace otters {

template<typename T, size_t num_attr>
class BitmapCopyOnlyDriver : public QueryDriver<T, num_attr> {
public:
    using GPUBitmap = GPURoaringBitmap<64, DefaultRoaringTraits>;
    using GPUBuffer = MultiAttributeBuffer<GPUBitmap, num_attr>;
    using GPUBinner = GPUDynamicEqualWidthBinner1<64, T>;
    using CPUBinner = EqualBinner<T>;
    using CPUBitmap = ParaRoaringBitmap<CPUBinner, cuda_host_buffer>;
    using CPUBuffer = MultiAttributeBuffer<CPUBitmap, num_attr>;
    using SliceType = TimeSlice<2, CPUBuffer>;
    using ConsumeOp = ConsumeOperator<SliceType>;
    using GPUSendRecvOp = GPUSendRecvOperator<ConsumeOp, BufferPool<CPUBuffer>, TimeSlice<2, GPUBuffer>>;
    using GPUInputOp = GPUBitmapInputOperator<GPUSendRecvOp, 2, T, num_attr, GPUBitmap, GPUBinner, BufferPool<GPUBuffer>>;

public:
    BitmapCopyOnlyDriver(std::array<GPUBinner, num_attr> binners,
            std::array<CPUBinner, num_attr> cpu_binners,
            int gpu_buffer_amount = 2,
            int cpu_buffer_amount = 4) {
        initialize_buffer_pool(_gpu_buffer_pool, gpu_buffer_amount);
        initialize_buffer_pool(_cpu_buffer_pool, cpu_buffer_amount);
        std::vector<ConsumeOp*> consume_op_ptrs;
        consume_op_ptrs.push_back(&_consume_op);
        _gpu_send_recv_op = std::make_unique<GPUSendRecvOp>(consume_op_ptrs, &_cpu_buffer_pool);
        _gpu_input_op = std::make_unique<GPUInputOp>(std::move(binners), _gpu_send_recv_op.get(), &_gpu_buffer_pool);
        _threads.emplace_back([this]() { _gpu_send_recv_op->drive(0); });
    }

    ~BitmapCopyOnlyDriver() {
        for(auto i = 0; i < _threads.size(); ++i) {
            _threads[i].join();
        }
    }
    virtual void add_timestep_data(int timestep_id, std::array<T*, num_attr> data, std::array<ssize_t, num_attr> sizes) override {
        Coordinates<2> coord;
        coord[0] = timestep_id;
        coord[1] = 1;
        _gpu_input_op->add_input(coord, data, sizes);
    }
    virtual void end_input() override {
        _gpu_input_op->end_input();
    }
private:
    BufferPool<GPUBuffer> _gpu_buffer_pool;
    BufferPool<CPUBuffer> _cpu_buffer_pool;

    ConsumeOp _consume_op;
    std::unique_ptr<GPUSendRecvOp> _gpu_send_recv_op;
    std::unique_ptr<GPUInputOp> _gpu_input_op;

    std::vector<std::thread> _threads;
};


}

#endif
