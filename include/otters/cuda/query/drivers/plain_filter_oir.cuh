#ifndef OTTERS_CUDA_QUERY_DRIVERS_PLAIN_FILTER_OIR_CUH
#define OTTERS_CUDA_QUERY_DRIVERS_PLAIN_FILTER_OIR_CUH

#include <otters/common/bitmap/binning.h>
#include <otters/cuda/bitmap/binning.cuh>
#include <otters/cuda/bitmap/query_driver.h>
#include <otters/cuda/bitmap/roaring.cuh>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/buffer_pool.cuh>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/query/ops/consume_operator.cuh>
#include <otters/cuda/query/ops/gpu_bitmap_input.cuh>
#include <otters/cuda/query/ops/gpu_plain_input.cuh>
#include <otters/cuda/query/ops/local_send_recv_operator.cuh>
#include <otters/cuda/query/ops/gpu_send_recv_operator.cuh>
#include <otters/cuda/query/ops/save_op.cuh>
#include <otters/cuda/query/ops/scan_op.cuh>
#include <otters/cuda/query/ops/consume_operator.cuh>
#include <otters/cuda/query/ops/filter_two_op.cuh>
#include <otters/cuda/query/timestep.cuh>
#include <otters/cuda/query/ops/plain_filter_oir.cuh>
#include <thread>
#include <vector>


namespace otters {

template<typename T, size_t num_dims, size_t num_attr>
class PlainFilterOIRDriver : public QueryDriver<T, num_attr> {
public:
    using GPUBuffer = MultiAttributeBuffer<cuda_device_buffer<T>, num_attr>;
    using CPUBuffer = MultiAttributeBuffer<cuda_host_buffer<T>, num_attr>;
    using Slice = TimeSlice<2, CPUBuffer>;
    using slice_type = Slice;
    using FilterOIROp = PlainFilterOIROperator3D<decltype(nullptr), double, 2, num_dims, num_attr>;
    using GPUSendRecvOp = GPUSendRecvOperator<FilterOIROp, BufferPool<CPUBuffer>, TimeSlice<2, GPUBuffer>>;
    using GPUInputOp = GPUPlainInputOperator<GPUSendRecvOp, 2, T, num_attr, BufferPool<GPUBuffer>>;
public:
    PlainFilterOIRDriver(
            int cpu_workers,
            std::array<int64_t, num_dims> dim_mins,
            std::array<int64_t, num_dims> dim_maxs,
            std::array<int64_t, num_dims> dim_sizes,
            std::array<Range<T>, num_attr> ranges,
            int gpu_buffer_amount = 2,
            int cpu_buffer_amount = 4) {
        initialize_buffer_pool(_gpu_buffer_pool, gpu_buffer_amount);
        initialize_buffer_pool(_cpu_buffer_pool, cpu_buffer_amount * cpu_workers);

        _filter_oir_op = std::make_unique<FilterOIROp>(
                nullptr, cpu_workers, cpu_workers * cpu_buffer_amount, dim_mins, dim_maxs, dim_sizes, ranges);

        std::vector<FilterOIROp*> filter_oir_ops;
        filter_oir_ops.push_back(_filter_oir_op.get());
        _gpu_send_recv_op = std::make_unique<GPUSendRecvOp>(filter_oir_ops, &_cpu_buffer_pool);
        _gpu_input_op = std::make_unique<GPUInputOp>(_gpu_send_recv_op.get(), &_gpu_buffer_pool);

        _threads.emplace_back([this]() {
          log(OTTERS_LOG_DEBUG, "started gpu thread.\n");
          _gpu_send_recv_op->drive(0);
          _filter_oir_op->end_input();
          log(OTTERS_LOG_DEBUG, "ended gpu thread.\n");
        });

        for (auto i = 0; i < cpu_workers; ++i) {
            _threads.emplace_back([this, i]() {
              log(OTTERS_LOG_DEBUG, "started filter thread {}.\n", i);
              _filter_oir_op->drive(i);
              log(OTTERS_LOG_DEBUG, "ended filter thread.\n");
            });
        }
    }

    ~PlainFilterOIRDriver() {
        for(auto& thread : _threads) {
            thread.join();
        }
    }
    virtual void add_timestep_data(int timestep_id,
            std::array<T*, num_attr> data,
            std::array<ssize_t, num_attr> sizes) override {
        Coordinates<2> coord;
        coord[0] = timestep_id;
        coord[1] = 0;
        _gpu_input_op->add_input(coord, data, sizes);
    }
    virtual void end_input() override {
        _gpu_input_op->end_input();
    }
private:
    BufferPool<GPUBuffer> _gpu_buffer_pool;
    BufferPool<CPUBuffer> _cpu_buffer_pool;

    std::unique_ptr<FilterOIROp> _filter_oir_op;
    std::unique_ptr<GPUSendRecvOp> _gpu_send_recv_op;
    std::unique_ptr<GPUInputOp> _gpu_input_op;

    std::vector<std::thread> _threads;
};

}

#endif