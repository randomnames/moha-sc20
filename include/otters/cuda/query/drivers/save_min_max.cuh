#ifndef OTTERS_CUDA_QUERY_DRIVERS_SAVE_MIN_MAX_CUH
#define OTTERS_CUDA_QUERY_DRIVERS_SAVE_MIN_MAX_CUH

#include <otters/common/bitmap/binning.h>
#include <otters/cuda/bitmap/binning.cuh>
#include <otters/cuda/bitmap/query_driver.h>
#include <otters/cuda/bitmap/roaring.cuh>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/buffer_pool.cuh>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/query/ops/consume_operator.cuh>
#include <otters/cuda/query/ops/gpu_bitmap_input.cuh>
#include <otters/cuda/query/ops/gpu_plain_input.cuh>
#include <otters/cuda/query/ops/gpu_send_recv_operator.cuh>
#include <otters/cuda/query/ops/save_op.cuh>
#include <otters/cuda/query/ops/scan_op.cuh>
#include <otters/cuda/query/ops/save_min_max.cuh>
#include <otters/cuda/query/timestep.cuh>
#include <thread>
#include <vector>

namespace otters {

template<typename T, size_t num_attr>
class SaveMinMaxDriver : public QueryDriver<T, num_attr> {
public:
    using GPUBuffer = MultiAttributeBuffer<cuda_device_buffer<T>, num_attr>;
    using CPUBuffer = MultiAttributeBuffer<cuda_host_buffer<T>, num_attr>;
    using SliceType = TimeSlice<2, CPUBuffer>;
    using SaveOp = SaveMinMaxOperator<T, num_attr>;
    using GPUSendRecvOp = GPUSendRecvOperator<SaveOp, BufferPool<CPUBuffer>, TimeSlice<2, GPUBuffer>>;
    using GPUInputOp = GPUPlainInputOperator<GPUSendRecvOp, 2, T, num_attr, BufferPool<GPUBuffer>>;

public:
    SaveMinMaxDriver(std::string prefix, int gpu_buffer_amount = 2, int cpu_buffer_amount = 4)
        : _save_op(prefix), _gpu_send_recv_op({&_save_op}, &_cpu_buffer_pool), _gpu_input_op(&_gpu_send_recv_op, &_gpu_buffer_pool) {
        initialize_buffer_pool(_gpu_buffer_pool, gpu_buffer_amount);
        initialize_buffer_pool(_cpu_buffer_pool, cpu_buffer_amount);

        _threads.emplace_back([this](){
            _gpu_send_recv_op.drive(0);
        });
    }

    ~SaveMinMaxDriver() {
        for(auto i = 0; i < _threads.size(); ++i) {
            _threads[i].join();
        }
    }
    virtual void add_timestep_data(int timestep_id, std::array<T*, num_attr> data, std::array<ssize_t, num_attr> sizes) override {
        Coordinates<2> coord;
        coord[0] = timestep_id;
        coord[1] = 1;
        _gpu_input_op.add_input(coord, data, sizes);
    }
    virtual void end_input() override {
        _gpu_input_op.end_input();
    }
private:
    BufferPool<GPUBuffer> _gpu_buffer_pool;
    BufferPool<CPUBuffer> _cpu_buffer_pool;

    SaveOp _save_op;
    GPUSendRecvOp _gpu_send_recv_op;
    GPUInputOp _gpu_input_op;

    std::vector<std::thread> _threads;
};


}

#endif
