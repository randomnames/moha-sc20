#ifndef OTTERS_CUDA_QUERY_DRIVERS_BITMAP_JOIN_COUNT_CUH
#define OTTERS_CUDA_QUERY_DRIVERS_BITMAP_JOIN_COUNT_CUH

#include <otters/common/bitmap/binning.h>
#include <otters/cuda/bitmap/binning.cuh>
#include <otters/cuda/bitmap/query_driver.h>
#include <otters/cuda/bitmap/roaring.cuh>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/buffer_pool.cuh>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/query/ops/bitmap_csm.cuh>
#include <otters/cuda/query/ops/bitmap_join_count.cuh>
#include <otters/cuda/query/ops/consume_operator.cuh>
#include <otters/cuda/query/ops/gpu_bitmap_input.cuh>
#include <otters/cuda/query/ops/gpu_plain_input.cuh>
#include <otters/cuda/query/ops/gpu_send_recv_operator.cuh>
#include <otters/cuda/query/ops/local_send_recv_operator.cuh>
#include <otters/cuda/query/ops/save_op.cuh>
#include <otters/cuda/query/ops/scan_op.cuh>
#include <otters/cuda/query/timestep.cuh>
#include <thread>
#include <vector>

namespace otters {

template<typename T, size_t num_attr, typename CPUBinner>
class BitmapJoinCountDriver : public QueryDriver<T, num_attr> {
public:
    using GPUBitmap = GPURoaringBitmap<64, DefaultRoaringTraits>;
    using GPUBuffer = MultiAttributeBuffer<GPUBitmap, num_attr>;
    using GPUBinner = GPUEqualWidthBinner<64, T>;
    using cpu_binner_type = CPUBinner;
    using CPUBitmap = ParaRoaringBitmap<CPUBinner, cuda_host_buffer>;
    using CPUBuffer = MultiAttributeBuffer<CPUBitmap, num_attr>;
    using Slice = TimeSlice<2, CPUBuffer>;

    struct MergeSelector {
        auto operator()(Slice& slice) {
            auto coord = slice.position();
            Coordinates<1> result(coord[0]);
            return std::make_pair(result, coord[1]);
        }
    };

    using ConsumeOp = ConsumeOperator<TimeSlice<2, long>>;
    using BitmapJoinCountOp = BitmapJoinCountOperator<ConsumeOp, Slice, T, num_attr>;
    using BitmapJoinCountDispatchOp = LocalSendRecvOperator<BitmapJoinCountOp>;
    using MergeOp = MergeTimeSliceOperator2<BitmapJoinCountDispatchOp, Slice, MergeSelector, 1>;
    // using LocalSendRecvOp = LocalSendRecvOperator<MergeOp>;
    using GPUSendRecvOp = GPUSendRecvOperator<MergeOp, BufferPool<CPUBuffer>, TimeSlice<2, GPUBuffer>>;
    using GPUInputOp = GPUBitmapInputOperator<GPUSendRecvOp, 2, T, num_attr, GPUBitmap, GPUBinner, BufferPool<GPUBuffer>>;
    using ScanOp = ScanOperator<MergeOp, Slice, BufferPool<CPUBuffer>>;

public:
    BitmapJoinCountDriver(int cpu_worker_amount,
            std::string prefix,
            std::array<GPUBinner, num_attr> binners
        ) {
        initialize_buffer_pool(_gpu_buffer_pool, 4);
        initialize_buffer_pool(_cpu_buffer_pool, cpu_worker_amount * 2);
        initialize_buffer_pool(_scan_buffer_pool, cpu_worker_amount * 2);


        for(auto i = 0; i < cpu_worker_amount; ++i) {
            _consume_ops.emplace_back();
        }

        for(auto i = 0; i < cpu_worker_amount; ++i) {
            _bitmap_join_count_op.emplace_back(&_consume_ops[i]);
        }

        std::vector<BitmapJoinCountOp*> bjc_op_ptrs;
        for(auto i = 0; i < cpu_worker_amount; ++i) {
            bjc_op_ptrs.push_back(&_bitmap_join_count_op[i]);
        }

        _bitmap_join_count_dispatch_op = std::make_unique<BitmapJoinCountDispatchOp>(bjc_op_ptrs, 1, cpu_worker_amount * 2);
        _merge_op = std::make_unique<MergeOp>(_bitmap_join_count_dispatch_op.get(), MergeSelector());

        std::vector<MergeOp*> merge_op_ptrs;
        merge_op_ptrs.push_back(_merge_op.get());

        // _local_send_recv_op = std::make_unique<LocalSendRecvOp>(merge_op_ptrs, 2, 4);
        // std::vector<LocalSendRecvOp*> local_send_recv_op_ptrs;
        // local_send_recv_op_ptrs.push_back(_local_send_recv_op.get());
        _gpu_send_recv_op = std::make_unique<GPUSendRecvOp>(merge_op_ptrs, &_cpu_buffer_pool);
        _gpu_input_op = std::make_unique<GPUInputOp>(std::move(binners), _gpu_send_recv_op.get(), &_gpu_buffer_pool);
        _scan_op = std::make_unique<ScanOp>(_merge_op.get(), prefix, &_scan_buffer_pool);


        for(auto i = 0; i < cpu_worker_amount; ++i)
            _threads.emplace_back([i, this]() { /* cpu workers */
                _bitmap_join_count_dispatch_op->drive(i);
                log(OTTERS_LOG_DEBUG, "worker thread {} ended", i);
            });
        
        _threads.emplace_back([this]() { /* merge thread */
            _merge_op->drive();
            _bitmap_join_count_dispatch_op->end_input();
            log(OTTERS_LOG_DEBUG, "merge thread ended");
        });

        _threads.emplace_back([this]() { /* gpu polling */
            _gpu_send_recv_op->drive(0);
            _merge_op->end_input();
            log(OTTERS_LOG_DEBUG, "polling thread ended");
        });

        _threads.emplace_back([this]() { /* scan */
            _scan_op->drive();
            // _local_send_recv_op->end_input();
            log(OTTERS_LOG_DEBUG, "scan thread ended");
        });


    }

    ~BitmapJoinCountDriver() {
        for(auto i = 0; i < _threads.size(); ++i) {
            _threads[i].join();
        }
    }
    virtual void add_timestep_data(int timestep_id, std::array<T*, num_attr> data, std::array<ssize_t, num_attr> sizes) override {
        // log(OTTERS_LOG_DEBUG, "adding timestep {}", timestep_id);
        Coordinates<2> coord;
        coord[0] = timestep_id;
        coord[1] = 0;
        _gpu_input_op->add_input(coord, data, sizes);
        // log(OTTERS_LOG_DEBUG, "added timestep {}", timestep_id);
    }
    virtual void end_input() override {
        _gpu_input_op->end_input();
    }
private:
    BufferPool<GPUBuffer> _gpu_buffer_pool;
    BufferPool<CPUBuffer> _cpu_buffer_pool;
    BufferPool<CPUBuffer> _scan_buffer_pool;

    std::vector<ConsumeOp> _consume_ops;
    std::vector<BitmapJoinCountOp> _bitmap_join_count_op;
    std::unique_ptr<BitmapJoinCountDispatchOp> _bitmap_join_count_dispatch_op;
    std::unique_ptr<MergeOp> _merge_op;
    // std::unique_ptr<LocalSendRecvOp> _local_send_recv_op;
    std::unique_ptr<GPUSendRecvOp> _gpu_send_recv_op;
    std::unique_ptr<GPUInputOp> _gpu_input_op;
    std::unique_ptr<ScanOp> _scan_op;

    std::vector<std::thread> _threads;
};


}

#endif
