#ifndef OTTERS_TEST_CUDA_QUERY_OPS_PLAIN_FILTER_OIR_CU_FILTER_OIR_PROCESSOR_3_D_BASE_CUH
#define OTTERS_TEST_CUDA_QUERY_OPS_PLAIN_FILTER_OIR_CU_FILTER_OIR_PROCESSOR_3_D_BASE_CUH

#include <otters/common/containers/buffer.h>
#include <otters/common/containers/time_slice.h>
#include <otters/common/utils/log.h>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/query/subgroup_discovery_support.cuh>
#include <vector>
namespace otters {

class FilterOIRProcessor3DBase {
public:
    struct Point {
        int32_t _x;
        int32_t _y;
    };
    constexpr static const int kNotInterested = -1;
    constexpr static const int kNotVisited = -2;

protected:
    int64_t to_hash(int x, int y) {
        return x * _dim_sizes[2] + y;
    }
    auto to_hash(Point p) {
        return to_hash(p._x, p._y);
    }

public:
    FilterOIRProcessor3DBase(std::array<int64_t, 3> dim_mins, std::array<int64_t, 3> dim_maxs, std::array<int64_t, 3> dim_sizes)
        : _dim_mins(dim_mins), _dim_maxs(dim_maxs), _dim_sizes(dim_sizes) {
        _planes[0].resize(_dim_sizes[1] * _dim_sizes[2]);
        _planes[1].resize(_dim_sizes[1] * _dim_sizes[2]);
        std::fill(_planes[1].begin(), _planes[1].end(), -1);
    }

    void flood_fill(int current_plane) {
        auto& sizes = _sizes[current_plane];
        auto& plane = _planes[current_plane];
        auto& last_plane = _planes[1 - current_plane];
        auto& last_sizes = _sizes[1 - current_plane];
        sizes.clear();
        for(auto i = 0; i < _dim_sizes[1]; ++i)
            for(auto j = 0; j < _dim_sizes[2]; ++j) {
                _stack.clear();
//                std::fill(plane.begin(), plane.end(), 0);
                // if not visited
                if(plane[to_hash(i,j)] != kNotVisited)
                    continue;
                _stack.push_back(Point{i,j});
                sizes.push_back(0);
                while(!_stack.empty()) {
                    auto point = _stack.back();
                    _stack.pop_back();
                    if(plane[to_hash(point)] != kNotVisited)
                        continue;
//                    fmt::print("popped {},{}, idx = {}, stack size = {}\n", point._x, point._y, sizes.size() - 1, _stack.size());
                    plane[to_hash(point)] = (int) sizes.size() - 1;
                    ++sizes.back();
                    auto last_id = last_plane[to_hash(point)];
                    if(last_id >= 0 && last_sizes[last_id] > 0) {
                        sizes.back() += last_sizes[last_id];
                        last_sizes[last_id] = 0;
                    }
                    if(point._x > 0) {
//                        fmt::print("pushed {},{}\n", point._x - 1, point._y);
                        _stack.push_back(Point{point._x - 1, point._y});
                    }
                    if(point._x < _dim_sizes[1] - 1) {
//                        fmt::print("pushed {},{}\n", point._x + 1, point._y);
                        _stack.push_back(Point{point._x + 1, point._y});
                    }
                    if(point._y > 0) {
//                        fmt::print("pushed {},{}\n", point._x, point._y - 1);
                        _stack.push_back(Point{point._x, point._y - 1});
                    }
                    if(point._y < _dim_sizes[2] - 1) {
//                        fmt::print("pushed {},{}\n", point._x, point._y + 1);
                        _stack.push_back(Point{ point._x, point._y + 1 });
                    }
                }
            }
#ifdef OTTERS_OIR_DISPLAY_RESULT
        for(auto i = 0; i < _dim_sizes[1]; ++i) {
            for (auto j = 0; j < _dim_sizes[2]; ++j) {
                fmt::print("{},", plane[to_hash(i,j)]);
            }
            fmt::print("\n");
        }
        fmt::print("\n");
#endif
    }
protected:
    // some tracked
    std::vector<int64_t> _sizes[2];
    std::vector<int> _planes[2];
    std::vector<Point> _stack;
    std::array<int64_t, 3> _dim_mins;
    std::array<int64_t, 3> _dim_maxs;
    std::array<int64_t, 3> _dim_sizes;
};

} // namespace otters

#endif // OTTERS_TEST_CUDA_QUERY_OPS_PLAIN_FILTER_OIR_CU_FILTER_OIR_PROCESSOR_3_D_BASE_CUH
