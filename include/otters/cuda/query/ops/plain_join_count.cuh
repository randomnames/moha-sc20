#ifndef OTTERS_CUDA_QUERY_OPS_PLAIN_JOIN_COUNT_CUH
#define OTTERS_CUDA_QUERY_OPS_PLAIN_JOIN_COUNT_CUH

#include <otters/common/utils/log.h>
#include <otters/cuda/query/ops/merge_timestep.cuh>
#include <queue>
#include <vector>
#include "csm_support.cuh"
#include <otters/cuda/query/plain_subgroup_discovery_support.cuh>

namespace otters {

template<typename ParentOp, typename Slice, typename MinMaxGetter, typename T, int num_attr>
class PlainJoinCountOperator {
public:
    using input_slice_type = PairSlice<Slice>;
    constexpr const static int num_coord_dims = Slice::num_coord_dims;
    using slice_type = PairSlice<Slice>;
    using output_slice_type = TimeSlice<num_coord_dims, long>;
public:
    PlainJoinCountOperator(
        ParentOp* op,
        std::array<MinMaxGetter, num_attr> min_max_getters,
        int attribute_buckets,
        std::string op_name = "plain_join_count")
        : _parent_op(op),
          _min_max_getter(std::move(min_max_getters)),
          _attribute_buckets(attribute_buckets),
          _timer(op_name + "_time") {
    }

    void consume(input_slice_type&& slice) {
        // log(OTTERS_LOG_DEBUG, "left = {}, right = {}", slice.first().position(), slice.second().position());
        output_slice_type output_slice(slice.position());
        // int total_searched = 0;
        auto timestep_id = slice.timestep_id();
        long result = 0;
        // double best_quality = 0;
        for(auto i = 0; i < num_attr; ++i) {
            auto min = _min_max_getter[i].min(timestep_id);
            auto max = _min_max_getter[i].max(timestep_id);
            auto delta = (max - min) / _attribute_buckets;
            T* left_ptr = slice.first().data().attribute(i).data();
            T* right_ptr = slice.second().data().attribute(i).data();
            auto size = slice.first().data().attribute(i).size();
            for(auto j = 0; j < size; ++j) {
                if(fabs(left_ptr[j] - right_ptr[j]) < delta)
                    ++result;
            }
        }
        _parent_op->consume(std::move(output_slice));
    }

private:
    ParentOp* _parent_op;
    Timer _timer;
    std::array<MinMaxGetter, num_attr> _min_max_getter;
    int _dimension_buckets;
    int _attribute_buckets;
};

}

#endif
