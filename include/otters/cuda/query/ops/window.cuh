#ifndef OTTERS_CUDA_QUERY_OPS_WINDOW_CUH
#define OTTERS_CUDA_QUERY_OPS_WINDOW_CUH

#include <map>
#include <otters/common/containers/safe_queue.h>
#include <otters/common/utils/profile.h>
#include <mutex>
#include <utility>
#include <tuple>

namespace otters {

template<typename ParentOp, typename InputSlice, typename QueryFunction, int window_size>
class WindowOperatorBase {
public:
    struct SlicePointer {
        SlicePointer() = default;
        SlicePointer(InputSlice&& input)
            : _ptr(std::make_unique<InputSlice>(std::move(input))), _finished(0) {
            }
        std::unique_ptr<InputSlice> _ptr;
        int _finished;
    };
    struct Job {
        std::array<int, window_size> _slice_ids;
        operator bool() {
            return _slice_ids[0] != -1;
        }
        Job() {
            _slice_ids[0] = -1;
        }
    };
public:
    WindowOperatorBase(ParentOp* parent_op, std::vector<QueryFunction>&& functions, int total_buffers)
        : _parent_op(parent_op), _jobs(1, total_buffers), _functions(std::move(functions)) {
    }

    void consume(InputSlice&& slice) {
        if((bool) slice == false) {
            end_input();
            return;
        }
        auto timestep_id = slice.timestep_id();
        {
            std::unique_lock<std::mutex> lck(_map_mutex);
            _slices.insert(std::make_pair(timestep_id, SlicePointer(std::move(slice))));
        }
        if(timestep_id >= window_size - 1) {
            Job job;
            for(int i = window_size - 1; i >= 0; --i) {
                job._slice_ids[i] = timestep_id - i;
            }
            _jobs.push(std::move(job));
        }
    }

    void end_input() {
        Job job;
        _jobs.push(std::move(job));

    }

    void drive(int i) {
        (void) i;
        Timer timer("window_time");
        while (true) {
            auto result = _jobs.pop();
            if((bool) result == false)
                break;
            timer.start();
            forward(i, result);
            timer.stop();
        }
    }

    void forward(int thread_id, Job& job) {
        std::array<InputSlice*, window_size> ptrs;
        {
            std::unique_lock<std::mutex> lck(_map_mutex);
            for (auto i = 0; i < window_size; ++i) {
                ptrs[i] = _slices[job._slice_ids[i]]._ptr.get();
            }
        }
        call_function(thread_id, ptrs, std::make_index_sequence<window_size>{});
        {
            std::unique_lock<std::mutex> lck(_map_mutex);
            for (auto i = 0; i < window_size; ++i) {
                auto id = job._slice_ids[i];
                _slices[id]._finished += 1;
                if (_slices[id]._finished == window_size) {
                    log(OTTERS_LOG_DEBUG, "removing buffer {}", id);
                    _slices.erase(id);
                }
        }
        }
    }

    template<size_t... I>
    void call_function(int thread_id, std::array<InputSlice*, window_size> tuple, std::index_sequence<I...>) {
        _functions[thread_id](*std::get<I>(tuple)...);
    }

private:
    ParentOp* _parent_op;
    std::map<int, SlicePointer> _slices;
    std::mutex _map_mutex;
    safe_queue<Job> _jobs;
    std::vector<QueryFunction> _functions;
};

}

#endif
