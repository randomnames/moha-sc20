#ifndef OTTERS_CUDA_OPS_BITMAP_JOIN_COUNT_CUH
#define OTTERS_CUDA_OPS_BITMAP_JOIN_COUNT_CUH

#include "csm_support.cuh"
#include <otters/common/bitmap/roaring_bitmap.h>
#include <otters/common/bitmap/roaring_filter_helper.h>
#include <otters/common/utils/log.h>
#include <otters/cuda/query/ops/merge_timestep.cuh>
#include <queue>
#include <type_traits>
#include <vector>

namespace otters {

template<typename ParentOp, typename Slice, typename T, int num_attr>
class BitmapJoinCountOperator {
public:
    using input_slice_type = PairSlice<Slice>;
    using slice_type = PairSlice<Slice>;
    constexpr const static int num_coord_dims = Slice::num_coord_dims;
    using output_slice_type = TimeSlice<num_coord_dims, long>;

public:
    BitmapJoinCountOperator(ParentOp* op, std::string op_name = "bitmap_join_count")
        : _parent_op(op), _timer(op_name + "_time") {
    }

    void consume(input_slice_type&& slice) {
        // auto timestep_id = slice.timestep_id();
        output_slice_type output_slice(slice.position());

        long result = 0;

        for(auto i = 0; i < num_attr; ++i) {
            auto& bitmap1 = slice.first().data().attribute(i);
            auto& bitmap2 = slice.second().data().attribute(i);
            result += compute_roaring_and_count(bitmap1, bitmap2);
        }

        output_slice.data() = result;
        _parent_op->consume(std::move(output_slice));
    }


private:
    Timer _timer;
    ParentOp* _parent_op;
};

}


#endif
