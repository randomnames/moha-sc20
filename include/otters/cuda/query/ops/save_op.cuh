#ifndef OTTERS_CUDA_QUERY_SAVE_OP_CUH
#define OTTERS_CUDA_QUERY_SAVE_OP_CUH

#include <otters/common/containers/time_slice.h>
#include <otters/common/io/time_slice_array.h>

namespace otters {

template<typename Slice>
class SaveOperator {
public:
    using slice_type = Slice;
    using OpenType = typename TimeSliceArray<Slice>::OpenType;
public:
    SaveOperator(std::string prefix)
        : _array(prefix, OpenType::WRITE_ONLY) {
    }
    void consume(Slice&& slice) {
        _array.add_timestep(slice);
    }
private:
    TimeSliceArray<Slice> _array;
    std::string _prefix;
};

}

#endif
