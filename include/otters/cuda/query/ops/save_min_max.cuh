#ifndef OTTERS_CUDA_QUERY_OPS_SAVE_MIN_MAX_CUH
#define OTTERS_CUDA_QUERY_OPS_SAVE_MIN_MAX_CUH

#include <otters/cuda/query/ops/dump_min_max_info.cuh>
#include <otters/common/containers/time_slice.h>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <fstream>
#include <vector>
#include <mutex>
#include <string>

namespace otters {

template<typename T, size_t num_attr>
class SaveMinMaxOperator {
public:
    SaveMinMaxOperator(std::string prefix)
        : _prefix(prefix) {
        for(auto i = 0; i < num_attr; ++i)
            _results[i].reserve(1024);
    }
    using slice_type = TimeSlice<2, MultiAttributeBuffer<cuda_host_buffer<T>, num_attr>>;
    using result_type = MinMaxAggregatorResult<T>;
    using Aggregator = MinMaxAggregator<T>;
public:
    void consume(slice_type&& slice) {
        auto timestep_id = slice.timestep_id();
        for (auto attr = 0; attr < num_attr; ++attr) {
            Aggregator aggregator;

            auto& attribute = slice.data().attribute(attr);
            for (auto i = 0; i < attribute.size(); ++i) {
                aggregator.process(attribute.data()[i]);
            }
            std::unique_lock<std::mutex> lck(_mutex);
            if (timestep_id >= _results[attr].size())
                _results[attr].resize(timestep_id + 1);
            _results[attr][timestep_id] = aggregator.result();
            // fmt::print("We processed timestep {}, attr {}, first result = {} thread_id = {}.\n",
            //         timestep_id,
            //         attr,
            //         attribute.data()[3],
            //         std::this_thread::get_id());
        }
    }
    ~SaveMinMaxOperator() {
        for(auto attr = 0; attr < num_attr; ++attr) {
            std::ofstream of(_prefix + "." + std::to_string(attr));
            of << _results[attr].size() << "\n";
            for (auto i = 0; i < _results[attr].size(); ++i) {
                of << _results[attr][i];
                of << "\n";
            }
        }
    }

private:
    std::vector<result_type> _results[num_attr];
    std::string _prefix;
    std::mutex _mutex;


};


}

#endif