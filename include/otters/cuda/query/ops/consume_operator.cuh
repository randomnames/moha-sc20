#ifndef OTTERS_CUDA_QUERY_OPS_CONSUME_OPERATOR_CUH
#define OTTERS_CUDA_QUERY_OPS_CONSUME_OPERATOR_CUH

#include <otters/common/utils/log.h>

namespace otters {

template<typename Slice>
class ConsumeOperator {
public:
    using slice_type = Slice;
    void consume(Slice&& slice) {
        (void) slice;
        // log(OTTERS_LOG_DEBUG, "position = {}", slice.position());
        // do nothing
    }
};

}

#endif
