//
// Created by Haoyuan Xing on 4/16/20.
//

#ifndef OTTERS_INCLUDE_OTTERS_CUDA_QUERY_OPS_BITMAP_SUBGROUP_DISCOVERY_SUPPORT_CUH
#define OTTERS_INCLUDE_OTTERS_CUDA_QUERY_OPS_BITMAP_SUBGROUP_DISCOVERY_SUPPORT_CUH

#include <otters/common/bitmap/roaring_bitmap.h>
#include <otters/common/bitmap/roaring_filter_helper.h>
#include <otters/cuda/container/multi_attribute_buffer.cuh>

namespace otters {

template<typename T, typename BitmapBinner, template<typename> typename Container, typename RoaringTraits, typename Binner, size_t num_dimensions, size_t num_attributes>
MeanAggregator2<T> filter_slice(
        int timestep_id,
        MultiAttributeBuffer<ParaRoaringBitmap<BitmapBinner, Container, RoaringTraits>, num_attributes>& slice,
        std::array<int64_t, num_dimensions> dimension_sizes,
        std::array<Binner, num_attributes>& binners,
        SubsetCondition<T, num_attributes, num_dimensions>& condition,
        int target_attr) {
    assert(target_attr == num_attributes - 1);
    using Bitmap = ParaRoaringBitmap<BitmapBinner, Container, RoaringTraits>;
    using Iter = typename Bitmap::OrIterator;
    std::array<int64_t, num_dimensions> dim_mins;
    std::array<int64_t, num_dimensions> dim_maxs;
    for (auto i = 0u; i < num_dimensions; ++i) {
        auto range = condition.dim_range(i);
        if (range.enabled()) {
            dim_mins[i] = range.min();
            dim_maxs[i] = range.max();
        } else {
            dim_mins[i] = 0;
            dim_maxs[i] = dimension_sizes[i];
        }
    }

    DimensionRanges<DefaultRoaringTraits, num_dimensions> dim_ranges(dim_mins, dim_maxs, dimension_sizes);
    std::array<Iter, num_attributes - 1> attr_iters;
    for(auto i = 0; i < num_attributes - 1; ++i) {
        auto min_value = condition.attr_range(i).min();
        auto min_bucket = binners[i](min_value, timestep_id);
        auto max_value = condition.attr_range(i).max();
        auto max_bucket = binners[i](max_value, timestep_id) + 1;
        attr_iters[i] = slice.attribute(i).begin_or_iterator(min_bucket, max_bucket);
    }
    auto result = roaring_compute_mean_support(dim_ranges, attr_iters, slice.attribute(target_attr), binners[target_attr], timestep_id);
    return result;

}


}

#endif // OTTERS_INCLUDE_OTTERS_CUDA_QUERY_OPS_BITMAP_SUBGROUP_DISCOVERY_SUPPORT_CUH
