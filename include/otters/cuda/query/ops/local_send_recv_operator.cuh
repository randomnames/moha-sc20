#ifndef OTTERS_CUDA_QUERY_LOCAL_SEND_RECV_OP_CUH
#define OTTERS_CUDA_QUERY_LOCAL_SEND_RECV_OP_CUH

#include <otters/common/containers/safe_queue.h>
#include <otters/common/utils/log.h>
#include <fmt/core.h>

namespace otters {


template<typename ParentOperator>
class LocalSendRecvOperator {
public:
    using Slice = typename ParentOperator::slice_type;
    using slice_type = Slice;
public:
    LocalSendRecvOperator(std::vector<ParentOperator*> parent_operators, int total_producers, size_t max_buffer_size)
        : _parent_operators(std::move(parent_operators)), _messages(total_producers, max_buffer_size) {
    }

    void drive(int id) {
        while(true) {
            auto slice = _messages.pop();
            // log(OTTERS_LOG_DEBUG, "got slice {}", slice.position());
            if((bool) slice == false) {
                break;
            }
            _parent_operators[id]->consume(std::move(slice));
        }
        // log(OTTERS_LOG_DEBUG, "recv thread ended\n");
    }

    void consume(Slice&& slice) {
        // log(OTTERS_LOG_DEBUG, "sent slice {}", slice.position());
        _messages.push(std::move(slice));
    }

    void end_input() {
        Slice slice;
        _messages.push(std::move(slice));
    }
    
private:
    std::vector<ParentOperator*> _parent_operators;
    safe_queue<Slice> _messages;
};

}

#endif
