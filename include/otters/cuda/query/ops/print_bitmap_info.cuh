#ifndef OTTERS_CUDA_QUERY_OPS_PRINT_BITMAP_INFO_CUH
#define OTTERS_CUDA_QUERY_OPS_PRINT_BITMAP_INFO_CUH

#include <otters/cuda/exception/cuda_assert.h>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/bitmap/roaring.cuh>
#include <otters/cuda/query/timestep.cuh>

namespace otters {

template<typename GPUBitmap, int num_coord, int num_attribute>
class PrintBitmapInfoOperator {
public:
    using slice_type = TimeSlice<num_coord, MultiAttributeBuffer<GPUBitmap, num_attribute>>;
    void consume(slice_type&& slice) {
        for(auto i = 0; i < num_attribute; ++i) {
            slice.data().attribute(i).print_info();
        }
    }

};


}

#endif
