#ifndef OTTERS_CUDA_QUERY_OPS_READ_OP_CUH
#define OTTERS_CUDA_QUERY_OPS_READ_OP_CUH

#include <otters/common/containers/time_slice.h>
#include <otters/common/io/time_slice_array.h>
#include <otters/common/utils/profile.h>

namespace otters {

template<typename ParentOp, typename Slice, typename BufferPool>
class ScanOperator {
public:
    using OpenType = typename TimeSliceArray<Slice>::OpenType;
public:
    ScanOperator(ParentOp* parent_op,
            std::string prefix,
            BufferPool* pool,
            int total_timesteps = std::numeric_limits<int>::max(),
            const std::string& op_name = "scan_op")
        : _parent_op(parent_op),
          _array(prefix, OpenType::READ_ONLY),
          _prefix(prefix),
          _pool(pool),
          _total_timesteps_read(0),
          _total_timesteps_to_read(total_timesteps),
          _scan_timer(op_name + "_scan") {
    }

    void drive() {
        for(auto it = _array.begin(); it != _array.end(); ++it) {
//            log(OTTERS_LOG_WARNING, "Reading timestep {}", _total_timesteps_read);
            if(_total_timesteps_read >= _total_timesteps_to_read)
                break;
            auto coord = *it;
//             log(OTTERS_LOG_WARNING, "Scanning Timestep = {} from prefix {}", coord, _prefix);
            _scan_timer.start();
            auto slice = _array.read_timestep(coord, _pool);
            _scan_timer.stop();
//            log(OTTERS_LOG_WARNING, "Scanned Timestep = {}", coord);
            _parent_op->consume(std::move(slice));
            // log(OTTERS_LOG_WARNING, "Scanned Timestep = {}", coord);
//            log(OTTERS_LOG_WARNING, "Read timestep {}", _total_timesteps_read);
            ++_total_timesteps_read;
        }
        Slice slice;
        _parent_op->consume(std::move(slice));
        log(OTTERS_LOG_WARNING, "Total timesteps read = {}", _total_timesteps_read);
    }

private:
    Timer _scan_timer;
    ParentOp* _parent_op;
    TimeSliceArray<Slice> _array;
    std::string _prefix;
    BufferPool* _pool;
    int _total_timesteps_read;
    int _total_timesteps_to_read;

};

}

#endif
