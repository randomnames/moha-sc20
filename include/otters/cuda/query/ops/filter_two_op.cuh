#ifndef OTTERS_CUDA_QUERY_OPS_FILTER_TWO_OP_CUH
#define OTTERS_CUDA_QUERY_OPS_FILTER_TWO_OP_CUH

#include <otters/common/utils/log.h>
#include <otters/cuda/query/ops/merge_timestep.cuh>
#include <queue>
#include <vector>
#include "csm_support.cuh"
#include <otters/cuda/query/plain_subgroup_discovery_support.cuh>

namespace otters {

template<typename ParentOp, typename Slice>
class FilterTwoOperator {
public:
    using input_slice_type = PairSlice<Slice>;
public:
    FilterTwoOperator(ParentOp* op)
        :_parent_op(op) {
    }

    void consume(input_slice_type&& slice) {
        log(OTTERS_LOG_DEBUG, "left = {}, right = {}", slice.first().position(), slice.second().position());
    }
private:
    ParentOp* _parent_op;
};

template<typename ParentOp, typename Slice, typename MinMaxGetter, typename T, int num_dimensions, int num_attr, int num_best_results = 10>
class PlainCSMOperator {
public:
    using input_slice_type = PairSlice<Slice>;
    constexpr const static int num_coord_dims = Slice::num_coord_dims;
    using slice_type = PairSlice<Slice>;
    static const int max_search_size = 1024;
    using output_slice_type = TimeSlice<num_coord_dims, BestContrastSets<T, num_attr, num_dimensions, num_best_results>>;

    using condition_type = SubsetCondition<T, num_attr, 3>;
public:
    PlainCSMOperator(
        ParentOp* op,
        std::array<int64_t, num_dimensions> dimension_sizes,
        std::array<MinMaxGetter, num_attr> min_max_getters,
        int dimension_buckets,
        int attribute_buckets)
        : _parent_op(op),
          _dimension_sizes(dimension_sizes),
          _min_max_getter(std::move(min_max_getters)),
          _dimension_buckets(dimension_buckets),
          _attribute_buckets(attribute_buckets) {
    }

    void consume(input_slice_type&& slice) {
// #pragma clang diagnostic push
// #pragma clang diagnostic ignored "-Wexit-time-destructors"
        std::queue<condition_type> q;
        std::vector<condition_type> visited;
// #pragma clang diagnostic pop
        // log(OTTERS_LOG_DEBUG, "left = {}, right = {}", slice.first().position(), slice.second().position());
        output_slice_type output_slice(slice.position());
        int total_searched = 0;
        auto timestep_id = slice.timestep_id();
        q.push(condition_type());
        // double best_quality = 0;
        while(total_searched < max_search_size && !q.empty()) {
            auto condition = q.front(); q.pop();
            // log(OTTERS_LOG_DEBUG, "condition = {}", condition.format());
            auto left_result = filter_plain(slice.first(), condition, num_attr - 1);
            auto right_result = filter_plain(slice.second(), condition, num_attr - 1);
            ContrastSet<T, num_attr, num_dimensions> result(condition, left_result, right_result);
            // auto quality = result.quality();
            output_slice.data().insert(std::move(result));
            ++total_searched;
            // if(quality > best_quality)
                // best_quality = quality;
            for(auto i = 0; i < num_dimensions; ++i) {
                if (condition.dim_range(i).enabled() && condition.dim_range(i).span() * _dimension_buckets < _dimension_sizes[i]) {
                    continue;
                }
                auto new_condition = split_condition_dimension(condition, i);
                auto it = std::find(visited.begin(), visited.end(), new_condition.first);
                if(it == visited.end())
                    q.push(new_condition.first);
                it = std::find(visited.begin(), visited.end(), new_condition.second);
                if(it == visited.end())
                    q.push(new_condition.second);
            }
            for (auto i = 0; i < num_attr - 1; ++i) {
                if (condition.attr_range(i).enabled() &&
                        condition.attr_range(i).span() * _attribute_buckets <
                                _min_max_getter[i].max(timestep_id) - _min_max_getter[i].min(timestep_id)) {
                    continue;
                }
                auto new_condition = split_condition_attribute(timestep_id, condition, i);
                auto it = std::find(visited.begin(), visited.end(), new_condition.first);
                if(it == visited.end())
                    q.push(new_condition.first);
                it = std::find(visited.begin(), visited.end(), new_condition.second);
                if(it == visited.end())
                    q.push(new_condition.second);
            }
        }
        _parent_op->consume(std::move(output_slice));
    }

private:
    std::pair<condition_type, condition_type> split_condition_dimension(condition_type& old_condition,
            int dimension_id) {
        auto new_conditions = std::make_pair(old_condition, old_condition);
        if(old_condition.dim_range(dimension_id).enabled() == false) {
            new_conditions.first.dim_range(dimension_id).assign(0, _dimension_sizes[dimension_id] / 2);
            new_conditions.second.dim_range(dimension_id)
                    .assign(_dimension_sizes[dimension_id] / 2, _dimension_sizes[dimension_id]);
        } else {
            auto min = old_condition.dim_range(dimension_id).min();
            auto max = old_condition.dim_range(dimension_id).max();
            auto mid = min + (max - min) / 2;
            new_conditions.first.dim_range(dimension_id).assign(min, mid);
            new_conditions.second.dim_range(dimension_id).assign(mid, max);
        }
        return new_conditions;
    }

    std::pair<condition_type, condition_type> split_condition_attribute(int timestep_id, condition_type& old_condition,
            int attribute_id) {
        auto new_conditions = std::make_pair(old_condition, old_condition);
        if(old_condition.attr_range(attribute_id).enabled() == false) {
            auto min = _min_max_getter[attribute_id].min(timestep_id);
            auto max = _min_max_getter[attribute_id].max(timestep_id);
            auto mid = min + (max - min) / 2;
            new_conditions.first.attr_range(attribute_id).assign(min, mid);
            new_conditions.second.attr_range(attribute_id).assign(mid, max);
        } else {
            auto min = old_condition.attr_range(attribute_id).min();
            auto max = old_condition.attr_range(attribute_id).max();
            auto mid = min + (max - min) / 2;
            new_conditions.first.attr_range(attribute_id).assign(min, mid);
            new_conditions.second.attr_range(attribute_id).assign(mid, max);
        }
        return new_conditions;
    }

    double compute_quality(MeanAggregator<T>& left, MeanAggregator<T>& right) {
        return (1.0 + fabs(left.mean() - right.mean())) * (0.0 + abs(left.size() - right.size()));
    }

    MeanAggregator<T> filter_plain(Slice& slice, SubsetCondition<T, num_attr, num_dimensions>& condition,
            int target_attr) {
        MeanAggregator<T> agg;
        std::array<int64_t, num_dimensions> dim_mins;
        std::array<int64_t, num_dimensions> dim_maxs;
        for (auto i = 0u; i < num_dimensions; ++i) {
            auto range = condition.dim_range(i);
            if (range.enabled()) {
                dim_mins[i] = range.min();
                dim_maxs[i] = range.max();
            } else {
                dim_mins[i] = 0;
                dim_maxs[i] = _dimension_sizes[i];
            }
        }
        std::array<T*, num_attr> attr_ptrs;
        for(auto i = 0; i < num_attr; ++i) {
            attr_ptrs[i] = slice.data().attribute(i).data();
        }
        filter_plain_array<T, num_attr, num_dimensions>(
                attr_ptrs, dim_mins, dim_maxs, _dimension_sizes, [&agg, &condition, target_attr](auto& dims, auto& attrs) {
                    (void) dims;
                    for (auto i = 0u; i < num_attr; ++i) {
                        auto range = condition.attr_range(i);
                        if (range.enabled() && (attrs[i] < range.min() || attrs[i] > range.max()))
                            return;
                    }
                    agg.update(attrs[target_attr]);
                });
        return agg;
    }

private:
    ParentOp* _parent_op;
    std::array<int64_t, num_dimensions> _dimension_sizes;
    std::array<MinMaxGetter, num_attr> _min_max_getter;
    int _dimension_buckets;
    int _attribute_buckets;
};

}

#endif
