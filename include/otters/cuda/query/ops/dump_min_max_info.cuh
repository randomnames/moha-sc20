#ifndef OTTERS_CUDA_QUERY_OPS_DUMP_MIN_MAX_INFO_CUH
#define OTTERS_CUDA_QUERY_OPS_DUMP_MIN_MAX_INFO_CUH

#include <fstream>

namespace otters {

template<typename T>
struct MinMaxAggregatorResult {
    T _min;
    T _max;


    void print(std::ostream& os) const {
        auto result = fmt::format("{} {}", _min, _max);
        os << result;
    }
};

template<typename T>
class MinMaxAggregator {
public:
    using result_type = MinMaxAggregatorResult<T>;

    MinMaxAggregator() {
        _result._min = std::numeric_limits<T>::max();
        _result._max = std::numeric_limits<T>::min();
    }

    void process(T element) {
        _result._min = std::min(_result._min, element);
        _result._max = std::max(_result._max, element);
    }

    
    auto& result() {
        return _result;
    }

    const auto& result() const {
        return _result;
    }

private:
    result_type _result;
};

template<typename T>
std::ostream& operator<<(std::ostream& os, const MinMaxAggregatorResult<T>& result) {
    result.print(os);
    return os;
}

template<typename T>
std::istream& operator>>(MinMaxAggregatorResult<T>& result, std::istream& is) {
    is >> result._min;
    is >> result._max;
    return is;
}


template<typename Slice, typename T, size_t num_attr, typename Aggregator>
class DumpMinMaxInfoOperator {
public:
    using result_type = typename Aggregator::result_type;
    DumpMinMaxInfoOperator(std::string prefix) : _prefix(prefix) {
    }

    void consume(Slice&& slice) {

        auto timestep_id = slice.timestep_id();

        for(auto attr = 0; attr < num_attr; ++attr) {

            Aggregator aggregator;

            for (auto i = 0; i < slice.attribute(attr).data().size(); ++i) {
                aggregator.process(slice.attribute(attr).data()[i]);
            }
            if(timestep_id >= _results[attr].size())
                _results[attr].resize(timestep_id + 1);
            _results[attr][timestep_id] = aggregator.result();
            fmt::print("We processed timestep {}, attr {}, first result = {} thread_id = {}.\n",
                    timestep_id,
                    attr,
                    slice.attribute(attr).data()[3],
                    std::this_thread::get_id());
        }

    }

    ~DumpMinMaxInfoOperator() {
        for (auto attr = 0; attr < num_attr; ++attr) {
            std::ofstream of(_prefix + "." + std::to_string(attr));
            of << _results[attr].size() << "\n";
            for (auto i = 0; i < _results[attr].size(); ++i) {
                of << _results[attr][i];
                of << "\n";
            }
        }
    }
private:
    std::vector<result_type> _results[num_attr];
    std::string _prefix;
};

}

#endif