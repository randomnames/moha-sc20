#ifndef OTTERS_CUDA_QUERY_OPS_GPU_SEND_RECV_OPERATOR_CUH
#define OTTERS_CUDA_QUERY_OPS_GPU_SEND_RECV_OPERATOR_CUH

#include <atomic>
#include <fmt/core.h>
#include <mutex>
#include <otters/common/utils/log.h>
#include <otters/common/utils/profile.h>
#include <otters/cuda/container/buffer_pool.cuh>
#include <otters/cuda/exception/cuda_assert.h>
#include <queue>

namespace otters {


template<typename ParentOperator, typename BufferPool, typename GPUSlice>
class GPUSendRecvOperator {
public:
    using slice_type = typename ParentOperator::slice_type;
    static void callback(cudaStream_t stream, cudaError_t status, void* data) {
        (void) stream;
        OTTERS_CUDA_ERRORCHECK( status );
        auto* op = (GPUSendRecvOperator*) data;
        {
            std::unique_lock<std::mutex> lck(op->_mutex);
            ++op->_copied_slices;
            // fmt::print("copied_slices : {}\n", op->_copied_slices);
        }
        op->_new_result.notify_one();
    }

    void notify_all() {
        _new_result.notify_all();
    }

public:
    GPUSendRecvOperator(std::vector<ParentOperator*> op, BufferPool* pool)
        : _parent_ops(std::move(op)), _pool(pool), _copied_slices(0), _finished_slices(0) {
        cudaStreamCreate(&_copy_stream);
    }

    void drive(int id) {
        Timer timer("gpu_wait_" + std::to_string(id));
        do {
            // fmt::print("thread {} retriving slice\n", id);
            timer.start();
            auto slice = next_slice();
            timer.stop();
            // log(OTTERS_LOG_DEBUG, "slice = {}", slice.position());
            if((bool) slice == false)
                break;
            // fmt::print("slice[{}][{}] = {}\n", slice.timestep_id(), slice.timestep_id() - 1, 
                // slice.data().data()[(slice.timestep_id() - 1) % 10]);
            _parent_ops[id]->consume(std::move(slice));
        } while (true);
    }

    void consume(GPUSlice&& gpu_slice) {
        // log(OTTERS_LOG_DEBUG, "Sending one slice");
        slice_type cpu_slice(gpu_slice.position(), _pool);
        if((bool) gpu_slice != false) {
            gpu_slice.data().resize_for_copy(cpu_slice.data());
            // fmt::print("cpu_slice capacity = {}", cpu_slice.data().capacity());
            gpu_slice.data().copy_to_host_async_stream(cpu_slice.data(), _copy_stream);
        }
        std::unique_lock<std::mutex> lck(_mutex);
        _gpu_slices.push(std::move(gpu_slice));
        _cpu_slices.push(std::move(cpu_slice));
        lck.unlock();
        // log(OTTERS_LOG_DEBUG, "Sent one slice");
        // record a finish event.
        OTTERS_CUDA_ERRORCHECK( cudaStreamAddCallback(_copy_stream, &GPUSendRecvOperator::callback, this, 0));
    }

    slice_type next_slice() {
        std::unique_lock<std::mutex> lck2(_mutex);
        while (_finished_slices != -1 && _copied_slices <= _finished_slices) { /* copied slices all processed */
            _new_result.wait(lck2);
        }
        if(_finished_slices == -1)
            return slice_type();
        else
            ++_finished_slices;
        // fmt::print("finished slices: {}.\n", _finished_slices);

        auto gpu_slice = std::move(_gpu_slices.front());
        _gpu_slices.pop();
        if(gpu_slice == false) {
            assert(_copied_slices == _finished_slices);
            _finished_slices = -1;
            /* we are the last thread, wake up all others. */
            _new_result.notify_all();
        }
        auto cpu_slice = std::move(_cpu_slices.front());
        _cpu_slices.pop();
        return cpu_slice;
    }

private:
    std::vector<ParentOperator*> _parent_ops;
    BufferPool* _pool;
    cudaStream_t _copy_stream;
    int _copied_slices;
    int _finished_slices;
    std::condition_variable _new_result;

    std::queue<GPUSlice> _gpu_slices;
    std::queue<slice_type> _cpu_slices;
    std::mutex _mutex;


};

}

#endif
