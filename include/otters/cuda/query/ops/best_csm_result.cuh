#ifndef OTTERS_INCLUDE_OTTERS_CUDA_QUERY_OPS_BEST_CSM_RESULT_CUH
#define OTTERS_INCLUDE_OTTERS_CUDA_QUERY_OPS_BEST_CSM_RESULT_CUH

#include <otters/cuda/query/ops/csm_support.cuh>
#include <otters/common/containers/time_slice.h>
#include <otters/common/utils/log.h>

#include <unordered_map>
#include <map>
#include <vector>
#include <utility>

namespace otters {

template<typename T, size_t num_coordinates, size_t num_attr, size_t num_dimensions, int best_to_keep>
class BestCSMResultOperator {
public:
    using CSMSet = ContrastSet<T, num_attr, num_dimensions>;
    using CSMResult = BestContrastSets<T, num_attr, num_dimensions, best_to_keep>;
    using input_slice_type = TimeSlice<num_coordinates, CSMResult>;
public:
    explicit BestCSMResultOperator(int total_nodes)
            : _total_nodes(total_nodes) {}
    void consume(input_slice_type&& slice) {
        auto timestep_id = slice.position()[0];
//        log(OTTERS_LOG_DEBUG, "Processing slice = {}, timestep_id = {}", slice.position(), timestep_id);
        auto& csm_result = slice.data();
        auto& csm_vector = _cache[timestep_id];
//        log(OTTERS_LOG_DEBUG, "CSM_RESULT_SIZE = {}", csm_result._size);
        for(auto i = 0; i < csm_result._size; ++i) {
//            log(OTTERS_LOG_DEBUG, "processing {}/{}", i + 1, csm_result._size);
            CSMSet& csm_set = csm_result._best[i];
            for(auto j = 0; j < _cache[timestep_id].size(); ++j)  {
                if(_cache[timestep_id][j]._condition == csm_set._condition) {
//                    log(OTTERS_LOG_DEBUG, "aggregating j = {}", j);
                    _cache[timestep_id][j].aggregate(csm_set);
                    goto next_set;
                }
            }
//            log(OTTERS_LOG_DEBUG, "appending result instead.");
            _cache[timestep_id].emplace_back(std::move(csm_set));
        next_set:
            ;
        }
        if(++_processed[timestep_id] == _total_nodes) {
            auto& best = *std::max_element(_cache[timestep_id].begin(), _cache[timestep_id].end());
            _cache.erase(timestep_id);
            _best_csm[timestep_id] = best;
        }
    }
private:
    std::unordered_map<int, int> _processed;
    std::unordered_map<int, CSMSet > _best_csm;
    std::unordered_map<int, std::vector<CSMSet>> _cache;
    int _total_nodes;
};

}

#endif // OTTERS_INCLUDE_OTTERS_CUDA_QUERY_OPS_BEST_CSM_RESULT_CUH
