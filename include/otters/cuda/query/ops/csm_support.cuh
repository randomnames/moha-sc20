#ifndef OTTERS_CUDA_QUERY_OPS_CSM_SUPPORT_CUH
#define OTTERS_CUDA_QUERY_OPS_CSM_SUPPORT_CUH

#include <array>
#include <cstdlib>
#include <cstdint>
#include <limits>

#include <otters/common/io/serialization.h>
#include <otters/common/utils/log.h>

#include <fmt/core.h>

namespace otters {

template<typename T>
struct Range {
    T _min;
    T _max;
    const T& min() const {
        return _min;
    }

    const T& max() const {
        return _max;
    }

    T span() const {
        return _max - _min;
    }

    void assign(T min, T max) {
        _min = min;
        _max = max;
    }

    bool enabled() const {
        return _max > _min;
    }

    void disable() {
        _max = _min - (T) 1;
    }

    bool contain_half_closed(T value) {
        return _min <= value && _max > value;
    }

    bool operator==(const Range& other) const {
        if(enabled() == false && other.enabled() == false)
            return true;
        else
            return _min - other._min < std::numeric_limits<T>::epsilon() &&
                   other._min - _min < std::numeric_limits<T>::epsilon() &&
                   _max - other._max < std::numeric_limits<T>::epsilon() &&
                   other._max - _max < std::numeric_limits<T>::epsilon();
    }

    bool operator!=(const Range& other) const {
        return !operator==(other);
    }

    std::string format() {
        if(enabled())
            return fmt::format("[{} -> {}]", _min, _max);
        else
            return "[]";
    }
};

template<typename T, size_t num_attrs, size_t num_dims>
struct SubsetCondition {
    std::array<Range<int64_t>, num_dims> _dim_ranges;
    std::array<Range<T>, num_attrs> _attr_ranges;

    SubsetCondition() {
        for(auto& dim: _dim_ranges) {
            dim.disable();
        }
        for(auto& range: _attr_ranges) {
            range.disable();
        }
    }

    bool operator==(const SubsetCondition& other) const {
        for(auto i = 0u; i < num_dims; ++i) {
            if(dim_range(i) != other.dim_range(i))
                return false;
        }
        for(auto i = 0u; i < num_attrs; ++i) {
            if(attr_range(i) != other.attr_range(i))
                return false;
        }
        return true;
    }

    bool operator!=(const SubsetCondition& other) const {
        return !operator==(other);
    }

    auto& dim_range(int idx) {
        return _dim_ranges[idx];
    }

    auto& attr_range(int idx) {
        return _attr_ranges[idx];
    }

    const auto& dim_range(int idx) const {
        return _dim_ranges[idx];
    }

    const auto& attr_range(int idx) const {
        return _attr_ranges[idx];
    }

    std::string format() {
        std::string result;
        for(auto i = 0u; i < num_dims; ++i) {
            result += dim_range(i).format();
            result += "d ";
        }
        for(auto i = 0u; i < num_attrs; ++i) {
            result += attr_range(i).format();
            result += "a ";
        }
        return result;
    }
};

template<typename T, size_t num_attrs, size_t num_dims>
struct ContrastSet {
    SubsetCondition<T, num_attrs, num_dims> _condition;
    double _mean[2];
    ssize_t _support[2];
    ContrastSet() = default;
    template<typename Aggregator>
    ContrastSet(SubsetCondition<T, num_attrs, num_dims> condition, Aggregator left, Aggregator right)
        : _condition(condition),
          _mean{left.mean(), right.mean()},
          _support{left.size(), right.size()} {
    }

    double mean(int idx) const {
        return _mean[idx];
    }
    size_t support(int idx) const {
        return _support[idx];
    }
    double quality() const {
        return (1.0 + fabs(mean(0) - mean(1))) * (0.0 + (double) abs((long long)(support(0) - support(1))));
    }
    bool operator<(const ContrastSet& rhs) const {
        return quality() < rhs.quality();
    }

    bool operator<=(const ContrastSet& rhs) const {
        return quality() < rhs.quality();
    }
    bool operator>(const ContrastSet& rhs) const {
        return quality() > rhs.quality();
    }

    bool operator>=(const ContrastSet& rhs) const {
        return quality() > rhs.quality();
    }

    bool operator==(const ContrastSet& rhs) const {
        return quality() == rhs.quality();
    }

    void aggregate(const ContrastSet& set) {
        assert(_condition == set._condition);
        auto new_support0 = _support[0] + set._support[0];
        auto new_support1 = _support[1] + set._support[1];
        auto new_mean0 = _mean[0] * _support[0] + set._mean[0] * set._support[0];
        auto new_mean1 = _mean[1] * _support[1] + set._mean[1] * set._support[1];
        _support[0] = new_support0;
        _support[1] = new_support1;
        _mean[0] = new_mean0;
        _mean[1] = new_mean1;
    }
};

template<typename T, size_t num_attrs, size_t num_dims, size_t num_items>
struct BestContrastSets {
    using CS = ContrastSet<T, num_attrs, num_dims>;
    std::array<CS, num_items> _best;
    int _size;
    BestContrastSets() : _size(0) {}

    void insert(CS&& cs) {
        auto it = std::upper_bound(_best.data(), _best.data() + _size, cs, [](const CS& a, const CS& b) {
            return a > b;
        });
        auto idx = it - _best.data();
        // log(OTTERS_LOG_DEBUG, "idx = {}, num_items = {}, _size = {}", idx, num_items, _size);
        assert(idx <= num_items);
        assert(it >= 0);
        if(idx == num_items) {
            return;
        }
        auto end = std::min((int) num_items, _size + 1);
        for(auto i = end - 1; i >= idx; --i) {
            _best[i] = _best[i - 1];
        }
        *it = std::move(cs);
        if(_size < num_items)
            ++_size;
    }

    ssize_t serialize_size() const {
        return sizeof(BestContrastSets);
    }

    span<char> serialize(span<char> buffer) const {
        assert(buffer.size() >= serialize_size());
        auto* char_ptr = (const char*)(this);
        std::copy(char_ptr, char_ptr + serialize_size(), buffer.data());
        return buffer.advance(serialize_size());
    }

    span<char> deserialize(span<char> buffer) {
        assert(buffer.size() >= serialize_size());
        auto* char_ptr = (char*)(this);
        std::copy(buffer.data(), buffer.data() + serialize_size(), char_ptr);
        return buffer.advance(serialize_size());
    }

    void clear() {
        _size = 0;
    }

};

}

#endif
