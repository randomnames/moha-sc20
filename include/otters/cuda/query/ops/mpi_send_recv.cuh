#ifndef OTTERS_CUDA_QUERY_OPS_MPI_SEND_RECV_CUH
#define OTTERS_CUDA_QUERY_OPS_MPI_SEND_RECV_CUH

#include <otters/common/containers/safe_queue.h>
#include <otters/common/third_party/mpi.h>
#include <otters/common/io/serialization.h>
#include <otters/common/utils/log.h>
#include <otters/cuda/container/buffer_pool.cuh>
#include <condition_variable>

#include <immintrin.h>

#include <array>
#include <atomic>
#include <chrono>
#include <mutex>
#include <thread>

namespace otters {

void MPI_CHECK_ERROR(int mpi_error) {
    if(mpi_error != MPI_SUCCESS) {
        fmt::print(stderr, "ERROR!\n");
        char buffer[256];
        int result_len;
        MPI_Error_string(mpi_error, buffer, &result_len);
        log(OTTERS_LOG_ERROR, "Got MPI Error: {}", buffer);
    }
    assert(mpi_error == MPI_SUCCESS);
}

static const int magic_number = 0x26;

struct MPINodeInfo {
    static const size_t max_nodes = 32;

    int _rank;
    int _total_nodes;
    std::array<int, max_nodes> _node_ids;
};

template<typename Slice, typename NodeSelector>
class MPISendOperator {
public:
    using slice_type = Slice;
    static const int default_max_queue_items = 8;
    enum class JobStatus {
        QUEUED,
        SENDING,
        SENT
    };
    struct Job {
        Job() : _target(-1), _status(JobStatus::SENT) {}
        Job(int target, byte_buffer&& buffer)
            : _buffer(std::move(buffer)), _target(target), _status(JobStatus::SENT) {
            // log(OTTERS_LOG_DEBUG, "Created job with receving rank {}", _target);
        }
        char* data() {
            return _buffer.data();
        }
        size_t size() {
            return _buffer.size();
        }
        operator bool() {
            return _target != -1;
        }

        void send() {
            MPI_Isend(data(), (int) size(), MPI_BYTE, _target, 0, MPI_COMM_WORLD, &_request);
        }
        
        byte_buffer _buffer;
        int _target;
        JobStatus _status;
        MPI_Request _request;
    };
public:
    MPISendOperator(MPINodeInfo node_info,
            NodeSelector selector,
            int total_producers,
            int total_consumers,
            int max_queue_items = default_max_queue_items)
        : _node_info(node_info),
          _node_selector(selector),
          _jobs(max_queue_items),
          _empty_buffers(1, max_queue_items),
          _total_producers(total_producers),
          _total_consumers(total_consumers),
          _ended_producers(0) {
        for(auto i = 0; i < max_queue_items; ++i) {
            _empty_buffers.push(i + 1);
        }
    }

    void send_loop() {
        log(OTTERS_LOG_DEBUG, "initialzed send loop.");
        bool no_new_job = false; 
        while(true) {
            // log(OTTERS_LOG_DEBUG, "Pending for job.");
            no_new_job = true;
            for(size_t i = 0; i < _jobs.size(); ++i) {
                auto& job = _jobs[i];
                switch(job._status) {
                    case JobStatus::QUEUED: {
                        log(OTTERS_LOG_DEBUG, "Sending async, message size = {} rank = {}", job.size(), job._target);
//                        MPI_Isend(job.data(), (int)job.size(), MPI_BYTE, job._target, 0, MPI_COMM_WORLD, &job._request);
                        MPI_Isend(job.data(), (int)job.size(), MPI_BYTE, job._target, 0, MPI_COMM_WORLD, &job._request);

                        job._status = JobStatus::SENDING;
                        no_new_job = false;
                        break;
                    }
                    case JobStatus::SENDING: {
                        int flag;
                        MPI_Status status;
                        MPI_Test(&job._request, &flag, &status);
                        if (flag) {
                            int cancelled = 0;
                            MPI_Test_cancelled(&status, &cancelled);
                            if (cancelled) {
                                MPI_Isend(job.data(),
                                        (int)job.size(),
                                        MPI_BYTE,
                                        job._target,
                                        0,
                                        MPI_COMM_WORLD,
                                        &job._request);
                                job._status = JobStatus::QUEUED;
                                no_new_job = false;
                                --i; continue;
                            } else {
                                job._status = JobStatus::SENT;
                                _empty_buffers.push((int)(i + 1));
                            }
                        }
                        break;
                    }
                    case JobStatus::SENT: {
                        break;
                    }
                }
            }

            if (_ended_producers >= _total_producers && no_new_job)
                break;
            std::this_thread::yield();
        }
        // log(OTTERS_LOG_INFO, "Send ending message.\n");
        std::vector<int> targets;
        for (auto i = 0; i < _total_consumers; ++i)
            targets.push_back(i * 2 + 1); // FIXME: Need to consider mapping
        send_end_messages(std::move(targets));
        log(OTTERS_LOG_DEBUG, "ended send loop.");
    }

    void consume(Slice&& slice_ref) {
        // log(OTTERS_LOG_DEBUG, "position = {}", slice_ref.position());
        auto slice = std::move(slice_ref);
        if((bool) slice == false) {
            // log(OTTERS_LOG_DEBUG, "job equals false.");
            _ended_producers.fetch_add(1);
            return;
        }
        // log(OTTERS_LOG_DEBUG, "node equals {}.", _node_selector(slice));
        auto buffer_id = _empty_buffers.pop() - 1;
        auto& job = _jobs[buffer_id];
        assert(job._status == JobStatus::SENT);
        auto& buffer = job._buffer;
        // buffer.clear();
        buffer.resize(slice.serialize_size());
        slice.serialize(span<char>(buffer.data(), buffer.size()));
        job._target = _node_selector(slice);
        _mm_sfence();
        job._status = JobStatus::QUEUED;
    }

    void end_input() {
        log(OTTERS_LOG_DEBUG, "ending input.");
        _ended_producers.fetch_add(1);
        return;
    }
private:
    void send_end_messages(std::vector<int> targets) {
        std::vector<MPI_Request> requests(targets.size());
        for(size_t i = 0; i < targets.size(); ++i) {
            log(OTTERS_LOG_DEBUG, "Sending end message to {}", targets[i]);
            MPI_CHECK_ERROR( MPI_Isend(nullptr, 0, MPI_CHAR, targets[i], magic_number, MPI_COMM_WORLD, &requests[i]) );
        }
        for(size_t i = 0; i < targets.size(); ++i) {
            MPI_Status status;
            auto err = MPI_Wait(&requests[i], &status);
            assert(err == MPI_SUCCESS);
            int cancelled = 0;
            MPI_Test_cancelled(&status, &cancelled);
            assert(cancelled == 0);
        }
    }
private:
    MPINodeInfo _node_info;
    NodeSelector _node_selector;
    std::vector<Job> _jobs;
    safe_queue<int> _empty_buffers;
    int _max_queue_items;
    int _total_producers;
    int _total_consumers;
    std::atomic<int> _ended_producers;
};

template<typename ParentOp, typename Slice, typename BufferPool>
class MPIRecvOperator {
public:
    enum class JobStatus {
        PROCESSED,
        RECEIVING,
        RECEIVED,
    };
    struct Job {
        const static int init_size = 1024;
        MPI_Request _mpi_request;
        MPI_Status _mpi_status;
        JobStatus _status;
        byte_buffer _buffer;
        bool post_irecv() {
            int got_message;
            MPI_CHECK_ERROR( MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &got_message, &_mpi_status));
            if(got_message != 0) {
                int count;
                MPI_Get_count(&_mpi_status, MPI_BYTE, &count);
                _buffer.resize(count);
                log(OTTERS_LOG_DEBUG, "issuing Irecv");
                MPI_CHECK_ERROR( MPI_Irecv(_buffer.data(), (int) _buffer.size(),
                        MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &_mpi_request));
                log(OTTERS_LOG_DEBUG, "Issued Irecv");
                return true;
            } else {
                return false;
            }
        }
        bool check_irecv() {
            int flag;
            MPI_CHECK_ERROR( MPI_Test(&_mpi_request, &flag, &_mpi_status) );
            return flag;
        }
    };
public:
    MPIRecvOperator(std::vector<ParentOp*> parent_ops, int total_producers, int total_recv_buffers, BufferPool* pool)
        : _parent_ops(std::move(parent_ops)),
          _recvs(1, total_recv_buffers),
          _jobs(total_recv_buffers),
          _ended_producers(0),
          _pool(pool),
          _total_producers(total_producers) {
    }

    void drive(int id) {
        while(true) {
            auto buffer_num = _recvs.pop();
            log(OTTERS_LOG_DEBUG, "Got buffer number : {}", buffer_num);
            if(buffer_num == 0)
                break;
            else {
                auto buffer_id = buffer_num - 1;
                log(OTTERS_LOG_DEBUG, "Creating buffer for buffer num: {}, {}", buffer_num, buffer_id);
                Slice slice(_jobs[buffer_id]._buffer, _pool);
                log(OTTERS_LOG_DEBUG, "Processing buffer {}, timestep {}", buffer_id, slice.position());
                _parent_ops[id]->consume(std::move(slice));
                _mm_sfence();
                _jobs[buffer_id]._status = JobStatus::PROCESSED;
            }
        }
        log(OTTERS_LOG_DEBUG, "ending drive thread.");
    }
    
    void recv_loop() {
        log(OTTERS_LOG_DEBUG, "initialzed recv loop.");
        auto size = _jobs.size();
        while(_ended_producers < _total_producers) {
            for(int i = 0; i < (int) size; ++i) {
                auto posted_request = 0;
                auto recved_request = 0;
                auto& job = _jobs[i];
                switch(job._status) {
                    case JobStatus::PROCESSED: {
                        auto posted = job.post_irecv();
                        if(posted) {
                            job._status = JobStatus::RECEIVING;
                            ++posted_request;
                        }
                        break;
                    }
                    case JobStatus::RECEIVING: {
                        auto received = job.check_irecv();
                        if(received) {
                            log(OTTERS_LOG_DEBUG, "Received a message at buffer {}, size = {}, tag = {}.", i, job._buffer.size(), job._mpi_status.MPI_TAG);
                            if(job._mpi_status.MPI_TAG == magic_number) {
                                ++_ended_producers;
                                log(OTTERS_LOG_DEBUG, "ended producer = {}.", _ended_producers);
                                job._status = JobStatus::PROCESSED;
                            } else {
                                job._status = JobStatus::RECEIVED;
                                log(OTTERS_LOG_DEBUG, "Adding buffer {}", i + 1);
                                _recvs.push(std::move(i + 1));
                            }
                            ++recved_request;
                        }
                        break;
                    }
                    case JobStatus::RECEIVED: {
                        // do nothing
                        break;
                    }
                }
                if(posted_request + recved_request == 0) {
                    std::this_thread::yield();
                }
            }
        }
        log(OTTERS_LOG_DEBUG, "ended recv loop.");
        _recvs.push(0);
    }
private:
private:
    std::vector<ParentOp*> _parent_ops;
    safe_queue<int> _recvs;
    std::vector<Job> _jobs;
    int _ended_producers;
    BufferPool* _pool;
    int _total_producers;
};

}

#endif
