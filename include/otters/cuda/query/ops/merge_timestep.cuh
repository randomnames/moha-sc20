#ifndef OTTERS_CUDA_QUERY_OPS_MERGE_TIMESTEP_CUH
#define OTTERS_CUDA_QUERY_OPS_MERGE_TIMESTEP_CUH

#include <otters/common/containers/coordinates.h>
#include <otters/common/containers/time_slice.h>
#include <otters/common/containers/safe_queue.h>
#include <otters/common/utils/log.h>
#include <map>

namespace otters {



template<typename ParentOp, typename Slice, typename Selector, int dimensions>
class MergeTimeSliceOperator {
public:
    using coord_type = Coordinates<dimensions>;
    using output_slice_type = PairSlice<Slice>;
    using slice_type = Slice;

public:
    MergeTimeSliceOperator(ParentOp* parent_op, Selector selector)
        : _parent_op(parent_op), _selector(std::move(selector)) {

        }
    void consume(Slice&& slice) {
        std::pair<coord_type, bool> select = _selector(slice);
        // log(OTTERS_LOG_DEBUG, "MergeOp: Got index {}", slice.position());
        auto it = _slice_cache.find(select.first);
        if (it != _slice_cache.end()) {
            auto other_slice = std::move(it->second);
            _slice_cache.erase(it);
            if (select.second == 0) {
                PairSlice<Slice> pair_slice(std::move(slice), std::move(other_slice));
                _parent_op->consume(std::move(pair_slice));
            } else {
                PairSlice<Slice> pair_slice(std::move(other_slice), std::move(slice));
                _parent_op->consume(std::move(pair_slice));
            }
        } else {
            _slice_cache[select.first] = std::move(slice);
        }
    }

private:
    ParentOp* _parent_op;
    std::map<coord_type, Slice> _slice_cache;
    Selector _selector;
};

template<typename ParentOp, typename Slice, typename Selector, int dimensions>
class MergeTimeSliceOperator2 {
public:
    using coord_type = Coordinates<dimensions>;
    using output_slice_type = PairSlice<Slice>;
    using slice_type = Slice;

public:
    MergeTimeSliceOperator2(ParentOp* parent_op, Selector selector)
        : _parent_op(parent_op), _left_queue(1, 4), _right_queue(1, 4), _selector(std::move(selector)), _ended() {
    }
    void consume(Slice&& slice) {
        if((bool) slice == false) {
            end_input();
            return;
        }
        std::pair<coord_type, bool> select = _selector(slice);
        // log(OTTERS_LOG_DEBUG, "MergeOp: Got index {}", slice.position());
        if(select.second == 0) {
            _left_queue.push(std::move(slice));
        } else {
            _right_queue.push(std::move(slice));
        }
    }

    void end_input() {
        auto result = _ended.fetch_add(1);
        // log(OTTERS_LOG_DEBUG, "MergeOp: ended = {}", result);
        if(result == 1) {
            Slice left_slice;
            _left_queue.push(std::move(left_slice));
            Slice right_slice;
            _right_queue.push(std::move(right_slice));
        }
    }

    void drive() {
        do {
            auto left = _left_queue.pop();
            // log(OTTERS_LOG_DEBUG, "MergeOp: Got left {}", left.position());

            if((bool) left ==  false)
                break;
            auto right = _right_queue.pop();
            // log(OTTERS_LOG_DEBUG, "MergeOp: Got right {}", right.position());
            if((bool) right == false)
                break;

#ifndef NDEBUG
            auto selector_left = _selector(left);
            auto selector_right = _selector(right);
            assert(selector_left.first == selector_right.first);
            assert(selector_left.second == 0 );
            assert(selector_right.second == 1);
#endif

            PairSlice<Slice> pair_slice(std::move(left), std::move(right));
            _parent_op->consume(std::move(pair_slice));
        } while (true);
    }

private:
    ParentOp* _parent_op;
    safe_queue<Slice> _left_queue;
    safe_queue<Slice> _right_queue;
    Selector _selector;
    std::atomic<int> _ended;
};


} // namespace otters

#endif
