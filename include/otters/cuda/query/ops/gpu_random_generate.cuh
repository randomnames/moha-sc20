#ifndef INCLUDE_OTTERS_CUDA_QUERY_OPS_GPU_RANDOM_GENERATE_CUH
#define INCLUDE_OTTERS_CUDA_QUERY_OPS_GPU_RANDOM_GENERATE_CUH

#include <otters/cuda/primitives/random.cuh>
#include <otters/common/containers/coordinates.h>
#include <otters/common/containers/time_slice.h>
#include <otters/cuda/container/multi_attribute_buffer.cuh>

namespace otters {

enum class Distribution {
    Normal,
    Uniform,
    LogNormal
};

template<typename ParentOp, int dimension, typename T, int num_attributes, typename BufferPool>
class GPUGenerateOperator {
public:
    using GPUSlice = TimeSlice<dimension, MultiAttributeBuffer<cuda_device_buffer<T>, num_attributes>>;
    using slice_type = GPUSlice;
    static const unsigned long long magic_seed = 0x19260817ULL;

public:
    GPUGenerateOperator(ParentOp* parent_op, Distribution distribution, int timestep_size, int total_timestep, BufferPool* pool)
        : _parent_op(parent_op),
          _distribution(distribution),
          _timestep_size(timestep_size),
          _total_timesteps(total_timestep),
          _pool(pool),
          _generate_timer("gpu_gen_time") {
        cudaStreamCreate(&_processing_stream);
        OTTERS_CUDA_ERRORCHECK( cudaMalloc(&data, sizeof(T) * _timestep_size) );
        auto block_size = 256;
        auto num_of_blocks = (_timestep_size % 256 == 0) ? _timestep_size / 256 : _timestep_size % 256 + 1;
        if (_distribution == Distribution::Uniform)
            generate_uniform<<<num_of_blocks, block_size, 0, _processing_stream>>>(data, _timestep_size, magic_seed);
        else if (_distribution == Distribution::Normal)
            generate_normal<<<num_of_blocks, block_size, 0, _processing_stream>>>(data, _timestep_size, magic_seed);
        else if (_distribution == Distribution::LogNormal)
            generate_log_normal<<<num_of_blocks, block_size, 0, _processing_stream>>>(data, _timestep_size, magic_seed);
        else
            assert(false);
        OTTERS_CUDA_ERRORCHECK(cudaStreamSynchronize(_processing_stream));
    }

    ~GPUGenerateOperator() {
        cudaStreamDestroy(_processing_stream);
    }


    GPUGenerateOperator(const GPUGenerateOperator&) = delete;

    GPUGenerateOperator& operator=(const GPUGenerateOperator&) = delete;

    void drive() {
        for(auto i = 0; i < _total_timesteps; ++i) {
            _generate_timer.start();
            Coordinates<dimension> coord;
            coord.reset();
            coord[0] = i;
            GPUSlice slice(coord, _pool);
            // log(OTTERS_LOG_DEBUG, "Generating timestep {}", i);
            for(auto k = 0; k < num_attributes; ++k) {
                slice.data().attribute(k).resize(_timestep_size);
                auto* slice_data = slice.data().attribute(k).data();
                OTTERS_CUDA_ERRORCHECK(cudaMemcpyAsync(
                        slice_data, data, sizeof(T) * _timestep_size, cudaMemcpyDeviceToDevice, _processing_stream));
            }
            OTTERS_CUDA_ERRORCHECK(cudaStreamSynchronize(_processing_stream));
            _generate_timer.stop();

            // log(OTTERS_LOG_DEBUG, "Generated timestep {}", i);
            // log(OTTERS_LOG_DEBUG, "calling parent");
            _parent_op->consume(std::move(slice));
            // log(OTTERS_LOG_DEBUG, "called parent");
        }
        // log(OTTERS_LOG_DEBUG, "Generated timestep {}", -1);
        slice_type end_slice;
        _parent_op->consume(std::move(end_slice));
        cudaFree(data);
    }
private:
    Timer _generate_timer;
    ParentOp* _parent_op;
    Distribution _distribution;
    int _timestep_size;
    int _total_timesteps;
    BufferPool* _pool;
    cudaStream_t _processing_stream;
    T* data;
};

}

#endif
