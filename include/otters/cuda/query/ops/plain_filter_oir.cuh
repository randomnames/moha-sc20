#ifndef OTTERS_INCLUDE_OTTERS_CUDA_QUERY_OPS_PLAIN_FILTER_OIR_CUH
#define OTTERS_INCLUDE_OTTERS_CUDA_QUERY_OPS_PLAIN_FILTER_OIR_CUH

#include <otters/cuda/query/ops/filter_oir_processor_3_d_base.cuh>
#include <otters/cuda/query/ops/window.cuh>
#include <otters/common/containers/buffer.h>
#include <otters/common/containers/time_slice.h>
#include <otters/common/utils/log.h>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/query/subgroup_discovery_support.cuh>

#include <vector>
#include <array>

namespace otters {

template<typename T, size_t num_coordinates, size_t num_dims, int num_attrs>
class PlainFilterOIRProcessor3D : public FilterOIRProcessor3DBase {
public:
    using input_slice_type = TimeSlice<num_coordinates, MultiAttributeBuffer<cuda_host_buffer<T>, num_attrs>>;

public:
    PlainFilterOIRProcessor3D(std::array<int64_t, num_dims> dim_mins, std::array<int64_t, num_dims> dim_maxs,
            std::array<int64_t, num_dims> dim_sizes, std::array<Range<T>, num_attrs> attr_ranges)
        : FilterOIRProcessor3DBase(dim_mins, dim_maxs, dim_sizes), _attr_ranges(attr_ranges) {
    }

    /* should totally change this to a virtual method. */
    void process(input_slice_type& slice1, input_slice_type& slice2) {
        log(OTTERS_LOG_DEBUG, "processing left = {}, right = {}", slice1.position(), slice2.position());
        auto num_planes = _dim_sizes[0];

        for(auto i = 0; i < num_planes; ++i) {
            //produce_new_plane
//            log(OTTERS_LOG_DEBUG, "filter_plane: {}/{}", i + 1, num_planes);
            filter_plane(_planes[i % 2], i, slice1, slice2);
            //do a flood fill
//            log(OTTERS_LOG_DEBUG, "flood_fill: {}/{}", i + 1, num_planes);
            flood_fill(i % 2);
        }

    }

    void operator()(input_slice_type& slice1, input_slice_type& slice2) {
        process(slice1, slice2);
    }

    void filter_plane(std::vector<int>& plane, int plane_number, input_slice_type& slice1, input_slice_type& slice2) {
        for(auto j = 0; j < _dim_sizes[1]; ++j) {
            for(auto k = 0; k < _dim_sizes[2]; ++k) {
                auto offset = j * _dim_sizes[2] + k;
                auto goffset = plane_number * _dim_sizes[1] * _dim_sizes[2] + offset;
                int valid = kNotVisited;
                for(auto a = 0; a < num_attrs; ++a) {
                    auto* data1 = slice1.data().attribute(a).data();
                    auto* data2 = slice2.data().attribute(a).data();
                    if(_attr_ranges[a].contain_half_closed(data1[goffset]) == false) {
                        valid = kNotInterested;
                        break;
                    }
                    if(_attr_ranges[a].contain_half_closed(data2[goffset]) == false) {
                        valid = kNotInterested;
                        break;
                    }
                }
                plane[offset] = valid;
            }
        }
    }

private:
    std::array<Range<T>, num_attrs> _attr_ranges;
};


template<typename ParentOp, typename T, size_t num_coordinates, size_t num_dims, int num_attrs>
class PlainFilterOIROperator3D {
public:
    using Function = PlainFilterOIRProcessor3D<T, num_coordinates, num_dims, num_attrs>;
    using input_slice_type = typename Function::input_slice_type;
    using slice_type = input_slice_type;
    using WindowOp = WindowOperatorBase<ParentOp, input_slice_type, Function, 2>;
public:
    PlainFilterOIROperator3D(ParentOp* op, int processing_threads, int total_buffers, std::array<int64_t, num_dims> dim_mins, std::array<int64_t, num_dims> dim_maxs, std::array<int64_t, num_dims> dim_sizes, std::array<Range<T>, num_attrs> attr_ranges) {
        std::vector<Function> functions;
        for(auto i = 0; i < processing_threads; ++i) {
            functions.emplace_back(dim_mins, dim_maxs, dim_sizes, attr_ranges);
        }
        _op = std::make_unique<WindowOp>(op, std::move(functions), total_buffers);
    }

    void consume(input_slice_type&& slice) {
        // log(OTTERS_LOG_DEBUG, "got slice position = {}", slice.position());
        _op->consume(std::move(slice));
    }

    void end_input() {
        _op->end_input();
    }

    void drive(int i) {
        _op->drive(i);
    }

private:
    std::unique_ptr<WindowOp> _op;
};

}

#endif // OTTERS_INCLUDE_OTTERS_CUDA_QUERY_OPS_PLAIN_FILTER_OIR_H
