#ifndef OTTERS_CUDA_QUERY_OPS_GPU_BITMAP_INPUT_CUH
#define OTTERS_CUDA_QUERY_OPS_GPU_BITMAP_INPUT_CUH

#include <array>
#include <otters/common/utils/profile.h>
#include <otters/cuda/bitmap/roaring.cuh>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/exception/cuda_assert.h>
#include <otters/cuda/query/timestep.cuh>

namespace otters {

template<typename ParentOp, int dimension, typename T, int num_attributes, typename GPUBitmap, typename GPUBinner, typename BufferPool>
class GPUBitmapInputOperator {
public:
    using GPUSlice = TimeSlice<dimension, MultiAttributeBuffer<GPUBitmap, num_attributes>>;
public:
    GPUBitmapInputOperator(std::array<GPUBinner, num_attributes>&& binner, ParentOp* parent_op, BufferPool* pool, std::string op_name = "bitmap_input")
        : _timer(op_name + "_bitmap_gen_timer"), _binner(std::move(binner)), _parent_op(parent_op), _pool(pool) {
            OTTERS_CUDA_ERRORCHECK( cudaStreamCreate(&_process_stream) );
        }

    ~GPUBitmapInputOperator() {
        OTTERS_CUDA_ERRORCHECK( cudaStreamDestroy(_process_stream) );
    }

    GPUBitmapInputOperator(const GPUBitmapInputOperator&) = delete;

    GPUBitmapInputOperator& operator=(const GPUBitmapInputOperator&) = delete;

    void add_input(Coordinates<dimension> timestep_id, std::array<T*, num_attributes> data, std::array<ssize_t, num_attributes> sizes) {
        _timer.start();
        GPUSlice slice(timestep_id, _pool);
        for (auto i = 0; i < num_attributes; ++i) {
            _bin_indices[i].resize(sizes[i]);
            _binner[i].generate_bins(_bin_indices[i].data(), data[i], sizes[i], _process_stream, slice.timestep_id());
        }
        // log(OTTERS_LOG_DEBUG, "Finished binning generation.");
        OTTERS_CUDA_ERRORCHECK(cudaStreamSynchronize(_process_stream));
        for (auto i = 0; i < num_attributes; ++i) {
            slice.data().attribute(i).construct_bitmap(_bin_indices[i].data(), (unsigned) sizes[i], true, _process_stream);
        }
        _timer.stop();
        // should be unnecessary because construct bitmap synchronize the stream anyway.
        // OTTERS_CUDA_ERRORCHECK(cudaStreamSynchronize(_process_stream));
        _parent_op->consume(std::move(slice));
    }

    void end_input() {
        GPUSlice slice;
        _parent_op->consume(std::move(slice));
    }
private:
    otters::Timer _timer;
    std::array<GPUBinner, num_attributes> _binner;
    std::array<cuda_device_buffer<int>, num_attributes> _bin_indices;
    ParentOp* _parent_op;
    BufferPool* _pool;
    cudaStream_t _process_stream;
};

}

#endif
