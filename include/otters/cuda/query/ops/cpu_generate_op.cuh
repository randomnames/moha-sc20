#ifndef OTTERS_CUDA_QUERY_OPS_CPU_GENERATE_OP_CUH
#define OTTERS_CUDA_QUERY_OPS_CPU_GENERATE_OP_CUH

#include <otters/common/containers/buffer.h>
#include <otters/common/containers/buffer_pool.h>
#include <otters/common/containers/time_slice.h>
#include <otters/common/utils/log.h>

namespace otters {

template<typename ParentOp, int dimension, typename Buffer, typename CoordinateFunction, typename BufferFunction, typename BufferPool>
class CPUGenerateOperator {
public:
    using slice_type = TimeSlice<dimension, Buffer>;
public:
    CPUGenerateOperator(ParentOp* parent_op,
            int total_timesteps,
            CoordinateFunction coord_func,
            BufferFunction buffer_func,
            BufferPool* pool)
        : _parent_op(parent_op),
          _total_steps(total_timesteps),
          _coord_func(coord_func),
          _buffer_func(buffer_func),
          _pool(pool) {
    }
    void drive() {
        for(auto i = 0; i < _total_steps; ++i) {
            auto coord = _coord_func(i);
            slice_type slice(coord, _pool);
            _buffer_func(slice.data(), coord);
            _parent_op->consume(std::move(slice));
            // log(OTTERS_LOG_DEBUG, "sent step {}", i);
        }
        slice_type slice;
        _parent_op->consume(std::move(slice));
    }
private:
    ParentOp* _parent_op;
    int _total_steps;
    CoordinateFunction _coord_func;
    BufferFunction _buffer_func;
    BufferPool* _pool;
};

}

#endif
