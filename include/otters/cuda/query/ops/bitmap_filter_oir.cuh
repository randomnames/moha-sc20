#ifndef OTTERS_INCLUDE_CUDA_QUERY_OPS_BITMAP_FILTER_OIR_CUH
#define OTTERS_INCLUDE_CUDA_QUERY_OPS_BITMAP_FILTER_OIR_CUH

#include <otters/common/containers/buffer.h>
#include <otters/common/containers/time_slice.h>
#include <otters/common/utils/log.h>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/query/subgroup_discovery_support.cuh>
#include <otters/common/bitmap/roaring_filter_helper.h>

#include <otters/common/bitmap/bit_manipulation.h>
#include <otters/cuda/query/ops/filter_oir_processor_3_d_base.cuh>
#include <otters/cuda/query/ops/window.cuh>
#include <vector>

namespace otters {


template<typename Bitmap, typename T, typename CPUBinner, size_t num_coordinates, size_t num_dims, size_t num_attrs>
class BitmapFilterOIRProcessor3D : public FilterOIRProcessor3DBase {
public:
    using input_slice_type = TimeSlice<num_coordinates, MultiAttributeBuffer<Bitmap, num_attrs>>;

public:
    BitmapFilterOIRProcessor3D(std::array<CPUBinner, num_attrs> binners, std::array<int64_t, num_dims> dim_mins, std::array<int64_t, num_dims> dim_maxs,
            std::array<int64_t, num_dims> dim_sizes, std::array<Range<T>, num_attrs> attr_ranges, int attribute_buckets)
        : FilterOIRProcessor3DBase(dim_mins, dim_maxs, dim_sizes), _attr_ranges(attr_ranges), _binners(binners), _attribute_buckets(attribute_buckets) {
    }

    void process(input_slice_type& slice1, input_slice_type& slice2) {
        auto num_planes = _dim_sizes[0];
        std::array<typename Bitmap::OrIterator, num_attrs * 2> attr_iters;
        auto timestep_id = slice1.timestep_id();
        _current_chunk_pos = 65536;

        for(auto i = 0u; i < num_attrs; ++i) {
            auto min_value = _attr_ranges[i].min();
            auto max_value = _attr_ranges[i].max();
            auto min_bucket = bucket_id(timestep_id, i, min_value);
            auto max_bucket = bucket_id(timestep_id, i, max_value) + 1;
            attr_iters[i] = slice1.data().attribute(i).begin_or_iterator(min_bucket, max_bucket);
            attr_iters[i + num_attrs] = slice2.data().attribute(i).begin_or_iterator(min_bucket, max_bucket);
        }

        for(auto i = 0; i < num_planes; ++i) {
            //produce_new_plane
            //            log(OTTERS_LOG_DEBUG, "filter_plane: {}/{}", i + 1, num_planes);
            filter_next_plane(_planes[i % 2], attr_iters);
            //do a flood fill
            //            log(OTTERS_LOG_DEBUG, "flood_fill: {}/{}", i + 1, num_planes);
            flood_fill(i % 2);
        }
    }

    void operator()(input_slice_type& slice1, input_slice_type& slice2) {
        process(slice1, slice2);
    }

    void filter_next_plane(std::vector<int>& plane, std::array<typename Bitmap::OrIterator, num_attrs * 2>& attr_iters) {
        int64_t total_bits = _dim_sizes[1] * _dim_sizes[2];
        int64_t current_bit = 0;
        int64_t constexpr bits_in_chunk = 65536;
        while(current_bit < total_bits) {
            if(_current_chunk_pos == bits_in_chunk) {
                filter_chunk(_chunk_storage, attr_iters);
//                log(OTTERS_LOG_DEBUG, "{:x} {:x} {:x} {:x}", _chunk_storage[0], _chunk_storage[1], _chunk_storage[2], _chunk_storage[3]);
                _current_chunk_pos = 0;
            }
            auto min_bits = std::min<int64_t>(total_bits - current_bit, bits_in_chunk - _current_chunk_pos);
//            log(OTTERS_LOG_DEBUG, "current_bit = {} current_pos = {} min_bits = {}",
//                    current_bit, _current_chunk_pos, min_bits);
            for(int64_t i = 0; i < min_bits; ++i) {
                bool is_valid = retrieve_bit_from_bitvector(_chunk_storage, (int)(_current_chunk_pos + i));
                plane[current_bit + i] = (is_valid) ? kNotVisited : kNotInterested;
            }
            current_bit += min_bits;
            _current_chunk_pos += min_bits;
        }
    }

    int bucket_id(int timestep_id, int attr_id, T value) {
        auto min = _binners[attr_id].min(timestep_id);
        auto max = _binners[attr_id].max(timestep_id);
        auto bin_id = (int)((value - min) / (value - max) * _attribute_buckets);
        if(bin_id < 0)
            bin_id = 0;
        if(bin_id >= _attribute_buckets - 1)
            bin_id = _attribute_buckets - 1;
        return bin_id;
    }

private:
    uint64_t _chunk_storage[1024];
    int64_t _current_chunk_pos;
    std::array<Range<double>, num_attrs> _attr_ranges;
    std::array<CPUBinner, num_attrs> _binners;
    int _attribute_buckets;
};

template<typename ParentOp, typename Bitmap, typename T, typename CPUBinner, size_t num_coordinates, size_t num_dims, int num_attrs>
class BitmapFilterOIROperator3D {
public:
    using Function = BitmapFilterOIRProcessor3D<Bitmap, T, CPUBinner, num_coordinates, num_dims, num_attrs>;
    using input_slice_type = typename Function::input_slice_type;
    using slice_type = input_slice_type;
    using WindowOp = WindowOperatorBase<ParentOp, input_slice_type, Function, 2>;
public:
    BitmapFilterOIROperator3D(ParentOp* op, int processing_threads, int total_buffers, std::array<CPUBinner, num_attrs> binners, std::array<int64_t, num_dims> dim_mins, std::array<int64_t, num_dims> dim_maxs,
            std::array<int64_t, num_dims> dim_sizes, std::array<Range<T>, num_attrs> attr_ranges, int attribute_buckets) {
        std::vector<Function> functions;
        for(auto i = 0; i < processing_threads; ++i) {
            functions.emplace_back(binners, dim_mins, dim_maxs, dim_sizes, attr_ranges, attribute_buckets);
        }
        _op = std::make_unique<WindowOp>(op, std::move(functions), total_buffers);
    }
    void consume(input_slice_type&& slice) {
        _op->consume(std::move(slice));
    }

    void end_input() {
        _op->end_input();
    }

    void drive(int i) {
        _op->drive(i);
    }

private:
    std::unique_ptr<WindowOp> _op;
};

}

#endif // OTTERS_TEST_CUDA_QUERY_OPS_BITMAP_FILTER_OIR_CUH
