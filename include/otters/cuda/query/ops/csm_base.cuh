//
// Created by Haoyuan Xing on 4/16/20.
//

#ifndef OTTERS_INCLUDE_OTTERS_CUDA_QUERY_OPS_CSM_BASE_CUH
#define OTTERS_INCLUDE_OTTERS_CUDA_QUERY_OPS_CSM_BASE_CUH

#include "csm_support.cuh"
#include <otters/common/containers/time_slice.h>
#include <otters/cuda/query/ops/bitmap_subgroup_discovery_support.cuh>
#include <otters/cuda/query/plain_subgroup_discovery_support.cuh>
#include <otters/common/utils/profile.h>
#include <queue>
#include <vector>

namespace otters {

template<typename ParentOp, typename Slice, typename CPUBinner, typename T, int num_dims, int num_attr, int num_best_results = 10>
class CSMOperator {
public:
    using input_slice_type = PairSlice<Slice>;
    using slice_type = PairSlice<Slice>;
    constexpr const static int num_coord_dims = Slice::num_coord_dims;
    static const int max_search_size = 1024;
    using condition_type = SubsetCondition<T, num_attr, num_dims>;
    using output_slice_type = TimeSlice<num_coord_dims, BestContrastSets<T, num_attr, num_dims, num_best_results>>;

public:
    CSMOperator(ParentOp* op,
            std::array<int64_t, num_dims> dimension_sizes,
            std::array<CPUBinner, num_attr> cpu_binners,
            int dimension_buckets,
            int attribute_buckets,
            std::string op_name = "csm")
        : _csm_timer(op_name + "_total"),
          _parent_op(op),
          _dimension_sizes(dimension_sizes),
          _binners(std::move(cpu_binners)),
          _dimension_buckets(dimension_buckets),
          _attribute_buckets(attribute_buckets) {
    }

    void consume(input_slice_type&& slice) {
        _csm_timer.start();
        std::queue<condition_type> q;
        std::vector<condition_type> visited;

        int total_searched = 0;
        output_slice_type output_slice(slice.position());
        q.push(condition_type());
        auto timestep_id = slice.timestep_id();

//        log(OTTERS_LOG_DEBUG, "Slice timestep = {}", slice.position());

        while (total_searched < max_search_size && !q.empty()) {
            auto condition = q.front();
            q.pop();
            auto left_result = filter_slice(timestep_id, slice.first().data(), _dimension_sizes, _binners, condition, num_attr - 1);
            auto right_result = filter_slice(timestep_id, slice.second().data(), _dimension_sizes, _binners, condition, num_attr - 1);
            ContrastSet<T, num_attr, num_dims> result(condition, left_result, right_result);
            // auto quality = result.quality();
            output_slice.data().insert(std::move(result));

            ++total_searched;
            for (auto i = 0; i < num_dims; ++i) {
                if (condition.dim_range(i).enabled() &&
                        condition.dim_range(i).span() * _dimension_buckets < _dimension_sizes[i]) {
                    continue;
                }
                auto new_condition = split_condition_dimension(condition, i);
                auto it = std::find(visited.begin(), visited.end(), new_condition.first);
                if (it == visited.end())
                    q.push(new_condition.first);
                it = std::find(visited.begin(), visited.end(), new_condition.second);
                if (it == visited.end())
                    q.push(new_condition.second);
            }
            for (auto i = 0; i < num_attr - 1; ++i) {
                if (condition.attr_range(i).enabled() &&
                        condition.attr_range(i).span() * _attribute_buckets <
                                _binners[i].max(timestep_id) - _binners[i].min(timestep_id)) {
                    continue;
                }
                auto new_condition = split_condition_attribute(timestep_id, condition, i);
                auto it = std::find(visited.begin(), visited.end(), new_condition.first);
                if (it == visited.end())
                    q.push(new_condition.first);
                it = std::find(visited.begin(), visited.end(), new_condition.second);
                if (it == visited.end())
                    q.push(new_condition.second);
            }
        }
        _csm_timer.stop();
//        log(OTTERS_LOG_DEBUG, "Finished timestep = {}", slice.position());
        _parent_op->consume(std::move(output_slice));

    }

    double compute_quality(MeanAggregator<T>& left, MeanAggregator<T>& right) {
        return (1.0 + fabs(left.mean() - right.mean())) * (0.0 + abs(left.size() - right.size()));
    }

    std::pair<condition_type, condition_type> split_condition_dimension(condition_type& old_condition,
            int dimension_id) {
        auto new_conditions = std::make_pair(old_condition, old_condition);
        if (old_condition.dim_range(dimension_id).enabled() == false) {
            new_conditions.first.dim_range(dimension_id).assign(0, _dimension_sizes[dimension_id] / 2);
            new_conditions.second.dim_range(dimension_id)
                    .assign(_dimension_sizes[dimension_id] / 2, _dimension_sizes[dimension_id]);
        } else {
            auto min = old_condition.dim_range(dimension_id).min();
            auto max = old_condition.dim_range(dimension_id).max();
            auto mid = min + (max - min) / 2;
            new_conditions.first.dim_range(dimension_id).assign(min, mid);
            new_conditions.second.dim_range(dimension_id).assign(mid, max);
        }
        return new_conditions;
    }

    std::pair<condition_type, condition_type> split_condition_attribute(int timestep_id,
            condition_type& old_condition,
            int attribute_id) {
        auto new_conditions = std::make_pair(old_condition, old_condition);
        if (old_condition.attr_range(attribute_id).enabled() == false) {
            auto min = _binners[attribute_id].min(timestep_id);
            auto max = _binners[attribute_id].max(timestep_id);
            auto mid = min + (max - min) / 2;
            new_conditions.first.attr_range(attribute_id).assign(min, mid);
            new_conditions.second.attr_range(attribute_id).assign(mid, max);
        } else {
            auto min = old_condition.attr_range(attribute_id).min();
            auto max = old_condition.attr_range(attribute_id).max();
            auto mid = min + (max - min) / 2;
            new_conditions.first.attr_range(attribute_id).assign(min, mid);
            new_conditions.second.attr_range(attribute_id).assign(mid, max);
        }
        return new_conditions;
    }

private:
    Timer _csm_timer;
    ParentOp* _parent_op;
    std::array<int64_t, num_dims> _dimension_sizes;
    std::array<CPUBinner, num_attr> _binners;
    int _dimension_buckets;
    int _attribute_buckets;
};

} // namespace otters

#endif // OTTERS_INCLUDE_OTTERS_CUDA_QUERY_OPS_CSM_BASE_CUH
