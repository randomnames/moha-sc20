#ifndef INCLUDE_OTTERS_CUDA_QUERY_OPS_GPU_BITMAP_BUILD_CUH
#define INCLUDE_OTTERS_CUDA_QUERY_OPS_GPU_BITMAP_BUILD_CUH

#include <otters/cuda/exception/cuda_assert.h>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/bitmap/roaring.cuh>
#include <otters/cuda/query/timestep.cuh>

namespace otters {

template<typename ParentOp, int dimension, typename T, int num_attributes, typename GPUBitmap, typename GPUBinner, typename BufferPool>
class GPUBitmapBuildOperator {
public:
    using GPUSlice = TimeSlice<dimension, MultiAttributeBuffer<GPUBitmap, num_attributes>>;
    using input_slice_type = TimeSlice<dimension, MultiAttributeBuffer<cuda_device_buffer<T>, num_attributes>>;
public:
    GPUBitmapBuildOperator(std::array<GPUBinner, num_attributes>&& binner, ParentOp* parent_op, BufferPool* pool, std::string op_name = "gpu_bitmap_build")
        : _bitmap_build_timer(op_name + "_bitmap_build_timer"), _binner(std::move(binner)), _parent_op(parent_op), _pool(pool) {
            OTTERS_CUDA_ERRORCHECK( cudaStreamCreate(&_process_stream) );
        }

    ~GPUBitmapBuildOperator() {
        OTTERS_CUDA_ERRORCHECK( cudaStreamDestroy(_process_stream) );
    }

    GPUBitmapBuildOperator(const GPUBitmapBuildOperator&) = delete;

    GPUBitmapBuildOperator& operator=(const GPUBitmapBuildOperator&) = delete;

    void consume(input_slice_type&& input_slice) {
        _bitmap_build_timer.start();
        GPUSlice slice(input_slice.position(), _pool);
        if((bool) input_slice == false) {
            end_input();
            return;
        }
        for (auto i = 0; i < num_attributes; ++i) {
            auto data = input_slice.data().attribute(i).data();
            auto size = input_slice.data().attribute(i).size();
            _bin_indices[i].resize(size);
            _binner[i].generate_bins(_bin_indices[i].data(), data, size, _process_stream, slice.timestep_id());
        }
        // log(OTTERS_LOG_DEBUG, "Finished binning generation.");
        OTTERS_CUDA_ERRORCHECK(cudaStreamSynchronize(_process_stream));
        for (auto i = 0; i < num_attributes; ++i) {
            auto size = input_slice.data().attribute(i).size();
            slice.data().attribute(i).construct_bitmap(_bin_indices[i].data(), (unsigned) size, true, _process_stream);
        }
        _bitmap_build_timer.stop();
        // should be unnecessary because construct bitmap synchronize the stream anyway.
        // OTTERS_CUDA_ERRORCHECK(cudaStreamSynchronize(_process_stream));
        _parent_op->consume(std::move(slice));
    }

    void end_input() {
        GPUSlice slice;
        _parent_op->consume(std::move(slice));
    }
private:
    Timer _bitmap_build_timer;
    std::array<GPUBinner, num_attributes> _binner;
    std::array<cuda_device_buffer<int>, num_attributes> _bin_indices;
    ParentOp* _parent_op;
    BufferPool* _pool;
    cudaStream_t _process_stream;
};

}

#endif
