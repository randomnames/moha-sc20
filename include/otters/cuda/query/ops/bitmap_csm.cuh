#ifndef OTTERS_CUDA_OPS_BITMAP_CSM_CUH
#define OTTERS_CUDA_OPS_BITMAP_CSM_CUH

#include "csm_support.cuh"
#include <otters/common/bitmap/roaring_bitmap.h>
#include <otters/common/bitmap/roaring_filter_helper.h>
#include <otters/common/utils/log.h>
#include <otters/cuda/query/ops/csm_base.cuh>
#include <otters/cuda/query/ops/merge_timestep.cuh>
#include <queue>
#include <type_traits>
#include <vector>

namespace otters {

template<typename ParentOp, typename Slice, typename CPUBinner, typename T, int num_attr, int num_best_results = 10>
class BitmapCSMOperator3 {
public:
    using input_slice_type = PairSlice<Slice>;
    using slice_type = PairSlice<Slice>;
    constexpr const static int num_coord_dims = Slice::num_coord_dims;
    static const int max_search_size = 1024;
    using condition_type = SubsetCondition<T, num_attr, 3>;
    using output_slice_type = TimeSlice<num_coord_dims, BestContrastSets<T, num_attr, 3, num_best_results>>;

public:
    BitmapCSMOperator3(
        ParentOp* op,
        std::array<int64_t, 3> dimension_sizes,
        std::array<CPUBinner, num_attr> binners,
        int dimension_buckets,
        int attribute_buckets)
        : _parent_op(op),
          _dimension_sizes(dimension_sizes),
          _binners(binners),
          _dimension_buckets(dimension_buckets),
          _attribute_buckets(attribute_buckets) {
    }

    void consume(input_slice_type&& slice) {
        std::queue<condition_type> q;
        std::vector<condition_type> visited;

        int total_searched = 0;
        output_slice_type output_slice(slice.position());
        q.push(condition_type());
        auto timestep_id = slice.timestep_id();

        while(total_searched < max_search_size && !q.empty()) {
            auto condition = q.front(); q.pop();
            // log(OTTERS_LOG_DEBUG, "condition = {}", condition.format());
            auto left_result = filter_bitmap(slice.first(), condition, num_attr - 1);
            auto right_result = filter_bitmap(slice.second(), condition, num_attr - 1);
            ContrastSet<T, num_attr, 3> result(condition, left_result, right_result);
            // auto quality = result.quality();
            output_slice.data().insert(std::move(result));

            ++total_searched;
            for(auto i = 0; i < 3; ++i) {
                if (condition.dim_range(i).enabled() && condition.dim_range(i).span() * _dimension_buckets < _dimension_sizes[i]) {
                    continue;
                }
                auto new_condition = split_condition_dimension(condition, i);
                auto it = std::find(visited.begin(), visited.end(), new_condition.first);
                if(it == visited.end())
                    q.push(new_condition.first);
                it = std::find(visited.begin(), visited.end(), new_condition.second);
                if(it == visited.end())
                    q.push(new_condition.second);
            }
            for (auto i = 0; i < num_attr - 1; ++i) {
                if (condition.attr_range(i).enabled() &&
                        condition.attr_range(i).span() * _attribute_buckets <
                                _binners[i].max(timestep_id) - _binners[i].min(timestep_id)) {
                    continue;
                }
                auto new_condition = split_condition_attribute(timestep_id, condition, i);
                auto it = std::find(visited.begin(), visited.end(), new_condition.first);
                if(it == visited.end())
                    q.push(new_condition.first);
                it = std::find(visited.begin(), visited.end(), new_condition.second);
                if(it == visited.end())
                    q.push(new_condition.second);
            }
        }
        _parent_op->consume(std::move(output_slice));
    }

    MeanAggregator2<T> filter_bitmap(Slice& slice, condition_type& condition, int target_attr) {
        auto timestep_id = slice.timestep_id();
        assert(target_attr == num_attr - 1);
        using Bitmap = typename std::decay<decltype(slice.data().attribute(0))>::type;
        using Iter = typename Bitmap::OrIterator;
        std::array<int64_t, 3> dim_mins;
        std::array<int64_t, 3> dim_maxs;
        for (auto i = 0u; i < 3; ++i) {
            auto range = condition.dim_range(i);
            if (range.enabled()) {
                dim_mins[i] = range.min();
                dim_maxs[i] = range.max();
            } else {
                dim_mins[i] = 0;
                dim_maxs[i] = _dimension_sizes[i];
            }
        }
        DimensionRanges<DefaultRoaringTraits, 3> dim_ranges(dim_mins, dim_maxs, _dimension_sizes);
        std::array<Iter, num_attr - 1> attr_iters;
        for(auto i = 0; i < num_attr - 1; ++i) {
            auto min_value = condition.attr_range(i).min();
            auto min_bucket = bucket_id(timestep_id, i, min_value);
            auto max_value = condition.attr_range(i).max();
            auto max_bucket = bucket_id(timestep_id, i, max_value) + 1;
            attr_iters[i] = slice.data().attribute(i).begin_or_iterator(min_bucket, max_bucket);
        }
        auto result = roaring_compute_mean_support(dim_ranges, attr_iters, slice.data().attribute(target_attr), _binners[target_attr], timestep_id);
        return result;
    }
private:
    int bucket_id(int timestep_id, int attr_id, T value) {
        auto min = _binners[attr_id].min(timestep_id);
        auto max = _binners[attr_id].max(timestep_id);
        auto bin_id = (int)((value - min) / (value - max) * _attribute_buckets);
        if(bin_id < 0)
            bin_id = 0;
        if(bin_id >= _attribute_buckets - 1)
            bin_id = _attribute_buckets - 1;
        return bin_id;
    }
    double compute_quality(MeanAggregator2<T>& left, MeanAggregator2<T>& right) {
        return (1.0 + fabs(left.mean() - right.mean())) * (0.0 + abs(left.size() - right.size()));
    }
    /* FIXME: code duplicate */
    std::pair<condition_type, condition_type> split_condition_dimension(condition_type& old_condition,
            int dimension_id) {
        auto new_conditions = std::make_pair(old_condition, old_condition);
        if(old_condition.dim_range(dimension_id).enabled() == false) {
            new_conditions.first.dim_range(dimension_id).assign(0, _dimension_sizes[dimension_id] / 2);
            new_conditions.second.dim_range(dimension_id)
                    .assign(_dimension_sizes[dimension_id] / 2, _dimension_sizes[dimension_id]);
        } else {
            auto min = old_condition.dim_range(dimension_id).min();
            auto max = old_condition.dim_range(dimension_id).max();
            auto mid = min + (max - min) / 2;
            new_conditions.first.dim_range(dimension_id).assign(min, mid);
            new_conditions.second.dim_range(dimension_id).assign(mid, max);
        }
        return new_conditions;
    }

    std::pair<condition_type, condition_type> split_condition_attribute(int timestep_id,condition_type& old_condition,
            int attribute_id) {
        auto new_conditions = std::make_pair(old_condition, old_condition);
        if(old_condition.attr_range(attribute_id).enabled() == false) {
            auto min = _binners[attribute_id].min(timestep_id);
            auto max = _binners[attribute_id].max(timestep_id);
            auto mid = min + (max - min) / 2;
            new_conditions.first.attr_range(attribute_id).assign(min, mid);
            new_conditions.second.attr_range(attribute_id).assign(mid, max);
        } else {
            auto min = old_condition.attr_range(attribute_id).min();
            auto max = old_condition.attr_range(attribute_id).max();
            auto mid = min + (max - min) / 2;
            new_conditions.first.attr_range(attribute_id).assign(min, mid);
            new_conditions.second.attr_range(attribute_id).assign(mid, max);
        }
        return new_conditions;
    }

private:
    ParentOp* _parent_op;
    std::array<int64_t, 3> _dimension_sizes;
    std::array<CPUBinner, num_attr> _binners;
    int _dimension_buckets;
    int _attribute_buckets;
};

}


#endif
