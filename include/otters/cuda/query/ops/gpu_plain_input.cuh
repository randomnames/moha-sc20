#ifndef OTTERS_CUDA_QUERY_OPS_GPU_PLAIN_INPUT_CUH
#define OTTERS_CUDA_QUERY_OPS_GPU_PLAIN_INPUT_CUH

#include <otters/cuda/exception/cuda_assert.h>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/query/timestep.cuh>

#include <array>

namespace otters {

template<typename ParentOp, int dimension, typename T, int num_attributes, typename BufferPool>
class GPUPlainInputOperator {
public:
    using GPUSlice = TimeSlice<dimension, MultiAttributeBuffer<cuda_device_buffer<T>, num_attributes>>;
public:
    GPUPlainInputOperator(ParentOp* parent_op, BufferPool* pool)
        : _parent_op(parent_op), _pool(pool) {
            OTTERS_CUDA_ERRORCHECK( cudaStreamCreate(&_copy_stream) );
        }
    ~GPUPlainInputOperator() {
        OTTERS_CUDA_ERRORCHECK( cudaStreamDestroy(_copy_stream) );
    }
    GPUPlainInputOperator(const GPUPlainInputOperator&) = delete;
    GPUPlainInputOperator& operator=(const GPUPlainInputOperator&) = delete;
    void add_input(Coordinates<dimension> timestep_id, std::array<T*, num_attributes> data, std::array<ssize_t, num_attributes> sizes) {
        GPUSlice slice(timestep_id, _pool);

        for (size_t i = 0; i < num_attributes; ++i) {
            auto& attribute = slice.data().attribute(i);
            attribute.resize(sizes[i]);
            cudaMemcpyAsync(attribute.data(), data[i], sizes[i] * sizeof(T), cudaMemcpyDeviceToDevice, _copy_stream);
        }

        OTTERS_CUDA_ERRORCHECK( cudaStreamSynchronize(_copy_stream));
        _parent_op->consume(std::move(slice));
    }

    void end_input() {
        GPUSlice slice;
        _parent_op->consume(std::move(slice));
    }
private:
    ParentOp* _parent_op;
    BufferPool* _pool;
    cudaStream_t _copy_stream;
};

}

#endif
