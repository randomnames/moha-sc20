#ifndef OTTERS_CUDA_QUERY_PLAIN_AGGREGATE_HELPER
#define OTTERS_CUDA_QUERY_PLAIN_AGGREGATE_HELPER

#include <stdint.h>

namespace otters {

template<typename T>
struct MeanAggregator {
public:
    MeanAggregator() {
        _sum = (T) 0;
        _size = 0;
    }
    void update(T t) {
        _sum += t;
        _size += 1;
    }
    T sum() {
        return _sum;
    }
    ssize_t size() {
        return _size;
    }
    T mean() {
        return sum() / (T) size();
    }
public:
    T _sum;
    ssize_t _size;
};

}

#endif
