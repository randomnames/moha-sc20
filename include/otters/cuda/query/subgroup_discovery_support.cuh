#ifndef OTTERS_CUDA_QUERY_SUBGROUP_DISCOVERY_SUPPORT_CUH
#define OTTERS_CUDA_QUERY_SUBGROUP_DISCOVERY_SUPPORT_CUH

#include <array>
#include <vector>

#include <stdint.h>
#include <otters/cuda/query/ops/csm_support.cuh>

namespace otters {


template<typename T, int num_attrs, int num_dims>
struct SubsetRanges {
    std::array<std::vector<Range<int64_t>>, num_dims> _dim_ranges;
    std::array<std::vector<Range<T>>, num_attrs> _attr_ranges;
};

template<typename T, int num_attrs, int num_dims>
struct SubsetConditionIter {
    std::array<int64_t, num_dims> _dim_ranges;
    std::array<int64_t, num_attrs> _attr_ranges;
    int _last_attr;
    SubsetRanges<T, num_attrs, num_dims>& _range;

    SubsetConditionIter(SubsetRanges<T, num_attrs, num_dims>& range) : _last_attr(-1), _range(range) {
        std::fill(_dim_ranges.begin(), _dim_ranges.end(), -1);
        std::fill(_attr_ranges.begin(), _attr_ranges.end(), -1);
    }
};


}

#endif
