#ifndef OTTERS_CUDA_QUERY_PLAIN_FILTER_HELPER_CUH
#define OTTERS_CUDA_QUERY_PLAIN_FILTER_HELPER_CUH

#include <array>
#include <type_traits>

namespace otters {

// template<typename T, size_t num_attr, size_t num_dims, typename Func>
// void filter_plain_array(std::array<T*, num_attr> attrs, std::array<int64_t, num_dims> dim_size, Func&& func);

/* TODO: this should obviously be varadic-templated */
template<typename T, size_t num_attr, size_t num_dims, typename Func>
typename std::enable_if<num_dims == 3, void>::type
filter_plain_array(std::array<T*, num_attr> attrs,
        std::array<int64_t, 3> dim_mins,
        std::array<int64_t, 3> dim_maxs,
        std::array<int64_t, 3> dim_sizes,
        Func&& func) {
    for (auto i = dim_mins[0]; i < dim_maxs[0]; ++i)
        for (auto j = dim_mins[1]; j < dim_maxs[1]; ++j)
            for (auto k = dim_mins[2]; k < dim_maxs[2]; ++k) {
                std::array<int64_t, 3> dims{ i, j, k };
                auto offset = (i * dim_sizes[1] + j) * dim_sizes[2] + k;
                std::array<T, num_attr> attr_vals;
                for (auto l = 0u; l < num_attr; ++l) {
                    attr_vals[l] = attrs[l][offset];
                }
                func(dims, attr_vals);
            }
}

template<typename T, size_t num_attr, size_t num_dims, typename Func>
typename std::enable_if<num_dims == 2, void>::type
filter_plain_array(std::array<T*, num_attr> attrs,
        std::array<int64_t, 2> dim_mins,
        std::array<int64_t, 2> dim_maxs,
        std::array<int64_t, 2> dim_sizes,
        Func&& func) {
    for (auto i = dim_mins[0]; i < dim_maxs[0]; ++i)
        for (auto j = dim_mins[1]; j < dim_maxs[1]; ++j) {
            std::array<int64_t, 2> dims{ i, j};
            auto offset = i * dim_sizes[1] + j;
            std::array<T, num_attr> attr_vals;
            for (auto l = 0u; l < num_attr; ++l) {
                attr_vals[l] = attrs[l][offset];
            }
            func(dims, attr_vals);
        }
}

template<typename T, size_t num_attr, size_t num_dims, typename Func>
typename std::enable_if<num_dims == 1, void>::type
filter_plain_array(std::array<T*, num_attr> attrs,
        std::array<int64_t, 1> dim_mins,
        std::array<int64_t, 1> dim_maxs,
        std::array<int64_t, 1> dim_sizes,
        Func&& func) {
    for (auto i = dim_mins[0]; i < dim_maxs[0]; ++i) {
            std::array<int64_t, 2> dims{ i };
            auto offset = i * dim_sizes[1];
            std::array<T, num_attr> attr_vals;
            for (auto l = 0u; l < num_attr; ++l) {
                attr_vals[l] = attrs[l][offset];
            }
            func(dims, attr_vals);
    }
}

} // namespace otters

#endif
