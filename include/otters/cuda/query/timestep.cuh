#ifndef OTTERS_CUDA_QUERY_BUFFERS_CUH
#define OTTERS_CUDA_QUERY_BUFFERS_CUH

#include <otters/common/utils/void_t.h>
#include <otters/common/containers/coordinates.h>
#include <otters/common/containers/time_slice.h>
#include <otters/common/io/serialization.h>
#include <otters/common/containers/span.h>
#include <otters/common/utils/log.h>
#include <otters/cuda/container/buffer_pool.cuh>
#include <type_traits>

#include <fmt/core.h>

namespace otters {


template<typename Container>
struct TimeStep {
    using buffer_type = Container;

    TimeStep()
        : _data(), _timestep_id(-1) {
    }

    TimeStep(int timestep_id) : _timestep_id(timestep_id) { }

    auto& data() { return _data; }
    const auto& data() const { return _data; }

    //auto& operator[](int i) { return buffer()[i]; }
    //const auto& operator[](int i) const { return buffer()[i]; }

    auto timestep_id() const { return _timestep_id; }
    void timestep_id(int timestep_id) { _timestep_id = timestep_id; }

    void clear() {
        // _data.clear();
    }

    //auto size() const { return _data.size(); }
    //auto serialize_size() const { return _data.serialize_size(); }
    //void resize(serialize_size_type size) {
    //    _data.resize(size);
    //}

    operator bool() {
        return _timestep_id == -1;
    }

    Container _data;
    int _timestep_id;
};



//template<typename T, typename Enable = void_t<>>
//struct TimeStep : TimeStepBase<T, ssize_t> {
//    using Base = TimeStepBase<T, ssize_t>;
//    using Base::Base;
//};
//
//template<typename T>
//struct TimeStep<T, void_t<typename T::serialize_size_type>> : TimeStepBase<T, typename T::serialize_size_type> {
//    using Base = TimeStepBase<T, typename T::serialize_size_type>;
//    using Base::Base;
//};

/*
template<template<typename> typename Container>
struct TimeStepBufferT {
    template<typename T>
    using type = TimeStepBuffer<T, Container>
};
*/

template<typename Container>
void write(std::ostream& os, const TimeStep<Container>& buffer) {
    write(os, buffer.data());
}

template<typename Container>
void read(TimeStep<Container>& buffer, std::istream& is) {
    read(buffer.data(), is);
}

}

#endif
