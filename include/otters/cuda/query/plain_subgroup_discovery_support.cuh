#ifndef OTTERS_CUDA_QUERY_PLAIN_SUBGROUP_DISCOVERY_SUPPORT_CUH
#define OTTERS_CUDA_QUERY_PLAIN_SUBGROUP_DISCOVERY_SUPPORT_CUH

#include "plain_filter_helper.cuh"
#include "plain_aggregate_helper.cuh"
#include "subgroup_discovery_support.cuh"
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/common/containers/buffer.h>

#include <array>

namespace otters {

template<typename T, size_t num_attrs, size_t num_dims>
MeanAggregator<T> aggregate_subgroup(std::array<T*, num_attrs> attr_ptrs,
        std::array<int64_t, num_dims> dim_sizes,
        SubsetCondition<T, num_attrs, num_dims>& condition,
        int target_attr) {
    MeanAggregator<T> agg;
    std::array<int64_t, num_dims> dim_mins;
    std::array<int64_t, num_dims> dim_maxs;
    for(auto i = 0u; i < num_dims; ++i) {
        auto range = condition.dim_range(i);
        if(range.enabled()) {
            dim_mins[i] = range.min(); dim_maxs[i] = range.max();
        } else {
            dim_mins[i] = 0; dim_maxs[i] = dim_sizes[i];
        }
    }
    filter_plain_array<T, num_attrs, num_dims>(attr_ptrs, dim_mins, dim_maxs, dim_sizes,
    [&agg, &condition, target_attr](auto& dims, auto& attrs) {
        (void) dims;
        for(auto i = 0u; i < num_attrs; ++i) {
            auto range = condition.attr_range(i);
            if(range.enabled() && (attrs[i] < range.min() || attrs[i] > range.max()))
                return;
        }
        agg.update(attrs[target_attr]);
    });
    return agg;
}

template<typename T, typename Allocator, typename Binner, size_t num_dimensions, size_t num_attr>
MeanAggregator<T> filter_slice(
        int timestep_id,
        MultiAttributeBuffer<resizable_buffer<T, Allocator>, num_attr>& slice,
        std::array<int64_t, num_dimensions> dim_sizes,
        std::array<Binner, num_attr> Binners,
        SubsetCondition<T, num_attr, num_dimensions>& condition,
        int target_attr) {
    MeanAggregator<T> agg;
    std::array<int64_t, num_dimensions> dim_mins;
    std::array<int64_t, num_dimensions> dim_maxs;
    for (auto i = 0u; i < num_dimensions; ++i) {
        auto range = condition.dim_range(i);
        if (range.enabled()) {
            dim_mins[i] = range.min();
            dim_maxs[i] = range.max();
        } else {
            dim_mins[i] = 0;
            dim_maxs[i] = dim_sizes[i];
        }
    }
    std::array<T*, num_attr> attr_ptrs;
    for(auto i = 0; i < num_attr; ++i) {
        attr_ptrs[i] = slice.attribute(i).data();
    }
    if constexpr (num_dimensions == 0) {
        auto slice_size = slice.attribute(0).size();
        for(auto j = 0; j < slice_size; ++j) {
            bool valid = true;
            for (auto i = 0u; i < num_attr; ++i) {
                auto range = condition.attr_range(i);
                if (range.enabled() && (attr_ptrs[i][j] < range.min() || attr_ptrs[i][j] > range.max())) {
                    valid = false;
                    break;
                }
            }
            if(valid)
                agg.update(attr_ptrs[target_attr][j]);
        }
    } else {
        filter_plain_array<T, num_attr, num_dimensions>(attr_ptrs,
                dim_mins,
                dim_maxs,
                dim_sizes,
                [&agg, &condition, target_attr](auto& dims, auto& attrs) {
                  (void)dims;
                  for (auto i = 0u; i < num_attr; ++i) {
                      auto range = condition.attr_range(i);
                      if (range.enabled() && (attrs[i] < range.min() || attrs[i] > range.max()))
                          return;
                  }
                  agg.update(attrs[target_attr]);
                });
    }
    return agg;
}

}

#endif
