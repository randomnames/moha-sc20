#ifndef OTTERS_CUDA_QUERY_THREAD_POOL_CUH
#define OTTERS_CUDA_QUERY_THREAD_POOL_CUH

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#include <boost/asio.hpp>
#pragma clang diagnostic pop

#include <fmt/core.h>
#include <utility>

namespace otters {

class thread_pool {
public:
    thread_pool(int workers, std::string name = "pool")
        : _work(boost::asio::make_work_guard(_io_service)), _name(name) {
        for(auto i = 0; i < workers; ++i) {
            _threads.emplace_back([id = i, name, this]() {
                _io_service.run();
                fmt::print("pool {}, thread {} exited\n", name, id);
            });
        }
    }
    thread_pool(const thread_pool&) = delete;
    thread_pool& operator=(const thread_pool&) = delete;

    template<typename Job>
    void submit_job(Job&& job) {
        boost::asio::post(_io_service.get_executor(), std::forward<Job>(job));
    }

    ~thread_pool() {
        fmt::print("pool {} stopping\n", _name);
        _work.reset();
        for(auto& thread: _threads) {
            thread.join();
        }
        //_io_service.stop();
        fmt::print("pool {} stopped\n", _name);
    }
private:
    boost::asio::io_context _io_service;
    std::vector<std::thread> _threads;
    boost::asio::executor_work_guard<boost::asio::io_context::executor_type> _work;

    std::string _name;
};

}

#endif
