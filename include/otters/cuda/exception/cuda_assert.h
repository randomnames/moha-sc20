#pragma once

#ifndef OTTERS_CUDA_EXCEPTION_CUDA_ASSERT_H
#define OTTERS_CUDA_EXCEPTION_CUDA_ASSERT_H

#include <stdio.h>
#include <exception>
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything" // Code that causes warning goes here #pragma GCC diagnostic pop
#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ostream.h>
#define BOOST_STACKTRACE_USE_ADDR2LINE
#include <boost/stacktrace.hpp>
#pragma clang diagnostic pop
#include <otters/cuda/clion_helper.h>

#define OTTERS_CUDA_ERRORCHECK(ans) otters::cuda_assert((ans), __FILE__, __LINE__)

namespace otters {

class CUDARuntimeError : public std::runtime_error {
public:
   CUDARuntimeError(std::string message)
       : std::runtime_error(message) { }
};

__host__ inline void cuda_assert(cudaError_t code, const char *file, int line, bool abort=true)
{
   (void) abort;
   if (code != cudaSuccess) {
      auto error = fmt::format("CUDA error: {0} at file {1}, line {2}. Stacktrace: {3}", cudaGetErrorString(code), file, line, boost::stacktrace::stacktrace());
      throw CUDARuntimeError(error);
   }
}

}

#endif
