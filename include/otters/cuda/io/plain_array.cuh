#ifndef OTTERS_CUDA_IO_TIMESTEP_ARRAY_CUH
#define OTTERS_CUDA_IO_TIMESTEP_ARRAY_CUH

#include <otters/cuda/query/timestep.cuh>

#include <fstream>
#include <ios>
#include <string>
#include <type_traits>

// #pragma clang diagnostic push
// #pragma clang diagnostic ignored "-Weverything"
// #include <boost/log/trivial.hpp>
// #pragma clang diagnostic pop


namespace otters {

template<typename Buffer, size_t num_attr = 1>
class TimeStepArray {
public:
    using TimeStepBufferT = TimeStep<std::array<Buffer, num_attr>>;
    using serialize_size_type = typename Buffer::serialize_size_type;

    enum class OpenType { READ_ONLY, WRITE_ONLY };

    struct Header {
        int _total_timesteps;
    };

    struct TimeStepInfo {
        ssize_t _offset;
        serialize_size_type _size[num_attr];
    };

    static_assert(std::is_pod<TimeStepInfo>::value == true);

public:
    TimeStepArray(std::string prefix, OpenType type)
        : _prefix(prefix), _type(type) {
        if(type == OpenType::READ_ONLY) {
            read_info();
            read_map();
            _data_file.open(data_file_name(), std::ios_base::in | std::ios_base::binary);
        } else {
            _data_file.open(data_file_name(), std::ios_base::out | std::ios_base::binary);
        }
    }

    void add_timestep(int timestep_id, const TimeStepBufferT& buffer) {
        // BOOST_LOG_TRIVIAL(debug) << "Adding timestep id = " << timestep_id << "\n";
        auto offset = (ssize_t)_data_file.tellp();
        // BOOST_LOG_TRIVIAL(debug) << "Writing data to offset = " << offset << "\n";
        TimeStepInfo info;
        info._offset = offset;
        for(auto i = 0u; i < num_attr; ++i) {
            info._size[i] = buffer.data()[i].serialize_size();
        }
        if(timestep_id + 1 >= (int) _offsets.size()) {
            _offsets.resize(timestep_id + 1);
            //BOOST_LOG_TRIVIAL(debug) << "Resized offsets = " << _offsets.size() << "\n";
        }
        _offsets[timestep_id] = info;
        for(auto i = 0u; i < num_attr; ++i) {
            write(_data_file, buffer);
        }
        //BOOST_LOG_TRIVIAL(debug) << "Added timestep id = " << timestep_id << "\n";
    }

    void read_timestep(TimeStepBufferT& buffer, int timestep_id) {
        auto& info = _offsets[timestep_id];
        buffer.timestep_id(timestep_id);

        for(auto i = 0u; i < num_attr; ++i) {
            buffer.data()[i].resize(info._size[i]);
            _data_file.seekg(info._offset);
        }
        read(buffer, _data_file);
    }

    TimeStepBufferT read_timestep(int timestep_id) {
        TimeStepBufferT buffer;
        read_timestep(buffer, timestep_id);
        return buffer;
    }


    ~TimeStepArray() {
        if(_type == OpenType::WRITE_ONLY) {
            write_info();
            write_map();
        }
    }

private:
    void write_info() {
        Header header {  (int) _offsets.size() };
        std::ofstream info_file(info_file_name(), std::ios_base::binary);
        write(info_file, header);
        // BOOST_LOG_TRIVIAL(debug) << "Output size = " << header._total_timesteps;
    }

    void read_info() {
        Header header;
        std::ifstream info_file(info_file_name(), std::ios_base::binary);
        read(header, info_file);
        _offsets.resize(header._total_timesteps);
    }

    void write_map() {
        std::ofstream map_file(map_file_name(), std::ios_base::binary);
        map_file.write((const char*)_offsets.data(), sizeof(TimeStepInfo) * _offsets.size());
    }

    void read_map() {
        std::ifstream map_file(map_file_name(), std::ios_base::binary);
        map_file.read((char*)_offsets.data(), sizeof(TimeStepInfo) * _offsets.size());
    }

    std::string info_file_name() {
        return _prefix + ".info";
    }

    std::string map_file_name() {
        return _prefix + ".map";
    }

    std::string data_file_name() {
        return _prefix + ".data";
    }

private:
    std::fstream _data_file;
    std::vector<TimeStepInfo> _offsets;
    std::string _prefix;
    OpenType _type;
};

} // namespace otters

#endif
