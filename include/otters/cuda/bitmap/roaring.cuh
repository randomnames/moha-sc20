#pragma once
/*
 * Haoyuan Xing, Apr 15 2019
 * Interfaces for the roaring bitmaps.
 */
#ifndef OTTERS_CUDA_BITMAP_ROARING_H
#define OTTERS_CUDA_BITMAP_ROARING_H

#include <otters/common/bitmap/roaring_bitmap.h>
#include <otters/cuda/container/buffer.cuh>
#include <stdint.h>

namespace otters {

enum class GPURoaringImplType {
    ForLoop,
    Sort,
    UncompressedBitmap
};

static const GPURoaringImplType GPURoaringDefaultImplType = GPURoaringImplType::ForLoop;

/*
 * Interface for creating an roaring bitmap structure based on
 * an input array of bucket ids.
 *
 */

template<int TotalBuckets, typename RoaringTraits, GPURoaringImplType ImplType = GPURoaringDefaultImplType>
struct GPURoaringBitmap {
public:
    static const int chunk_length = RoaringTraits::chunk_length;
    static const int block_threads = RoaringTraits::block_threads;
    using ChunkType = typename RoaringTraits::ChunkType;
    using ChunkSize = typename RoaringTraits::ChunkSize;
    using ChunkAddress = typename RoaringTraits::ChunkAddress;
    using StorageWord = typename RoaringTraits::StorageWord;

public:
    GPURoaringBitmap(int reserve_size = 65535);

    template<typename BucketID>
    void construct_bitmap(BucketID* input_buckets,
            ChunkSize size,
            bool useStream = false,
            cudaStream_t stream = nullptr) {
        construct_bitmap(input_buckets, size, useStream, stream,
        std::integral_constant<GPURoaringImplType, ImplType>());
    }


    template<typename BucketID>
    void construct_bitmap(BucketID* input_buckets,
            ChunkSize size,
            bool useStream,
            cudaStream_t stream,
            std::integral_constant<GPURoaringImplType, GPURoaringImplType::ForLoop>);

    template<typename BucketID>
    void construct_bitmap(BucketID* input_buckets,
            ChunkSize size,
            bool useStream,
            cudaStream_t stream,
            std::integral_constant<GPURoaringImplType, GPURoaringImplType::Sort>);

    template<typename BucketID>
    void construct_bitmap(BucketID* input_buckets,
            ChunkSize size,
            bool useStream,
            cudaStream_t stream,
            std::integral_constant<GPURoaringImplType, GPURoaringImplType::UncompressedBitmap>);


    template<typename Binner, template<typename> typename Container = cuda_host_buffer>
    void resize_for_copy(ParaRoaringBitmap<Binner, Container>& host_bitmap) {
        host_bitmap.clear();
        host_bitmap.resize({ size(), _storage.size() });
        host_bitmap.offsets().resize(_chunk_offsets.size());
        host_bitmap.types().resize(_chunk_types.size());
        host_bitmap.storage().resize(_storage.size());
    }

    template<typename Binner, template<typename> typename Container = cuda_host_buffer>
    void copy_to_host_async_stream(ParaRoaringBitmap<Binner, Container>& host_bitmap, cudaStream_t stream);
    int size() const {
        return _size;
    }

    void clear() {
        _chunk_types.clear();
        _chunk_offsets.clear();
        _storage.clear();
    }

private:
    cuda_device_buffer<ChunkType> _chunk_types;
    cuda_device_buffer<ChunkAddress> _chunk_offsets;
    cuda_device_buffer<StorageWord> _storage;
    // cuda_device_buffer<uint32_t> _uncompressed_storage;
    ChunkSize _size;

private:
};

} // namespace otters

#include "roaring_impl.cuh"

#endif
