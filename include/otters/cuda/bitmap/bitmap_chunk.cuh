#include <otters/cuda/primitives/histogram.cuh>
#include <otters/cuda/primitives/scan.cuh>

namespace otters {

/*
 * write a bitmap chunk collectively
 */
template<int TotalBuckets, typename RoaringTraits, typename BucketID>
__device__ void write_bitmap_chunk(
    typename RoaringTraits::StorageWord* chunk_addr_in,
    BucketID* bucket_ids,
    BucketID bucket,
    typename RoaringTraits::ChunkSize size) {
    auto chunk_addr = reinterpret_cast<uint16_t*>(chunk_addr_in);
    assert(size % detail::WARP_SIZE == 0);
    for (auto i = 0u; i < size; i += blockDim.x * 32) {
        uint32_t bitmap_segment = 0;
        #pragma unroll
        for(auto j = 0; j < 32; ++j) {
            bool in_bucket = (bucket_ids[i + threadIdx.x * 32 + j] == bucket);
            bitmap_segment = (bitmap_segment | (in_bucket << j));
        }
            // printf("blockIdx.x = %d, threadIdx.x = %d, result segment = %u\n", blockIdx.x, threadIdx.x, bitmap_segment);
            // printf("blockIdx.x = %d, threadIdx.x = %d, i = %u\n", blockIdx.x, threadIdx.x, i);
        chunk_addr[(i + threadIdx.x * 32) / sizeof(uint16_t) / 8] = (uint16_t) (bitmap_segment & 0xFFFF);
        chunk_addr[(i + threadIdx.x * 32) / sizeof(uint16_t) / 8] = (uint16_t) (bitmap_segment >> 16);
    }
}

}
