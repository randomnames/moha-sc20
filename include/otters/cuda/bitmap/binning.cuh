#pragma once
#ifndef OTTERS_CUDA_BITMAP_BINNING_CUH
#define OTTERS_CUDA_BITMAP_BINNING_CUH

#include <assert.h>
#include <stdio.h>
#include <vector>
#include <fstream>

namespace otters {

template<int NBuckets, typename T, typename BucketID>
__global__ void equal_binning(BucketID* bucket_ids, T* data, size_t size, T min, T max) {
    auto thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    if(thread_id < size) {
        T delta = data[thread_id] - min;
        T total = max - min;

        auto bucket_id = (BucketID)(delta / total * NBuckets);
        // if constexpr (std::is_same_v<T, double>) {
        //     if (threadIdx.x <= 10)
        //         printf("blockIdx.x = %d, threadIdx.x = %d, thread_id = %d, value = %lf, delta = %lf, total = %lf bucket_id = %d\n",
        //                 blockIdx.x,
        //                 threadIdx.x,
        //                 thread_id,
        //                 data[thread_id],
        //                 delta,
        //                 total,
        //                 bucket_id);
        // }
        if (bucket_id < 0)
            bucket_id = 0;
        else if ( bucket_id >= NBuckets)
            bucket_id = NBuckets - 1;
        bucket_ids[thread_id] = bucket_id;
    }
}

template<int NBuckets, typename T>
class GPUEqualWidthBinner {
public:
    GPUEqualWidthBinner(T min, T max)
        : _min(min), _max(max) {
        }

    template<typename BucketID>
    void generate_bins(BucketID* bucket_ids, T* data, size_t size, cudaStream_t stream, int timestep = 0) {
        (void) timestep;
        unsigned num_threads = 256;
        unsigned num_blocks = (size % num_threads == 0) ? (unsigned)size / num_threads : (unsigned)size / num_threads + 1;
        equal_binning<NBuckets, T, BucketID><<<num_blocks, num_threads, 0, stream>>>
                (bucket_ids, data, size, _min, _max);
    }

    T min(int timestep_id = 0) {
        (void) timestep_id;
        return _min;
    }

    T max(int timestep_id = 0) {
        (void) timestep_id;
        return _max;
    }

private:
    T _min;
    T _max;
};

template<size_t NBuckets, typename T>
class GPUDynamicEqualWidthBinner1 {
public:
    GPUDynamicEqualWidthBinner1(std::string prefix1) {
        std::ifstream file1(prefix1);
        T min1, max1;
        int max_timestep = 0;
        file1 >> max_timestep;
        for(auto i = 0; i < max_timestep; ++i) {
            file1 >> min1 >> max1;
            _mins.push_back(min1);
            _maxs.push_back(max1);
        }
    }

    template<typename BucketID>
    void generate_bins(BucketID* bucket_ids, T* data, size_t size, cudaStream_t stream, int timestep = 0) {
        unsigned num_threads = 256;
        unsigned num_blocks = (size % num_threads == 0) ? (unsigned)size / num_threads : (unsigned)size / num_threads + 1;
        equal_binning<NBuckets, T, BucketID><<<num_blocks, num_threads, 0, stream>>>
                (bucket_ids, data, size, _mins[timestep], _maxs[timestep]);
    }

    T min(int timestep_id) {
        return _mins[timestep_id];
    }

    T max(int timestep_id = 0) {
        return _maxs[timestep_id];
    }


private:
    std::vector<T> _mins;
    std::vector<T> _maxs;
};


template<size_t NBuckets, typename T>
class GPUDynamicEqualWidthBinner {
public:
    GPUDynamicEqualWidthBinner(std::string prefix1, std::string prefix2, int max_timestep) {
        std::ifstream file1(prefix1);
        std::ifstream file2(prefix2);
        T min1, min2, max1, max2;
        for(auto i = 0; i < max_timestep; ++i) {
            file1 >> min1 >> max1;
            file2 >> min2 >> max2;
            _mins.push_back(std::min(min1, min2));
            _maxs.push_back(std::max(max1, max2));
        }
    }

    template<typename BucketID>
    void generate_bins(BucketID* bucket_ids, T* data, size_t size, cudaStream_t stream, int timestep = 0) {
        unsigned num_threads = 256;
        unsigned num_blocks = (size % num_threads == 0) ? (unsigned)size / num_threads : (unsigned)size / num_threads + 1;
        equal_binning<NBuckets, T, BucketID><<<num_blocks, num_threads, 0, stream>>>
                (bucket_ids, data, size, _mins[timestep], _maxs[timestep]);
    }

    T min(int timestep_id) {
        return _mins[timestep_id];
    }

    T max(int timestep_id = 0) {
        return _maxs[timestep_id];
    }

private:
    std::vector<T> _mins;
    std::vector<T> _maxs;
};

}

#endif
