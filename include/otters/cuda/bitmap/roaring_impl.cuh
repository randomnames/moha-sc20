#pragma once // vs code is not happy without it.

#include <otters/common/utils/scope_guard.h>
#include <otters/common/utils/log.h>

#include <otters/cuda/primitives/histogram.cuh>
#include <otters/cuda/primitives/scan.cuh>
#include <otters/cuda/bitmap/array_chunk.cuh>
#include <otters/cuda/bitmap/bitmap_chunk.cuh>
#include <otters/cuda/bitmap/run_chunk.cuh>
#include <otters/cuda/bitmap/thrust_fix.cuh>

#include <thrust/device_ptr.h>
#include <thrust/scan.h>

// #pragma clang diagnostic push
// #pragma clang diagnostic ignored "-Wreserved-id-macro"
// #if defined(__clang__) && defined(__CUDA__)
//     #define __CUDACC_VER_MAJOR__ 10
//     #define __CUDACC_VER_MINOR__ 1
// #endif
// 
// #pragma clang diagnostic ignored "-Weverything"
// #pragma clang diagnostic ignored "-Wall"
// #pragma clang diagnostic ignored "-Wextra"
// #include <cub/cub.cuh>
// #pragma clang diagnostic pop


namespace otters {

template<int TotalBuckets, typename RoaringTraits, typename BucketID>
__global__ void calculate_chunk_info(typename RoaringTraits::ChunkSize chunk_sizes[][TotalBuckets],
        typename RoaringTraits::ChunkType chunk_types[][TotalBuckets],
        BucketID* bucket_ids,
        typename RoaringTraits::ChunkSize size);

template<int TotalBuckets, typename RoaringTraits, typename BucketID>
__global__ void generate_uncompressed_storage_and_chunk_info(typename RoaringTraits::ChunkSize chunk_sizes[][TotalBuckets],
        typename RoaringTraits::ChunkType chunk_types[][TotalBuckets],
        uint32_t* uncompressed_storage,
        BucketID* bucket_ids,
        typename RoaringTraits::ChunkSize size);


template<int TotalBuckets, typename RoaringTraits, typename BucketID>
__global__ void write_chunks(typename RoaringTraits::StorageWord* chunk_address,
        typename RoaringTraits::ChunkAddress chunk_size[][TotalBuckets],
        typename RoaringTraits::ChunkType chunk_type[][TotalBuckets],
        BucketID* bucket_ids,
        typename RoaringTraits::ChunkSize size);

template<int TotalBuckets, typename RoaringTraits, GPURoaringImplType ImplType>
GPURoaringBitmap<TotalBuckets, RoaringTraits, ImplType>::GPURoaringBitmap(int reserve_size) {
    auto total_chunks = RoaringTraits::num_of_chunks(reserve_size);
    _chunk_offsets.reserve(total_chunks * TotalBuckets + 1);
    _chunk_types.reserve(total_chunks * TotalBuckets);
}

template<int TotalBuckets, typename RoaringTraits, GPURoaringImplType ImplType>
template<typename BucketID>
void GPURoaringBitmap<TotalBuckets, RoaringTraits, ImplType>::construct_bitmap(BucketID* input_buckets,
        ChunkSize size,
        bool useStream,
        cudaStream_t stream,
        std::integral_constant<GPURoaringImplType, GPURoaringImplType::ForLoop>) {
    _size = size;

    auto delete_stream = make_scope_guard([&]() {
        OTTERS_CUDA_ERRORCHECK( cudaStreamDestroy(stream) );
    });
    if(!useStream) {
        cudaStreamCreate(&stream);
    } else {
        delete_stream.dismiss();
    }

    auto total_chunks = (unsigned int) RoaringTraits::num_of_chunks(_size);
    _chunk_offsets.resize(total_chunks * TotalBuckets + 1);
    _chunk_types.resize(total_chunks * TotalBuckets);

    // log(OTTERS_LOG_DEBUG, "total_chunks = {}, TotalBuckets = {}", total_chunks, TotalBuckets);
    // count the runs
    calculate_chunk_info<TotalBuckets, RoaringTraits, BucketID>
            <<<total_chunks, block_threads, 0, stream>>>(
                    reinterpret_cast<ChunkSize(*)[TotalBuckets]>(_chunk_offsets.get()),
                    reinterpret_cast<ChunkType(*)[TotalBuckets]>(_chunk_types.get()),
                    input_buckets,
                    size);

    //std::vector<int32_t> sizes(total_chunks * TotalBuckets + 1);
    //cudaMemcpy(sizes.data(), _chunk_offsets.get(), )
    OTTERS_CUDA_ERRORCHECK( cudaStreamSynchronize(stream) );
    // log(OTTERS_LOG_DEBUG, "total_chunks = {}, TotalBuckets = {}", total_chunks, TotalBuckets);

// #pragma clang diagnostic push
// #pragma clang diagnostic ignored "-Wexit-time-destructors"
//     thread_local cuda_device_buffer<char> cub_storage;
// #pragma clang diagnostic pop
//     size_t temp_storage_bytes;
//     auto* d_in = _chunk_offsets.get();
//     auto* d_out = d_in;
//     int num_items = total_chunks * TotalBuckets + 1;
//     cub::DeviceScan::ExclusiveSum(nullptr, temp_storage_bytes, d_in, d_out, num_items, stream);
//     if((size_t) cub_storage.size() < temp_storage_bytes) {
//         cub_storage.resize(temp_storage_bytes);
//         temp_storage_bytes = (size_t) cub_storage.size();
//     }
//     cub::DeviceScan::ExclusiveSum(cub_storage.data(), temp_storage_bytes, d_in, d_out, num_items, stream);
// 
//     ChunkSize total_size;
//     cudaMemcpyAsync(&total_size, &d_in[total_chunks * TotalBuckets], sizeof(ChunkSize), cudaMemcpyDeviceToHost, stream);
//     OTTERS_CUDA_ERRORCHECK( cudaStreamSynchronize(stream) );

    // do an aggregate to determine the size
    thrust::exclusive_scan(thrust::cuda::par.on(stream),
        thrust::device_pointer_cast(_chunk_offsets.get()),
        thrust::device_pointer_cast(_chunk_offsets.get() + total_chunks * TotalBuckets + 1),
        thrust::device_pointer_cast(_chunk_offsets.get()));


    auto total_size = *thrust::device_pointer_cast(_chunk_offsets.get() + total_chunks * TotalBuckets);
    // assert(total_size % sizeof(Count) == 0);
    // log(OTTERS_LOG_DEBUG, "Total size = {}\n", (total_size + sizeof(StorageWord) - 1) / sizeof(StorageWord));
    // fmt::print("Total words = {}\n", (total_size + sizeof(StorageWord) - 1) / sizeof(StorageWord));
    _storage.resize((total_size + sizeof(StorageWord) - 1) / sizeof(StorageWord));
    // fmt::print("Resizing done\n");
    
    // write chunk
    write_chunks<TotalBuckets, RoaringTraits, BucketID>
            <<<total_chunks * TotalBuckets, 256, 0, stream>>>(
                    _storage.get(),
                    reinterpret_cast<ChunkSize(*)[TotalBuckets]>(_chunk_offsets.get()),
                    reinterpret_cast<ChunkType(*)[TotalBuckets]>(_chunk_types.get()),
                    input_buckets,
                    size);
    OTTERS_CUDA_ERRORCHECK( cudaStreamSynchronize(stream) );
}

template<int TotalBuckets, typename RoaringTraits, GPURoaringImplType ImplType>
template<typename Binner, template<typename> typename Container>
void GPURoaringBitmap<TotalBuckets, RoaringTraits, ImplType>::copy_to_host_async_stream(
        ParaRoaringBitmap<Binner, Container>& host_bitmap,
        cudaStream_t stream) {
    // host_bitmap.resize({size(), _storage.size()});
    // host_bitmap.offsets().resize(_chunk_offsets.size());
    // host_bitmap.types().resize(_chunk_types.size());
    // host_bitmap.storage().resize(_storage.size());
    _chunk_offsets.copy_to_host_async_stream(host_bitmap.offsets(), stream);
    _chunk_types.copy_to_host_async_stream(host_bitmap.types(), stream);
    _storage.copy_to_host_async_stream(host_bitmap.storage(), stream);
}

/*
 * Helper function, calculates the size in bytes and the type of the chunk in ChunkSize
 * chunks, each block is responsible for one chunk.
 */
template<int TotalBuckets, typename RoaringTraits, typename BucketID>
__global__ void calculate_chunk_info(typename RoaringTraits::ChunkSize chunk_sizes[][TotalBuckets],
        typename RoaringTraits::ChunkType chunk_types[][TotalBuckets],
        BucketID* bucket_ids,
        typename RoaringTraits::ChunkSize size) {

    
    (void) size; //FIXME: not using size
    constexpr auto chunk_length = GPURoaringBitmap<TotalBuckets, RoaringTraits>::chunk_length;
    constexpr auto block_threads = RoaringTraits::block_threads;
    //using ChunkType = typename RoaringTraits::ChunkType;
    using ChunkSize = typename RoaringTraits::ChunkSize;
    //using ChunkAddress = typename RoaringTraits::ChunkAddress;
    //using StorageWord = typename RoaringTraits::StorageWord;

    __shared__ ChunkSize cardinality[TotalBuckets];
    // __shared__ ChunkSize runs[TotalBuckets];
    __shared__ ChunkSize buffer[block_threads][TotalBuckets];

    for (int i = threadIdx.x; i < TotalBuckets; i += blockDim.x) {
        cardinality[i] = 0;
        // runs[i] = 0;
    }
    for (int i = threadIdx.x; i < TotalBuckets * block_threads; i += blockDim.x) {
        ChunkSize* ptr = &buffer[0][0];
        ptr[i] = 0;
    }
    __syncthreads();

    auto chunk_id = blockIdx.x;
    auto chunk_offset = blockIdx.x * chunk_length;
    // if(threadIdx.x < 10) {
    //     printf("blockIdx.x = %d, threadIdx.x = %d, chunk_offset = %d, bucket_id = %d\n",
    //         blockIdx.x,
    //         threadIdx.x,
    //         chunk_offset,
    //         bucket_ids[chunk_offset + threadIdx.x]
    //     );
    // }
    detail::build_histograms_fixed_len<TotalBuckets>( cardinality, buffer[threadIdx.x],
        bucket_ids + chunk_offset, bucket_ids + chunk_offset + chunk_length);

    __syncthreads();
    // detail::run_count_iterator<BucketID, ChunkSize> run_iter{ bucket_ids + chunk_offset, 0 };

    // for (int i = threadIdx.x; i < TotalBuckets * block_threads; i += blockDim.x) {
    //     ChunkSize* ptr = &buffer[0][0];
    //     ptr[i] = 0;
    // }
    // __syncthreads();
    // detail::build_histograms_fixed_len<TotalBuckets, ChunkSize, decltype(run_iter), detail::not_equal_t<-1>>(
    //         runs, buffer[threadIdx.x], run_iter, run_iter + chunk_length);

    // __syncthreads();
    for (int i = 0; i < TotalBuckets; i += block_threads) {
        auto idx = i + threadIdx.x;
        if (idx < TotalBuckets) {
            if (cardinality[idx] == RoaringTraits::chunk_length) {
                chunk_sizes[chunk_id][idx] = 0;
                chunk_types[chunk_id][idx] = static_cast<uint8_t>(RoaringChunkType::AllOne);
            } else if (cardinality[idx] * 2 < chunk_length / 8) {
                chunk_sizes[chunk_id][idx] = cardinality[idx] * 2;
                chunk_types[chunk_id][idx] = static_cast<uint8_t>(RoaringChunkType::Array);
            } else {
                chunk_sizes[chunk_id][idx] = chunk_length / 8;
                chunk_types[chunk_id][idx] = static_cast<uint8_t>(RoaringChunkType::Uncompressed);
            }
            // if (runs[idx] * 4 < cardinality[idx] * 2 && runs[idx] * 4 < chunk_length / 8) {
            //     chunk_sizes[chunk_id][idx] = runs[idx] * 4;
            //     chunk_types[chunk_id][idx] = static_cast<uint8_t>(RoaringChunkType::Run);
            //     if(runs[idx] > 1)
                     // printf("block = %d bucket = %d cardinality = %d runs = %d type = %d chunk_size = %d\n",
                     //         blockIdx.x,
                     //         idx,
                     //         cardinality[idx],
                     //         0,
            //       //           runs[idx],
                     //         chunk_types[chunk_id][idx],
                     //         chunk_sizes[chunk_id][idx]);
            // }
        }
    }
}

/*
template<int TotalBuckets, typename RoaringTraits, GPURoaringImplType ImplType>
template<typename BucketID>
void GPURoaringBitmap<TotalBuckets, RoaringTraits, ImplType>::
construct_bitmap(BucketID* input_buckets,
        ChunkSize size,
        bool useStream,
        cudaStream_t stream,
        std::integral_constant<GPURoaringImplType, GPURoaringImplType::UncompressedBitmap>) {
    (void) input_buckets;
    (void) size;
    (void) useStream;
    (void) stream;
    _size = size;

    auto delete_stream = make_scope_guard([&]() {
        OTTERS_CUDA_ERRORCHECK( cudaStreamDestroy(stream) );
    });
    if(!useStream) {
        cudaStreamCreate(&stream);
    } else {
        delete_stream.dismiss();
    }

    auto total_chunks = (unsigned int) RoaringTraits::num_of_chunks(_size);
    _chunk_offsets.resize(total_chunks * TotalBuckets + 1);
    _chunk_types.resize(total_chunks * TotalBuckets);
    _uncompressed_storage.resize(total_chunks * TotalBuckets * (chunk_length / sizeof(uint32_t) * 8));

    // count the runs
    generate_uncompressed_storage_and_chunk_info<TotalBuckets, RoaringTraits, BucketID>
            <<<total_chunks * TotalBuckets, block_threads, 0, stream>>>(
                    reinterpret_cast<ChunkSize(*)[TotalBuckets]>(_chunk_offsets.get()),
                    reinterpret_cast<ChunkType(*)[TotalBuckets]>(_chunk_types.get()),
                    _uncompressed_storage.data(),
                    input_buckets,
                    size);

}*/


template<int TotalBuckets, typename RoaringTraits, GPURoaringImplType ImplType>
template<typename BucketID>
void GPURoaringBitmap<TotalBuckets, RoaringTraits, ImplType>::
construct_bitmap(BucketID* input_buckets,
        ChunkSize size,
        bool useStream,
        cudaStream_t stream,
        std::integral_constant<GPURoaringImplType, GPURoaringImplType::Sort>) {
    (void) input_buckets;
    (void) size;
    (void) useStream;
    (void) stream;
}

/*
 * Write all bins of a chunk collectively using a block.
 * 
template<int TotalBuckets, typename RoaringTraits, typename BucketID>
__global__ void write_chunks(typename RoaringTraits::StorageWord* chunk_address,
        typename RoaringTraits::ChunkAddress chunk_offset[][TotalBuckets],
        typename RoaringTraits::ChunkType chunk_type[][TotalBuckets],
        BucketID* bucket_ids,
        typename RoaringTraits::ChunkSize size) {
    (void) size;
    constexpr auto chunk_length = GPURoaringBitmap<TotalBuckets, RoaringTraits>::chunk_length;
    // using ChunkType = typename RoaringTraits::ChunkType;
    using StorageWord = typename RoaringTraits::StorageWord;
    __shared__ int array_elements[TotalBuckets];
    __shared__ int elements[1024];
    __shared__ int buffer[32];
    if(threadIdx.x < TotalBuckets) {
        array_elements[threadIdx.x] = 0;
    }
    __syncthreads();
    // printf("Hello Hello Hello\n");

    auto chunk_id = blockIdx.x / TotalBuckets;
    // auto chunk_id_start = bucket_ids + chunk_id * chunk_length;
    for(auto element_id = threadIdx.x; element_id < chunk_length + threadIdx.x; element_id += blockDim.x) {
        elements[threadIdx.x] = bucket_ids[element_id];
        for (auto i = 0; i < TotalBuckets; ++i) {
            auto element = elements[threadIdx.x];
            auto chunk_addr = chunk_address + chunk_offset[chunk_id][i] / sizeof(StorageWord);
            if (chunk_type[chunk_id][i] == (uint8_t) RoaringChunkType::Uncompressed) {
                bool in_bucket = (element == i);
                uint32_t bitmap_segment = __ballot_sync(detail::WARP_MASK_FULL, in_bucket);
                if (threadIdx.x % detail::WARP_SIZE == 0) {
                    // printf("blockIdx.x = %d, threadIdx.x = %d, result segment = %u\n",
                    //         blockIdx.x,
                    //         threadIdx.x,
                    //         bitmap_segment);
                    // // printf("blockIdx.x = %d, threadIdx.x = %d, i = %u\n", blockIdx.x, threadIdx.x, i);
                    auto word_addr = reinterpret_cast<uint32_t*>(&chunk_addr[element_id / (sizeof(uint32_t) * 8) * sizeof(StorageWord)]);
                    *word_addr = bitmap_segment;
                }
            } else if (chunk_type[chunk_id][i] == (uint8_t) RoaringChunkType::Array) {
                int in_bucket = (element == i) ? 1 : 0;
                // if (blockIdx.x == 0 && i == 0 && threadIdx.x == 0)
                //     printf("blockIdx.x = %d, threadIdx.x = %d, id = %d\n",
                //             blockIdx.x,
                //             threadIdx.x,
                //             array_elements[i]);

                auto scanned_idx =
                        block_scan_commutative<ScanType::Exclusive, plus<int>>(buffer, in_bucket, array_elements[i]);
                // if (blockIdx.x == 0 && i == 0 && threadIdx.x % 32 == 0)
                //     printf("blockIdx.x = %d, threadIdx.x = %d, idx = %d, elementIdx = %d in = %d\n",
                //             blockIdx.x,
                //             threadIdx.x,
                //             scanned_idx,
                //             element_id,
                //             in_bucket);

                if(in_bucket) {
                    chunk_addr[scanned_idx] = (StorageWord) element_id;
                }
                if(threadIdx.x == blockDim.x - 1) {
                    array_elements[i] = scanned_idx + in_bucket;
                }
            }
            __syncthreads();
        }
    }
}
*/


/*
 * Write all bins of a chunk collectively using a block.
 */
template<int TotalBuckets, typename RoaringTraits, typename BucketID>
__global__ void write_chunks(typename RoaringTraits::StorageWord* chunk_address,
        typename RoaringTraits::ChunkAddress chunk_offset[][TotalBuckets],
        typename RoaringTraits::ChunkType chunk_type[][TotalBuckets],
        BucketID* bucket_ids,
        typename RoaringTraits::ChunkSize size) {
    (void) size;
    constexpr auto chunk_length =GPURoaringBitmap<TotalBuckets, RoaringTraits>::chunk_length;
    using ChunkType = typename RoaringTraits::ChunkType;
    using StorageWord = typename RoaringTraits::StorageWord;

    auto chunk_id = blockIdx.x / TotalBuckets;
    int i = blockIdx.x % TotalBuckets;
    auto chunk_id_start = bucket_ids + chunk_id * chunk_length;
    // for(int i = 0; i < TotalBuckets; ++i) {

        auto ptr = (typename RoaringTraits::ChunkAddress*)&chunk_offset[chunk_id][i];
        if(* ptr == * (ptr + 1))
            return;

        // if(threadIdx.x == 0)
        //     printf("i = %d blockIdx.x = %d, chunk_offset = %d\n", i, chunk_id, chunk_offset[chunk_id][i]);

        auto chunk_addr = chunk_address + chunk_offset[chunk_id][i] / sizeof(StorageWord);
        if (chunk_type[chunk_id][i] == static_cast<ChunkType>(RoaringChunkType::Array)) {
            // if (threadIdx.x == 0)
                // printf("i = %d blockIdx.x = %d, chunk_offset = %d, type:array\n", i, chunk_id, chunk_offset[chunk_id][i]);
            write_array_chunk<TotalBuckets, RoaringTraits>(chunk_addr, chunk_id_start, i, chunk_length);
        } else if (chunk_type[chunk_id][i] == static_cast<ChunkType>(RoaringChunkType::Uncompressed)) {
            // if (threadIdx.x == 0)
                // printf("i = %d blockIdx.x = %d, chunk_offset = %d, type:uncompressed\n", i, chunk_id, chunk_offset[chunk_id][i]);
            write_bitmap_chunk<TotalBuckets, RoaringTraits>(chunk_addr, chunk_id_start, i, chunk_length);
        }
    // }
}

template<int TotalBuckets, typename RoaringTraits, typename BucketID>
__global__ void generate_uncompressed_storage_and_chunk_info(typename RoaringTraits::ChunkSize chunk_sizes[][TotalBuckets],
        typename RoaringTraits::ChunkType chunk_types[][TotalBuckets],
        uint32_t* uncompressed_storage,
        BucketID* bucket_ids,
        typename RoaringTraits::ChunkSize size) {
    (void) size;
    __shared__ uint32_t cardinalities[32];
    auto WARP_SIZE = detail::WARP_SIZE;
    auto chunk_id = blockIdx.x / TotalBuckets;
    auto bucket_id = blockIdx.x % TotalBuckets;
    auto chunk_length = RoaringTraits::chunk_length;
    auto chunk_words = chunk_length / (sizeof(uint32_t) * 4);
    cardinalities[threadIdx.x] = 0;
    for(auto element_id = threadIdx.x * 32; element_id < chunk_length; element_id += blockDim.x * 32) {
        auto bitmap_segment = 0;
        auto local_card = 0;
        for(auto i = 0; i < 32; ++i) {
            auto element_bucket_id = bucket_ids[element_id + i];
            int in_bucket = (element_bucket_id == bucket_id);
            local_card += in_bucket;
            bitmap_segment = (bitmap_segment | (in_bucket << i));
        }
        auto word_idx = element_id / 32;
        auto chunk_start = chunk_words * (chunk_id * TotalBuckets + bucket_id);
        uncompressed_storage[chunk_start + word_idx] = bitmap_segment;
        auto warp_cardinality = detail::warp_reduction<plus<uint32_t>>(local_card);
        if(threadIdx.x % WARP_SIZE == 0) {
            cardinalities[threadIdx.x / WARP_SIZE] += warp_cardinality;
        }
    }
    __syncthreads();
    // auto warp_id = threadIdx.x / WARP_SIZE;
    auto idx_in_warp = threadIdx.x % WARP_SIZE;
    // auto total_warp = blockDim.x / WARP_SIZE;
    auto cardinality = detail::warp_reduction<plus<uint32_t>>(cardinalities[idx_in_warp]);
    if (cardinality == RoaringTraits::chunk_length) {
        chunk_sizes[chunk_id][bucket_id] = 0;
        chunk_types[chunk_id][bucket_id] = static_cast<uint8_t>(RoaringChunkType::AllOne);
    } else if (cardinality * 2 < chunk_length / 8) {
        chunk_sizes[chunk_id][bucket_id] = cardinality * 2;
        chunk_types[chunk_id][bucket_id] = static_cast<uint8_t>(RoaringChunkType::Array);
    } else {
        chunk_sizes[chunk_id][bucket_id] = chunk_length / 8;
        chunk_types[chunk_id][bucket_id] = static_cast<uint8_t>(RoaringChunkType::Uncompressed);
    }
}



} // namespace otters
