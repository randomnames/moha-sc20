#include <otters/cuda/primitives/histogram.cuh>
#include <otters/cuda/primitives/scan.cuh>

namespace otters {

struct RunInfo {
    uint16_t _run_id;
    uint16_t _run_length;

    using can_reinterpret_as_t = uint32_t;

    __host__ __device__ RunInfo() : _run_id(0), _run_length(0) { }
    __host__ __device__ RunInfo(uint16_t run_id, uint16_t run_length) : _run_id(run_id), _run_length(run_length) { }
};


template<typename BucketID>
struct RunInfoComputeFunc {
public:
    __host__ __device__ RunInfoComputeFunc(BucketID* head, BucketID tgt)
            : _head(head), _id(tgt) { }
    __host__ __device__ RunInfo operator()(BucketID* iter) {
        RunInfo result{0,0};
        result._run_length = (_id == *iter) ? 1 : 0;
        if(result._run_length == 1) {
            if (_head == iter || *iter != *(iter - 1)) {
                result._run_id = 1;
                result._run_length = 0;
            }
        }
        // printf("blockIdx.x = %d, threadIdx.x = %d, result[%d] = {%d, %d}\n",
            // blockIdx.x, threadIdx.x, iter - _head ,result._run_id, result._run_length);
        return result;
    }
public:
    BucketID* _head;
    BucketID _id;
};

struct RunInfoOPlus {
    __host__ __device__ RunInfo operator()(const RunInfo& lhs, const RunInfo& rhs) {
        RunInfo result;
        result._run_id = lhs._run_id + rhs._run_id;
        if(rhs._run_id > 0) {
            result._run_length = rhs._run_length;
        } else {
            result._run_length = lhs._run_length + rhs._run_length;
        }
        return result;
    }
};

template<typename BucketID, typename InputIter>
struct RunInfoOutputFunc {
    struct run_info_t {
        uint16_t _start;
        uint16_t _length;
    };
    static_assert(sizeof(run_info_t) == sizeof(int32_t), "run_info_t should be packed into 4-byte variables.");
    struct result_t {
        bool _flag;
        int _index;
        run_info_t _result;
    };
    __host__ __device__ result_t operator()(InputIter& iter, const RunInfo& index) {
        result_t res;
        // Let's decide if this is a tail
        // printf("blockIdx.x = %d, threadIdx.x = %d, idx = %d, info = {%d,%d}\n",
        //         blockIdx.x,
        //         threadIdx.x,
        //         iter - (_tail - 65535),
        //         index._run_id,
        //         index._run_length);

        res._flag = false;
        if(iter.original_value() == _bucket) {
            // this is a 1.
            if(iter == _tail || (iter + 1).original_value() != _bucket) {
                // and the next one is not.
                res._flag = true;
            }
        }
        res._index = index._run_id - 1;
        //todo: fix the magic number
        res._result._start = static_cast<uint16_t>(iter - (_tail - 65535)) - index._run_length;
        res._result._length = index._run_length;

        // printf("blockIdx.x = %d, threadIdx.x = %d, idx = %d, info = {%d,%d} , %d, result[%d] = {%d, %d}\n",
        //         blockIdx.x,
        //         threadIdx.x,
        //         iter - (_tail - 65535),
        //         index._run_id,
        //         index._run_length,
        //         res._flag,
        //         res._index,
        //         res._result._start,
        //         res._result._length);
        return res;
    }
    InputIter _tail;
    BucketID _bucket;
};



template<int TotalBuckets, typename RoaringTraits, typename BucketID>
__device__ void write_run_chunk(
    typename RoaringTraits::StorageWord* chunk_addr,
    BucketID* bucket_ids,
    BucketID bucket,
    typename RoaringTraits::ChunkSize size) {
    assert(size % detail::WARP_SIZE == 0);
    // constexpr auto warp_count_size = RoaringTraits::BlockSize / detail::WARP_SIZE;
    /* __shared__ variable cannot be initialized, allocate the same-size struct */
    __shared__ int32_t warp_count[32];
    // auto warp_id = threadIdx.x / detail::WARP_SIZE;
    // auto idx_in_warp = threadIdx.x % detail::WARP_SIZE;
    using input_func_t = RunInfoComputeFunc<BucketID>;
    using input_iter_t = iterator_adaptor<BucketID*, input_func_t>;
    auto begin_input = input_iter_t(bucket_ids, input_func_t(bucket_ids, bucket));
    auto end_input = input_iter_t(bucket_ids, input_func_t(bucket_ids, bucket)) + size;
    using output_func_t = RunInfoOutputFunc<BucketID, input_iter_t>;
    using run_info_t = typename output_func_t::run_info_t;
    using output_iter_t = gather_output_iterator<run_info_t*, input_iter_t, RunInfo, output_func_t>;
    auto output_iter = output_iter_t((run_info_t*)chunk_addr, begin_input,
            output_func_t{end_input - 1, bucket});
    scan<ScanType::Inclusive, RunInfoOPlus>((RunInfo*)warp_count, output_iter, begin_input, end_input, RunInfo{0,0});
}

}
