#pragma once

#ifndef OTTERS_CUDA_BITMAP_QUERY_DRIVER_H
#define OTTERS_CUDA_BITMAP_QUERY_DRIVER_H

#include <otters/cuda/bitmap/binning.cuh>

#include <otters/cuda/bitmap/thrust_fix.cuh>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <array>

namespace otters {

template<typename T, ssize_t num_attr>
class QueryDriver {
public:
    virtual void add_timestep_data(int timestep_id,
            std::array<T*, num_attr> data,
            std::array<ssize_t, num_attr> size) = 0;
    virtual void end_input() { }
    //static minmax_pair<T> determine_min_max(T* begin, T* end);
    virtual ~QueryDriver() = default;
};

template<typename T, size_t num_attr>
class NoneQueryDriver : public QueryDriver<T, num_attr> {
public:
    virtual void add_timestep_data(int timestep_id,
            std::array<T*, num_attr> data,
            std::array<ssize_t, num_attr> size) override {
                (void) timestep_id;
                (void) data;
                (void) size;
    }
};

}

#endif
