#include <otters/cuda/primitives/histogram.cuh>
#include <otters/cuda/primitives/scan.cuh>

namespace otters {

template<typename BucketID, typename Int = BucketID>
struct bucket_id_compare_func {
public:
    __host__ __device__ bucket_id_compare_func(BucketID tgt) : _id(tgt) {
    }
    __host__ __device__ Int operator()(BucketID* id) {
        //if(_id == 0)
        //    printf("blockIdx.x = %d, threadIdx.x = %d, res.flag = %d, id = %d\n",
        //            blockIdx.x,
        //            threadIdx.x,
        //            (*id == _id) ? 1 : 0,
        //            *id);
        return (*id == _id) ? 1 : 0;
    }
public:
    BucketID _id;
};

template<typename InputIter>
struct bucket_id_output_func {
    struct result_t {
        bool _flag;
        int _index;
        uint16_t _result;
    };
    __host__ __device__ result_t operator()(InputIter& iter, int index) {
        result_t res;
        res._flag = *iter;
        res._index = index;
        res._result = static_cast<uint16_t>(iter - _head);
        //printf("blockIdx.x = %d, threadIdx.x = %d, res.flag = %d, res._inex = %d, res._result = %d, id = %d\n",
        //        blockIdx.x,
        //        threadIdx.x,
        //        res._flag,
        //        res._index,
        //        res._result,
        //        iter.original_value());
        return res;
    }
    InputIter _head;
};

template<int TotalBuckets, typename RoaringTraits, typename BucketID>
__device__ void write_array_chunk(
    typename RoaringTraits::StorageWord* chunk_addr,
    BucketID* bucket_ids,
    BucketID bucket,
    typename RoaringTraits::ChunkSize size) {
    assert(size % detail::WARP_SIZE == 0);
    //constexpr auto warp_count_size = RoaringTraits::BlockSize / detail::WARP_SIZE;
    //todo: fix this buffer overflow
    __shared__ BucketID warp_count[32];
    //auto warp_id = threadIdx.x / detail::WARP_SIZE;
    //auto idx_in_warp = threadIdx.x % detail::WARP_SIZE;
    using input_func_t = bucket_id_compare_func<BucketID>;
    using input_iter_t = iterator_adaptor<BucketID*, input_func_t>;
    auto begin_input = input_iter_t(bucket_ids, input_func_t(bucket));
    auto end_input = input_iter_t(bucket_ids, input_func_t(bucket)) + size;
    using output_func_t = bucket_id_output_func<input_iter_t>;
    using output_iter_t = gather_output_iterator<uint16_t*, input_iter_t, BucketID, output_func_t>;
    auto output_iter = output_iter_t((uint16_t*)chunk_addr, begin_input, output_func_t{begin_input});
    scan<ScanType::Exclusive, plus<BucketID>>(warp_count, output_iter, begin_input, end_input, 0);
}
}
