//
// Created by Haoyuan Xing on 5/5/19.
//

#pragma once

#ifndef OTTERS_INCLUDE_OTTERS_CUDA_CLION_HELPER_H
#define OTTERS_INCLUDE_OTTERS_CUDA_CLION_HELPER_H

#ifdef __PATCH_CUDA__
#define __host__
#define __device__
#define __shared__
#define __constant__
#define __global__
#define CUDA_VERSION 10000

// This is slightly mental, but gets it to properly index device function calls like __popc and whatever.
#define __CUDACC__
#include <__clang_cuda_device_functions.h>


// These headers are all implicitly present when you compile CUDA with clang. Clion doesn't know that, so
// we include them explicitly to make the indexer happy. Doing this when you actually build is, obviously,
// a terrible idea :D
#include <__clang_cuda_builtin_vars.h>
#include <__clang_cuda_intrinsics.h>
#include <__clang_cuda_math_forward_declares.h>
#include <__clang_cuda_complex_builtins.h>
#include <__clang_cuda_cmath.h>
#endif // __PATCH_CUDA__

#endif //OTTERS_INCLUDE_OTTERS_CUDA_CLION_HELPER_H
