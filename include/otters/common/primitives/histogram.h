#pragma once

#ifndef OTTERS_COMMON_PRIMITIVES_HISTOGRAM_H
#define OTTERS_COMMON_PRIMITIVES_HISTOGRAM_H

#include <otters/common/utils/gpu_arch.h>
#include <stdlib.h>
#include <stdint.h>

namespace otters {

namespace detail {


struct NoCheck {
    OTTERS_ARCH_COMMON_FUNC bool operator()(int32_t id) {
        (void) id;
        return true;
    }
};

template<int Value>
struct not_equal_t {
    OTTERS_ARCH_COMMON_FUNC bool operator()(int32_t id) {
        return id != Value;
    }
};

/**
 * Build histogram for a strided key-range marked by bucket_ids .. bucket_ids_end,
 * all the keys are in the range of [0..total_buckets).
 * The elements are only counted every stride input.
 */
template<typename Count, typename Iter, typename Check = NoCheck>
OTTERS_ARCH_COMMON_FUNC inline void build_local_histograms_with_bucket_id(
    Count* local_histogram, size_t total_buckets,
    Iter bucket_ids, Iter bucket_ids_end, int stride) {
    (void) total_buckets;
    Check check;
    for(auto id = bucket_ids; id < bucket_ids_end; id += stride) {
        // printf("threadIdx.x = %d, id._idx = %d, *id = %d, _head[_idx] = %d\n",
                // threadIdx.x, id._idx, *id, id._head[id._idx]);
        if(check(*id)) {
            local_histogram[*id] += 1;
        }
    }
}

}

}

#endif
