#ifndef OTTERS_COMMON_CONTAINERS_BYTE_BUFFER_H
#define OTTERS_COMMON_CONTAINERS_BYTE_BUFFER_H

#include <vector>

namespace otters {

using byte_buffer = std::vector<char>;

void write(std::ostream& os, const byte_buffer& buffer) {
    os.write(buffer.data(), buffer.size());
}

void read(byte_buffer& buffer, std::istream& is) {
    is.read(buffer.data(), buffer.size());
}

}

#endif
