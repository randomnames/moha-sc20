#ifndef OTTERS_COMMON_CONTAINERS_BUFFER_POOL_H
#define OTTERS_COMMON_CONTAINERS_BUFFER_POOL_H

#include <vector>
#include <mutex>
#include <condition_variable>

namespace otters {

template<typename Buffer>
class BufferPool {
public:
    using buffer_type = Buffer;
public:
    BufferPool() {}

    void push(Buffer&& buffer) {
        buffer.clear();
        std::unique_lock<std::mutex> lock(_mutex);
        _buffers.push_back(std::move(buffer));
        // fmt::print("size = {}\n", _buffers.size());
        _empty.notify_one();
        //fmt::print("notified\n");
    }

    Buffer pop() {
        std::unique_lock<std::mutex> lock(_mutex);
        while(_buffers.size() == 0) {
            _empty.wait(lock);
        }
        auto buffer = std::move(_buffers.back());
        _buffers.pop_back();
        return buffer;
    }

private:
    std::vector<Buffer> _buffers;
    std::mutex _mutex;
    std::condition_variable _empty;
};

template<>
void BufferPool<long>::push(long&& buffer) {
    (void) buffer;
}

template<>
long BufferPool<long>::pop() {
    return 0;
}


template<typename Buffer>
void initialize_buffer_pool(BufferPool<Buffer>& pool, int number_of_buffers) {
    for (auto i = 0; i < number_of_buffers; ++i) {
        Buffer buffer;
        pool.push(std::move(buffer));
    }
}

}

#endif
