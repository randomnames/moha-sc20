#ifndef OTTERS_COMMON_CONTAINERS_TIME_SLICE_H
#define OTTERS_COMMON_CONTAINERS_TIME_SLICE_H

#include <otters/common/containers/coordinates.h>
#include <otters/common/containers/buffer_pool.h>
#include <otters/common/containers/byte_buffer.h>
#include <otters/common/io/serialization.h>

namespace otters {

template<int coord_dimensions, typename Container, typename BufferPool = BufferPool<Container>>
struct TimeSlice {
    using buffer_type = Container;
    using coordinate_type = Coordinates<coord_dimensions>;
    using buffer_pool_type = BufferPool;
    static const int num_coord_dims = coord_dimensions;

    TimeSlice(coordinate_type timestep = -1, BufferPool* pool = nullptr) : _data(), _pos(timestep), _pool(pool) {
        if (_pool != nullptr) {
            _data = _pool->pop();
        }
    }

    TimeSlice(byte_buffer& buffer, BufferPool* pool = nullptr) : _pool(pool) {
        if (_pool != nullptr) {
            _data = _pool->pop();
        }
        span<char> s(buffer.data(), buffer.size());
        deserialize(s);
    }

    auto& data() { return _data; }
    const auto& data() const { return _data; }

    //auto& operator[](int i) { return buffer()[i]; }
    //const auto& operator[](int i) const { return buffer()[i]; }

    auto timestep_id() const { return _pos[0]; }
    void timestep_id(int timestep_id) { _pos[0] = timestep_id; }

    coordinate_type& position() { return _pos; }
    const coordinate_type& position() const { return _pos; }

    //auto size() const { return _data.size(); }
    //auto serialize_size() const { return _data.serialize_size(); }
    //void resize(serialize_size_type size) {
    //    _data.resize(size);
    //}

    operator bool() {
        return timestep_id() != -1;
    }

    void mark_end() noexcept {
        timestep_id(-1);
        _pool = nullptr;
    }
 
    TimeSlice(const TimeSlice&) = delete;
    TimeSlice(TimeSlice&& other) noexcept
        : _data(std::move(other._data)), _pos(other._pos), _pool(other._pool) {
            other.mark_end();
    }

    TimeSlice& operator=(TimeSlice&& other) noexcept {
        if(this == &other)
            return *this;
        close();
        _data = std::move(other._data);
        _pos = other._pos;
        _pool = other._pool;
        other.mark_end();
        return *this;
    }

    TimeSlice& operator=(const TimeSlice&) = delete;

    ~TimeSlice() {
        close();
    }

    void close() {
        if(_pool != nullptr) {
            _pool->push(std::move(_data));
        }
    }

    ssize_t serialize_size() const {
        // log(OTTERS_LOG_DEBUG, "size = {},{}\n",otters::serialize_size(_data), otters::serialize_size(_pos));
        return otters::serialize_size(_data) + otters::serialize_size(_pos);
    }

    span<char> serialize(span<char> buffer) const {
        buffer = otters::serialize(buffer, _data);
        buffer = otters::serialize(buffer, _pos);
        return buffer;
    }

    span<char> deserialize(span<char> buffer) {
        // log(OTTERS_LOG_DEBUG, "deserialize timestep: size = {}", buffer.size());
        buffer = otters::deserialize(_data, buffer);
        // log(OTTERS_LOG_DEBUG, "deserialize timestep 2: size = {}", buffer.size());
        buffer = otters::deserialize(_pos, buffer);
        return buffer;
    }

    Container _data;
    coordinate_type _pos;
    BufferPool* _pool;
};

template<typename Slice>
class PairSlice {
public:
    PairSlice() {
    }
    PairSlice(Slice&& first, Slice&& second) : _first(std::move(first)), _second(std::move(second)) {
    }
    operator bool() {
        return (bool)_first;
    }
    Slice& first() {
        return _first;
    }
    Slice& second() {
        return _second;
    }
    auto position() {
        return _first.position();
    }

    auto timestep_id() {
        return _first.timestep_id();
    }
private:
    Slice _first;
    Slice _second;
};


}

#endif
