#ifndef OTTERS_COMMON_CONTAINERS_BUFFER_H
#define OTTERS_COMMON_CONTAINERS_BUFFER_H

#include <type_traits>
#include <otters/common/containers/span.h>
#include <otters/common/io/serialization.h>

namespace otters {

template<typename T, typename Allocator>
class resizable_buffer {
public:
    using serialize_size_type = ssize_t;
    static_assert(std::is_trivial<T>::value == true, "Requiring T is a trivial type");
public:
    resizable_buffer() : _data(nullptr), _size(0), _capacity(0) {
    }
    resizable_buffer(ssize_t capacity) : _data(nullptr), _size(0), _capacity(capacity) {
        // fmt::print("allocating {} bytes.\n", _capacity);
        realloc(capacity);
    }

    resizable_buffer(const resizable_buffer&) = delete;
    resizable_buffer(resizable_buffer&& other) noexcept {
        _data = other._data;
        _size = other._size;
        _capacity = other._capacity;
        other._data = nullptr;
        other._size = 0;
        other._capacity = 0;
    }
    resizable_buffer& operator=(const resizable_buffer&) = delete;
    resizable_buffer& operator=(resizable_buffer&& other) {
        close();
        _data = other._data;
        _size = other._size;
        _capacity = other._capacity;
        other._data = nullptr;
        other._size = 0;
        other._capacity = 0;
        return *this;
    }
    ~resizable_buffer() {
        close();
    }
    T& operator[](int idx) {
        return _data[idx];
    }

    const T& operator[](int idx) const {
        return _data[idx];
    }

public:
    T* get() {
        return _data;
    }

    T* data() {
        return get();
    }

    const T* get() const {
        return _data;
    }

    const T* data() const {
        return get();
    }

    T* end() {
        return data() + size();
    }

    void close_buffer(T*& data) {
        Allocator::free(data);
    }

    void close() {
        if (_data != nullptr) {
            close_buffer(_data);
            _data = nullptr;
        }
    }
    void realloc(ssize_t capacity) {
        auto old_data = _data;
        _capacity = capacity;
        if (_capacity != 0) {
            _data = Allocator::alloc(_capacity);
            if (size() != 0) {
                // fmt::print("copying size = {}\n", size());
                Allocator::memcpy(_data, old_data, size());
            }
        }
        if (old_data != nullptr)
            close_buffer(old_data);
    }

    auto size() const {
        return _size;
    }

    serialize_size_type serialize_size() const {
        return otters::serialize_size(_size) + _size * sizeof(T);
    }

    void clear() {
        _size = 0;
    }

    auto capacity() const {
        return _capacity;
    }

    void resize(serialize_size_type size) {
        reserve(size);
        _size = size;
    }

    auto push_back(T&& element) {
        if(size() == capacity()) {
            reserve(std::max<size_t>(4, capacity() * 3 / 2));
        }
        _data[_size++] = std::move(element);
    }

    auto push_back(const T& element) {
        if(size() == capacity()) {
            reserve(std::max<size_t>(4, capacity() * 3 / 2));
        }
        _data[_size++] = element;
    }

    void reserve(ssize_t capacity) {
        if (_capacity < capacity) {
            // fmt::print(stderr, "reallocating {} elements\n", capacity);
            realloc(std::max(capacity, _capacity * 3 / 2));
        }
    }

    span<char> serialize(span<char> buffer) const {
        assert(buffer.size() >= serialize_size());
        buffer = otters::serialize(buffer, _size);
        std::copy((char*)_data, (char*)(_data + _size), buffer.data());
        return buffer.advance(sizeof(T) * _size);
    }

    span<char> deserialize(span<char> buffer) {
        ssize_t count;
        buffer = otters::deserialize(count, buffer);
        resize(count);
        std::copy(buffer.begin(), buffer.begin() + _size * sizeof(T), (char*) _data);
        return buffer.advance(sizeof(T) * _size);
    }

private:
    T* _data;
    ssize_t _size;
    ssize_t _capacity;
};

template<typename T>
struct NewDeleteAllocator {
    static T* alloc(ssize_t size) {
        return new T[size];
    }

    static void free(T* data) {
        delete[] data;
    }

    static void memcpy(T* target, T* src, ssize_t size) {
        std::copy(src, src + size, target);
    }
};

template<typename T>
using system_malloc_buffer = resizable_buffer<T, NewDeleteAllocator<T>>;

template<typename T, typename Allocator>
void write(std::ostream& os, const resizable_buffer<T, Allocator>& buffer) {
    os.write((const char*)buffer.data(), buffer.size() * sizeof(T));
}

template<typename T, typename Allocator>
void read(resizable_buffer<T, Allocator>& buffer, std::istream& is) {
    is.read((char*)buffer.data(), buffer.size() * sizeof(T));
}

} // namespace otters

#endif
