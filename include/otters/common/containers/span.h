#ifndef OTTERS_COMMON_CONTAINERS_SPAN_H
#define OTTERS_COMMON_CONTAINERS_SPAN_H

namespace otters {

template<typename T>
class span {
public:
    span(T* data, ssize_t size)
        : _data(data), _size(size) {
    }
    T* begin() noexcept { return _data; }
    const T* begin() const noexcept { return _data; }
    T* end() noexcept { return _data + _size; }
    const T* end() const noexcept { return _data + _size; }

    T* data() noexcept { return begin(); }
    const T* data() const noexcept { return begin(); }

    ssize_t size() const { return _size; }

    span<T> advance(ssize_t skip) {
        assert(size() >= skip);
        return span<T>(begin() + skip, size() - skip);
    }

private:
    T* _data;
    ssize_t _size;
};

}

#endif
