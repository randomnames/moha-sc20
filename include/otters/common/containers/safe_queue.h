#ifndef OTTERS_COMMON_CONTAINERS_SAFE_QUEUE_H
#define OTTERS_COMMON_CONTAINERS_SAFE_QUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>
#include <cstdlib>
#include <otters/common/utils/log.h>

namespace otters {

template<typename T>
class safe_queue {
public:
    safe_queue(size_t total_producers, size_t max_buffer_size)
        : _total_producers(total_producers), _max_items(max_buffer_size), _ended(0) {
    }

    void push(T&& t) {
        std::unique_lock<std::mutex> lck(_mutex);
        if((bool) t == false) {
            ++_ended;
//             log(OTTERS_LOG_DEBUG, "Safe queue producer ended = {} required = {}.\n", _ended, _total_producers);
            _non_empty.notify_all();
            return;
        }

        while (_queue.size() >= _max_items) {
            _non_full.wait(lck);
        }
        _queue.push(std::move(t));
        lck.unlock();
        _non_empty.notify_one();
    }

    T pop() {
        std::unique_lock<std::mutex> lck(_mutex);
        // log(OTTERS_LOG_DEBUG, "Popping from the safe queue.\n");
        while(unsafe_ended() == false && _queue.size() == 0) {
            _non_empty.wait(lck);
        }
        if(unsafe_ended() && _queue.size() == 0) {
            _non_empty.notify_all();
            return T();
        } else {
            T result = std::move(_queue.front());
            // log(OTTERS_LOG_DEBUG, "Popping from the safe queue, result = .");
            _queue.pop();
            lck.unlock();
            _non_full.notify_one();
            return result;
        }
    }
private:
    bool unsafe_ended() {
        return _ended == _total_producers;
    }
private:
    std::queue<T> _queue;
    std::mutex _mutex;
    std::condition_variable _non_empty;
    std::condition_variable _non_full;
    size_t _total_producers;
    size_t _max_items;
    size_t _ended;
};

}

#endif
