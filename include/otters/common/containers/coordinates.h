#ifndef OTTERS_COMMON_CONTAINERS_ARRAY_H
#define OTTERS_COMMON_CONTAINERS_ARRAY_H

#include <array>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wfloat-equal"
#pragma clang diagnostic ignored "-Wunused-member-function"
#include <fmt/core.h>
#include <fmt/format.h>
#pragma clang diagnostic pop

namespace otters {

template<int dimensions>
class Coordinates {
public:
    Coordinates() = default;
    Coordinates(int value) {
        _data[0] = value;
    }
    int* data() {
        return _data;
    }

    const int* data() const {
        return _data;
    }

    void reset() {
        for(auto i = 0; i < dimensions; ++i)
            _data[i] = 0;
    }

    const int& operator[](int i) const {
        return _data[i];
    }
    int& operator[](int i) {
        return _data[i];
    }
    bool operator==(const Coordinates& other) const {
        for(auto i = 0; i < dimensions; ++i) {
            if(_data[i] != other[i])
                return false;
        }
        return true;
    }

    bool operator<(const Coordinates& other) const {
        for(auto i = 0; i < dimensions; ++i) {
            if(_data[i] < other[i])
                return true;
            if(_data[i] > other[i])
                return false;
        }
        return false;
    }
public:
    int _data[dimensions];
};


}

template <int dimension>
struct fmt::formatter<otters::Coordinates<dimension>> {
  constexpr auto parse(format_parse_context& ctx) { return ctx.begin(); }

  template <typename FormatContext>
  auto format(const otters::Coordinates<dimension>& coord, FormatContext& ctx) {
    auto it = format_to(ctx.out(), "[");
    for(auto i = 0; i < dimension - 1; ++i) {
        it = format_to(ctx.out(), "{},", coord[i]);
    }
    it = format_to(ctx.out(), "{}]", coord[dimension - 1]);
    return it;
  }
};


#endif
