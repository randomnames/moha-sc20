#ifndef OTTERS_COMMON_BITMAP_ROARING_FILTER_HELPER_H
#define OTTERS_COMMON_BITMAP_ROARING_FILTER_HELPER_H

#include "roaring_bitmap.h"
#include <otters/common/utils/log.h>

#include <array>
#include <type_traits>
#include <boost/mp11.hpp>

namespace otters {

namespace mp11 = boost::mp11;


template<size_t num_ranges, typename RoaringTraits>
uint64_t get_uncompressed_word_and(std::array<RoaringRange<RoaringTraits>, num_ranges>& ranges, int idx) {
    uint64_t word = 0xFFFFFFFFFFFFFFFF;
    for(auto i = 1; i < num_ranges; ++i) {
        assert(ranges[i]._type == RoaringRangeType::Uncompressed);
        uint64_t* words = (uint64_t*) ranges[i]._storage;
        // log(OTTERS_LOG_DEBUG, "Got word at idx {},{} = {:x}", idx, i, words[idx]);
        word &= words[idx];
        // log(OTTERS_LOG_DEBUG, "and result idx {},{} = {:x}", idx, i, word);
    }

    return word;
}

struct MeanSupportAggregateResult {
    double _sum;
    size_t _size;
};


template<size_t num_ranges, typename RoaringTraits>
void merge_ranges_uncompressed(uint64_t* storage, int start, int end, std::array<RoaringRange<RoaringTraits>, num_ranges>& ranges) {
    constexpr auto words_in_chunk = RoaringTraits::chunk_length / 8 / sizeof(uint64_t);
    auto start_word = start / 64;
    auto end_word = (end - 1) / 64;
    // log(OTTERS_LOG_DEBUG, "Start word = {}, end word = {}", start_word, end_word);

    uint64_t z_mask = 0xFFFFFFFFFFFFFFFF;
    auto start_mask = (z_mask << (start % 64));
    auto end_mask = (z_mask >> (64 - ((end - 1) % 64)));

    // log(OTTERS_LOG_DEBUG, "Start mask = {}, end mask = {}", start_mask, end_mask);

    size_t result = 0;
    for(auto i = 0; i < start_word; ++i)
        storage[i] = 0;
    if(start_word == end_word) {
        auto word = get_uncompressed_word_and(ranges, start);
        word &= (start_mask & end_mask);
    } else {
        auto word = get_uncompressed_word_and(ranges, start);
        word &= (start_mask);
        result += __builtin_popcountl(word);
        for(auto i = start_word + 1; i < end_word; ++i) {
            word = get_uncompressed_word_and(ranges, i);
            result += __builtin_popcountl(word);
        }
        word = get_uncompressed_word_and(ranges, end_word);
        word &= end_mask;
        result += __builtin_popcountl(word);
    }
    for(auto i = end_word + 1; i < 1024; ++i) {
        storage[i] = 0;
    }
    // return result;
}

template<size_t num_ranges, typename RoaringTraits>
size_t merge_ranges_and_count_all_uncompressed(int start, int end, std::array<RoaringRange<RoaringTraits>, num_ranges>& ranges) {
    constexpr auto words_in_chunk = RoaringTraits::chunk_length / 8 / sizeof(uint64_t);
    auto start_word = start / 64;
    auto end_word = (end - 1) / 64;
    // log(OTTERS_LOG_DEBUG, "Start word = {}, end word = {}", start_word, end_word);

    uint64_t z_mask = 0xFFFFFFFFFFFFFFFF;
    auto start_mask = (z_mask << (start % 64));
    auto end_mask = (z_mask >> (64 - (end % 64)));

    // log(OTTERS_LOG_DEBUG, "Start mask = {}, end mask = {}", start_mask, end_mask);

    size_t result = 0;
    if(start_word == end_word) {
        auto word = get_uncompressed_word_and(ranges, start);
        word &= (start_mask & end_mask);
        result += __builtin_popcountl(word);
    } else {
        auto word = get_uncompressed_word_and(ranges, start);
        word &= (start_mask);
        result += __builtin_popcountl(word);
        for(auto i = start_word + 1; i < end_word; ++i) {
            word = get_uncompressed_word_and(ranges, i);
            result += __builtin_popcountl(word);
        }
        word = get_uncompressed_word_and(ranges, end_word);
        word &= end_mask;
        result += __builtin_popcountl(word);
    }
    return result;
}

// template<int num_ranges, typename RoaringTraits>
// size_t merge_ranges_and_count_array(int start, int end, std::array<RoaringRange<RoaringTraits>, num_ranges>& ranges) {
//     constexpr auto words_in_chunk = RoaringTraits::chunk_length / 8 / sizeof(uint64_t);
//     constexpr auto num_elements = words_in_chunk * (sizeof(uint64_t) / sizeof(uint16_t));
//     std::array<int16_t, num_elements> temp_storage;
//     int merged_size = -1;
//     auto* output_it = set_intersection(ranges[0], temp_storage);
//     merged_size = output_it - temp_storage;
//     for(auto i = 1; i < num_ranges; ++i) {
//         if(ranges[i]._type != RoaringRangeType::Array)
//             continue;
//         if(merged_size == -1) {
//             std::copy(ranges[i]._storage, ranges[i]._storage + ranges[i]._chunk_size, temp_storage);
//             merged_size = ranges[i]._chunk_size;
//         } else {
//             /* we are abusing the standard library - temp_storage are not supposed to overlap with
//              * the input range, but this happens to work since we would write the exactly same code
//              * as STL, only our precondition is safer. */
//             auto output_it = std::set_intersection(temp_storage,
//                     temp_storage + merged_size,
//                     ranges[i]._storage,
//                     ranges[i]._storage + ranges[i]._chunk_size,
//                     temp_storage);
//             merged_size = output_it - temp_storage;
//         }
//     }
//     size_t result = 0;
//     for(auto i = 0; i < merged_size; ++i) {
//         if(start > temp_storage[i])
//             continue;
//         if(end <= temp_storage[i])
//             break;
//         for(auto j = 1; j < ranges.size(); ++j) {
//             if(ranges._type == RoaringRangeType::Uncompressed)
//                 if (get_uncompressed_bit(ranges[j], temp_storage[i]) == false)
//                     continue;
//         }
//         ++result;
//     }
//     return result;
// }

/* merge all the ranges, return the count */
template<int num_ranges, typename RoaringTraits>
size_t merge_ranges_and_count(int start, int end, std::array<RoaringRange<RoaringTraits>, num_ranges>& ranges) {
    start %= RoaringTraits::chunk_length;
    end %= RoaringTraits::chunk_length;
    // for(auto i = 1; i < num_ranges; ++i) {
    //     auto& range = ranges[i];
    //     if(range._type == RoaringRangeType::Array)
    //         return merge_ranges_and_count_all_array(start, end, ranges);
    // }
    return merge_ranges_and_count_uncompressed(start, end, ranges);
}

template<typename RoaringTraits = DefaultRoaringTraits>
void set_bit_range(uint64_t* storage, int start, int end) {
    // constexpr auto words_in_chunk = RoaringTraits::chunk_length / 8 / sizeof(uint64_t);
    auto start_word = start / 64;
    auto end_word = (end - 1) / 64;
    // log(OTTERS_LOG_DEBUG, "Start word = {}, end word = {}", start_word, end_word);

    uint64_t z_mask = 0xFFFFFFFFFFFFFFFF;
    uint64_t start_mask = (z_mask << (start % 64));
    uint64_t end_mask = (z_mask >> (64 - (end % 64)));

    // log(OTTERS_LOG_DEBUG, "start % 64 = {}, Start mask = {:x}, end mask = {:x}", start_mask, end_mask);

    // size_t result = 0;
    if(start_word == end_word) {
        // log(OTTERS_LOG_DEBUG, "word = {:x}", start_mask & end_mask);
        storage[start_word] |= (start_mask & end_mask);
    } else {
        storage[start_word] |= (start_mask);
        for(auto i = start_word + 1; i < end_word; ++i) {
            storage[i] |= z_mask;
        }
        storage[end_word] |= end_mask;
    }
}

template<typename RoaringTraits, int dimensions>
struct DimensionRanges {
    constexpr static const int chunk_length = RoaringTraits::chunk_length;
    DimensionRanges(std::array<int64_t, dimensions> min, std::array<int64_t, dimensions> max, std::array<int64_t, dimensions> sizes)
            : _min(min), _max(max), _sizes(sizes), _current(min), _length(dimensions > 0 ? _max[dimensions - 1] - _min[dimensions - 1] : 0), _current_chunk_end(chunk_length) {
    }

    int64_t get_offset(std::array<int64_t, dimensions> current, std::array<int64_t, dimensions> dim_sizes) {
        int64_t offset = 0;
        int64_t multiplier = 1;
        mp11::mp_for_each<mp11::mp_iota_c<dimensions>>([&](auto I) {
            offset += multiplier * current[dimensions - 1 - I];
            multiplier *= dim_sizes[dimensions - 1 - I];
        });
        return offset;
    }

    void next_chunk(uint64_t* storage) {
        do {
            uint64_t start_pos = get_offset(_current, _sizes);
            uint64_t end_pos = start_pos + _length;
            if(start_pos >= _current_chunk_end || end()) {
                _current_chunk_end += chunk_length;
                break;
            } else {
                // log(OTTERS_LOG_DEBUG, "start_pos = {}, end_pos = {}", start_pos, end_pos);
                set_bit_range(storage,
                        std::max(_current_chunk_end - chunk_length, start_pos) % chunk_length,
                        std::min(_current_chunk_end, end_pos) % chunk_length);

                if(end_pos <= _current_chunk_end)
                    next_range();
                else
                    break; /* we reached the end of the chunk. */
            }
        } while(true);
    }

    template<size_t I>
    void next_range_impl() {
        ++_current[I];
        if(I > 0 && _current[I] >= _max[I]) {
            ++_current[I - 1];
            _current[I] = _min[I];
        }
    }

    void next_range() {
        if(dimensions >= 2)
            next_range_impl<dimensions - 2>();
        else if (dimensions == 1)
            _current[0] = _max[0] + 1;
    }

    bool end() {
        return _current[0] >= _max[0];
    }


    std::array<int64_t, dimensions> _min;
    std::array<int64_t, dimensions> _max;
    std::array<int64_t, dimensions> _sizes;
    std::array<int64_t, dimensions> _current;
    int64_t _length;
    uint64_t _current_chunk_end;
};

template<typename Iterator, int num_dims, size_t num_attrs, typename RoaringTraits = DefaultRoaringTraits>
void next_filter_mask(uint64_t* storage, DimensionRanges<RoaringTraits, num_dims>& dimension_ranges, std::array<Iterator, num_attrs>& iters) {
    if constexpr (num_dims == 0) {
        std::fill(storage, storage + 1024, 0xFFFF'FFFF'FFFF'FFFFUL);
    } else {
        dimension_ranges.next_chunk(storage);
    }
    for(auto i = 0u; i < iters.size(); ++i) {
        iters[i].compute_chunk();
        auto chunk_storage = (uint64_t*) iters[i].storage();
//        log(OTTERS_LOG_DEBUG, "BEFORE: storage[j] = {:x}, chunk_storage[j] = {:x}", storage[650], chunk_storage[650]);

        for(auto j = 0; j < 1024; ++j)
            storage[j] &= chunk_storage[j];
//        log(OTTERS_LOG_DEBUG, "storage[j] = {:x}, chunk_storage[j] = {:x}", storage[650], chunk_storage[650]);
        ++iters[i];
    }
}

template<typename Iterator, size_t num_attrs, typename RoaringTraits = DefaultRoaringTraits>
void filter_chunk(uint64_t* storage, std::array<Iterator, num_attrs>& iters) {
//    log(OTTERS_LOG_DEBUG, "called");
//    dimension_ranges.next_chunk(storage);
    std::uninitialized_fill(storage, storage + 1024, 0xFFFFFFFFFFFFFFFF);
    for(auto i = 0u; i < iters.size(); ++i) {
        iters[i].compute_chunk();
        auto chunk_storage = (uint64_t*) iters[i].storage();
//        log(OTTERS_LOG_DEBUG, "BEFORE: storage[j] = {:x}, chunk_storage[j] = {:x}", storage[0], chunk_storage[0]);
        for(auto j = 0; j < 1024; ++j)
            storage[j] &= chunk_storage[j];
//        log(OTTERS_LOG_DEBUG, "storage[j] = {:x}, chunk_storage[j] = {:x}", storage[0], chunk_storage[0]);
        ++iters[i];
    }
}

template<typename T>
struct MeanAggregator2 {
public:
    MeanAggregator2() {
        _sum = (T) 0;
        _size = 0;
    }
    void update(T t) {
        _sum += t;
        _size += 1;
    }

    void update(T sum, size_t count) {
        _sum += sum;
        _size += count;
    }

    T sum() {
        return _sum;
    }
    ssize_t size() {
        return _size;
    }
    T mean() {
        return sum() / (T) size();
    }
public:
    T _sum;
    ssize_t _size;
};

template<typename Bitmap, typename Iterator, typename Binner, int num_dims, size_t num_attrs, typename RoaringTraits = DefaultRoaringTraits>
MeanAggregator2<double> roaring_compute_mean_support(DimensionRanges<RoaringTraits, num_dims>& ranges,
        std::array<Iterator, num_attrs> iters, Bitmap& bitmap, Binner& binner, int timestep_id) {
    MeanAggregator2<double> aggregator;
    uint64_t storage[1024];
    for(auto i = 0; i < bitmap.num_of_chunks(); ++i) {
        //TODO: optimize
        std::fill(storage, storage + 1024, 0);
        next_filter_mask(storage, ranges, iters);

        for(auto j = 0; j < bitmap.num_of_buckets(); ++j) {
            auto chunk_id = bitmap.compute_chunk_id(i,j);
            auto chunk_type = bitmap.chunk_type(chunk_id);
            auto chunk_storage = bitmap.chunk_storage(chunk_id);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-align"
            uint64_t* chunk_storage64 = (uint64_t*)chunk_storage;
#pragma clang diagnostic pop            
            auto chunk_size = bitmap.chunk_size(chunk_id);
            size_t result = 0;
            if(chunk_type == RoaringChunkType::Uncompressed) {
                for(auto k = 0; k < 1024; ++k)
                    result += __builtin_popcountl(chunk_storage64[k] & storage[k]);
            } else if (chunk_type == RoaringChunkType::Array) {
                if(chunk_size == 0) {
                    continue;
                }
                for(auto k = 0u; k < chunk_size / 2; ++k) {
                    auto idx = chunk_storage[k];
                    auto word = idx / (sizeof(uint64_t) * 8);
                    auto bit = idx % (sizeof(uint64_t) * 8);
                    if(storage[word] & (1 << bit))
                        ++result;
                }
            } else if (chunk_type == RoaringChunkType::AllOne) {
                result += 65536;
            } else {
                assert(false);
            }
            // log(OTTERS_LOG_DEBUG, "bucket mean = {}", bitmap.mean_of_bucket(j));
            auto min = binner.min(timestep_id);
            auto max = binner.max(timestep_id);
            auto mean = min + (max - min) / binner.num_of_buckets() * j;
 
            aggregator.update(mean * (double) result, result);
            if (result == 65536)
                break;
        }
    }
    return aggregator;
}

template<typename Bitmap>
long compute_roaring_and_count( Bitmap& bitmap1, Bitmap& bitmap2) {
    long result = 0;
    for (auto j = 0; j < bitmap1.num_of_buckets(); ++j) {
        for (auto i = 0; i < bitmap1.num_of_chunks(); ++i) {
            auto chunk_id = bitmap1.compute_chunk_id(i,j);
            auto chunk_type1 = bitmap1.chunk_type(chunk_id);
            auto chunk_type2 = bitmap2.chunk_type(chunk_id);
            auto chunk_storage1 = bitmap1.chunk_storage(chunk_id);
            auto chunk_storage2 = bitmap2.chunk_storage(chunk_id);
            auto chunk_size1 = bitmap1.chunk_size(chunk_id);
            auto chunk_size2 = bitmap2.chunk_size(chunk_id);
            if(chunk_type1 == RoaringChunkType::AllOne)
                result += bitmap2.chunk_cardinality(chunk_id);
            else if (chunk_type2 == RoaringChunkType::AllOne)
                result += bitmap1.chunk_cardinality(chunk_id);
            else if (chunk_type1 == RoaringChunkType::Array && chunk_type2 == RoaringChunkType::Array) {
                auto idx1 = 0u; auto idx2 = 0u;
                auto size1 = chunk_size1 / sizeof(uint16_t); auto size2 = chunk_size2 / sizeof(uint16_t);
                while(idx1 < size1 && idx2 < size2) {
                    if(chunk_storage1[idx1] < chunk_storage2[idx2])
                        ++idx1;
                    else if(chunk_storage1[idx1] > chunk_storage2[idx2]) {
                        ++idx2;
                    } else {
                        ++result;
                        ++idx1;
                        ++idx2;
                    }
                }
            } else if (chunk_type1 == RoaringChunkType::Uncompressed && chunk_type2 == RoaringChunkType::Uncompressed) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-align"
                uint64_t* chunk_storage64_1 = (uint64_t*)chunk_storage1;
                uint64_t* chunk_storage64_2 = (uint64_t*)chunk_storage2;
#pragma clang diagnostic pop            
                for(auto idx = 0; idx < 1024; ++idx) {
                    result += __builtin_popcountl((chunk_storage64_1[idx] & chunk_storage64_2[idx]));
                }
            } else if (chunk_type1 == RoaringChunkType::Uncompressed && chunk_type2 == RoaringChunkType::Array) {
                result += 0;
            } else if (chunk_type1 == RoaringChunkType::Array && chunk_type2 == RoaringChunkType::Uncompressed) {
                result += 0;
            }
        }
    }
    return result;
}

}

#endif
