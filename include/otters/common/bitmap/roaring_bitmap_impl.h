#ifndef OTTERS_COMMON_BITMAP_ROARING_BITMAP_IMPL_H
#define OTTERS_COMMON_BITMAP_ROARING_BITMAP_IMPL_H

namespace otters {

template<typename Binner, template<typename> typename Container, typename RoaringTraits>
template<typename Iter>
std::pair<bool, std::string> ParaRoaringBitmap<Binner, Container, RoaringTraits>::verify_bitmap(Iter begin, Iter end) {
    // fmt::print("number of chunks = {}\n",num_of_chunks());
    for(auto i = 0; i < num_of_chunks(); ++i) {
        auto chunk_begin = begin + chunk_length * i;
        auto chunk_end = chunk_begin + chunk_length;
        if(i == num_of_chunks() - 1)
            chunk_end = end;
        auto chunk_result = verify_bitmap_chunk(i, chunk_begin, chunk_end);
        if(!chunk_result.first) {
            auto error_message = fmt::format("Error while checking chunk {}: {}", i, chunk_result.second);
            return std::make_pair(false, error_message);
        }
    }
    return std::make_pair(true, "");
}

template<typename Binner, template<typename> typename Container, typename RoaringTraits>
template<typename Iter>
std::pair<bool, std::string> ParaRoaringBitmap<Binner, Container, RoaringTraits>::verify_bitmap_chunk(int segment_id, Iter begin, Iter end) {
    //using T = typename std::decay<decltype(*begin)>::type;
    auto current_chunk_length = end - begin;
    std::array<int, chunk_length> bucket_ids;
    generate_bin_id(bucket_ids.begin(), begin, end);
    auto bucket_id_begin = bucket_ids.begin();
    auto bucket_id_end = bucket_ids.begin() + current_chunk_length;

    // fmt::print("number of buckets = {}\n",num_of_buckets());
    for(auto bucket_id = 0; bucket_id < num_of_buckets(); ++bucket_id) {
        auto chunk_id = compute_chunk_id(segment_id, bucket_id);
        auto cardinality = (int) std::count(bucket_id_begin, bucket_id_end, bucket_id);
        auto run = (int) std::count_if(bucket_id_begin, bucket_id_end, [last = (int32_t) -1, bucket_id](int32_t v) mutable {
            bool result = false;
            if(v != last && v == bucket_id)
                result = true;
            last = v;
            return result;
        });
        auto chunk_info = RoaringTraits::compute_chunk_info(cardinality, run);
        if(chunk_info.first != (RoaringChunkType)_types[chunk_id]) {
            auto error_message = fmt::format(
                "Chunk type error while compute chunk {}:{}, chunk_id {}: should be {}, got {}",
                    segment_id, bucket_id, chunk_id, chunk_info.first, (RoaringChunkType)_types[chunk_id]);
            return std::make_pair(false, error_message);
        }
        if(chunk_info.second != chunk_size(chunk_id)) {
            auto error_message = fmt::format(
                "Chunk length error while compute chunk {}:{}, chunk_id {}: should be {}, got {}",
                    segment_id, bucket_id, chunk_id, chunk_info.second, chunk_size(chunk_id));
            error_message += fmt::format(" chunk type: {}", chunk_info.first);
            return std::make_pair(false, error_message);
        }

        switch(chunk_info.first) {
            case RoaringChunkType::Uncompressed: {
                auto result = verify_uncompressed_chunk(
                        bucket_id, bucket_id_begin, bucket_id_end, chunk_storage(chunk_id), chunk_size(chunk_id));
                if(!result.first) {
                    auto error_message = fmt::format("Error while checking uncompressed chunk {}:{}, chunk_id {}: {}",
                        segment_id, bucket_id, chunk_id, result.second);
                    return std::make_pair(false, error_message);
                }
                break;
            }
            case RoaringChunkType::Array: {
                auto result = verify_array_chunk(
                        bucket_id, bucket_id_begin, bucket_id_end, chunk_storage(chunk_id), chunk_size(chunk_id) / sizeof(uint16_t));
                if(!result.first) {
                    auto error_message = fmt::format("Error while checking array chunk {}:{}, chunk_id {}: {}",
                        segment_id, bucket_id, chunk_id, result.second);
                    return std::make_pair(false, error_message);
                }
                break;
            }
            case RoaringChunkType::Run: {
                auto result = verify_run_chunk(
                        bucket_id, bucket_id_begin, bucket_id_end, chunk_storage(chunk_id), chunk_size(chunk_id) / sizeof(uint16_t));
                if (!result.first) {
                    auto error_message = fmt::format("Error while checking run chunk {}:{}, chunk_id {}: {}",
                        segment_id, bucket_id, chunk_id, result.second);
                    return std::make_pair(false, error_message);
                }
                break;
            }
            case RoaringChunkType::AllOne: {
                break;
            }
        }

    }
    return std::make_pair(true, "");
}

template<typename Binner, template<typename> typename Container, typename RoaringTraits>
template<typename Iter>
std::pair<bool, std::string> ParaRoaringBitmap<Binner, Container, RoaringTraits>::
        verify_uncompressed_chunk(int bucket_id, Iter begin, Iter end, StorageWord* storage, ChunkSize storage_size) {
    auto size = end - begin;
    uint32_t word = 0;
    auto num_of_words = static_cast<int>(sizeof(*storage) * 8);
    if(storage_size < size / num_of_words) {
        auto error_message = fmt::format("storage size should be at least {}, got {}",
            storage_size, num_of_words);
        return std::make_pair(false, error_message);
    }
    for(auto idx = 0; idx < size; ++idx) {
        if(idx % (sizeof(*storage) * 8) == 0)
            word = storage[idx / (sizeof(*storage) * 8)];
        bool bit = ((word >> (idx % (sizeof(*storage) * 8))) & 1);
        if(bit != (begin[idx] == bucket_id)) {
            auto error_message = fmt::format("bit {} == bucket_id {}, should be {}, but got bucket {}, word is {}",
                idx, bucket_id, bit, begin[idx], word);
            return std::make_pair(false, error_message);
        }
    }
    return std::make_pair(true, "");
}

template<typename Binner, template<typename> typename Container, typename RoaringTraits>
template<typename Iter>
std::pair<bool, std::string> ParaRoaringBitmap<Binner, Container, RoaringTraits>::
        verify_array_chunk(int bucket_id, Iter begin, Iter end, StorageWord* storage, ChunkSize storage_size) {
    auto size = end - begin;
    uint16_t* array_storage = reinterpret_cast<uint16_t*>(storage);
    auto last = 0;
    for(auto i = 0u; i < storage_size; ++i) {
        auto idx = array_storage[i];
        auto result = verify_bit_range(begin, last, idx, bucket_id, false);
        if(!result.first)
            return result;
        auto result2 = verify_bit_range(begin, idx, idx + 1, bucket_id, true);
        if(!result2.first)
            return result;
        last = idx + 1;
    }
    auto result = verify_bit_range(begin, last, size, bucket_id, false);
    if(!result.first)
        return result;
    return std::make_pair(true, "");
}

template<typename Binner, template<typename> typename Container, typename RoaringTraits>
template<typename Iter>
std::pair<bool, std::string> ParaRoaringBitmap<Binner, Container, RoaringTraits>::
        verify_run_chunk(int bucket_id, Iter begin, Iter end, StorageWord* storage, ChunkSize storage_size) {
    auto size = end - begin;
    uint16_t* array_storage = reinterpret_cast<uint16_t*>(storage);
    auto last = 0;
    std::string error;
    error += fmt::format("; storage = {:x};", *storage);
    error += fmt::format(" chunk_offset[64] = {};", chunk_offset(64));
    error += fmt::format(" storage_size = {};", storage_size);
    for(auto i = 0u; i < storage_size; i += 2) {
        auto start = array_storage[i];
        auto run_length = array_storage[i + 1] + 1;
        error += fmt::format(" i = {} run start = {}; run length = {};", i, start, run_length);
        auto result = verify_bit_range(begin, last, start, bucket_id, false);
        if(!result.first) {
            result.second = result.second +
                fmt::format(error + " last = {} ", last);
            return result;
        }
        auto result2 = verify_bit_range(begin, start, start + run_length, bucket_id, true);
        if(!result2.first) {
            result.second = result.second +
                fmt::format("run start = {}, run length = {}", start, run_length);
            return result;
        }
        last = start + run_length;
    }
    auto result = verify_bit_range(begin, last, size, bucket_id, false);
    if(!result.first) {
        result.second = result.second
            + fmt::format("last = {}, size = {}", last, size) + error;
        return result;
    }
    return std::make_pair(true, "");
}

template<typename Binner, template<typename> typename Container, typename RoaringTraits>
ssize_t ParaRoaringBitmap<Binner, Container, RoaringTraits>::chunk_and(
        const ParaRoaringBitmap& lhs,
        int left_bucket,
        const ParaRoaringBitmap& rhs,
        int right_bucket) {
    auto Uncompressed = RoaringChunkType::Uncompressed;
    auto Array = RoaringChunkType::Array;
    auto Run = RoaringChunkType::Run;

    //reserve data
    auto left_type = (RoaringChunkType) lhs.types()[left_bucket];
    auto right_type = (RoaringChunkType) rhs.types()[right_bucket];
    auto left_size = lhs.chunk_size(left_bucket);
    auto right_size = rhs.chunk_size(right_bucket);
    auto left_storage = lhs.chunk_storage(left_bucket);
    auto right_storage = rhs.chunk_storage(right_bucket);
    if (left_type == Uncompressed && right_type == RoaringChunkType::Uncompressed) {
        return do_chunk_and_uncompressed_uncompressed(left_storage, left_size, right_storage, right_size);
    } else if (left_type == Array && right_type == Array) {
        return do_chunk_and_array_array(left_storage, left_size, right_storage, right_size);
    } else if (left_type == Run && right_type == Run) {
        return do_chunk_and_run_run(left_storage, left_size, right_storage, right_size);
    } else if (left_type == Uncompressed && right_type == Array) {
        return do_chunk_and_uncompressed_array(left_storage, left_size, right_storage, right_size);
    } else if (left_type == Array && right_type == Uncompressed) {
        return do_chunk_and_uncompressed_array(right_storage, right_size, left_storage, left_size);
    } else if (left_type == Uncompressed && right_type == Run) {
        return do_chunk_and_uncompressed_run(left_storage, left_size, right_storage, right_size);
    } else if (left_type == Run && right_type == Uncompressed) {
        return do_chunk_and_uncompressed_run(right_storage, right_size, left_storage, left_size);
    } else if (left_type == Array && right_type == Run) {
        return do_chunk_and_array_run(left_storage, left_size, right_storage, right_size);
    } else if (left_type == Run && right_type == Array) {
        return do_chunk_and_array_run(right_storage, right_size, left_storage, left_size);
    } else {
        assert(false);
    }
}

template<typename Binner, template<typename> typename Container, typename RoaringTraits>
ssize_t ParaRoaringBitmap<Binner, Container, RoaringTraits>::do_chunk_and_uncompressed_uncompressed(
        const StorageWord* chunk1, 
        int size1,
        const StorageWord* chunk2,
        int size2) {
    // assert(size1 == )
    assert(size1 == size2);
    assert(size1 == RoaringTraits::max_container_size * sizeof(StorageWord));
    auto* target_storage = allocate_chunk_at_end(RoaringChunkType::Uncompressed, size1 / 2);
    for(auto i = 0; i < size1 / sizeof(StorageWord); ++i) {
        target_storage[i] = (chunk1[i] & chunk2[i]);
    }
    return size1;
}

template<typename Binner, template<typename> typename Container, typename RoaringTraits>
ssize_t ParaRoaringBitmap<Binner, Container, RoaringTraits>::do_chunk_and_array_array(
        const StorageWord* chunk1, 
        int size1,
        const StorageWord* chunk2,
        int size2) {
    // assert(size1 == )
    auto* target_storage = allocate_chunk_at_end(RoaringChunkType::Array, std::min(size1, size2));
    auto target_ptr = std::set_intersection(chunk1, chunk1 + size1, chunk2, chunk2 + size2, target_storage);
    auto target_size = target_ptr - target_storage;
    update_last_chunk_size(target_size);
    
    return target_size;
}


template<typename Binner, template<typename> typename Container, typename RoaringTraits>
ssize_t ParaRoaringBitmap<Binner, Container, RoaringTraits>::do_chunk_and_run_run(
        const StorageWord* chunk1, 
        int size1,
        const StorageWord* chunk2,
        int size2) {
    thread_local std::array<Run, RoaringTraits::max_run_size * 2> scratch_storage;

    // merge to scratch space
    auto scratch_runs = (Run*) scratch_storage.data();
    auto left_runs = (const Run*) chunk1;
    auto right_runs = (const Run*) chunk2;
    auto lsize  = size1 / 4; // FIXME: this assumes StorageWord* is uint16_t
    auto rsize = size2 / 4;
    // fmt::print("lsize = {}\n", lsize);
    // fmt::print("rsize = {}\n", rsize);
    auto ssize = 0;
    auto total_cardinality = 0;
    auto lidx = 0;
    auto ridx = 0;

    while(lidx < lsize && ridx < rsize) {
        //quick and dirty, need refractor
        const Run& lrun = left_runs[lidx];
        const Run& rrun = right_runs[ridx];
        // fmt::print("left run : [{},{}), size = {}\n", lrun.start(), lrun.end(), lrun.size());
        // fmt::print("right run : [{},{}), size = {}\n", rrun.start(), rrun.end(), rrun.size());
        if(lrun.before(rrun)) {
            ++lidx;
            continue;
        } else if (rrun.before(lrun)) {
            ++ridx;
            continue;
        } else {
            auto new_run = lrun.intersect(rrun);
            // fmt::print("produced new run : [{},{})\n", new_run.start(), new_run.end());
            if(lrun.end() == rrun.end()) {
                ++lidx; ++ridx;
            } else if(lrun.end() < rrun.end())
                ++lidx;
            else
                ++ridx;
            scratch_runs[ssize++] = new_run;
            total_cardinality += new_run.size();
        }
    }

    if(total_cardinality < ssize * 2) {
        // convert the runs
        auto* chunk_storage = allocate_chunk_at_end(RoaringChunkType::Array, total_cardinality);
        for (auto i = 0; i < ssize; ++i) {
            auto run = scratch_runs[i];
            for (auto pos = run.start(); pos < run.end(); ++pos) {
                *chunk_storage++ = pos;
            }
        }
        return total_cardinality;
    } else {
        auto chunk_storage = allocate_chunk_at_end(RoaringChunkType::Run, ssize * 2);
        std::copy(scratch_runs, scratch_runs + ssize, (Run*) chunk_storage);
        return ssize * 2;
    }
}

template<typename Binner, template<typename> typename Container, typename RoaringTraits>
ssize_t ParaRoaringBitmap<Binner, Container, RoaringTraits>::do_chunk_and_uncompressed_array(
        const StorageWord* chunk1, 
        int size1,
        const StorageWord* chunk2,
        int size2) {
    // fmt::print("Size of chunk2 = {}\n", size2);
    // we allocate an array chunk as large as the array chunk
    assert(size1 == RoaringTraits::max_container_size * 2);
    auto* target_storage = allocate_chunk_at_end(RoaringChunkType::Array, size2);
    auto chunk_size = 0;

    for(int i = 0; i < size2 / 2; ++i) {
        auto idx = chunk2[i];
        if(retrieve_uncompressed_bit(chunk1, idx)) {
            target_storage[chunk_size++] = idx;
        }
    }

    update_last_chunk_size(chunk_size);
    // fmt::print("generated chunk size = {}\n", size2);

    return chunk_size;
}

template<typename Binner, template<typename> typename Container, typename RoaringTraits>
ssize_t ParaRoaringBitmap<Binner, Container, RoaringTraits>::do_chunk_and_uncompressed_run(
        const StorageWord* chunk1, 
        int size1,
        const StorageWord* chunk2,
        int size2) {
    assert(size1 == RoaringTraits::max_container_size * 2);
    auto cardinality = count_run_cardinality(chunk2, size2 / 2);
    if(cardinality > RoaringTraits::max_array_size) {
        auto target_chunk = allocate_chunk_at_end(RoaringChunkType::Uncompressed, RoaringTraits::max_container_size);
        std::copy(chunk1, chunk1 + RoaringTraits::max_container_size, target_chunk);
        auto runs = (const Run*) chunk2;
        auto run_size = size2 / 4;
        auto last_end = 0;
        for(auto i = 0; i < run_size; ++i) {
            auto run = runs[i];
            clear_uncompressed_range(target_chunk, last_end, run.start());
            last_end = run.end();
        }
        clear_uncompressed_range(target_chunk, last_end, RoaringTraits::chunk_length);

        return RoaringTraits::max_container_size;
    } else {
        auto* target_chunk = allocate_chunk_at_end(RoaringChunkType::Array, RoaringTraits::max_array_size);
        auto target_size = 0;
        auto runs = (const Run*) chunk2;
        auto run_size = size2 / 4;
        for(auto i = 0; i < run_size; ++i) {
            auto run = runs[i];
            for(auto j = run.start(); j < run.end(); ++j) {
                if(retrieve_uncompressed_bit(chunk1, j)) {
                    target_chunk[target_size++] = j;
                }
            }
        }
        update_last_chunk_size(target_size);
        return target_size;
    }

}

template<typename Binner, template<typename> typename Container, typename RoaringTraits>
ssize_t ParaRoaringBitmap<Binner, Container, RoaringTraits>::do_chunk_and_array_run(
        const StorageWord* chunk1, 
        int size1,
        const StorageWord* chunk2,
        int size2) {
    auto* target_chunk = allocate_chunk_at_end(RoaringChunkType::Array, RoaringTraits::max_container_size);
    ssize_t target_size = 0;

    auto* runs = (const Run*) chunk2;
    auto run_size = size2 / 4;
    auto run_idx = 0;
    auto current_run = runs[run_idx++];
    for(auto i = 0; i < size1; ++i) {
        if(chunk1[i] >= current_run.start() && chunk1[i] < current_run.end()) {
            target_chunk[target_size++] = chunk1[i];
        } else if(run_idx < run_size && chunk1[i] >= current_run.end()) {
            current_run = runs[run_idx++];
        }
    }
    update_last_chunk_size(target_size);
    return target_size;
}


template<typename Binner, template<typename> typename Container, typename RoaringTraits>
ssize_t ParaRoaringBitmap<Binner, Container, RoaringTraits>::chunk_popcount(int target_chunk_id) {
    auto type = chunk_type(target_chunk_id);
    if(type == RoaringChunkType::Array) {
        return chunk_size(target_chunk_id) / 2;
    } else if (type == RoaringChunkType::Run) {
        return chunk_size(target_chunk_id) / 4;
    } else {
        assert(type == RoaringChunkType::Uncompressed);
        auto chunk_ptr = (uint64_t*) chunk_storage(target_chunk_id);
        ssize_t result = 0;
        for(auto i = 0; i < RoaringTraits::max_container_size * sizeof(StorageWord) / sizeof(uint64_t); ++i) {
            result += __builtin_popcountl(chunk_ptr[i]);
        }
        return result;
    }
}

}

#endif
