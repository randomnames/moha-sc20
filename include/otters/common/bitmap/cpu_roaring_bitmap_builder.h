#pragma once

#ifndef OTTERS_COMMON_BITMAP_CPU_ROARING_BITMAP_BUILDER_H
#define OTTERS_COMMON_BITMAP_CPU_ROARING_BITMAP_BUILDER_H
#include <boost/asio/io_service.hpp>
#include <boost/thread/thread.hpp>
#include <boost/asio/post.hpp>
#include <iostream>
#include <roaring/roaring.hh>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

namespace otters {

template<typename Count, int NBuckets = 64>
class CPURoaringBitmapBuilder {
public:
    CPURoaringBitmapBuilder(int workers = 1)
    : _work(boost::asio::make_work_guard(_io_service)) {
        //std::cerr << "parent pid = " << pthread_self() << std::endl;
        for(int i = 0; i < workers; ++i) {
            _thread_pool.create_thread([id=i,this]() {
                _io_service.run();
                printf("thread %d exited\n", id);
            });
        }
    }
    template<typename Buffer>
    void submit_buffer(Buffer&& buffer, int size) {
        boost::asio::post(_io_service.get_executor(), [buf(std::move(buffer)), size]() {
            //std::cerr << "using pid = " << pthread_self() << std::endl;
            std::vector<uint32_t> bins[NBuckets]; 
            size_t total_bytes = 0;
            for(int i = 0; i < NBuckets; ++i)
                bins[i].reserve(65536);
            for(int i = 0; i < size; ++i) {
                bins[buf[i]].push_back(i);
            }
            for(int i = 0; i < NBuckets; ++i) {
                Roaring bitmap(bins[i].size(), bins[i].data());
                bitmap.runOptimize();
                total_bytes += bitmap.getSizeInBytes();
            }
            //std::cerr << "total_bytes = " << total_bytes << "\n";
            // std::cerr << "submitted a buffer\n";
            //do some processing
        });
    }

    ~CPURoaringBitmapBuilder() {
        _work.reset();
        _io_service.stop();
        _thread_pool.join_all();
    }

private:
    boost::asio::io_context _io_service;
    boost::thread_group _thread_pool;
    boost::asio::executor_work_guard<boost::asio::io_context::executor_type> _work;
};

}

#endif
