#pragma once

#ifndef OTTERS_COMMON_BITMAP_ROARING_BITMAP_H
#define OTTERS_COMMON_BITMAP_ROARING_BITMAP_H

#include <memory>
#include <string>
#include <algorithm>

/* fmt generates a bunch of warnings for floats, so...*/
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#pragma clang diagnostic ignored "-Werror"
#include <fmt/core.h>
#include <fmt/ostream.h>
#pragma clang diagnostic pop

#include <exception>
#include <stdexcept>
#include <iostream>
#include <fstream>

#include <otters/common/utils/log.h>
#include <otters/common/io/serialization.h>

namespace otters {

enum class RoaringChunkType : uint8_t { Uncompressed = 0, Run = 1, Array = 2, AllOne = 3 };

std::ostream& operator<<(std::ostream& os, RoaringChunkType type) {
    switch(type) {
        case RoaringChunkType::Uncompressed:
            os << "Uncompressed"; break;
        case RoaringChunkType::Run:
            os << "Run"; break;
        case RoaringChunkType::Array:
            os << "Array"; break;
        case RoaringChunkType::AllOne:
            os << "AllOne"; break;
        // default:
            // throw std::logic_error("Unexpected chunk types.");
    }
    return os;
}

struct DefaultRoaringTraits {
    using ChunkType = uint8_t;
    using ChunkSize = uint32_t;
    using ChunkAddress = uint32_t;
    using StorageWord = uint16_t;

    static const int block_threads = 64;
    static const int chunk_length = 65536;
    static const int max_container_size = chunk_length / (sizeof(StorageWord) * 8);
    static const int max_array_size = max_container_size;
    static const int max_run_size = max_array_size / 2;

    static ssize_t num_of_chunks(ssize_t size) {
        return (size + chunk_length - 1) / chunk_length;
    }
    static std::pair<RoaringChunkType, ChunkSize> compute_chunk_info(int cardinality, int run) {
        (void) run;
        auto result = std::make_pair(RoaringChunkType::Uncompressed, chunk_length / 8);
        if(cardinality == chunk_length) {
            result.first = RoaringChunkType::AllOne;
            result.second = 0;
        } else if(cardinality * 2 < result.second) {
            result.first = RoaringChunkType::Array;
            result.second = cardinality * 2;
        }
        // if(run * 4 < result.second) {
        //     result.first = RoaringChunkType::Run;
        //     result.second = run * 4;
        // } 
        return result;
    }
};

enum class RoaringRangeType {
    Uncompressed,
    Run,
    Array
};

template<typename RoaringTraits>
struct RoaringRange {
    using StorageWord = typename RoaringTraits::StorageWord;
    uint64_t _start;
    uint64_t _end;
    RoaringRangeType _type;
    uint16_t _size;
    StorageWord* _storage;

    RoaringRange() = default;
    void set_uncompressed(uint64_t* words) {
        _start = 0;
        _end = 65536;
        _type = RoaringRangeType::Uncompressed;
        _size = 65536 / 64;
        _storage = (StorageWord*) words;
    }

    std::string format() {
        return fmt::format("_start = {}, _end = {}, _type = {}, _size = {}", _start, _end, (int)_type, _size);
    }
};

template<typename Binner, template<typename> typename Container, typename RoaringTraits = DefaultRoaringTraits>
class ParaRoaringBitmap {
public:
    static const int chunk_length = RoaringTraits::chunk_length;

    using ChunkAddress = typename RoaringTraits::ChunkAddress;
    using ChunkSize = typename RoaringTraits::ChunkSize;
    using ChunkType = typename RoaringTraits::ChunkType;
    using StorageWord = typename RoaringTraits::StorageWord;

    struct Run {
        StorageWord _start;
        StorageWord _size;

        StorageWord start() const {
            return _start;
        }

        int end() const {
            return (int) start() + size();
        }

        StorageWord size() const {
            return _size + 1;
        }

        bool before(const Run& rhs) const {
            return end() <= rhs._start;
        }

        Run intersect(const Run& rhs) const {
            auto istart = std::max(start(), rhs.start());
            auto iend = std::min(end(), rhs.end());
            return Run{istart, iend - istart - 1};
        }
    };

    struct OrIterator {
        using range_type = RoaringRange<RoaringTraits>;
        OrIterator(ParaRoaringBitmap& bitmap, int start_bucket, int end_bucket)
            : _bitmap(&bitmap),
              _chunk_id(0),
              _total_chunks((int) bitmap.num_of_chunks()),
              _min_bucket_id(start_bucket),
              _max_bucket_id(end_bucket),
              _computed(false) {
            // log(OTTERS_LOG_DEBUG, "total_chunks = {}", _total_chunks);
        }

        OrIterator() = default;

        uint64_t max() {
            return (_chunk_id + 1) * chunk_length;
        }
        uint64_t min() {
            return (_chunk_id) * chunk_length;
        }
        bool end() {
            return _chunk_id == _total_chunks;
        }
        void compute_chunk() {
            if (_computed == false) {
                _computed = true;
                if(_min_bucket_id == 0 && _max_bucket_id == _bitmap->num_of_buckets()) {
                    std::fill(_storage, _storage + 1024, 0xFFFFFFFFFFFFFFFF);
                } else {
                    std::fill(_storage, _storage + 1024, 0x0);
                    for (auto i = _min_bucket_id; i < _max_bucket_id; ++i) {
                        auto chunk_id = _bitmap->compute_chunk_id(_chunk_id, i);
                        _bitmap->or_chunk_to_uncompressed(_storage, chunk_id);
                    }
                }
            }
        }

        range_type range() {
            compute_chunk();
            range_type rt;
            rt._start = min();
            rt._end = max();
            rt._type = RoaringRangeType::Uncompressed;
            rt._size = chunk_length / 8;
            rt._storage = (uint16_t*) _storage;
            return rt;
        }

        uint64_t* storage() {
            return _storage;
        }

        OrIterator& operator++() {
            ++_chunk_id;
            _computed = false;
            return *this;
        }
        uint64_t _storage[1024];
        ParaRoaringBitmap* _bitmap;
        int _chunk_id;
        int _total_chunks;
        int _min_bucket_id;
        int _max_bucket_id;
        bool _computed;
    };
    
    static_assert(sizeof(Run) == sizeof(StorageWord) * 2);

    struct serialize_size_type {
        ssize_t _size;
        ssize_t _storage_size;
    };
public:
    ParaRoaringBitmap(
            Binner binner,
            ssize_t size,
            Container<ChunkSize>&& offsets,
            Container<ChunkType>&& types,
            Container<StorageWord>&& storage)
        : _offsets(std::move(offsets)),
          _types(std::move(types)),
          _storage(std::move(storage)),
          _binner(binner),
          _size(size) {
    }

    ParaRoaringBitmap(Binner binner) : _binner(binner), _size(0) {
    }

    ParaRoaringBitmap(serialize_size_type serialize_size) {
        resize(serialize_size);
    }

    ParaRoaringBitmap() : _size(0) {
    }

    template<typename Iter>
    std::pair<bool, std::string> verify_bitmap(Iter begin, Iter end);

    template<typename Iter>
    std::pair<bool, std::string> verify_bitmap_chunk(int segment_id, Iter begin, Iter end);

 
public:
    auto& offsets() { return _offsets; }
    const auto& offsets() const { return _offsets; }
    
    auto& types() { return _types; }
    const auto& types() const { return _types; }

    auto& storage() { return _storage; }
    const auto& storage() const { return _storage; }

    auto storage_size() const { return _storage.size(); }

    ssize_t size() const { return _size; }

    ssize_t num_of_chunks() const {
        return RoaringTraits::num_of_chunks(_size);
    }

    int num_of_buckets() const { return _binner.num_of_buckets(); }

    auto binner() const { return _binner; }

    ssize_t serialize_size() const {
        ssize_t result = 0;
        result += otters::serialize_size(_size);
        result += otters::serialize_size(_offsets);
        result += otters::serialize_size(_types);
        // result += otters::serialize_size(_binner);
        result += otters::serialize_size(_storage);
        return result;
    }

    span<char> serialize(span<char> buffer) const {
        assert(buffer.size() >= serialize_size());
        buffer = otters::serialize(buffer, _size);
        buffer = otters::serialize(buffer, _offsets);
        buffer = otters::serialize(buffer, _types);
        // buffer = otters::serialize(buffer, _binner);
        buffer = otters::serialize(buffer, _storage);
        return buffer;
    }

    span<char> deserialize(span<char> buffer) {
        assert(buffer.size() >= serialize_size());
        buffer = otters::deserialize(_size, buffer);
        buffer = otters::deserialize(_offsets, buffer);
        buffer = otters::deserialize(_types, buffer);
        // buffer = otters::deserialize(_binner, buffer);
        buffer = otters::deserialize(_storage, buffer);
        return buffer;
    }

    void resize(const serialize_size_type& serialize_size) {
        _size = serialize_size._size;
        offsets().resize(num_of_chunks() * num_of_buckets() + 1);
        types().resize(num_of_chunks() * num_of_buckets());
        storage().resize(serialize_size._storage_size);
    }

    ssize_t chunk_and(const ParaRoaringBitmap& lhs, int left_bucket, const ParaRoaringBitmap& rhs, int right_bucket);

    void add_chunk_from_bitmap(uint64_t* bitmap) {
        auto cardinality = count_bitmap_cardinality(bitmap);
        auto run_number = count_run_number(bitmap);
        // fmt::print(stderr, "cardinality = {}\n", cardinality);
        // fmt::print(stderr, "run_number = {}\n", run_number);

        auto max_container_size = RoaringTraits::max_container_size;
        auto total_words = RoaringTraits::chunk_length / sizeof(uint64_t) / 8;
        if(cardinality > RoaringTraits::max_array_size && run_number * 2 >= max_container_size) {
            // fmt::print(stderr, "allocating uncompressed = {}\n", run_number);
            auto chunk_ptr = allocate_chunk_at_end(RoaringChunkType::Uncompressed, max_container_size);
            std::copy((uint16_t*)bitmap, (uint16_t*)bitmap + total_words * 4, chunk_ptr);
        } else if(cardinality <= RoaringTraits::max_array_size && run_number >= cardinality / 2) {
            // fmt::print(stderr, "allocating array = {}\n", run_number);
            auto chunk_ptr = allocate_chunk_at_end(RoaringChunkType::Array, cardinality);
            // fmt::print(stderr, "storage.size() = {}", _storage.size());
            auto chunk_size = 0;
            for (auto i = 0u; i < total_words; ++i) {
                auto word = bitmap[i];
                auto word_ctz = 0;
                auto word_bits = sizeof(uint64_t) * 8;
                // fmt::print("i = {}\n",i);
                while ((word_ctz = __builtin_ffsll(word)) != 0) {
                    auto one_bit = word_ctz - 1;
                    chunk_ptr[chunk_size++] = (uint16_t) (word_bits * i + one_bit);
                    word &= (~(1ull << one_bit));
                    // fmt::print("one_bit = {0}, word = {1:x}\n", one_bit, word);
                }
            }
        } else {
            // fmt::print(stderr, "allocating run = {}\n", run_number);
            // algorithm 2 from https://arxiv.org/pdf/1603.06549.pdf
            auto chunk_ptr = allocate_chunk_at_end(RoaringChunkType::Run, run_number * 2);
            auto runs = (Run*)chunk_ptr;
            auto run_size = 0;
            uint64_t i = 0;
            auto t = bitmap[0];
            while (i < 1024) {
                if (t == 0) {
                    ++i;
                    t = bitmap[i];
                } else {
                    auto j = __builtin_ffsll(t);
                    if (j == 0) {
                        j = 64;
                    } else {
                        j -= 1;
                    }
                    auto x = j + 64 * i;
                    t = (t | (t - 1));
                    while (i + 1 < total_words && t == 0xFFFFFFFFFFFFFFFF) {
                        ++i;
                        t = bitmap[i];
                    }
                    auto y = 0;
                    if (t == 0xFFFFFFFFFFFFFFFF) {
                        assert(i == 1023);
                        y = 65536;
                    } else {
                        auto k = __builtin_ffsll(~t);
                        if (k == 0) {
                            k = 64;
                        } else {
                            k -= 1;
                        }
                        y = (int) (k + 64 * i);
                    }
                    Run new_run{ (StorageWord)x, (StorageWord) (y - x - 1) };
                    // fmt::print("adding run x = {} y = {} length = {} run_size = {}\n", x, y, y - x - 1, run_size);
                    runs[run_size++] = new_run;
                    t = (t & (t + 1));
                }
            }
            assert(run_size == run_number);
        }
        _size += RoaringTraits::chunk_length;
    }

    ssize_t chunk_popcount(int target_chunk_id);

    void clear() {
        _size = 0;
        _offsets.clear();
        _types.clear();
        _storage.clear();
    }

    OrIterator begin_or_iterator(int start_bucket, int end_bucket) {
        return OrIterator(*this, start_bucket, end_bucket);
    }

    void or_chunk_to_uncompressed(uint64_t* storage, int chunk_id) {
        // log(OTTERS_LOG_DEBUG, "chunk_id = {}", chunk_id);
        auto type = chunk_type(chunk_id);
        // log(OTTERS_LOG_DEBUG, "chunk_type = {}", (int) type);
        switch(type) {
            case RoaringChunkType::AllOne: {
                std::fill(storage, storage + chunk_length / sizeof(uint64_t) / 8, 0xFFFFFFFFFFFFFFFF);
                break;
            }
            case RoaringChunkType::Array: {
                auto size = chunk_size(chunk_id);
                if(size == 0)
                    return;
                uint16_t* cstorage = chunk_storage(chunk_id);
                for(auto i = 0u; i < size / 2; ++i) {
                    auto idx = cstorage[i];
                    auto word = idx / (sizeof(uint64_t) * 8);
                    auto bit = idx % (sizeof(uint64_t) * 8);
                    // log(OTTERS_LOG_DEBUG, "got idx {}, storage word = {}", cstorage[i], storage[word]);
                    storage[word] |= ((1 << bit));
                }
                break;
            }
            case RoaringChunkType::Uncompressed: {

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-align"
                auto* cstorage = (uint64_t*) chunk_storage(chunk_id);
#pragma clang diagnostic pop
                for(auto i = 0u; i < (chunk_length / sizeof(uint64_t) / 8); ++i) {
                    // log(OTTERS_LOG_DEBUG, "got word {}", cstorage[i]);
                    storage[i] |= cstorage[i];
                }
                break;
            }

            case RoaringChunkType::Run:
                assert(false);
        }
    }

    void set_size(size_t size) {
        _size = size;
    }

    int compute_chunk_id(int segment_id, int bucket_id) {
        return segment_id * num_of_buckets() + bucket_id;
    }

    auto chunk_size(int chunk_id) const {
        return _offsets[chunk_id + 1] - _offsets[chunk_id];
    } 

    ssize_t chunk_cardinality(int chunk_id) {
        auto type = chunk_type(chunk_id);
        switch(type) {
            case RoaringChunkType::AllOne:
                return 65536;
            case RoaringChunkType::Array:
                return chunk_size(chunk_id) / 2;
            case RoaringChunkType::Run:
                return count_run_cardinality(chunk_storage(chunk_id), chunk_size(chunk_id) / sizeof(StorageWord));
            #pragma clang diagnostic push
            #pragma clang diagnostic ignored "-Wcast-align"
            case RoaringChunkType::Uncompressed:
                return count_bitmap_cardinality((uint64_t*) chunk_storage(chunk_id));
            #pragma clang diagnostic pop 
        }
    }

    auto chunk_offset(int chunk_id) const {
        return _offsets[chunk_id];
    }

    RoaringChunkType chunk_type(int chunk_id) const {
        return (RoaringChunkType)_types[chunk_id];
    }

    auto chunk_storage(int chunk_id) {
        return _storage.data() + chunk_offset(chunk_id) / sizeof(StorageWord);
    }

    const StorageWord* chunk_storage(int chunk_id) const {
        return _storage.data() + chunk_offset(chunk_id) / sizeof(StorageWord);
    }

    auto mean_of_bucket(int timestep_id, int bucket_id) {
        auto min = _binner.min(timestep_id);
        auto max = _binner.max(timestep_id);
        return min + (max - min) / num_of_buckets() * bucket_id;
    }

    void print_info() {
        log(OTTERS_LOG_WARNING, "total_size = {}", _offsets[(int)_offsets.size() - 1l]);
        for(auto i = 0; i < num_of_chunks(); ++i) {
            for(auto j = 0; j < num_of_buckets(); ++j) {
                auto chunk_id = compute_chunk_id(i, j);
                auto type = chunk_type(chunk_id);
                auto cardinality = chunk_cardinality(chunk_id);
                auto size = chunk_size(chunk_id);
                log(OTTERS_LOG_WARNING, "chunk = [{},{}], type = {}, cardinality = {}, size in bytes = {}",
                    i, j, type, cardinality, size);
            }
        }

    }
private:
    template<typename InputIt, typename OutputInt>
    void generate_bin_id(OutputInt oit, InputIt begin, InputIt end) {
        std::transform(begin, end, oit, binner());
    }




    template<typename Iter>
    std::pair<bool, std::string> verify_uncompressed_chunk(int bucket_id, Iter begin, Iter end, StorageWord* storage, ChunkSize size);

    template<typename Iter>
    std::pair<bool, std::string> verify_array_chunk(int bucket_id, Iter begin, Iter end, StorageWord* storage, ChunkSize size);

    template<typename Iter>
    std::pair<bool, std::string> verify_run_chunk(int bucket_id, Iter begin, Iter end, StorageWord* storage, ChunkSize size);

    template<typename Iter>
    std::pair<bool, std::string> verify_bit_range(Iter data, size_t start, size_t end, int bucket_id, bool value) {
        for(auto i = start; i < end; ++i) {
            if ((data[i] == bucket_id) != value) {
                auto error_message = fmt::format("Bit {} == bucketid {} should be {}, instead got {}",
                    i, bucket_id, value, data[i] == bucket_id);
                return std::make_pair(false, error_message);
            }
        }
        return std::make_pair(true, "");
    }

    ssize_t do_chunk_and_uncompressed_uncompressed(
            const StorageWord* chunk1,
            int size1,
            const StorageWord* chunk2,
            int size2);

    ssize_t do_chunk_and_array_array(
            const StorageWord* chunk1,
            int size1,
            const StorageWord* chunk2,
            int size2);

    ssize_t do_chunk_and_run_run(
            const StorageWord* chunk1,
            int size1,
            const StorageWord* chunk2,
            int size2);

    ssize_t do_chunk_and_uncompressed_array(
            const StorageWord* chunk1,
            int size1,
            const StorageWord* chunk2,
            int size2);

    ssize_t do_chunk_and_uncompressed_run(
            const StorageWord* chunk1,
            int size1,
            const StorageWord* chunk2,
            int size2);

    ssize_t do_chunk_and_array_run(
            const StorageWord* chunk1,
            int size1,
            const StorageWord* chunk2,
            int size2);

    static bool retrieve_uncompressed_bit(const StorageWord* chunk, int idx) {
        auto word = idx / (sizeof(StorageWord) * 8);
        auto bit = idx % (sizeof(StorageWord) * 8);
        return (chunk[word] & ((StorageWord)1 << bit));
    }

    static ssize_t count_run_cardinality(const StorageWord* chunk, int size) {
        auto runs = (const Run*) chunk;
        auto num_runs = size / 2;
        ssize_t cardinality = 0;
        for(auto i = 0; i < num_runs; ++i) {
            auto run = runs[i];
            cardinality += run._size;
        }
        return cardinality;
    }

    static void set_uncompressed_range(StorageWord* chunk, int start,int end) {
        if(start >= end)
            return;
        auto bitmap = (uint64_t*) chunk;
        auto x = start / 64;
        auto y = (end - 1) / 64;
        uint64_t z_mask = 0xFFFFFFFFFFFFFFFF;

        auto x_mask = (z_mask << (start % 64));
        auto y_mask = ((z_mask >> (64 - (end % 64))) % 64);
        if(x == y) {
            bitmap[x] |= (x_mask & y_mask);
        } else {
            bitmap[x] |= x_mask;
            for(auto i = x + 1; i < y; ++i) {
                bitmap[i] |= z_mask;
            }
            bitmap[y] |= y_mask;
        }
    }

    static void clear_uncompressed_range(StorageWord* chunk, int start, int end) {
        if(start >= end)
            return;
        auto bitmap = (uint64_t*) chunk;
        auto x = start / 64;
        auto y = (end - 1) / 64;
        uint64_t z_mask = 0xFFFFFFFFFFFFFFFF;

        auto x_mask = (z_mask << (start % 64));
        auto y_mask = ((z_mask >> (64 - (end % 64))) % 64);
        if(x == y) {
            bitmap[x] = (bitmap[x] & ~(x_mask & y_mask));
        } else {
            bitmap[x] = (bitmap[x] & ~x_mask);
            for(auto i = x + 1; i < y; ++i) {
                bitmap[i] = (bitmap[i] & ~z_mask);
            }
            bitmap[y] = (bitmap[y] & ~y_mask);
        }
    }
    /*
     * A helper function to allocate new chunk at the end of the storage.
     * It works by adding the new end and types to the containers, and do a resize of _storage.
     */
    StorageWord* allocate_chunk_at_end(RoaringChunkType chunk_type, ssize_t target_chunk_size) {
        if(_offsets.size() == 0)
            _offsets.push_back(0);
        _types.push_back((unsigned char) chunk_type);
        // fmt::print(stderr, "target_chunk_size = {}\n", target_chunk_size);
        _storage.resize(_storage.size() + target_chunk_size);
        _offsets.push_back((unsigned) _storage.size() * sizeof(StorageWord));
        auto* chunk_start = _storage.data() + _storage.size() - target_chunk_size;
        return chunk_start;
    }

    void update_last_chunk_size(ssize_t target_size) {
        auto total_chunks = _offsets.size() - 1;
        auto new_end = _offsets[total_chunks - 1] + target_size;
        _offsets[total_chunks] = new_end * 2;
        _storage.resize(new_end);
    }

    static ssize_t count_bitmap_cardinality(const uint64_t* bitmap) {
        auto result = 0;
        for(auto i = 0u; i < RoaringTraits::chunk_length / sizeof(uint64_t) / 8; ++i) {
            result += __builtin_popcountl(bitmap[i]);
        }
        return result;
    }

    static ssize_t count_run_number(const uint64_t* bitmap) {
        uint64_t r = 0;
        auto total_words = RoaringTraits::chunk_length / sizeof(uint64_t) / 8;
        for(auto i = 0u; i < total_words - 1; ++i) {
            // fmt::print("word:{0:x}", bitmap[i]);
            r += __builtin_popcountl((bitmap[i] << 1) & ~bitmap[i]);
            r += ((bitmap[i] >> 63) & ~bitmap[i + 1]);
        }
        r += __builtin_popcountl((bitmap[total_words - 1] << 1) & ~bitmap[total_words - 1]);
        r += (bitmap[total_words - 1] >> 63);
        return r ;
    }

private:
    Container<ChunkAddress> _offsets;
    Container<ChunkType> _types;
    Container<StorageWord> _storage;
    Binner _binner;
    ssize_t _size;
};


template<typename Binner, template<typename> typename Container, typename RoaringTraits>
void write(std::ostream& os, const ParaRoaringBitmap<Binner, Container, RoaringTraits>& para_roaring_bitmap) {
    write(os, para_roaring_bitmap.offsets());
    write(os, para_roaring_bitmap.types());
    write(os, para_roaring_bitmap.storage());
}

template<typename Binner, template<typename> typename Container, typename RoaringTraits>
void read(ParaRoaringBitmap<Binner, Container, RoaringTraits>& para_roaring_bitmap, std::istream& is) {
    read(para_roaring_bitmap.offsets(), is);
    read(para_roaring_bitmap.types(), is);
    read(para_roaring_bitmap.storage(), is);
}

}

#include "roaring_bitmap_impl.h"

#endif
