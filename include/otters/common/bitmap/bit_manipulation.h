#ifndef OTTERS_INCLUDE_OTTERS_COMMON_BITMAP_BIT_MANIPULATION_H
#define OTTERS_INCLUDE_OTTERS_COMMON_BITMAP_BIT_MANIPULATION_H

#include <otters/common/utils/log.h>

namespace otters {

template<typename StorageWord>
bool retrieve_bit_from_bitvector(StorageWord* storage, int idx) {
    auto word = idx / (sizeof(StorageWord) * 8);
    auto bit = idx % (sizeof(StorageWord) * 8);
    return (storage[word] & ((StorageWord)1 << bit));
}

}

#endif // OTTERS_INCLUDE_OTTERS_COMMON_BITMAP_BIT_MANIPULATION_H
