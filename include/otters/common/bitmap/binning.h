#ifndef OTTERS_COMMON_BITMAP_BINNING_H
#define OTTERS_COMMON_BITMAP_BINNING_H

#include <otters/common/utils/log.h>
#include <otters/common/io/serialization.h>
#include <otters/common/containers/span.h>

#include <fstream>

namespace otters {

template<typename T>
struct EqualBinner {
    EqualBinner(int bucket_number = 64, T min = 0, T max = 0) : _bucket_number(bucket_number), _min(min), _max(max) {
    }

    int operator()(T value, int timestep = 0) const {
        T total = _max - _min;
        T delta = value - _min;
        auto bucket_id = (int)(delta / total * _bucket_number);
        if(bucket_id >= _bucket_number)
            bucket_id = _bucket_number - 1;
        if(bucket_id < 0)
            bucket_id = 0;
        return bucket_id;
    }

    int num_of_buckets() const {
        return _bucket_number;
    }

    ssize_t serialize_size() const {
        return otters::serialize_size(_bucket_number) + otters::serialize_size(_min) + otters::serialize_size(_max);
    }

    span<char> serialize(span<char> buffer) const {
        assert(serialize_size() <= buffer.size());
        buffer = otters::serialize(buffer, _bucket_number);
        buffer = otters::serialize(buffer, _min);
        buffer = otters::serialize(buffer, _max);
        return buffer;
    }

    span<char> deserialize(span<char> buffer) {
        // assert(serialize_size() >= buffer.size());
        buffer = otters::deserialize(_bucket_number, buffer);
        buffer = otters::deserialize(_min, buffer);
        buffer = otters::deserialize(_max, buffer);
        return buffer;
    }

    T min(int timestep = 0) {
        (void) timestep;
        return _min;
    }

    T max(int timestep = 0) {
        (void) timestep;
        return _max;
    }

    int _bucket_number;
    T _min;
    T _max;
};

// FIXME: we should decouple binner and bitmaps
template<typename T>
class CPUDynamicEqualWidthBinner1 {
public:
    CPUDynamicEqualWidthBinner1(int bucket_number, std::string prefix1)
        : _bucket_number(bucket_number) {
        std::ifstream file1(prefix1);
        T min1, max1;
        int max_timestep = 0;
        file1 >> max_timestep;
        for(auto i = 0; i < max_timestep; ++i) {
            file1 >> min1 >> max1;
            _mins.push_back(min1);
            _maxs.push_back(max1);
        }
    }

    CPUDynamicEqualWidthBinner1()
        : _bucket_number(64) {
    }

    int operator()(T value, int timestep) const {
        T total = max(timestep) - min(timestep);
        T delta = value - min(timestep);
        auto bucket_id = (int)(delta / total * _bucket_number);
        if(bucket_id >= _bucket_number)
            bucket_id = _bucket_number - 1;
        if(bucket_id < 0)
            bucket_id = 0;
        return bucket_id;
    }

    int num_of_buckets() const {
        return _bucket_number;
    }

    T min(int timestep_id) const {
        assert(timestep_id < _mins.size());
        return _mins[timestep_id];
    }

    T max(int timestep_id = 0) const {
        assert(timestep_id < _maxs.size());
        return _maxs[timestep_id];
    }


private:
    int _bucket_number;
    std::vector<T> _mins;
    std::vector<T> _maxs;
};



} // namespace otters

#endif
