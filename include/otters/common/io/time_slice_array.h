#ifndef OTTERS_COMMON_IO_TIME_SLICE_ARRAY_H
#define OTTERS_COMMON_IO_TIME_SLICE_ARRAY_H

#include <otters/common/containers/buffer_pool.h>
#include <otters/common/containers/byte_buffer.h>
#include <otters/common/containers/time_slice.h>
#include <otters/common/io/storage_interface.h>
#include <otters/common/utils/log.h>
#include <map>
#include <type_traits>

namespace otters {

template<typename Slice>
class TimeSliceArray {
public:
    using slice_type = Slice;
    using coordinate_type = typename Slice::coordinate_type;
    using buffer_pool_type = typename Slice::buffer_pool_type;
    enum class OpenType { READ_ONLY, WRITE_ONLY };
public:
    struct Header {
        int _total_slices;
    };
    struct SliceInfo {
        ssize_t _offset;
        ssize_t _size;
    };
    static_assert(std::is_pod<SliceInfo>::value == true);
    struct iterator {
        typename std::map<coordinate_type, SliceInfo>::iterator _iter;
        bool operator==(const iterator& other) const {
            return _iter == other._iter;
        }
        bool operator!=(const iterator& other) const {
            return _iter != other._iter;
        }
        void operator++() {
            ++_iter;
        }
        auto operator*() const {
            return _iter->first;
        }
    };

public:
    TimeSliceArray(std::string prefix, OpenType type)
        : _prefix(prefix), _type(type) {
        if(type == OpenType::READ_ONLY) {
            read_info();
            _data_file.open(data_file_name(), std::ios_base::in | std::ios_base::binary);
        } else {
            _data_file.open(data_file_name(), std::ios_base::out | std::ios_base::binary);
        }
    }

    ~TimeSliceArray() {
        if(_type == OpenType::WRITE_ONLY) {
            write_info();
        }
    }

    iterator begin() {
        return iterator{  _offsets.begin() };

    }

    iterator end() {
        return iterator{ _offsets.end() };
    }

public:
    void add_timestep(slice_type& slice) {
        auto offset = (ssize_t)_data_file.tellp();

        auto coordinate = slice.position();
        SliceInfo info;
        info._offset = offset;
        info._size = otters::serialize_size(slice.data());
        _offsets[coordinate] = info;
        Coordinates<1> coord(0);
        // log(OTTERS_LOG_DEBUG, "info = {}, {}", _offsets[coord]._offset, _offsets[coord]._size);
        // log(OTTERS_LOG_DEBUG, "step = {}", coordinate[0]);
        // log(OTTERS_LOG_DEBUG, "offsets = {}", _offsets[coordinate]._offset);
        _buffer.clear();
        _buffer.resize(info._size);
        otters::serialize(span<char>(_buffer.data(), info._size), slice.data());
        write(_data_file, _buffer);
    }

    Slice read_timestep(coordinate_type coordinate, buffer_pool_type* pool) {
        auto& info = _offsets[coordinate];

        _buffer.resize(info._size);
        _data_file.seekg(info._offset);

        read(_buffer, _data_file);
        slice_type slice(coordinate, pool);
        slice.data().deserialize(span<char>(_buffer.data(), _buffer.size()));
        return slice;
    }

private:
    void write_info() {
        Header header {  (int) _offsets.size() };
        std::ofstream info_file(info_file_name(), std::ios_base::binary);
        // log(OTTERS_LOG_DEBUG, "header.size = {}", header._total_slices);
        write(info_file, header);
        Coordinates<1> coord(0);
        // log(OTTERS_LOG_DEBUG, "info = {}, {}", _offsets[coord]._offset, _offsets[coord]._size);
        for(auto& p : _offsets) {
            // log(OTTERS_LOG_DEBUG, "coordinates = {}", p.first[0]);
            // log(OTTERS_LOG_DEBUG, "info = {}, {}", p.second._offset, p.second._size);
            write(info_file, p.first);
            write(info_file, p.second);
        }
        // BOOST_LOG_TRIVIAL(debug) << "Output size = " << header._total_timesteps;
    }

    void read_info() {
        Header header;
        std::ifstream info_file(info_file_name(), std::ios_base::binary);
        read(header, info_file);
        // log(OTTERS_LOG_DEBUG, "READ header.size = {}", header._total_slices);
        for(auto i = 0; i < header._total_slices; ++i) {
            coordinate_type coordinate;
            SliceInfo info;

            read(coordinate, info_file);
            read(info, info_file);
            // log(OTTERS_LOG_DEBUG, "READ coordinates = {}", coordinate[0]);
            // log(OTTERS_LOG_DEBUG, "READ info = {}, {}", info._offset, info._size);
            _offsets[coordinate] = info;
        }
    }

    std::string info_file_name() {
        return _prefix + ".info";
    }

    std::string data_file_name() {
        return _prefix + ".data";
    }

private:
    std::fstream _data_file;
    std::map<coordinate_type, SliceInfo> _offsets;
    std::string _prefix;
    OpenType _type;
    byte_buffer _buffer;

};

}

#endif
