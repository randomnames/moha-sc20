#ifndef OTTERS_CUDA_IO_STORAGE_INTERFACE_H
#define OTTERS_CUDA_IO_STORAGE_INTERFACE_H

#include <fstream>
#include <type_traits>
#include <array>

namespace otters {

template<typename Buffer>
typename std::enable_if<std::is_pod<Buffer>::value, void>::type
    write(std::ostream& os, const Buffer& buffer) {
    os.write((const char*)&buffer, sizeof(Buffer));
    // return os;
}

template<typename Buffer>
typename std::enable_if<std::is_pod<Buffer>::value, void>::type
    read(Buffer& buffer, std::istream& is) {
    is.read((char*)&buffer, sizeof(Buffer));
    // return is;
}

template<typename T, size_t N>
typename std::enable_if<std::is_pod<T>::value, void>::type
    write(std::ostream& os, const std::array<T, N>& buffer) {
    os.write((const char*) &buffer, sizeof(buffer));
}

template<typename T, size_t N>
typename std::enable_if<std::is_pod<T>::value, void>::type
    read(std::array<T, N>& buffer, std::istream& is) {
    is.read((char*)&buffer, sizeof(buffer));
}

template<typename T, size_t N>
typename std::enable_if<std::is_pod<T>::value == false, void>::type
    write(std::ostream& os, const std::array<T, N>& buffer) {
    for (auto i = 0u; i < N; ++i)
        write(os, buffer[i]);
}

template<typename T, size_t N>
typename std::enable_if<std::is_pod<T>::value == false, void>::type
    read(std::array<T, N>& buffer, std::istream& is) {
    for (auto i = 0u; i < N; ++i)
        read(buffer[i], is);
}



} // namespace perkit

#endif
