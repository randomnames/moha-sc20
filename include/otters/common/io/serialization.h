#ifndef OTTERS_CUDA_IO_SERIALIZATION_H
#define OTTERS_CUDA_IO_SERIALIZATION_H

#include <otters/common/utils/void_t.h>
#include <type_traits>

#include <otters/common/containers/span.h>
#include <otters/common/containers/byte_buffer.h>

namespace otters {

// primary template handles types that have no nested ::type member:
template< class, class = void_t<> >
struct can_serialize_self : std::false_type { };
 
// specialization recognizes types that do have a nested ::type member:
template<class T>
struct can_serialize_self<T, void_t<decltype(std::declval<T>().serialize_size())>> : std::true_type { };

template<typename T>
typename std::enable_if<can_serialize_self<T>::value,ssize_t>::type serialize_size(const T& t) {
    return t.serialize_size();
}

template<typename T>
constexpr typename std::enable_if<std::is_trivial<T>::value, ssize_t>::type serialize_size(const T& t) {
    (void) t;
    return sizeof(T);
}

template<typename T>
typename std::enable_if<can_serialize_self<T>::value,span<char>>::type serialize(span<char> buffer, const T& t) {
    return t.serialize(buffer);
}

template<typename T>
typename std::enable_if<std::is_trivial<T>::value, span<char>>::type serialize(span<char> buffer, const T& t) {
    assert(buffer.size() >= (ssize_t) sizeof(T));
    char* data = buffer.data();
    std::copy((const char*) &t, ((const char*) &t) + sizeof(T), data);
    return buffer.advance(sizeof(T));
}

template<typename T>
typename std::enable_if<can_serialize_self<T>::value,span<char>>::type deserialize(T& t, span<char> buffer) {
    return t.deserialize(buffer);
}


template<typename T>
typename std::enable_if<std::is_trivial<T>::value, span<char>>::type deserialize(T& t, span<char> buffer) {
    assert(buffer.size() >= (ssize_t) sizeof(T));
    char* data = buffer.data();
    std::copy(data, data + sizeof(T), (char*) &t);
    return buffer.advance(sizeof(T));
}


static_assert(serialize_size((int) 0) == sizeof(int), "");

}

#endif
