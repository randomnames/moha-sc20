#ifndef OTTERS_COMMON_UTILS_PROCESS_H
#define OTTERS_COMMON_UTILS_PROCESS_H

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h> 
#include <cstdlib>

namespace otters {

class Process {
public:
template<typename Func, typename ...Args>
    Process(Func&& func, Args... args) {
        auto pid = fork();
        if(pid == 0) {
            func();
        } else {
            assert(_pid != -1);
            _pid = pid;
        }
    }
    Process(const Process&) = delete;
    Process(Process&&) = default;
    Process& operator=(const Process&) = delete;
    Process& operator=(Process&&) = default;
    int wait() {
        int status;
        ::waitpid(_pid, &status, 0);
        return status;
    }
private:
    int _pid;
};

}

#endif