#ifndef OTTERS_COMMON_UTILS_VOID_T_H
#define OTTERS_COMMON_UTILS_VOID_T_H

namespace otters {

template<typename... Ts> struct make_void { typedef void type;};
template<typename... Ts> using void_t = typename make_void<Ts...>::type;


}

#endif
