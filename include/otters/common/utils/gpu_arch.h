#pragma once

#ifndef OTTERS_COMMON_UTILS_GPU_ARCH_H
#define OTTERS_COMMON_UTILS_GPU_ARCH_H

#ifdef __CUDACC__
    #define OTTERS_ARCH_CUDA_CODE
#else
    #define OTTERS_ARCH_CXX_CODE
#endif

#ifdef OTTERS_ARCH_CUDA_CODE
    #define OTTERS_ARCH_COMMON_FUNC __host__ __device__
#else
    #define OTTERS_ARCH_COMMON_FUNC
#endif

#endif
