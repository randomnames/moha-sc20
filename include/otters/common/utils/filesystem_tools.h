#ifndef OTTERS_COMMON_UTILS_FILESYSTEM_TOOLS_H
#define OTTERS_COMMON_UTILS_FILESYSTEM_TOOLS_H

#include <string>
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#pragma clang diagnostic ignored "-Werror"
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#pragma clang diagnostic pop
#include <fmt/core.h>

namespace otters {

void remove_files_with_prefix(const std::string& path) {
    const std::string target_path(".");
    const boost::regex my_filter(path + ".*");


    boost::filesystem::directory_iterator end_itr; // Default ctor yields past-the-end
    for (boost::filesystem::directory_iterator i(target_path); i != end_itr; ++i) {
        // Skip if not a file
        if (!boost::filesystem::is_regular_file(i->status()))
            continue;

        boost::smatch what;

        // Skip if no match for V2:
        if( !boost::regex_match( i->path().filename().string(), what, my_filter ) ) continue;

        // File matches, store it
        // all_matching_files.push_back(i->leaf());
        fmt::print("removing file: {}\n", i->path().filename().string());
        boost::filesystem::remove(i->path().filename().string());
    }
}

} // namespace otters

#endif
