#ifndef OTTERS_COMMON_UTILS_LOG_H
#define OTTERS_COMMON_UTILS_LOG_H

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wfloat-equal"
#pragma clang diagnostic ignored "-Wunused-member-function"

#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/chrono.h>

#pragma clang diagnostic pop

#include <string>
#include <sys/syscall.h> /* For SYS_xxx definitions */
#include <sys/types.h>
#include <thread>
#include <unistd.h>

#if !defined(OTTERS_LOG_LEVEL)
#if defined(NDEBUG)
    #define OTTERS_LOG_LEVEL OTTERS_LOG_WARNING
#else
    #define OTTERS_LOG_LEVEL OTTERS_LOG_DEBUG
#endif
#endif

namespace otters {

enum OttersLogLevel {
    OTTERS_LOG_DEBUG,
    OTTERS_LOG_INFO,
    OTTERS_LOG_WARNING,
    OTTERS_LOG_ERROR,
};

static const char* log_levels[] = {
    "DEBUG",
    "INFO",
    "WARNING",
    "ERROR",
};

static const int output_log_level = OTTERS_LOG_LEVEL;

size_t get_thread_id() {
    return (size_t) syscall(SYS_gettid);
}

template<int LogLevel, typename Logger, typename... Args>
void log(std::string msg, Args&&... args) {
    if(LogLevel < output_log_level)
        return;
    thread_local size_t thread_id = get_thread_id();
    fmt::memory_buffer buffer;
    fmt::format_to(buffer, msg, std::forward<Args>(args)...);
    std::time_t t = std::time(nullptr);
    auto* tm = std::localtime(&t);
    Logger::log(to_string(buffer), tm, LogLevel, thread_id);
}

struct ClogLogger {
    template<typename Buffer>
    static void log(Buffer data, std::tm* time, int log_level, size_t thread_id) {
        char time_buffer[32];
        std::strftime(time_buffer, 31, "%c %z", time);
        fmt::print(stderr, "{} [{}]: thread_id = {}: {}\n", time_buffer, log_levels[log_level], thread_id, data);
        fflush(stderr);
    }
};

#if 1
    using DefaultLogger = ClogLogger;
#endif

template<typename... Args>
void log(int log_level, std::string msg, Args&&... args) {
    switch(log_level) {
        case OTTERS_LOG_DEBUG:
            log<OTTERS_LOG_DEBUG, DefaultLogger>(msg, std::forward<Args>(args)...);
            break;
        case OTTERS_LOG_INFO:
            log<OTTERS_LOG_INFO, DefaultLogger>(msg, std::forward<Args>(args)...);
            break;
        case OTTERS_LOG_WARNING:
            log<OTTERS_LOG_WARNING, DefaultLogger>(msg, std::forward<Args>(args)...);
            break;
        case OTTERS_LOG_ERROR:
            log<OTTERS_LOG_ERROR, DefaultLogger>(msg, std::forward<Args>(args)...);
            break;
        default:
            break;
    }
}

}

#endif
