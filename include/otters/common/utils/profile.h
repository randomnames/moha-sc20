//
// Created by Haoyuan Xing on 4/17/20.
//

#ifndef OTTERS_INCLUDE_OTTERS_COMMON_UTILS_PROFILE_H
#define OTTERS_INCLUDE_OTTERS_COMMON_UTILS_PROFILE_H

#include <chrono>

namespace otters {

#ifdef OTTERS_ENABLE_PROFILE
class Timer {
public:
    static const constexpr auto nanoseconds_in_seconds = 1000000000;
public:
    Timer(std::string profile_name)
            : _total_nanoseconds(0), _profile(profile_name) {
    }
    Timer(Timer&& timer)
        : _last_start(timer._last_start), _total_nanoseconds(timer._total_nanoseconds), _profile(timer._profile) {
        timer._profile = "";
    }

    ~Timer() {
        if(_profile.empty() == false)
            print();
    }

    void start() {
        clock_gettime(CLOCK_MONOTONIC, &_last_start);
    }
    void stop() {
        struct timespec tm_end;
        clock_gettime(CLOCK_MONOTONIC, &tm_end);
        _total_nanoseconds += ((uint64_t)tm_end.tv_nsec - (uint64_t)_last_start.tv_nsec);
        _total_nanoseconds += ((uint64_t)tm_end.tv_sec - (uint64_t)_last_start.tv_sec) * nanoseconds_in_seconds;
    }
    void print() {
        fmt::print("{}={}\n", _profile, (double)_total_nanoseconds / nanoseconds_in_seconds);
    }

private:
    struct timespec _last_start;
    uint64_t _total_nanoseconds;
    std::string _profile;
};
#else
class Timer {
public:
    Timer(std::string) {}
    void start() {}
    void stop() {}
};
#endif

}

#endif // OTTERS_INCLUDE_OTTERS_COMMON_UTILS_PROFILE_H
