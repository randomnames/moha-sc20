//
// Created by Haoyuan Xing on 5/12/19.
//



#ifndef OTTERS_COMMON_UTILS_SCOPE_GUARD_H
#define OTTERS_COMMON_UTILS_SCOPE_GUARD_H

#include <type_traits>
#include <memory>
#include <iostream>

/*
 * stolen component from folly library: https://github.com/facebook/folly/blob/master/folly/ScopeGuard.h
 */

namespace otters {

namespace detail {

class scope_guard_impl_base {
public:
    void dismiss() noexcept {
        dismissed_ = true;
    }

protected:
    scope_guard_impl_base() noexcept : dismissed_(false) {}

    static void warnAboutToCrash() noexcept {
        std::ios_base::Init ioInit;
        std::cerr
                << "This program will now terminate because a scope_guard callback "
                   "threw an \nexception.\n";
    }

    static scope_guard_impl_base makeEmptyScopeGuard() noexcept {
        return scope_guard_impl_base{};
    }

    template<typename T>
    static const T& asConst(const T& t) noexcept {
        return t;
    }

    bool dismissed_;
};

template<typename FunctionType, bool InvokeNoexcept>
class scope_guard_impl : public scope_guard_impl_base {
public:
    explicit scope_guard_impl(FunctionType& fn) noexcept(
    std::is_nothrow_copy_constructible<FunctionType>::value)
            : scope_guard_impl(
            asConst(fn),
            makeFailsafe(
                    std::is_nothrow_copy_constructible<FunctionType>{},
                    &fn)) {}

    explicit scope_guard_impl(const FunctionType& fn) noexcept(
    std::is_nothrow_copy_constructible<FunctionType>::value)
            : scope_guard_impl(
            fn,
            makeFailsafe(
                    std::is_nothrow_copy_constructible<FunctionType>{},
                    &fn)) {}

    explicit scope_guard_impl(FunctionType&& fn) noexcept(
    std::is_nothrow_move_constructible<FunctionType>::value)
            : scope_guard_impl(
            std::move_if_noexcept(fn),
            makeFailsafe(
                    std::is_nothrow_move_constructible<FunctionType>{},
                    &fn)) {}

    scope_guard_impl(scope_guard_impl&& other) noexcept(
    std::is_nothrow_move_constructible<FunctionType>::value)
            : function_(std::move_if_noexcept(other.function_)) {
        // If the above line attempts a copy and the copy throws, other is
        // left owning the cleanup action and will execute it (or not) depending
        // on the value of other.dismissed_. The following lines only execute
        // if the move/copy succeeded, in which case *this assumes ownership of
        // the cleanup action and dismisses other.
        dismissed_ = std::exchange(other.dismissed_, true);
    }

    ~scope_guard_impl() noexcept(InvokeNoexcept) {
        if (!dismissed_) {
            execute();
        }
    }

private:
    static scope_guard_impl_base makeFailsafe(std::true_type, const void*) noexcept {
        return makeEmptyScopeGuard();
    }

    template<typename Fn>
    static auto makeFailsafe(std::false_type, Fn* fn) noexcept
    -> scope_guard_impl<decltype(std::ref(*fn)), InvokeNoexcept> {
        return scope_guard_impl<decltype(std::ref(*fn)), InvokeNoexcept>{
                std::ref(*fn)};
    }

    template<typename Fn>
    explicit scope_guard_impl(Fn&& fn, scope_guard_impl_base&& failsafe)
            : scope_guard_impl_base{}, function_(std::forward<Fn>(fn)) {
        failsafe.dismiss();
    }

    void* operator new(std::size_t) = delete;

    void execute() noexcept(InvokeNoexcept) {
        if (InvokeNoexcept) {
            using R = decltype(function_());
            auto catcher = []() -> R { warnAboutToCrash(); std::terminate(); };
            try {
                function_();
            } catch (...) {
                catcher();
            }
        } else {
            function_();
        }
    }

    FunctionType function_;
};

template<typename F, bool INE>
using scope_guard_impl_decay = scope_guard_impl<typename std::decay<F>::type, INE>;

}

template<typename F>
[[nodiscard]] detail::scope_guard_impl_decay<F, true> make_scope_guard(F&& f) noexcept(
noexcept(detail::scope_guard_impl_decay<F, true>(static_cast<F&&>(f)))) {
    return detail::scope_guard_impl_decay<F, true>(static_cast<F&&>(f));

}

}

#endif //COMPASS_INCLUDE_COMPASS_UTILS_SCOPE_GUARD_H
