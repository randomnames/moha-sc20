#pragma once

#ifndef OTTERS_COMMON_UTILS_GIT_VERSION_H
#define OTTERS_COMMON_UTILS_GIT_VERSION_H

#include <string>
#include <fmt/core.h>

namespace otters {

extern const char git_commit_sha1[];

inline std::string version_string() {
    return git_commit_sha1;
}

inline void print_version() {
    fmt::print("OTTERS_VERSION={}\n", version_string());
}

}

#endif