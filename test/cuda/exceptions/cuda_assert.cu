#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <stdio.h>
#include <iostream>
#include <otters/cuda/primitives/histogram.cuh>

#include <otters/cuda/exception/cuda_assert.h>

// __global__ void kernel() {
   //  int x = threadIdx.x;
   //  printf("x = %d\n", x);
// }

TEST_CASE("An cuda assert error should be able to throw exception.", "[exception]") {
   //  kernel<<<10,10>>>();
   #pragma clang push
   #pragma clang diagnostic ignored "-Wgnu-statement-expression"
   REQUIRE_THROWS_AS((OTTERS_CUDA_ERRORCHECK(cudaError_t(0x0d))), otters::CUDARuntimeError);
   #pragma clang pop
}
