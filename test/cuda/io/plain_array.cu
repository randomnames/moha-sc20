#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <otters/common/io/storage_interface.h>
#include <otters/common/io/time_slice_array.h>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/io/plain_array.cuh>
//#include <otters/cuda/query/buffers.cuh>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#include <boost/filesystem.hpp>
#pragma clang diagnostic pop

#include <algorithm>
#include <string>
#include <fmt/core.h>

using namespace otters;

    /*
TEST_CASE("We can write 100 timesteps and read 100 back") {
    using TimeStepArrayT = TimeStepArray<system_malloc_buffer<double>, 1>;
    auto buffer_size = 1024;
    std::string file_prefix = "plain_array_test_read_write";
    auto delta = 10e-12;
    {
        TimeStepArrayT array(file_prefix, TimeStepArrayT::OpenType::WRITE_ONLY);
        std::vector<int> timesteps;
        for(auto i = 0; i < 100; ++i)
            timesteps.push_back(i);
        std::random_shuffle(timesteps.begin(), timesteps.end());
        for(auto timestep_id : timesteps) {
            // BOOST_LOG_TRIVIAL(debug) << "Timestep id = " << timestep_id << "\n";
            TimeStep<std::array<system_malloc_buffer<double>, 1>>
                    buffer(timestep_id);
            buffer.data()[0].resize(buffer_size);
            std::fill(buffer.data()[0].data(), buffer.data()[0].data() + buffer_size, timestep_id);
            array.add_timestep(timestep_id, buffer);
        }
    }
    {
        TimeStepArrayT array(file_prefix, TimeStepArrayT::OpenType::READ_ONLY);
        std::vector<int> timesteps;
        for(auto i = 0; i < 100; ++i)
            timesteps.push_back(i);
        std::random_shuffle(timesteps.begin(), timesteps.end());
        for(auto timestep_id : timesteps) {
            auto buffer = array.read_timestep(timestep_id);
            REQUIRE(buffer.data()[0].size() == buffer_size);
            for(auto i = 0; i < buffer_size; ++i) {
                REQUIRE(fabs(buffer.data()[0][i] - timestep_id) < delta);
            }
        }

    }
    boost::filesystem::remove(file_prefix + ".info");
    boost::filesystem::remove(file_prefix + ".map");
    boost::filesystem::remove(file_prefix + ".data");
}
    */

TEST_CASE("We can write 100 time slices and read 100 back") {
    using Slice = TimeSlice<1, system_malloc_buffer<double>>;
    using SliceArray = TimeSliceArray<Slice>;
    auto buffer_size = 1024;
    std::string file_prefix = "plain_slice_read_write";
    auto delta = 10e-12;
    {
        TimeSliceArray<Slice> array(file_prefix, SliceArray::OpenType::WRITE_ONLY);
        std::vector<int> timesteps;
        for(auto i = 0; i < 100; ++i)
            timesteps.push_back(i);
        std::random_shuffle(timesteps.begin(), timesteps.end());
        for(auto timestep_id : timesteps) {
            // BOOST_LOG_TRIVIAL(debug) << "Timestep id = " << timestep_id << "\n";
            Slice slice(timestep_id);
            slice.data().resize(buffer_size);
            std::fill(slice.data().data(), slice.data().data() + buffer_size, timestep_id);
            array.add_timestep(slice);
        }
    }
    {
        BufferPool<system_malloc_buffer<double>> pool;
        {
            system_malloc_buffer<double> buffer;
            pool.push(std::move(buffer));
        }
        TimeSliceArray<Slice> array(file_prefix, SliceArray::OpenType::READ_ONLY);
        std::vector<int> timesteps;
        for(auto i = 0; i < 100; ++i)
            timesteps.push_back(i);
        std::random_shuffle(timesteps.begin(), timesteps.end());
        for(auto timestep_id : timesteps) {
            auto slice = array.read_timestep(timestep_id, &pool);
            REQUIRE(slice.data().size() == buffer_size);
            for(auto i = 0; i < buffer_size; ++i) {
                REQUIRE(fabs(slice.data()[i] - timestep_id) < delta);
            }
        }

    }
    boost::filesystem::remove(file_prefix + ".info");
    boost::filesystem::remove(file_prefix + ".map");
    boost::filesystem::remove(file_prefix + "plain_array_test_read_write.data");
}
