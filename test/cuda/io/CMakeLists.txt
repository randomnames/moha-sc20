add_executable(plain_array plain_array.cu)
target_link_libraries(plain_array otter-header Catch2::Catch2
    Boost::system
    Threads::Threads
    Boost::filesystem
    dl
)
add_cuda_test(CUDA_QUERY_PLAIN_ARRAY "unit" plain_array)
