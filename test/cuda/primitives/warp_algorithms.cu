#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <stdio.h>
#include <stdint.h>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/exception/cuda_assert.h>
#include <otters/cuda/primitives/functional.cuh>
#include <otters/cuda/primitives/warp_algorithms.cuh>
#include <functional>

static const int WARP_SIZE = otters::detail::WARP_SIZE;

using namespace otters;

__global__
void warp_reduction_test(int32_t* data) {
    auto ptr = &data[threadIdx.x];
    auto result = otters::detail::warp_reduction<otters::plus<int32_t>>(*ptr);
    if(threadIdx.x % WARP_SIZE == 0)
        *ptr = result;
}

TEST_CASE("simple reduction inside warp should work","[reduction]") {
    int32_t* data;
    cudaMallocManaged(&data, sizeof(int32_t) * WARP_SIZE * 2);
    for(int i = 0; i < WARP_SIZE * 2; ++i)
        data[i] = i;

    warp_reduction_test<<<1,64>>>(data);
    OTTERS_CUDA_ERRORCHECK(cudaDeviceSynchronize());

    REQUIRE(data[0] == (0 + 31) * 32 / 2);
    REQUIRE(data[WARP_SIZE] == (32 + 63) * 32 / 2);

    cudaFree(data);
}

__global__
void warp_reduction_multiple_test(int32_t* begin, int32_t* end) {
    auto ptr = begin;
    auto result = otters::detail::warp_reduction_multiple<otters::plus<int32_t>>(ptr, end);
    if(threadIdx.x % WARP_SIZE == 0)
        *ptr = result;
}

TEST_CASE("multiple reduction inside warp should work","[reduction]") {
    cuda_managed_buffer<int32_t> data(WARP_SIZE * 16);
    for(int i = 0; i < WARP_SIZE * 16; ++i)
        data[i] = i;

    warp_reduction_multiple_test<<<1,32>>>(data.get(), data.get() + WARP_SIZE * 16);
    OTTERS_CUDA_ERRORCHECK(cudaDeviceSynchronize());

    auto answer = (0 + 16*32-1) * 16 * 32 / 2;
    REQUIRE(data[0] == answer);
//    REQUIRE(data[WARP_SIZE] == (32 + 63) * 32 / 2);

}


__global__ void warp_scan_test(int32_t* answer, int32_t* data) {
    auto ptr = &data[threadIdx.x];
    auto result = otters::detail::warp_scan<ScanType::Inclusive, otters::plus<int32_t>>(*ptr, 0);
    answer[threadIdx.x] = result;
}


TEST_CASE("simple warp inclusive scan should work") {
    cuda_managed_buffer<int32_t> data(WARP_SIZE * 2);
    cuda_managed_buffer<int32_t> answer(WARP_SIZE * 2);
    for(int i = 0; i < WARP_SIZE * 2; ++i) {
        data[i] = i;
    }

    warp_scan_test<<<1,64>>>(answer.get(), data.get());
    OTTERS_CUDA_ERRORCHECK(cudaDeviceSynchronize());

    for(int i = 0; i < WARP_SIZE * 2; ++i) {
        if (i % 32 == 0) {
            REQUIRE(data[i] == answer[i]);
        } else {
            REQUIRE(answer[i] == data[i] + answer[i - 1]);
        }
    }
}

__global__ void warp_scan_multiple_test(int32_t* answer, int32_t* data, int data_per_block) {
    auto warp_id = threadIdx.x / WARP_SIZE;
    //auto idx_in_warp = threadIdx.x % WARP_SIZE;
    auto warp_data = data + data_per_block * warp_id;
    auto warp_answer = answer + data_per_block * warp_id;
    detail::warp_scan_multiple<ScanType::Inclusive, plus<int32_t>>
            (warp_answer, warp_data, warp_data + data_per_block, 0);
}


TEST_CASE("simple warp inclusive multiple scan should work") {
    cuda_managed_buffer<int32_t> data(WARP_SIZE * 8);
    cuda_managed_buffer<int32_t> answer(WARP_SIZE * 8);
    constexpr auto DATA_PER_BLOCK = WARP_SIZE * 4;
    for(int i = 0; i < WARP_SIZE * 8; ++i) {
        data[i] = i;
    }

    warp_scan_multiple_test<<<1,64>>>(answer.get(), data.get(), DATA_PER_BLOCK);
    OTTERS_CUDA_ERRORCHECK(cudaDeviceSynchronize());

    for(int i = 0; i < WARP_SIZE * 4; ++i) {
        CAPTURE(i);
        if (i % DATA_PER_BLOCK == 0) {
            REQUIRE(data[i] == answer[i]);
            REQUIRE(data[i + DATA_PER_BLOCK] == answer[i + DATA_PER_BLOCK]);
        } else {
            REQUIRE(answer[i] == data[i] + answer[i - 1]);
            REQUIRE(answer[i + DATA_PER_BLOCK] ==
                data[i + DATA_PER_BLOCK] + answer[i - 1 + DATA_PER_BLOCK]);
        }
    }
}


__global__ void warp_scan_exclusive_test(int32_t* answer, int32_t* data) {
    auto ptr = &data[threadIdx.x];
    auto result = otters::detail::warp_scan<ScanType::Exclusive, otters::plus<int32_t>>(*ptr, 0);
    answer[threadIdx.x] = result;
}


TEST_CASE("simple warp exclusive scan should work") {
    cuda_managed_buffer<int32_t> data(WARP_SIZE * 2);
    cuda_managed_buffer<int32_t> answer(WARP_SIZE * 2);
    for(int i = 0; i < WARP_SIZE * 2; ++i) {
        data[i] = i;
    }

    warp_scan_exclusive_test<<<1,64>>>(answer.get(), data.get());
    OTTERS_CUDA_ERRORCHECK(cudaDeviceSynchronize());

    for(int i = 0; i < WARP_SIZE * 2; ++i) {
        CAPTURE(i);
        if (i % 32 == 0) {
            REQUIRE(answer[i] == 0);
        } else {
            REQUIRE(answer[i] == data[i - 1] + answer[i - 1]);
        }
    }
}

__global__ void warp_scan_multiple_exclusive_test(int32_t* answer, int32_t* data, int data_per_block) {
    auto warp_id = threadIdx.x / WARP_SIZE;
    //auto idx_in_warp = threadIdx.x % WARP_SIZE;
    auto warp_data = data + data_per_block * warp_id;
    auto warp_answer = answer + data_per_block * warp_id;
    detail::warp_scan_multiple<ScanType::Exclusive, plus<int32_t>>
            (warp_answer, warp_data, warp_data + data_per_block, 0);
}


TEST_CASE("simple warp exclusive multiple scan should work") {
    cuda_managed_buffer<int32_t> data(WARP_SIZE * 8);
    cuda_managed_buffer<int32_t> answer(WARP_SIZE * 8);
    constexpr auto DATA_PER_BLOCK = WARP_SIZE * 4;
    for(int i = 0; i < WARP_SIZE * 8; ++i) {
        data[i] = i;
    }

    warp_scan_multiple_exclusive_test<<<1,64>>>(answer.get(), data.get(), DATA_PER_BLOCK);
    OTTERS_CUDA_ERRORCHECK(cudaDeviceSynchronize());

    for(int i = 0; i < WARP_SIZE * 4; ++i) {
        CAPTURE(i);
        if (i % DATA_PER_BLOCK == 0) {
            REQUIRE(answer[i] == 0);
            REQUIRE(answer[i + DATA_PER_BLOCK] == 0);
        } else {
            REQUIRE(answer[i] == data[i - 1] + answer[i - 1]);
            REQUIRE(answer[i + DATA_PER_BLOCK] ==
                data[i + DATA_PER_BLOCK - 1] + answer[i - 1 + DATA_PER_BLOCK]);
        }
    }
    // cudaProfilerStop();
}
