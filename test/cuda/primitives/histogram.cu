#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <stdio.h>
#include <otters/common/utils/gpu_arch.h>
#include <otters/cuda/exception/cuda_assert.h>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/iterator.cuh>
#include <otters/cuda/primitives/histogram.cuh>
#include <otters/cuda/clion_helper.h>

using namespace otters;

template<int DataPerBlock, int BlockSize, int NBlock, int NKey>
__global__ void histograms_fixed_len_test1(int* histogram, int* data) {
    __shared__ int32_t local_histograms[BlockSize][NKey];    
    for(size_t i = 0; i < NKey; ++i)
        local_histograms[threadIdx.x][i] = 0;
    auto histogram_address = histogram + NKey * blockIdx.x;
    auto data_address = data + DataPerBlock * blockIdx.x;
    otters::detail::build_histograms_fixed_len<NKey>(histogram_address,
        &local_histograms[threadIdx.x][0],
        data_address, data_address + DataPerBlock);
}

template<int DataPerBlock, int BlockSize, int NBlock, int NKey>
__global__ void histograms_fixed_len_test2(int* histogram, int* data) {
    __shared__ int32_t local_histograms[BlockSize][NKey];    
    for(size_t i = 0; i < NKey; ++i)
        local_histograms[threadIdx.x][i] = 0;
    auto histogram_address = histogram + NKey * blockIdx.x;
    auto data_address = data + DataPerBlock * blockIdx.x;
    detail::run_count_iterator<int32_t> it{data_address, 0};
    otters::detail::build_histograms_fixed_len
    <NKey, int32_t, decltype(it), otters::detail::not_equal_t<-1>>(
            histogram_address,
            &local_histograms[threadIdx.x][0],
            it, it + DataPerBlock
    );
}


TEST_CASE("build_histograms_fixed_len should be able to count elements","[histogram]") {
    const int DATA_PER_BLOCK = 1024;
    const int BLOCK_SIZE = 64;
    const int NBLOCK = 29;
    const int NKEY = 64;

    cuda_managed_buffer<int32_t> data(DATA_PER_BLOCK * NBLOCK);
    cuda_managed_buffer<int32_t> histogram(NKEY * NBLOCK);
    for(int i = 0; i < DATA_PER_BLOCK * NBLOCK; ++i)    
        data[i] = i % NKEY;
    for(int i = 0; i < NBLOCK * NKEY; ++i)    
        histogram[i] = 0;

    histograms_fixed_len_test1<DATA_PER_BLOCK, BLOCK_SIZE, NBLOCK, NKEY>
            <<<NBLOCK,BLOCK_SIZE>>>(histogram.get(), data.get());
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );

    for(int i = 0; i < NBLOCK * NKEY; ++i)    
        REQUIRE(histogram[i] == DATA_PER_BLOCK / NKEY);
}



TEST_CASE("build_histograms_fixed_len should be able to count transformed elements","[histogram]") {
    const int DATA_PER_BLOCK = 1024;
    const int BLOCK_SIZE = 64;
    const int NBLOCK = 29;
    const int NKEY = 64;

    cuda_managed_buffer<int32_t> data(DATA_PER_BLOCK * NBLOCK);
    cuda_managed_buffer<int32_t> histogram(NKEY * NBLOCK);
    for(int i = 0; i < DATA_PER_BLOCK * NBLOCK; ++i)    
        data[i] = (i) % NKEY;
    for(int i = 0; i < NBLOCK * NKEY; ++i)    
        histogram[i] = 0;

    histograms_fixed_len_test1<DATA_PER_BLOCK, BLOCK_SIZE, NBLOCK, NKEY>
            <<<NBLOCK,BLOCK_SIZE>>>(histogram.get(), data.get());
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );

    for(int i = 0; i < NBLOCK * NKEY; ++i) {    
        CAPTURE(i);
        REQUIRE(histogram[i] == DATA_PER_BLOCK / NKEY);
    }
}
