add_executable(histogram histogram.cu)
target_link_libraries(histogram otter-header Boost::boost dl)
add_cuda_test(CUDA_PRIMITIVES_HISTOGRAM "unit" histogram)

add_executable(warp_algorithms warp_algorithms.cu)
target_link_libraries(warp_algorithms otter-header Catch2::Catch2 dl)
#target_compile_options(warp_algorithms PRIVATE $<JOIN:$<$<COMPILE_LANGUAGE:CUDA>:${CUDA_DEVICE_CXX_FLAGS}>," ">)
add_cuda_test(CUDA_PRIMITIVES_WARP_ALGORITHMS "unit" warp_algorithms)

add_executable(scan scan.cu)
target_link_libraries(scan otter-header Catch2::Catch2 dl)
#target_compile_options(warp_algorithms PRIVATE $<JOIN:$<$<COMPILE_LANGUAGE:CUDA>:${CUDA_DEVICE_CXX_FLAGS}>," ">)
add_cuda_test(CUDA_PRIMITIVES_SCAN "unit" scan)
