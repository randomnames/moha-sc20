#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <stdio.h>
#include <stdint.h>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/exception/cuda_assert.h>
#include <otters/cuda/primitives/functional.cuh>
#include <otters/cuda/primitives/warp_algorithms.cuh>
#include <otters/cuda/primitives/scan.cuh>
#include <functional>

static const int WARP_SIZE = otters::detail::WARP_SIZE;

using namespace otters;

template<int NBlock, int DataPerBlock, int BlockSize>
__global__ void block_scan_test(int32_t* answer, int32_t* data) {
    auto ptr = data + DataPerBlock * blockIdx.x;
    __shared__ int32_t warp_counts[WARP_SIZE];
    scan<ScanType::Inclusive, otters::plus<int32_t>>
        (warp_counts, answer + DataPerBlock * blockIdx.x, ptr, ptr + DataPerBlock, 0);
}


TEST_CASE("simple block inclusive scan should work") {
    constexpr auto NBLOCK = 2;
    constexpr auto DATA_PER_BLOCK = WARP_SIZE * 8;
    constexpr auto DATA_SIZE = DATA_PER_BLOCK * NBLOCK;
    constexpr auto BLOCK_SIZE = 64;

    cuda_managed_buffer<int32_t> data(DATA_SIZE);
    cuda_managed_buffer<int32_t> answer(DATA_SIZE);
    for(int i = 0; i < DATA_SIZE; ++i) {
        data[i] = i;
    }

    block_scan_test<NBLOCK, DATA_PER_BLOCK, BLOCK_SIZE><<<NBLOCK,BLOCK_SIZE>>>(answer.get(), data.get());
    OTTERS_CUDA_ERRORCHECK(cudaDeviceSynchronize());

    for(int i = 0; i < WARP_SIZE * 2; ++i) {
        CAPTURE(i);
        if (i % DATA_PER_BLOCK == 0) {
            REQUIRE(answer[i] == data[i]);
        } else {
            REQUIRE(answer[i] == data[i] + answer[i - 1]);
        }
    }
}
/*

__global__ void warp_scan_exclusive_test(int32_t* answer, int32_t* data) {
    auto ptr = &data[threadIdx.x];
    auto result = otters::detail::warp_scan<ScanType::Exclusive, otters::plus<int32_t>>(*ptr, 0);
    answer[threadIdx.x] = result;
}


TEST_CASE("simple warp exclusive scan should work") {
    cuda_managed_buffer<int32_t> data(WARP_SIZE * 2);
    cuda_managed_buffer<int32_t> answer(WARP_SIZE * 2);
    for(int i = 0; i < WARP_SIZE * 2; ++i) {
        data[i] = i;
    }

    warp_scan_exclusive_test<<<1,64>>>(answer.get(), data.get());
    OTTERS_CUDA_ERRORCHECK(cudaDeviceSynchronize());

    for(int i = 0; i < WARP_SIZE * 2; ++i) {
        CAPTURE(i);
        if (i % 32 == 0) {
            REQUIRE(answer[i] == 0);
        } else {
            REQUIRE(answer[i] == data[i - 1] + answer[i - 1]);
        }
    }
}
*/
