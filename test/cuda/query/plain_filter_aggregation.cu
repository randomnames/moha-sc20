#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <otters/cuda/query/plain_filter_helper.cuh>
#include <otters/cuda/query/ops/csm_support.cuh>
#include <otters/cuda/query/subgroup_discovery_support.cuh>
#include <otters/cuda/query/plain_subgroup_discovery_support.cuh>

#include <fmt/core.h>

using namespace otters;

TEST_CASE("dumb case") {
    double selection[25] = {
        1,0,1,0,1,
        0,1,0,1,0,
        1,0,1,0,1,
        0,1,0,1,0,
        1,0,1,0,1,
    };
    double target[25] = {
        1,1,1,1,1,
        1,1,1,1,1,
        1,1,1,1,1,
        1,1,1,1,1,
        1,1,1,1,1,
    };

    SubsetCondition<double, 2, 2> condition;
    condition.dim_range(0).assign(1,4);
    condition.dim_range(1).assign(1,4);
    condition.attr_range(0).assign(1,2);
    std::array<double*, 2> attr_ptrs = {selection, target};
    std::array<int64_t, 2> dim_sizes = {5, 5};
    auto result = aggregate_subgroup<double, 2, 2>(attr_ptrs, dim_sizes, condition, 1);
    REQUIRE(fabs(result.sum() - 5) < 0.00001);
    REQUIRE(result.size() == 5);
    fmt::print("Sum = {} Size = {}\n", result.sum(), result.size());

}
