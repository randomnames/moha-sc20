#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <otters/common/bitmap/binning.h>
#include <otters/common/containers/buffer.h>
#include <otters/common/containers/buffer_pool.h>
#include <otters/cuda/bitmap/binning.cuh>
#include <otters/cuda/bitmap/query_driver.h>
#include <otters/cuda/bitmap/roaring.cuh>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/buffer_pool.cuh>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/primitives/random.cuh>
#include <otters/cuda/query/ops/bitmap_csm.cuh>
#include <otters/cuda/query/ops/consume_operator.cuh>
#include <otters/cuda/query/ops/gpu_bitmap_input.cuh>
#include <otters/cuda/query/ops/gpu_bitmap_build.cuh>
#include <otters/cuda/query/ops/gpu_plain_input.cuh>
#include <otters/cuda/query/ops/gpu_random_generate.cuh>
#include <otters/cuda/query/ops/gpu_send_recv_operator.cuh>
#include <otters/cuda/query/ops/print_bitmap_info.cuh>
#include <otters/cuda/query/ops/save_op.cuh>
#include <otters/cuda/query/ops/scan_op.cuh>
#include <otters/cuda/query/timestep.cuh>
#include <thread>
#include <vector>

using namespace otters;

TEST_CASE("A random array can be generated and convert to bitmap") {
    using PlainBuffer = MultiAttributeBuffer<cuda_device_buffer<double>, 1>;
    using PlainBufferPool = BufferPool<PlainBuffer>;
    using GPUBitmap = GPURoaringBitmap<64, DefaultRoaringTraits>;
    using BitmapBuffer = MultiAttributeBuffer<GPUBitmap, 1>;
    using BitmapBufferPool = BufferPool<BitmapBuffer>;
    using GPUBinner = GPUEqualWidthBinner<64, double>;
    using CPUBinner = EqualBinner<double>;
    using CPUBitmap = ParaRoaringBitmap<CPUBinner, cuda_host_buffer>;
    using CPUBuffer = MultiAttributeBuffer<CPUBitmap, 1>;
    using CPUBufferPool = BufferPool<CPUBuffer>;
    using GPUSlice = TimeSlice<2, BitmapBuffer>;

    PlainBufferPool plain_pool;
    initialize_buffer_pool(plain_pool, 4);
    BitmapBufferPool bitmap_pool;
    initialize_buffer_pool(bitmap_pool, 4);
    CPUBufferPool cpu_pool;
    initialize_buffer_pool(cpu_pool, 4);

    using PrintOp = PrintBitmapInfoOperator<CPUBitmap,2,1>;
    PrintOp info_op;
    std::vector<PrintOp*> info_op_ptrs;
    info_op_ptrs.push_back(&info_op);
    GPUSendRecvOperator<decltype(info_op), CPUBufferPool, GPUSlice> gpu_send_recv_op(info_op_ptrs,&cpu_pool);
    std::array<GPUBinner, 1> gpu_binners {
        GPUBinner(0.0,1.0)
    };
    GPUBitmapBuildOperator<decltype(gpu_send_recv_op), 2, double, 1, GPUBitmap, GPUBinner, BitmapBufferPool>
        bitmap_build_op(std::move(gpu_binners), &gpu_send_recv_op, &bitmap_pool);
    GPUGenerateOperator<decltype(bitmap_build_op), 2, double, 1, PlainBufferPool> gpu_generate_op(
            &bitmap_build_op, Distribution::Uniform, 64 * 64 * 64, 10, &plain_pool);

    std::thread parent_thread([&gpu_send_recv_op]() {
        gpu_send_recv_op.drive(0);
    });
    std::thread gpu_thread([&gpu_generate_op]() {
        gpu_generate_op.drive();
    });
    gpu_thread.join();
    parent_thread.join();
}

TEST_CASE("A normal array can be generated and convert to bitmap") {
    using PlainBuffer = MultiAttributeBuffer<cuda_device_buffer<double>, 1>;
    using PlainBufferPool = BufferPool<PlainBuffer>;
    using GPUBitmap = GPURoaringBitmap<64, DefaultRoaringTraits>;
    using BitmapBuffer = MultiAttributeBuffer<GPUBitmap, 1>;
    using BitmapBufferPool = BufferPool<BitmapBuffer>;
    using GPUBinner = GPUEqualWidthBinner<64, double>;
    using CPUBinner = EqualBinner<double>;
    using CPUBitmap = ParaRoaringBitmap<CPUBinner, cuda_host_buffer>;
    using CPUBuffer = MultiAttributeBuffer<CPUBitmap, 1>;
    using CPUBufferPool = BufferPool<CPUBuffer>;
    using GPUSlice = TimeSlice<2, BitmapBuffer>;

    PlainBufferPool plain_pool;
    initialize_buffer_pool(plain_pool, 4);
    BitmapBufferPool bitmap_pool;
    initialize_buffer_pool(bitmap_pool, 4);
    CPUBufferPool cpu_pool;
    initialize_buffer_pool(cpu_pool, 4);

    using PrintOp = PrintBitmapInfoOperator<CPUBitmap,2,1>;
    PrintOp info_op;
    std::vector<PrintOp*> info_op_ptrs;
    info_op_ptrs.push_back(&info_op);
    GPUSendRecvOperator<decltype(info_op), CPUBufferPool, GPUSlice> gpu_send_recv_op(info_op_ptrs,&cpu_pool);
    std::array<GPUBinner, 1> gpu_binners {
        GPUBinner(-1.0,1.0)
    };
    GPUBitmapBuildOperator<decltype(gpu_send_recv_op), 2, double, 1, GPUBitmap, GPUBinner, BitmapBufferPool>
        bitmap_build_op(std::move(gpu_binners), &gpu_send_recv_op, &bitmap_pool);
    GPUGenerateOperator<decltype(bitmap_build_op), 2, double, 1, PlainBufferPool> gpu_generate_op(
            &bitmap_build_op, Distribution::Normal, 64 * 64 * 64, 10, &plain_pool);

    std::thread parent_thread([&gpu_send_recv_op]() {
        gpu_send_recv_op.drive(0);
    });
    std::thread gpu_thread([&gpu_generate_op]() {
        gpu_generate_op.drive();
    });
    gpu_thread.join();
    parent_thread.join();
}

TEST_CASE("A log normal array can be generated and convert to bitmap") {
    using PlainBuffer = MultiAttributeBuffer<cuda_device_buffer<double>, 1>;
    using PlainBufferPool = BufferPool<PlainBuffer>;
    using GPUBitmap = GPURoaringBitmap<64, DefaultRoaringTraits>;
    using BitmapBuffer = MultiAttributeBuffer<GPUBitmap, 1>;
    using BitmapBufferPool = BufferPool<BitmapBuffer>;
    using GPUBinner = GPUEqualWidthBinner<64, double>;
    using CPUBinner = EqualBinner<double>;
    using CPUBitmap = ParaRoaringBitmap<CPUBinner, cuda_host_buffer>;
    using CPUBuffer = MultiAttributeBuffer<CPUBitmap, 1>;
    using CPUBufferPool = BufferPool<CPUBuffer>;
    using GPUSlice = TimeSlice<2, BitmapBuffer>;

    PlainBufferPool plain_pool;
    initialize_buffer_pool(plain_pool, 4);
    BitmapBufferPool bitmap_pool;
    initialize_buffer_pool(bitmap_pool, 4);
    CPUBufferPool cpu_pool;
    initialize_buffer_pool(cpu_pool, 4);

    using PrintOp = PrintBitmapInfoOperator<CPUBitmap,2,1>;
    PrintOp info_op;
    std::vector<PrintOp*> info_op_ptrs;
    info_op_ptrs.push_back(&info_op);
    GPUSendRecvOperator<decltype(info_op), CPUBufferPool, GPUSlice> gpu_send_recv_op(info_op_ptrs,&cpu_pool);
    std::array<GPUBinner, 1> gpu_binners {
        GPUBinner(-1.0,1.0)
    };
    GPUBitmapBuildOperator<decltype(gpu_send_recv_op), 2, double, 1, GPUBitmap, GPUBinner, BitmapBufferPool>
        bitmap_build_op(std::move(gpu_binners), &gpu_send_recv_op, &bitmap_pool);
    GPUGenerateOperator<decltype(bitmap_build_op), 2, double, 1, PlainBufferPool> gpu_generate_op(
            &bitmap_build_op, Distribution::LogNormal, 64 * 64 * 64, 1, &plain_pool);

    std::thread parent_thread([&gpu_send_recv_op]() {
        gpu_send_recv_op.drive(0);
    });
    std::thread gpu_thread([&gpu_generate_op]() {
        gpu_generate_op.drive();
    });
    gpu_thread.join();
    parent_thread.join();
}
