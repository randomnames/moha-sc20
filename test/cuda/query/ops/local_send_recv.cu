#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <otters/cuda/query/ops/consume_operator.cuh>
#include <otters/cuda/query/ops/local_send_recv_operator.cuh>
#include <otters/cuda/container/buffer.cuh>
#include <otters/common/containers/time_slice.h>
#include <fmt/core.h>

#include <thread>

using namespace otters;

TEST_CASE("A local send/recv operator can work through two threads.") {
    ConsumeOperator<TimeSlice<1, system_malloc_buffer<int>>> consume_int_op;
    std::vector<decltype(&consume_int_op)> ops;
    ops.push_back(&consume_int_op);
    LocalSendRecvOperator<decltype(consume_int_op)> send_recv_op(std::move(ops), 1, 3);   

    std::thread child_thread([&send_recv_op](){
        for(int i = 1; i < 100; ++i) {
            TimeSlice<1, system_malloc_buffer<int>> slice;
            slice.data().resize(1);
            slice.data()[0] = i;
            slice.timestep_id(i);
            // fmt::print("sending i = {}\n", i);
            send_recv_op.consume(std::move(slice));
        }
        TimeSlice<1, system_malloc_buffer<int>> slice;
        // fmt::print("sending i = {}\n", slice.timestep_id());
        send_recv_op.consume(std::move(slice));
    });

    std::thread parent_thread([&send_recv_op](){
        send_recv_op.drive(0);
    });

    child_thread.join();
    parent_thread.join();
}

TEST_CASE("A local send/recv operator can work through two threads via BufferPool.") {
    BufferPool<std::vector<int>> pool;
    const int total_threads = 4;
    using ConsumeOp = ConsumeOperator<TimeSlice<1, std::vector<int>>>;
    ConsumeOp consume_int_op[total_threads];
    std::vector<ConsumeOp*> ops;
    for(auto i = 0; i < total_threads; ++i)
        ops.push_back(&consume_int_op[i]);
    LocalSendRecvOperator<ConsumeOp> send_recv_op(ops, 1, 3);   

    for(auto i = 0; i < 5; ++i) {
        std::vector<int> buffer(i,i);
        pool.push(std::move(buffer));
    }

    std::thread child_thread([&pool, &send_recv_op](){
        for(int i = 1; i < 100; ++i) {
            TimeSlice<1, std::vector<int>> slice(i, &pool);
            // fmt::print("sending i = {}\n", i);
            send_recv_op.consume(std::move(slice));
        }
        TimeSlice<1, std::vector<int>> slice;
        // fmt::print("sending i = {}\n", slice.timestep_id());
        send_recv_op.consume(std::move(slice));
    });

    std::vector<std::thread> parent_threads;
    for(auto i = 0; i < total_threads; ++i) {
        parent_threads.emplace_back([i, &send_recv_op](){
            send_recv_op.drive(i);
        });
    }

    child_thread.join();
    for(auto i = 0; i < total_threads; ++i)
        parent_threads[i].join();
}
