#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <otters/cuda/query/ops/gpu_send_recv_operator.cuh>
#include <otters/cuda/query/ops/consume_operator.cuh>

#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/buffer_pool.cuh>
#include <otters/cuda/query/timestep.cuh>

using namespace otters;

__global__ void fill_buffer(int* data, int size) {
    if((int) threadIdx.x < size)
        data[threadIdx.x] = threadIdx.x;
}

TEST_CASE("A gpu send/recv operator can work through two threads via BufferPool.") {
    cudaStream_t stream;
    cudaStreamCreate(&stream);
    using GPUBuffer = cuda_device_buffer<int>;
    BufferPool<GPUBuffer> gpu_pool;
    for(auto i = 0; i < 5; ++i) {
        cuda_device_buffer<int> buffer(i);
        buffer.resize(i);
        gpu_pool.push(std::move(buffer));
    }

    using CPUBuffer = cuda_host_buffer<int>;
    BufferPool<CPUBuffer> cpu_pool;
    for(auto i = 0; i < 5; ++i) {
        cuda_host_buffer<int> buffer;
        cpu_pool.push(std::move(buffer));
    }

    const int total_threads = 2;

    using Op = ConsumeOperator<TimeSlice<1, CPUBuffer>>;

    Op consume_int_op[total_threads];
    std::vector<Op*> ops;
    for(auto i = 0; i < total_threads; ++i)
        ops.push_back(&consume_int_op[i]);
    GPUSendRecvOperator<Op, decltype(cpu_pool), TimeSlice<1, GPUBuffer>>
        send_recv_op(ops, &cpu_pool);   
    

    std::thread child_thread([&gpu_pool, &send_recv_op, stream](){
        for(int i = 1; i < 100; ++i) {
            TimeSlice<1, GPUBuffer> slice(i, &gpu_pool);
            auto size = (i - 1) % 10 + 1;
            // fmt::print("i = {}, size = {}, new_size = {}\n", slice.timestep_id(), slice.data().size(), size);
            slice.data().clear();
            slice.data().resize(size);
            fill_buffer<<<size, size, 0, stream>>>(slice.data().data(), size);
            cudaStreamSynchronize(stream);
            // fmt::print("sending i = {}, size = {}\n", i, size);
            send_recv_op.consume(std::move(slice));
        }
        TimeSlice<1, GPUBuffer> slice;
        // fmt::print("sending i = {}\n", slice.timestep_id());
        send_recv_op.consume(std::move(slice));
    });

    std::vector<std::thread> parent_threads;

    for(auto i = 0; i < total_threads; ++i) {
        parent_threads.emplace_back([i, &send_recv_op](){
            send_recv_op.drive(i);
        });
    }

    child_thread.join();
    for(auto i = 0; i < total_threads; ++i) {
        parent_threads[i].join();
    }
}
