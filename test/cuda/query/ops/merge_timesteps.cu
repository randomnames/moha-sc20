#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

// #define OTTERS_LOG_LEVEL OTTERS_LOG_DEBUG

#include <otters/common/bitmap/binning.h>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/bitmap/binning.cuh>
#include <otters/cuda/query/ops/bitmap_csm.cuh>
#include <otters/cuda/query/ops/consume_operator.cuh>
#include <otters/cuda/query/ops/cpu_generate_op.cuh>
#include <otters/cuda/query/ops/filter_two_op.cuh>
#include <otters/cuda/query/ops/gpu_bitmap_input.cuh>
#include <otters/cuda/query/ops/gpu_plain_input.cuh>
#include <otters/cuda/query/ops/gpu_send_recv_operator.cuh>
#include <otters/cuda/query/ops/local_send_recv_operator.cuh>
#include <otters/cuda/query/ops/merge_timestep.cuh>
#include <otters/cuda/query/ops/plain_join_count.cuh>
#include <otters/cuda/query/ops/bitmap_join_count.cuh>
#include <thread>
#include <vector>

using namespace otters;

TEST_CASE("A filter_two operator can merge two streams of timesteps.") {
    using buffer_type = system_malloc_buffer<double>;
    using Slice = TimeSlice<2, buffer_type>;
    // using PairSlice = PairSlice<Slice>;
    FilterTwoOperator<decltype(nullptr), Slice> filter_two_op(nullptr);
    auto merge_selector = [](auto& slice) {
        auto coord = slice.position();
        Coordinates<1> result(coord[0]);
        return std::make_pair(result, coord[1]);
    };
    MergeTimeSliceOperator<decltype(filter_two_op), Slice, decltype(merge_selector), 1> merge_op(
            &filter_two_op, merge_selector);

    using MergeOp = decltype(merge_op);
    std::vector<MergeOp*> send_recv_op_parents;
    send_recv_op_parents.push_back(&merge_op);
    LocalSendRecvOperator<MergeOp> send_recv_op(send_recv_op_parents, 2, 6);

    auto gen_coord_func_left = [](int i) {
        Coordinates<2> coord;
        coord[0] = i;
        coord[1] = 0;
        return coord;
    };

    auto gen_coord_func_right = [](int i) {
        Coordinates<2> coord;
        coord[0] = i;
        coord[1] = 1;
        return coord;
    };

    auto gen_buffer = [](buffer_type& buffer, const Coordinates<2>& coord) {
        auto size = coord[0];
        buffer.resize(size);
        for(auto i = 0; i < size; ++i) {
            buffer[i] = i;
        }
    };


    const int total_timesteps = 100;

    BufferPool<system_malloc_buffer<double>> _left_pool;
    initialize_buffer_pool(_left_pool, 4);

    BufferPool<system_malloc_buffer<double>> _right_pool;
    initialize_buffer_pool(_right_pool, 4);

    CPUGenerateOperator<decltype(send_recv_op),
            2,
            buffer_type,
            decltype(gen_coord_func_left),
            decltype(gen_buffer),
            decltype(_left_pool)>
            left_gen_op(&send_recv_op, total_timesteps, gen_coord_func_left, gen_buffer, &_left_pool);
    CPUGenerateOperator<decltype(send_recv_op),
            2,
            buffer_type,
            decltype(gen_coord_func_right),
            decltype(gen_buffer),
            decltype(_right_pool)>
            right_gen_op(&send_recv_op, total_timesteps, gen_coord_func_right, gen_buffer, &_right_pool);

    std::thread left_thread([&left_gen_op](){
        left_gen_op.drive();
        // send_recv_op.end_input();
    });
    std::thread right_thread([&right_gen_op](){
        right_gen_op.drive();
        // send_recv_op.end_input();
    });
    std::thread parent_thread([&send_recv_op]() {
        send_recv_op.drive(0);
    });
    left_thread.join();
    right_thread.join();
    parent_thread.join();
}

TEST_CASE("A csm operator can merge two streams of timesteps.") {
    using buffer_type = MultiAttributeBuffer<system_malloc_buffer<double>,2>;
    using Slice = TimeSlice<2, buffer_type>;
    BufferPool<buffer_type> _left_pool;
    initialize_buffer_pool(_left_pool, 4);

    BufferPool<buffer_type> _right_pool;
    initialize_buffer_pool(_right_pool, 4);

    // using PairSlice = PairSlice<Slice>;
    using ConsumeOp = ConsumeOperator<TimeSlice<2, BestContrastSets<double, 2, 3, 10>>>;
    ConsumeOp consume_op;
    using MinMaxGetter = EqualBinner<double>;
    std::array<MinMaxGetter, 2> min_max_getters = {
        MinMaxGetter(10, 0, 1000),
        MinMaxGetter(10, 0, 1000)
    };
    CSMOperator<ConsumeOp, Slice, MinMaxGetter, double, 3, 2> csm_op(
            &consume_op, { 10, 10, 10 }, min_max_getters, 10, 10);
    auto merge_selector = [](auto& slice) {
        auto coord = slice.position();
        Coordinates<1> result(coord[0]);
        // log(OTTERS_LOG_DEBUG, "coord = {} second = {}", coord, coord[1]);
        return std::make_pair(result, coord[1]);
    };
    MergeTimeSliceOperator2<decltype(csm_op), Slice, decltype(merge_selector), 1> merge_op(
            &csm_op, merge_selector);

    using MergeOp = decltype(merge_op);
    std::vector<MergeOp*> merge_ops;
    merge_ops.push_back(&merge_op);

    auto gen_coord_func_left = [](int i) {
        Coordinates<2> coord;
        coord[0] = i;
        coord[1] = 0;
        return coord;
    };

    auto gen_coord_func_right = [](int i) {
        Coordinates<2> coord;
        coord[0] = i;
        coord[1] = 1;
        return coord;
    };

    auto gen_buffer = [](buffer_type& buffer, const Coordinates<2>& coord) {
        (void) coord;
        auto size = 1000;
        buffer.attribute(0).resize(size);
        for(auto i = 0; i < size; ++i) {
            buffer.attribute(0)[i] = i;
        }
        buffer.attribute(1).resize(size);
        for(auto i = 0; i < size; ++i) {
            buffer.attribute(1)[i] = i;
        }
    };


    const int total_timesteps = 10;


    CPUGenerateOperator<decltype(merge_op),
            2,
            buffer_type,
            decltype(gen_coord_func_left),
            decltype(gen_buffer),
            decltype(_left_pool)>
            left_gen_op(&merge_op, total_timesteps, gen_coord_func_left, gen_buffer, &_left_pool);
    CPUGenerateOperator<decltype(merge_op),
            2,
            buffer_type,
            decltype(gen_coord_func_right),
            decltype(gen_buffer),
            decltype(_right_pool)>
            right_gen_op(&merge_op, total_timesteps, gen_coord_func_right, gen_buffer, &_right_pool);

    std::thread left_thread([&left_gen_op](){
        left_gen_op.drive();
        // merge_op.end_input(0);
        log(OTTERS_LOG_DEBUG, "left thread ended.");
    });
    std::thread right_thread([&right_gen_op](){
        right_gen_op.drive();
        // merge_op.end_input();
        log(OTTERS_LOG_DEBUG, "right thread ended.");
    });
    std::thread parent_thread([&merge_op]() {
        merge_op.drive();
        log(OTTERS_LOG_DEBUG, "merge thread ended.");
    });
    left_thread.join();
    right_thread.join();
    parent_thread.join();
}

__global__ void fill_buffer(double* data, int size) {
    auto thread_id = blockDim.x * blockIdx.x + threadIdx.x;
    if((int) thread_id < size)
        data[thread_id] = (thread_id % 1337) * (thread_id % 1337) % 1337 ;
}

__global__ void fill_buffer2(double* data, int size) {
    auto thread_id = blockDim.x * blockIdx.x + threadIdx.x;
    if((int) thread_id < size) {
        if(thread_id % 4 == 0)
            data[thread_id] = (thread_id % 1337) * (thread_id % 1337) % 1337;
        else
            data[thread_id] = 0;
    }
}



TEST_CASE("A bitmap csm operator can merge two streams of timesteps.") {
    using CPUBinner = EqualBinner<double>;
    using CPUBitmap = ParaRoaringBitmap<CPUBinner, cuda_host_buffer>;
    using CPUBuffer = MultiAttributeBuffer<CPUBitmap, 3>;
    using Slice = TimeSlice<2, CPUBuffer>;
    using GPUBitmap = GPURoaringBitmap<64, DefaultRoaringTraits>;
    using GPUBuffer = MultiAttributeBuffer<GPUBitmap, 3>;
    using GPUBinner = GPUEqualWidthBinner<64, double>;

    BufferPool<GPUBuffer> gpu_pool1;
    BufferPool<CPUBuffer> cpu_pool1;
    BufferPool<GPUBuffer> gpu_pool2;
    BufferPool<CPUBuffer> cpu_pool2;

    initialize_buffer_pool(gpu_pool1, 2);
    initialize_buffer_pool(cpu_pool1, 2);
    initialize_buffer_pool(gpu_pool2, 2);
    initialize_buffer_pool(cpu_pool2, 2);
    // using PairSlice = PairSlice<Slice>;

    using ConsumeOp = ConsumeOperator<TimeSlice<2, BestContrastSets<double, 3, 3, 10>>>;
    ConsumeOp consume_op;
    std::array<CPUBinner, 3> cpu_binners = {
        CPUBinner(64, 0, 1000),
        CPUBinner(64, 0, 1000),
        CPUBinner(64, 0, 1000)
    };
    CSMOperator<ConsumeOp, Slice, CPUBinner, double, 3, 3> csm_op(
            &consume_op, { 16, 64, 64 }, cpu_binners, 10, 64);
    auto merge_selector = [](auto& slice) {
        auto coord = slice.position();
        Coordinates<2> result(coord[0]);
        result[1] = 0;
        return std::make_pair(result, coord[1]);
    };
    MergeTimeSliceOperator2<decltype(csm_op), Slice, decltype(merge_selector), 2> merge_op(
            &csm_op, merge_selector);

    using MergeOp = decltype(merge_op);

    std::vector<MergeOp*> merge_ops;
    merge_ops.push_back(&merge_op);


    using GPUSendRecvOp = GPUSendRecvOperator<MergeOp, decltype(cpu_pool1), TimeSlice<2, GPUBuffer>>;
    GPUSendRecvOp gpu_send_recv_op1(merge_ops, &cpu_pool1);
    std::array<GPUBinner, 3> binners1 = { GPUBinner(0, 1337), GPUBinner(0, 1337), GPUBinner(0, 1337) };
    GPUBitmapInputOperator<GPUSendRecvOp, 2, double, 3, GPUBitmap, GPUBinner, decltype(gpu_pool1)> input_op1(
            std::move(binners1), &gpu_send_recv_op1, &gpu_pool1);
            
    GPUSendRecvOp gpu_send_recv_op2(merge_ops, &cpu_pool2);
    std::array<GPUBinner, 3> binners2 = { GPUBinner(0, 1337), GPUBinner(0, 1337), GPUBinner(0, 1337) };
    GPUBitmapInputOperator<GPUSendRecvOp, 2, double, 3, GPUBitmap, GPUBinner, decltype(gpu_pool2)> input_op2(
            std::move(binners2), &gpu_send_recv_op2, &gpu_pool2);

    std::thread cpu_thread([&merge_op]() {
        merge_op.drive();
        log(OTTERS_LOG_DEBUG, "cpu thread ended.");
    });
    std::thread gpu_recv_thread1([&gpu_send_recv_op1, &merge_op]() {
        gpu_send_recv_op1.drive(0);
        merge_op.end_input();
        log(OTTERS_LOG_DEBUG, "gpu recv thread1 ended.");
    });
    std::thread gpu_recv_thread2([&gpu_send_recv_op2, &merge_op]() {
        gpu_send_recv_op2.drive(0);
        merge_op.end_input();
        log(OTTERS_LOG_DEBUG, "gpu recv thread2 ended.");
    });
    std::thread gpu_send_thread1([&input_op1]() {
        auto size = 65536;
        cuda_device_buffer<double> gpu_data(size);
        cuda_device_buffer<double> gpu_data2(size);
        cuda_device_buffer<double> gpu_data3(size);
        cudaStream_t stream;
        cudaStreamCreate(&stream);
        for(int i = 0; i < 10; ++i) {
            // fmt::print("i = {}, size = {}, new_size = {}\n", slice.timestep_id(), slice.data().size(), size);
            fill_buffer2<<<size, 256, 0, stream>>>(gpu_data.data(), size);
            fill_buffer<<<size, 256, 0, stream>>>(gpu_data2.data(), size);
            fill_buffer2<<<size, 256, 0, stream>>>(gpu_data3.data(), size);
            OTTERS_CUDA_ERRORCHECK( cudaStreamSynchronize(stream) );
            // fmt::print("sending i = {}, size = {}\n", i, size);
            Coordinates<2> coord;
            coord[0] = i;
            coord[1] = 0;
            input_op1.add_input(coord, {gpu_data.data(), gpu_data2.data(), gpu_data3.data()}, {size, size, size});
        }
        input_op1.end_input();
        cudaStreamDestroy(stream);
        log(OTTERS_LOG_DEBUG, "gpu_send_thread1 ended.");
    });

    std::thread gpu_send_thread2([&input_op2]() {
        auto size = 65536;
        cuda_device_buffer<double> gpu_data(size);
        cuda_device_buffer<double> gpu_data2(size);
        cuda_device_buffer<double> gpu_data3(size);
        cudaStream_t stream;
        cudaStreamCreate(&stream);
        for(int i = 0; i < 10; ++i) {
            // fmt::print("i = {}, size = {}, new_size = {}\n", slice.timestep_id(), slice.data().size(), size);
            fill_buffer<<<size, 256, 0, stream>>>(gpu_data.data(), size);
            fill_buffer2<<<size, 256, 0, stream>>>(gpu_data2.data(), size);
            fill_buffer<<<size, 256, 0, stream>>>(gpu_data3.data(), size);
            OTTERS_CUDA_ERRORCHECK( cudaStreamSynchronize(stream) );
            // fmt::print("sending i = {}, size = {}\n", i, size);
            Coordinates<2> coord;
            coord[0] = i;
            coord[1] = 1;
            input_op2.add_input(coord, {gpu_data.data(), gpu_data2.data(), gpu_data3.data()}, {size, size, size});
        }
        input_op2.end_input();
        cudaStreamDestroy(stream);
        log(OTTERS_LOG_DEBUG, "gpu_send_thread2 ended.");
    });

    cpu_thread.join();
    gpu_recv_thread1.join();
    gpu_recv_thread2.join();
    gpu_send_thread1.join();
    gpu_send_thread2.join();
}


TEST_CASE("A plain join operator can merge two streams of timesteps.") {
    using buffer_type = MultiAttributeBuffer<system_malloc_buffer<double>,2>;
    using Slice = TimeSlice<2, buffer_type>;
    BufferPool<buffer_type> _left_pool;
    initialize_buffer_pool(_left_pool, 4);

    BufferPool<buffer_type> _right_pool;
    initialize_buffer_pool(_right_pool, 4);

    // using PairSlice = PairSlice<Slice>;
    using ConsumeOp = ConsumeOperator<TimeSlice<2, long>>;
    ConsumeOp consume_op;
    using MinMaxGetter = EqualBinner<double>;
    std::array<MinMaxGetter, 2> min_max_getters = {
        MinMaxGetter(10, 0, 1000),
        MinMaxGetter(10, 0, 1000)
    };
    PlainJoinCountOperator<ConsumeOp, Slice, MinMaxGetter, double, 2> plj_op(
            &consume_op, min_max_getters, 10);
    auto merge_selector = [](auto& slice) {
        auto coord = slice.position();
        Coordinates<1> result(coord[0]);
        // log(OTTERS_LOG_DEBUG, "coord = {} second = {}", coord, coord[1]);
        return std::make_pair(result, coord[1]);
    };
    MergeTimeSliceOperator2<decltype(plj_op), Slice, decltype(merge_selector), 1> merge_op(
            &plj_op, merge_selector);

    using MergeOp = decltype(merge_op);
    std::vector<MergeOp*> merge_ops;
    merge_ops.push_back(&merge_op);

    auto gen_coord_func_left = [](int i) {
        Coordinates<2> coord;
        coord[0] = i;
        coord[1] = 0;
        return coord;
    };

    auto gen_coord_func_right = [](int i) {
        Coordinates<2> coord;
        coord[0] = i;
        coord[1] = 1;
        return coord;
    };

    auto gen_buffer = [](buffer_type& buffer, const Coordinates<2>& coord) {
        (void) coord;
        auto size = 1000;
        buffer.attribute(0).resize(size);
        for(auto i = 0; i < size; ++i) {
            buffer.attribute(0)[i] = i;
        }
        buffer.attribute(1).resize(size);
        for(auto i = 0; i < size; ++i) {
            buffer.attribute(1)[i] = i;
        }
    };


    const int total_timesteps = 10;


    CPUGenerateOperator<decltype(merge_op),
            2,
            buffer_type,
            decltype(gen_coord_func_left),
            decltype(gen_buffer),
            decltype(_left_pool)>
            left_gen_op(&merge_op, total_timesteps, gen_coord_func_left, gen_buffer, &_left_pool);
    CPUGenerateOperator<decltype(merge_op),
            2,
            buffer_type,
            decltype(gen_coord_func_right),
            decltype(gen_buffer),
            decltype(_right_pool)>
            right_gen_op(&merge_op, total_timesteps, gen_coord_func_right, gen_buffer, &_right_pool);

    std::thread left_thread([&left_gen_op](){
        left_gen_op.drive();
        // merge_op.end_input(0);
        log(OTTERS_LOG_DEBUG, "left thread ended.");
    });
    std::thread right_thread([&right_gen_op](){
        right_gen_op.drive();
        // merge_op.end_input();
        log(OTTERS_LOG_DEBUG, "right thread ended.");
    });
    std::thread parent_thread([&merge_op]() {
        merge_op.drive();
        log(OTTERS_LOG_DEBUG, "merge thread ended.");
    });
    left_thread.join();
    right_thread.join();
    parent_thread.join();
}

TEST_CASE("A bitmap join operator can merge two streams of timesteps.") {
    using CPUBinner = EqualBinner<double>;
    using CPUBitmap = ParaRoaringBitmap<CPUBinner, cuda_host_buffer>;
    using CPUBuffer = MultiAttributeBuffer<CPUBitmap, 3>;
    using Slice = TimeSlice<2, CPUBuffer>;
    using GPUBitmap = GPURoaringBitmap<64, DefaultRoaringTraits>;
    using GPUBuffer = MultiAttributeBuffer<GPUBitmap, 3>;
    using GPUBinner = GPUEqualWidthBinner<64, double>;

    BufferPool<GPUBuffer> gpu_pool1;
    BufferPool<CPUBuffer> cpu_pool1;
    BufferPool<GPUBuffer> gpu_pool2;
    BufferPool<CPUBuffer> cpu_pool2;

    initialize_buffer_pool(gpu_pool1, 2);
    initialize_buffer_pool(cpu_pool1, 2);
    initialize_buffer_pool(gpu_pool2, 2);
    initialize_buffer_pool(cpu_pool2, 2);
    // using PairSlice = PairSlice<Slice>;

    using ConsumeOp = ConsumeOperator<TimeSlice<2, long>>;
    ConsumeOp consume_op;
    BitmapJoinCountOperator<ConsumeOp, Slice, double, 3> bjc_op( &consume_op);
    auto merge_selector = [](auto& slice) {
        auto coord = slice.position();
        Coordinates<2> result(coord[0]);
        result[1] = 0;
        return std::make_pair(result, coord[1]);
    };
    MergeTimeSliceOperator2<decltype(bjc_op), Slice, decltype(merge_selector), 2> merge_op(
            &bjc_op, merge_selector);

    using MergeOp = decltype(merge_op);

    std::vector<MergeOp*> merge_ops;
    merge_ops.push_back(&merge_op);


    using GPUSendRecvOp = GPUSendRecvOperator<MergeOp, decltype(cpu_pool1), TimeSlice<2, GPUBuffer>>;
    GPUSendRecvOp gpu_send_recv_op1(merge_ops, &cpu_pool1);
    std::array<GPUBinner, 3> binners1 = { GPUBinner(0, 1337), GPUBinner(0, 1337), GPUBinner(0, 1337) };
    GPUBitmapInputOperator<GPUSendRecvOp, 2, double, 3, GPUBitmap, GPUBinner, decltype(gpu_pool1)> input_op1(
            std::move(binners1), &gpu_send_recv_op1, &gpu_pool1);
            
    GPUSendRecvOp gpu_send_recv_op2(merge_ops, &cpu_pool2);
    std::array<GPUBinner, 3> binners2 = { GPUBinner(0, 1337), GPUBinner(0, 1337), GPUBinner(0, 1337) };
    GPUBitmapInputOperator<GPUSendRecvOp, 2, double, 3, GPUBitmap, GPUBinner, decltype(gpu_pool2)> input_op2(
            std::move(binners2), &gpu_send_recv_op2, &gpu_pool2);

    std::thread cpu_thread([&merge_op]() {
        merge_op.drive();
        log(OTTERS_LOG_DEBUG, "cpu thread ended.");
    });
    std::thread gpu_recv_thread1([&gpu_send_recv_op1, &merge_op]() {
        gpu_send_recv_op1.drive(0);
        merge_op.end_input();
        log(OTTERS_LOG_DEBUG, "gpu recv thread1 ended.");
    });
    std::thread gpu_recv_thread2([&gpu_send_recv_op2, &merge_op]() {
        gpu_send_recv_op2.drive(0);
        merge_op.end_input();
        log(OTTERS_LOG_DEBUG, "gpu recv thread2 ended.");
    });
    std::thread gpu_send_thread1([&input_op1]() {
        auto size = 65536;
        cuda_device_buffer<double> gpu_data(size);
        cuda_device_buffer<double> gpu_data2(size);
        cuda_device_buffer<double> gpu_data3(size);
        cudaStream_t stream;
        cudaStreamCreate(&stream);
        for(int i = 0; i < 10; ++i) {
            // fmt::print("i = {}, size = {}, new_size = {}\n", slice.timestep_id(), slice.data().size(), size);
            fill_buffer2<<<size, 256, 0, stream>>>(gpu_data.data(), size);
            fill_buffer<<<size, 256, 0, stream>>>(gpu_data2.data(), size);
            fill_buffer2<<<size, 256, 0, stream>>>(gpu_data3.data(), size);
            OTTERS_CUDA_ERRORCHECK( cudaStreamSynchronize(stream) );
            // fmt::print("sending i = {}, size = {}\n", i, size);
            Coordinates<2> coord;
            coord[0] = i;
            coord[1] = 0;
            input_op1.add_input(coord, {gpu_data.data(), gpu_data2.data(), gpu_data3.data()}, {size, size, size});
        }
        input_op1.end_input();
        cudaStreamDestroy(stream);
        log(OTTERS_LOG_DEBUG, "gpu_send_thread1 ended.");
    });

    std::thread gpu_send_thread2([&input_op2]() {
        auto size = 65536;
        cuda_device_buffer<double> gpu_data(size);
        cuda_device_buffer<double> gpu_data2(size);
        cuda_device_buffer<double> gpu_data3(size);
        cudaStream_t stream;
        cudaStreamCreate(&stream);
        for(int i = 0; i < 10; ++i) {
            // fmt::print("i = {}, size = {}, new_size = {}\n", slice.timestep_id(), slice.data().size(), size);
            fill_buffer<<<size, 256, 0, stream>>>(gpu_data.data(), size);
            fill_buffer2<<<size, 256, 0, stream>>>(gpu_data2.data(), size);
            fill_buffer<<<size, 256, 0, stream>>>(gpu_data3.data(), size);
            OTTERS_CUDA_ERRORCHECK( cudaStreamSynchronize(stream) );
            // fmt::print("sending i = {}, size = {}\n", i, size);
            Coordinates<2> coord;
            coord[0] = i;
            coord[1] = 1;
            input_op2.add_input(coord, {gpu_data.data(), gpu_data2.data(), gpu_data3.data()}, {size, size, size});
        }
        input_op2.end_input();
        cudaStreamDestroy(stream);
        log(OTTERS_LOG_DEBUG, "gpu_send_thread2 ended.");
    });

    cpu_thread.join();
    gpu_recv_thread1.join();
    gpu_recv_thread2.join();
    gpu_send_thread1.join();
    gpu_send_thread2.join();
}
