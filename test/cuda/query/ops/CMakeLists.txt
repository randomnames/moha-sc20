add_executable(local_send_recv local_send_recv.cu)
target_link_libraries(local_send_recv
    otter-header
    Threads::Threads
    fmt::fmt
    cudart
    dl
)
target_link_directories(local_send_recv PRIVATE ${CUDA_ROOT}/lib64)
add_cuda_test(CUDA_QUERY_LOCAL_SEND_RECV "unit" local_send_recv)

add_executable(gpu_send_recv gpu_send_recv.cu)
target_link_libraries(gpu_send_recv
    otter-header
    Threads::Threads
    fmt::fmt
    cudart
    dl
)
target_link_directories(gpu_send_recv PRIVATE ${CUDA_ROOT}/lib64)
add_cuda_test(CUDA_QUERY_GPU_SEND_RECV "unit" gpu_send_recv)

add_executable(gpu_plain_input gpu_plain_input.cu)
target_link_libraries(gpu_plain_input
    otter-header
    Threads::Threads
    fmt::fmt
    cudart
    dl
)
target_link_directories(gpu_plain_input PRIVATE ${CUDA_ROOT}/lib64)
add_cuda_test(CUDA_QUERY_GPU_PLAIN_INPUT "unit" gpu_plain_input)

add_executable(gpu_bitmap_input gpu_bitmap_input.cu)
target_link_libraries(gpu_bitmap_input
    otter-header
    Threads::Threads
    fmt::fmt
    cudart
    dl
)
target_link_directories(gpu_bitmap_input PRIVATE ${CUDA_ROOT}/lib64)
add_cuda_test(CUDA_QUERY_GPU_BITMAP_INPUT "unit" gpu_bitmap_input)

add_executable(gpu_random_generator gpu_random_generator.cu)
target_link_libraries(gpu_random_generator
    otter-header
    Threads::Threads
    fmt::fmt
    cudart
    dl
)
target_link_directories(gpu_random_generator PRIVATE ${CUDA_ROOT}/lib64)
add_cuda_test(CUDA_QUERY_GPU_RANDOM_GENERATOR "unit" gpu_random_generator)


add_executable(gpu_scan_save gpu_scan_save.cu)
target_link_libraries(gpu_scan_save
    otter-header
    Threads::Threads
    fmt::fmt
    cudart
    dl
)
target_link_directories(gpu_scan_save PRIVATE ${CUDA_ROOT}/lib64)
add_cuda_test(CUDA_QUERY_GPU_SCAN_SAVE "unit" gpu_scan_save)

add_executable(window window.cu)
target_link_libraries(window
    otter-header
    Threads::Threads
    fmt::fmt
    cudart
    dl
)
target_link_directories(window PRIVATE ${CUDA_ROOT}/lib64)
add_cuda_test(CUDA_QUERY_WINDOW "unit" window)

add_executable(plain_filter_oir plain_filter_oir.cu)
target_link_libraries(plain_filter_oir
        otter-header
        Threads::Threads
        fmt::fmt
        cudart
        dl
        )
target_link_directories(plain_filter_oir PRIVATE ${CUDA_ROOT}/lib64)
add_cuda_test(CUDA_QUERY_PLAIN_FILTER_OIR "unit" plain_filter_oir)

add_executable(bitmap_filter_oir bitmap_filter_oir.cu)
target_link_libraries(bitmap_filter_oir
        otter-header
        Threads::Threads
        fmt::fmt
        cudart
        dl
        )
target_link_directories(bitmap_filter_oir PRIVATE ${CUDA_ROOT}/lib64)
add_cuda_test(CUDA_QUERY_BITMAP_FILTER_OIR "unit" bitmap_filter_oir)

add_executable(merge_timesteps merge_timesteps.cu)
target_link_libraries(merge_timesteps
    otter-header
    Threads::Threads
    fmt::fmt
    cudart
    dl
)
target_link_directories(merge_timesteps PRIVATE ${CUDA_ROOT}/lib64)
add_cuda_test(CUDA_QUERY_GPU_MERGE_TIMESTEPS "unit" merge_timesteps)


add_executable(mpi_send_recv_simple mpi_send_recv_simple.cu)
target_link_libraries(mpi_send_recv_simple
    otter-header
    Threads::Threads
    fmt::fmt
    cudart
    MPI::MPI_CXX
    dl
)
target_link_directories(mpi_send_recv_simple PRIVATE ${CUDA_ROOT}/lib64)
add_mpi_test(CUDA_QUERY_MPI_SEND_RECV_SIMPLE "unit" 4 mpi_send_recv_simple)

