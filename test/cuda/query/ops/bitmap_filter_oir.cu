#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <otters/common/containers/buffer.h>
#include <otters/common/containers/time_slice.h>
#define OTTERS_OIR_DISPLAY_RESULT
#include <otters/cuda/query/ops/bitmap_filter_oir.cuh>
#include <otters/common/bitmap/binning.h>

#include <fmt/core.h>
#include <boost/dynamic_bitset.hpp>

using namespace otters;

constexpr int chunk_size = 65536;

auto from_dynamic_bitsets(std::vector<boost::dynamic_bitset<uint64_t>> bitsets) {
    EqualBinner<double> equal_binner((int) bitsets.size(), 0, 1);
    ParaRoaringBitmap<decltype(equal_binner), system_malloc_buffer> para_roaring1(equal_binner);

    for (auto k = 0u; k < bitsets[0].size() / chunk_size; ++k) {
        uint64_t range[1024];
        for (auto i = 0u; i < bitsets.size(); ++i) {
            boost::dynamic_bitset<uint64_t> temp(chunk_size);
            for (auto j = 0; j < chunk_size; ++j) {
                temp[j] = bitsets[i][k * chunk_size + j];
            }
            boost::to_block_range(temp, range);

            para_roaring1.add_chunk_from_bitmap(range);
        }
    }
    para_roaring1.set_size(bitsets[0].size());
    return para_roaring1;
}

auto get_bitsets(int num_of_buckets) {
    std::vector<boost::dynamic_bitset<uint64_t>> bitsets1;
    for(auto i = 0; i < num_of_buckets; ++i)
        bitsets1.emplace_back(chunk_size * 2);
    return bitsets1;
}

void set_bitset(std::vector<boost::dynamic_bitset<uint64_t>>& bitset) {
    for(auto i = 0; i < 10; ++i) {
        for(auto j = 0; j < 10; ++j) {
            for(auto k = 0; k < 10; ++k) {
                auto offset = i * 100 + j * 10 + k;
                if(i == 0 && j == 0) {
                    bitset[0].set(offset);
                } else if (i >= 2 && j == 2 && k == 0) {
                    bitset[0].set(offset);
                } else {
                    bitset[2].set(offset);
                }
            }
        }
    }
}

TEST_CASE("Case") {
    auto bitset1 = get_bitsets(3);
    auto bitset2 = get_bitsets(3);
    auto bitset3 = get_bitsets(3);
    auto bitset4 = get_bitsets(3);

    set_bitset(bitset1);
    set_bitset(bitset2);
    set_bitset(bitset2);
    set_bitset(bitset4);

    std::array<EqualBinner<double>, 2> equal_binner {
        EqualBinner<double>(3, 0.0, 3.0),
        EqualBinner<double>(3, 0.0, 3.0)
    };
    using Bitmap = decltype(from_dynamic_bitsets(bitset1));
    using slice_type = TimeSlice<1, MultiAttributeBuffer<Bitmap, 2>>;
    slice_type left_slice(0);
    slice_type right_slice(0);
    left_slice.data().attribute(0) = from_dynamic_bitsets(bitset1);
    left_slice.data().attribute(1) = from_dynamic_bitsets(bitset2);
    right_slice.data().attribute(0) = from_dynamic_bitsets(bitset3);
    right_slice.data().attribute(1) = from_dynamic_bitsets(bitset4);
    BitmapFilterOIRProcessor3D<Bitmap, double, EqualBinner<double>, 1, 3, 2> processor
            (equal_binner,
                    {0,0,0},
                    {10,10,10},
                    {10,10,10},
                    {Range<double>{0.0, 0.5}, Range<double>{0.0,0.5}}, 3);
    processor.process(left_slice, right_slice);
    processor.process(left_slice, right_slice);
}

TEST_CASE("Works") {
    std::array<int64_t, 3> dim_min = {0,0,0};
    std::array<int64_t, 3> dim_max = {10,10,10};
    std::array<int64_t, 3> dim_sizes = {10,10,10};

    std::array<Range<double>, 2> ranges = { Range<double>{0.0,3.0}, Range<double>{0.0, 3.0} };

    using CPUBinner = EqualBinner<double>;
    std::array<EqualBinner<double>, 2> equal_binner {
            EqualBinner<double>(3, 0.0, 3.0),
            EqualBinner<double>(3, 0.0, 3.0)
    };
    using Bitmap = decltype(from_dynamic_bitsets(get_bitsets(3)));
    using slice_type = TimeSlice<2, MultiAttributeBuffer<Bitmap, 2>>;

    BitmapFilterOIROperator3D<decltype(nullptr), Bitmap, double, CPUBinner, 2, 3, 2> op(nullptr, 8, 16, equal_binner, dim_min, dim_max, dim_sizes, ranges, 3);
    std::vector<std::thread> threads;
    for(auto i = 0; i < 8; ++i) {
        threads.emplace_back([&op, i](){
          op.drive(i);
        });
    }

    for(auto i = 0; i < 100; ++i) {
        Coordinates<2> coordinates;
        coordinates[0] = i;
        slice_type slice(coordinates);
        //        fmt::print("position = {} timestep_id = {} bool = {}\n", slice.position(), slice.timestep_id(), (bool) slice);
        auto bitset1 = get_bitsets(3);
        auto bitset2 = get_bitsets(3);

        set_bitset(bitset1);
        set_bitset(bitset2);
        slice.data().attribute(0) = from_dynamic_bitsets(bitset1);
        slice.data().attribute(1) = from_dynamic_bitsets(bitset2);
        op.consume(std::move(slice));
    }
    op.end_input();
    for(auto& thread: threads ){
        thread.join();
    }
}
