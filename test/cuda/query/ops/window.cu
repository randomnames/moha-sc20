#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <otters/common/containers/buffer.h>
#include <otters/common/containers/time_slice.h>
#include <otters/cuda/query/ops/window.cuh>

#include <fmt/core.h>

using namespace otters;

struct Func {
    using Buffer = system_malloc_buffer<double>;
    using slice_type = TimeSlice<2, Buffer>;
    void operator()(slice_type& left, slice_type& right) {
        log(OTTERS_LOG_WARNING, "left = {}, right = {}", left.position(), right.position());
    }
};

TEST_CASE("Case") {
    using Buffer = system_malloc_buffer<double>;
    using slice_type = TimeSlice<2, Buffer>;
    std::vector<Func> funcs(8);
    WindowOperatorBase<int, slice_type, Func,  2> op(nullptr, std::move(funcs), 16);
    std::vector<std::thread> threads;
    for(auto i = 0; i < 8; ++i) {
        threads.emplace_back([&op, i](){
            op.drive(i);
        });
    }
    for(auto i = 0; i < 100; ++i) {
        Coordinates<2> coordinates;
        coordinates[0] = i;
        slice_type slice(coordinates);
        slice.data().resize(1);
        slice.data()[0] = i;
//        fmt::print("position = {} timestep_id = {} bool = {}\n", slice.position(), slice.timestep_id(), (bool) slice);
        op.consume(std::move(slice));
    }
    op.end_input();
    for(auto& thread: threads ){
        thread.join();
    }
}
