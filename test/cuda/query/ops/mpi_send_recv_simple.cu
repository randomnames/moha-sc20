#define CATCH_CONFIG_RUNNER
#include <catch2/catch.hpp>

#include <otters/common/utils/log.h>
#include <otters/cuda/query/timestep.cuh>
#include <otters/cuda/container/buffer_pool.cuh>
#include <otters/cuda/query/ops/mpi_send_recv.cuh>
#include <otters/cuda/query/ops/consume_operator.cuh>
#include <otters/cuda/container/buffer.cuh>

#include <otters/common/third_party/mpi.h>

#include <vector>

using namespace otters;

TEST_CASE("MPI test") {
// void test() {
    int rank;
    int total_procs;
    int total_timesteps = 100;
    MPI_Comm_size(MPI_COMM_WORLD, &total_procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // char buff[6] = "Hello";
    // buff[rank] = '-';
    // for(auto i = 0; i < 100; ++i) {
    // MPI_Send(buff, 6, MPI_BYTE, (rank + 1) % 4, 0, MPI_COMM_WORLD);
    // MPI_Status status;
    // MPI_Recv(buff, 6, MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    // fmt::print("Received: Rank = {}, buff = {}\n", rank, buff);
    // }


    auto log_file_name = fmt::format("mpi_send_recv_simple.{}.log", rank);
    freopen(log_file_name.c_str(), "w", stderr);

    // fmt::print(stderr, "Received: Rank = {}, buff = {}\n", rank, buff);

    log(OTTERS_LOG_DEBUG, "Initialized MPI Process: {} / {}", rank, total_procs);

    auto total_nodes = total_procs / 2;

    MPINodeInfo node_info;
    node_info._rank = rank;
    node_info._total_nodes = total_nodes;

    if(rank % 2 == 0) {
        auto node_selector = [node_info](auto& slice) {
            auto i = slice.timestep_id();
            log(OTTERS_LOG_DEBUG, "Selected node: {}, {}", i, ((i / 2) % node_info._total_nodes) * 2 + 1);
            return ((i / 2) % node_info._total_nodes) * 2 + 1;
        };
        MPISendOperator<TimeSlice<1, system_malloc_buffer<int>>, decltype(node_selector)>
            mpi_send_op(node_info, node_selector, 1, total_nodes);
        std::thread send_loop([&mpi_send_op](){
            mpi_send_op.send_loop();
            log(OTTERS_LOG_DEBUG, "Ended loop thread.\n");
        });
        std::thread child_thread([&mpi_send_op, total_timesteps, total_procs]() {
            BufferPool<system_malloc_buffer<int>> _pool;
            for (auto i = 0; i < total_procs; ++i) {
                system_malloc_buffer<int> buf;
                _pool.push(std::move(buf));
            }

            for(auto i = 0; i < total_timesteps; ++i) {
                log(OTTERS_LOG_DEBUG, "Sending timestep {}", i);
                TimeSlice<1, system_malloc_buffer<int>> slice(i, &_pool);
                mpi_send_op.consume(std::move(slice));
            }
            TimeSlice<1, system_malloc_buffer<int>> slice(-1);
            mpi_send_op.consume(std::move(slice));
            log(OTTERS_LOG_DEBUG, "Ended consume thread.\n");
        });
        child_thread.join();
        send_loop.join();
    } else {
        using Buffer = system_malloc_buffer<int>;
        using Slice = TimeSlice<1, Buffer>;
        using Op = ConsumeOperator<TimeSlice<1, system_malloc_buffer<int>>>;
        Op consume_op;
        std::vector<Op*> parent_ops;
        parent_ops.push_back(&consume_op);
        BufferPool<Buffer> _pool;
        for(auto i = 0; i < 3; ++i) {
            Buffer buffer;
            _pool.push(std::move(buffer));
        }
        MPIRecvOperator<Op, Slice, BufferPool<Buffer>> mpi_recv_op(parent_ops, 2, 2, &_pool);
        std::thread recv_loop([&mpi_recv_op]() {
            mpi_recv_op.recv_loop();
            log(OTTERS_LOG_DEBUG, "Ended loop thread.\n");
        });
        std::thread parent_thread([&mpi_recv_op]() {
            mpi_recv_op.drive(0);
            log(OTTERS_LOG_DEBUG, "Ended consume thread.\n");
        });
        recv_loop.join();
        parent_thread.join();
    }
}

int main(int argc, char** argv) {
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    assert(provided >= MPI_THREAD_MULTIPLE);

    Catch::Session().run(argc, argv);

    // test();
    // int rank;
    // MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}
