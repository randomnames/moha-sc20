#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <otters/cuda/query/ops/gpu_send_recv_operator.cuh>
#include <otters/cuda/query/ops/gpu_bitmap_input.cuh>
#include <otters/cuda/query/ops/consume_operator.cuh>

#include <otters/common/bitmap/binning.h>
#include <otters/cuda/bitmap/binning.cuh>
#include <otters/cuda/bitmap/roaring.cuh>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/multi_attribute_buffer.cuh>
#include <otters/cuda/container/buffer_pool.cuh>
#include <otters/cuda/query/timestep.cuh>

using namespace otters;

__global__ void fill_buffer(int* data, int size) {
    auto thread_id = blockDim.x * blockIdx.x + threadIdx.x;
    if((int) thread_id < size)
        data[thread_id] = thread_id;
}

TEST_CASE("A gpu send/recv operator can work through two threads via BufferPool.") {
    cudaStream_t stream;
    cudaStreamCreate(&stream);
    using GPUBitmap = GPURoaringBitmap<64, DefaultRoaringTraits>;
    using GPUBuffer = MultiAttributeBuffer<GPUBitmap, 1>;
    using GPUBinner = GPUEqualWidthBinner<64, int>;
    std::array<GPUBinner, 1> binners = { GPUBinner(0, 1024) };

    BufferPool<GPUBuffer> gpu_pool;
    for(auto i = 0; i < 5; ++i) {
        GPUBuffer buffer;
        gpu_pool.push(std::move(buffer));
    }

    EqualBinner<int> cpu_binner(64, 0, 1024);
    using CPUBitmap = ParaRoaringBitmap<decltype(cpu_binner), cuda_host_buffer>;
    using CPUBuffer = MultiAttributeBuffer<CPUBitmap, 1>;
    BufferPool<CPUBuffer> cpu_pool;
    for(auto i = 0; i < 5; ++i) {
        CPUBuffer buffer;
        cpu_pool.push(std::move(buffer));
    }

    const int total_threads = 2;

    using Op = ConsumeOperator<TimeSlice<1, CPUBuffer>>;

    Op consume_int_op[total_threads];
    std::vector<Op*> ops;
    for(auto i = 0; i < total_threads; ++i)
        ops.push_back(&consume_int_op[i]);
    using GPUSendRecvOp = GPUSendRecvOperator<Op, decltype(cpu_pool), TimeSlice<1, GPUBuffer>>;
    GPUSendRecvOp send_recv_op(ops, &cpu_pool);   
    GPUBitmapInputOperator<GPUSendRecvOp, 1, int, 1, GPUBitmap, GPUBinner, decltype(gpu_pool)> input_op(std::move(binners), &send_recv_op, &gpu_pool);


    std::thread child_thread([&input_op, stream](){
        auto size = 65536;
        cuda_device_buffer<int> gpu_data(size);
        for(int i = 0; i < 100; ++i) {
            // fmt::print("i = {}, size = {}, new_size = {}\n", slice.timestep_id(), slice.data().size(), size);
            fill_buffer<<<size, 256, 0, stream>>>(gpu_data.data(), size);
            OTTERS_CUDA_ERRORCHECK( cudaStreamSynchronize(stream) );
            // fmt::print("sending i = {}, size = {}\n", i, size);
            input_op.add_input(i, {gpu_data.data()}, {size});
        }
        input_op.end_input();
        // fmt::print("sending i = {}\n", slice.timestep_id());
    });

    std::vector<std::thread> parent_threads;

    for(auto i = 0; i < total_threads; ++i) {
        parent_threads.emplace_back([i, &send_recv_op](){
            send_recv_op.drive(i);
        });
    }

    child_thread.join();
    for(auto i = 0; i < total_threads; ++i) {
        parent_threads[i].join();
    }
}
