#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <otters/common/containers/buffer.h>
#include <otters/common/containers/time_slice.h>
#include <otters/cuda/query/ops/plain_filter_oir.cuh>

#include <fmt/core.h>

using namespace otters;

void generate_data(double* data1) {
    for(auto i = 0; i < 10; ++i) {
        for(auto j = 0; j < 10; ++j) {
            for(auto k = 0; k < 10; ++k) {
                auto offset = i * 100 + j * 10 + k;
                if(i == 0 && j == 0) {
                    data1[offset] = 8.0;
                } else if (i >= 2 && j == 2 && k == 0) {
                    data1[offset] = 8.0;
                } else {
                    data1[offset] = 98.0;
                }
            }
        }
    }
}

TEST_CASE("Case") {
    PlainFilterOIRProcessor3D<double, 2, 3, 1> processor(
            {0,0,0},
            {10,10,10},
            {10,10,10 },
            {Range<double>{0.0,10.0}});
    using slice_type = TimeSlice<2, MultiAttributeBuffer<cuda_host_buffer<double>, 1>>;
    slice_type slice1;
    slice_type slice2;
    slice1.data().attribute(0).resize(1000);
    slice2.data().attribute(0).resize(1000);
    auto data1 = slice1.data().attribute(0).data();
    auto data2 = slice1.data().attribute(0).data();
    generate_data(data1);
    generate_data(data2);

    processor.process(slice1, slice2);
}

TEST_CASE("Operator works") {
    std::array<int64_t, 3> dim_min = {0,0,0};
    std::array<int64_t, 3> dim_max = {10,10,10};
    std::array<int64_t, 3> dim_sizes = {10,10,10};

    std::array<Range<double>, 1> ranges = { Range<double>{0.0,10.0} };
    PlainFilterOIROperator3D<decltype(nullptr), double, 2, 3, 1> op(nullptr, 8, 16, dim_min, dim_max, dim_sizes, ranges);
    std::vector<std::thread> threads;
    for(auto i = 0; i < 8; ++i) {
        threads.emplace_back([&op, i](){
          op.drive(i);
        });
    }

    using Buffer = cuda_host_buffer<double>;
    using slice_type = TimeSlice<2, MultiAttributeBuffer<Buffer, 1>>;

    for(auto i = 0; i < 100; ++i) {
        Coordinates<2> coordinates;
        coordinates[0] = i;
        slice_type slice(coordinates);
        slice.data().attribute(0).resize(1000);
        generate_data(slice.data().attribute(0).data());
    //        fmt::print("position = {} timestep_id = {} bool = {}\n", slice.position(), slice.timestep_id(), (bool) slice);
        op.consume(std::move(slice));
    }
    op.end_input();
    for(auto& thread: threads ){
        thread.join();
    }
}
