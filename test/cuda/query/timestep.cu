#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/query/timestep.cuh>

using namespace otters;

TEST_CASE("A timestep can be serialized") {
    TimeSlice<1,system_malloc_buffer<int>> slice(2, nullptr);
    slice.data().resize(1);
    slice.data()[0] = 0x19260817;
    byte_buffer buffer;
    buffer.resize(slice.serialize_size());
    slice.serialize(span<char>(buffer.data(), buffer.size()));

    TimeSlice<1, system_malloc_buffer<int>> slice2;
    slice2.deserialize(span<char>(buffer.data(), buffer.size()));

    REQUIRE(slice2.timestep_id() == 2);
    REQUIRE(slice2.data().size() == 1);
    REQUIRE(slice2.data()[0] == 0x19260817);
}
