#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#pragma clang diagnostic ignored "-Werror"
#include <boost/dynamic_bitset.hpp>
#pragma clang diagnostic pop
#include <stdio.h>
#include <otters/cuda/io/storage_engine.cuh>
#include <otters/cuda/query/io_processor.cuh>
#include <otters/common/bitmap/roaring_bitmap.h>
#include <otters/common/bitmap/binning.h>
#include <otters/common/utils/filesystem_tools.h>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/query/timestep.cuh>

using namespace otters;

template<typename BufferGenerator>
void write_buffers_to_disk(std::string prefix, int timesteps, BufferGenerator&& buffer_generator) {
    using TimeStepArrayT = TimeStepArray<typename std::decay<decltype(buffer_generator(0).data()[0])>::type, 1>;
    TimeStepArrayT array(prefix, TimeStepArrayT::OpenType::WRITE_ONLY);
    for(auto i = 0; i < timesteps; ++i) {
        auto buffer = buffer_generator(i);
        array.add_timestep(i, {buffer});
    }
}

TEST_CASE("An io processor can write plain and read it back.") {
    std::string prefix("io_processor_plain_can_write");
    auto timestep_count = 100;
    auto timestep_size = 1024;
    write_buffers_to_disk(prefix, timestep_count, [timestep_size](int i) {
        TimeStep<std::array<system_malloc_buffer<double>, 1>> step(i);
        step.data()[0].resize(timestep_size);

        std::fill(step.data()[0].get(), step.data()[0].get() + timestep_size, i);
        return step;
    });

    IOProcessor<system_malloc_buffer<double>, 1> io_processor(prefix);
    for(auto i = 0; i < timestep_count; ++i) {
        // fmt::print("submitting job for timestep {}\n", i);
        io_processor.read_timestep(i, [i, &io_processor](auto&& step) {
            fmt::print("The {} of timestep {} is {}\n", i, i, step.data()[0][i]);
            io_processor.return_buffer(std::move(step));
        });
    }
    //sleep(50);
}

auto from_dynamic_bitset(boost::dynamic_bitset<uint64_t> bitset) {
    EqualBinner<double> equal_binner(1, 0, 1);
    ParaRoaringBitmap<decltype(equal_binner), system_malloc_buffer> para_roaring1;

    uint64_t range[1024];

    boost::to_block_range(bitset, range);
    para_roaring1.add_chunk_from_bitmap(range);
    return para_roaring1;
}


TEST_CASE("An IO processor can write bitmap and read it back.") {
    std::string prefix("io_processor_bitmap_can_write");
    remove_files_with_prefix(prefix);
    auto timestep_count = 100;
    auto timestep_size = 65536;

    write_buffers_to_disk(prefix, timestep_count, [timestep_size](int i) {
        boost::dynamic_bitset<uint64_t> chunk(timestep_size);
        for (auto j = 2; j < 65536; j += 1337) {
            chunk.set(j);
        }
        auto para_roaring1 = from_dynamic_bitset(chunk);
        TimeStep<std::array<decltype(para_roaring1), 1>> step(i);
        step.data()[0] = std::move(para_roaring1);

        return step;
    });

    IOProcessor<ParaRoaringBitmap<EqualBinner<double>, system_malloc_buffer>, 1> io_processor(prefix);
    for(auto i = 0; i < timestep_count; ++i) {
        // fmt::print("submitting job for timestep {}\n", i);
        io_processor.read_timestep(i, [i, &io_processor](auto&& step) {
            fmt::print("The size of timestep {} is {}, ", i, step.data()[0].size());
            fmt::print("The storage size of timestep {} is {}\n", i, step.data()[0].serialize_size()._storage_size);
            io_processor.return_buffer(std::move(step));
        });
    }
   
}
