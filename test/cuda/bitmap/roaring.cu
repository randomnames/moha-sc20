#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include <otters/cuda/exception/cuda_assert.h>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/iterator.cuh>
#include <otters/common/bitmap/binning.h>
#include <otters/common/bitmap/roaring_bitmap.h>
#include <otters/cuda/bitmap/roaring.cuh>

using namespace otters;

TEST_CASE("Chunk info can be correctly calculated.") {
    constexpr auto NKEY = 64;
    constexpr auto chunk_length = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::chunk_length;
    constexpr auto block_threads = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::block_threads;
    using ChunkSize = typename DefaultRoaringTraits::ChunkSize;
    using ChunkType = typename DefaultRoaringTraits::ChunkType;
    cuda_managed_buffer<int32_t> bucket_ids(65536);
    cuda_managed_buffer<ChunkSize> chunk_sizes(NKEY);
    cuda_managed_buffer<ChunkType> chunk_types(NKEY);
    for(int i = 0; i < chunk_length ; ++i) {
        bucket_ids[i] = i % 2;
    }
    calculate_chunk_info<NKEY, DefaultRoaringTraits>
            <<<1, block_threads>>>(reinterpret_cast<ChunkSize(*)[64]>(chunk_sizes.get()),
                    reinterpret_cast<ChunkType(*)[64]>(chunk_types.get()),
                    bucket_ids.get(),
                    chunk_length);
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );
    REQUIRE(chunk_sizes[0] == 8192);
    REQUIRE(chunk_sizes[1] == 8192);
    REQUIRE(chunk_types[0] == (ChunkType) RoaringChunkType::Uncompressed);
    REQUIRE(chunk_types[1] == (ChunkType) RoaringChunkType::Uncompressed);
}

TEST_CASE("All-one chunk info can be correctly calculated.") {
    constexpr auto NKEY = 64;
    constexpr auto chunk_length = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::chunk_length;
    constexpr auto block_threads = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::block_threads;
    using ChunkSize = typename DefaultRoaringTraits::ChunkSize;
    using ChunkType = typename DefaultRoaringTraits::ChunkType;
    cuda_managed_buffer<int32_t> bucket_ids(65536);
    cuda_managed_buffer<ChunkSize> chunk_sizes(NKEY);
    cuda_managed_buffer<ChunkType> chunk_types(NKEY);
    for(int i = 0; i < chunk_length ; ++i) {
        bucket_ids[i] = 0;
    }
    // bucket_ids[0] = 1;
    calculate_chunk_info<NKEY, DefaultRoaringTraits>
            <<<1, block_threads>>>(reinterpret_cast<ChunkSize(*)[64]>(chunk_sizes.get()),
                    reinterpret_cast<ChunkType(*)[64]>(chunk_types.get()),
                    bucket_ids.get(),
                    chunk_length);
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );
    REQUIRE(chunk_sizes[0] == 0);
    REQUIRE(chunk_sizes[1] == 0);
    REQUIRE(chunk_types[0] == (ChunkType) RoaringChunkType::AllOne);
    REQUIRE(chunk_types[1] == (ChunkType) RoaringChunkType::Array);
}


template<typename BucketID, int TotalBuckets, typename RoaringTraits>
__global__ void write_bitmap_chunk_kernel
(uint16_t* chunk_addr, BucketID* bucket_ids, BucketID bucket, uint32_t size) {
    write_bitmap_chunk<TotalBuckets, RoaringTraits>(chunk_addr, bucket_ids, bucket, size);
}

TEST_CASE("All 1 chunk can be correctly written as bitmap chunk") {
    constexpr auto NBLOCK = 1;
    constexpr auto NKEY = 64;
    constexpr auto chunk_length = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::chunk_length;
    //constexpr auto BlockSize = GPURoaringBitmap<int32_t, int32_t, NKEY, DefaultRoaringTraits>::BlockSize;
    cuda_managed_buffer<int32_t> bucket_ids(65536 * NBLOCK);
    cuda_managed_buffer<uint16_t> bitmap_chunk(65536 / 16 * NBLOCK);
    for (int i = 0; i < chunk_length ; ++i)
        bucket_ids[i] = 0;
    write_bitmap_chunk_kernel<int32_t, 64, DefaultRoaringTraits><<<NBLOCK,64>>>(
        bitmap_chunk.get(), bucket_ids.get(), 0, 65536
    );
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );
    for(auto i = 0; i < 65536 / 16; ++i) {
        CAPTURE(i);
        REQUIRE(bitmap_chunk[i] == 0xFFFF );
    }
}

template<int TotalBuckets, typename RoaringTraits, typename BucketID>
__global__ void write_array_chunk_kernel
(uint16_t* chunk_addr, BucketID* bucket_ids, BucketID bucket, uint32_t size) {
    write_array_chunk<TotalBuckets, RoaringTraits, BucketID>(chunk_addr, bucket_ids + size * blockIdx.x, bucket, size);
}

TEST_CASE("All 1 chunk can be correctly written as array chunk") {
    constexpr auto NBLOCK = 1;
    constexpr auto NKEY = 64;
    constexpr auto chunk_length = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::chunk_length;
    //constexpr auto BlockSize = GPURoaringBitmap<int32_t, int32_t, NKEY, DefaultRoaringTraits>::BlockSize;
    cuda_managed_buffer<int32_t> bucket_ids(65536 * NBLOCK);
    cuda_managed_buffer<uint32_t> array_chunk(65536 * NBLOCK / 2);
    auto array_chunk_ptr = (uint16_t*) array_chunk.get();
    for (int i = 0; i < chunk_length; ++i)
        bucket_ids[i] = 0;
    write_array_chunk_kernel<64, DefaultRoaringTraits><<<NBLOCK,64>>>(
        array_chunk_ptr, bucket_ids.get(), 0, 65536
    );
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );
    for(auto i = 0; i < 65536; ++i) {
        CAPTURE(i);
        REQUIRE(array_chunk_ptr[i] == i);
    }
}

TEST_CASE("All random chunk can be correctly written as array chunk") {
    constexpr auto NBLOCK = 1;
    constexpr auto NKEY = 64;
    constexpr auto chunk_length = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::chunk_length;
    //constexpr auto BlockSize = GPURoaringBitmap<int32_t, int32_t, NKEY, DefaultRoaringTraits>::BlockSize;
    cuda_managed_buffer<int32_t> bucket_ids(65536 * NBLOCK);
    cuda_managed_buffer<uint32_t> array_chunk(65536 * NBLOCK / 2);
    auto array_chunk_ptr = (uint16_t*) array_chunk.get();
    srand((unsigned)time(nullptr));
    for(int i = 0; i < chunk_length * NBLOCK; ++i)
        bucket_ids[i] = rand() % 64;
    write_array_chunk_kernel<64, DefaultRoaringTraits><<<NBLOCK,64>>>(
        array_chunk_ptr, bucket_ids.get(), 0, 65536
    );
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );
    auto count = 0;
    for(auto i = 0; i < 65536 * NBLOCK; ++i) {
        if(bucket_ids[i] == 0) {
            CAPTURE(i);
            REQUIRE(array_chunk_ptr[count++] == i);
        }
    }
}


template<int TotalBuckets, typename RoaringTraits, typename BucketID>
__global__ void write_run_chunk_kernel
(uint16_t* chunk_addr, BucketID* bucket_ids, BucketID bucket, uint32_t size) {
    write_run_chunk<TotalBuckets, RoaringTraits, BucketID>(chunk_addr, bucket_ids, bucket, size);
}

TEST_CASE("All 1 chunk can be correctly written as run chunk") {
    constexpr auto NBLOCK = 1;
    constexpr auto NKEY = 64;
    constexpr auto chunk_length = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::chunk_length;
    // constexpr auto BlockSize = GPURoaringBitmap<int32_t, int32_t, NKEY, DefaultRoaringTraits>::BlockSize;
    cuda_managed_buffer<int32_t> bucket_ids(65536 * NBLOCK);
    cuda_managed_buffer<uint32_t> run_chunk(65536 * NBLOCK / 4);
    auto run_chunk_ptr = (uint16_t*) run_chunk.get();
    for (int i = 0; i < chunk_length; ++i)
        bucket_ids[i] = 0;
    // bucket_ids[65535] = 1;
    write_run_chunk_kernel<64, DefaultRoaringTraits><<<NBLOCK,64>>>(
        run_chunk_ptr, bucket_ids.get(), 0, 65536
    );
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );
    REQUIRE(run_chunk_ptr[1] == 65535);
    REQUIRE(run_chunk_ptr[0] == 0);
}

TEST_CASE("Two run chunk can be correctly written as run chunk") {
    constexpr auto NBLOCK = 1;
    constexpr auto NKEY = 64;
    constexpr auto chunk_length = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::chunk_length;
    // constexpr auto BlockSize = GPURoaringBitmap<int32_t, int32_t, NKEY, DefaultRoaringTraits>::BlockSize;
    cuda_managed_buffer<int32_t> bucket_ids(65536 * NBLOCK);
    cuda_managed_buffer<uint32_t> run_chunk(65536 * NBLOCK / 4);
    auto run_chunk_ptr = (uint16_t*) run_chunk.get();
    for (int i = 0; i < chunk_length; ++i)
        bucket_ids[i] = 0;
    bucket_ids[0] = 1;
    // bucket_ids[65535] = 1;
    write_run_chunk_kernel<64, DefaultRoaringTraits><<<NBLOCK,64>>>(
        run_chunk_ptr, bucket_ids.get(), 0, 65536
    );
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );
    REQUIRE(run_chunk_ptr[0] == 1);
    REQUIRE(run_chunk_ptr[1] == 65534);
}


TEST_CASE("All 1 chunk can be correctly written using RoaringBitmap") {
    constexpr auto NBLOCK = 128;
    constexpr auto NKEY = 64;
    constexpr auto chunk_length = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::chunk_length;
    //constexpr auto BlockSize = GPURoaringBitmap<int32_t, int32_t, NKEY, DefaultRoaringTraits>::BlockSize;
    cuda_managed_buffer<int32_t> bucket_ids(chunk_length * NBLOCK);
    for(int i = 0; i < chunk_length * NBLOCK; ++i)
        bucket_ids[i] = 0;
    GPURoaringBitmap<NKEY, DefaultRoaringTraits>
        roaring_bitmap(chunk_length * NBLOCK);
    roaring_bitmap.construct_bitmap(bucket_ids.get(), chunk_length * NBLOCK);
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );
    auto binner = EqualBinner<double>(NKEY, 0.0, 64.0);
    ParaRoaringBitmap<decltype(binner), cuda_host_buffer> bitmap(binner);
    cudaStream_t stream;
    OTTERS_CUDA_ERRORCHECK( cudaStreamCreate(&stream) );
    roaring_bitmap.resize_for_copy(bitmap);
    roaring_bitmap.copy_to_host_async_stream(bitmap, stream);
    OTTERS_CUDA_ERRORCHECK( cudaStreamSynchronize(stream));
    //printf("size = %x=======\n", bitmap.storage().size());
    //printf("2nd run = %x=======\n", bitmap.storage()[1]);
    auto result = bitmap.verify_bitmap(bucket_ids.get(), bucket_ids.get() + chunk_length * NBLOCK);
    CAPTURE(result.second);
    REQUIRE(result.first);
    cudaStreamDestroy(stream);
}

TEST_CASE("Two-run chunk can be correctly written using RoaringBitmap") {
    constexpr auto NBLOCK = 1;
    constexpr auto NKEY = 64;
    constexpr auto chunk_length = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::chunk_length;
    //constexpr auto BlockSize = GPURoaringBitmap<int32_t, int32_t, NKEY, DefaultRoaringTraits>::BlockSize;
    cuda_managed_buffer<int32_t> bucket_ids(chunk_length * NBLOCK);
    for(int i = 0; i < chunk_length * NBLOCK; ++i)
        bucket_ids[i] = 0;
    bucket_ids[0] = 1;
    GPURoaringBitmap<NKEY, DefaultRoaringTraits>
        roaring_bitmap(chunk_length * NBLOCK);
    roaring_bitmap.construct_bitmap(bucket_ids.get(), chunk_length * NBLOCK);
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );
    auto binner = EqualBinner<double>(NKEY, 0.0, 64.0);
    ParaRoaringBitmap<decltype(binner), cuda_host_buffer> bitmap(binner);
    cudaStream_t stream;
    OTTERS_CUDA_ERRORCHECK( cudaStreamCreate(&stream) );
    roaring_bitmap.resize_for_copy(bitmap);
    roaring_bitmap.copy_to_host_async_stream(bitmap, stream);
    OTTERS_CUDA_ERRORCHECK( cudaStreamSynchronize(stream));
    //printf("size = %x=======\n", bitmap.storage().size());
    //printf("2nd run = %x=======\n", bitmap.storage()[1]);
    auto result = bitmap.verify_bitmap(bucket_ids.get(), bucket_ids.get() + chunk_length * NBLOCK);
    CAPTURE(result.second);
    REQUIRE(result.first);
    cudaStreamDestroy(stream);
}


TEST_CASE("All random chunk can be correctly written using RoaringBitmap") {
    constexpr auto NBLOCK = 1;
    constexpr auto NKEY = 64;
    constexpr auto chunk_length = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::chunk_length;
    //constexpr auto BlockSize = GPURoaringBitmap<int32_t, int32_t, NKEY, DefaultRoaringTraits>::BlockSize;
    cuda_managed_buffer<int32_t> bucket_ids(chunk_length * NBLOCK);
    srand((unsigned)time(nullptr));
    for(int i = 0; i < chunk_length * NBLOCK; ++i)
        bucket_ids[i] = i % 64;
    GPURoaringBitmap<NKEY, DefaultRoaringTraits> roaring_bitmap(chunk_length * NBLOCK);
    roaring_bitmap.construct_bitmap(bucket_ids.get(), chunk_length * NBLOCK);
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );

    auto binner = EqualBinner<double>(NKEY, 0.0, 64.0);
    ParaRoaringBitmap<decltype(binner), cuda_host_buffer> bitmap(binner);
    cudaStream_t stream;
    OTTERS_CUDA_ERRORCHECK( cudaStreamCreate(&stream) );
    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );
    roaring_bitmap.resize_for_copy(bitmap);
    roaring_bitmap.copy_to_host_async_stream(bitmap, stream);
    OTTERS_CUDA_ERRORCHECK( cudaStreamSynchronize(stream));
    auto result = bitmap.verify_bitmap(bucket_ids.get(), bucket_ids.get() + chunk_length * NBLOCK);
    CAPTURE(result.second);
    REQUIRE(result.first);
    cudaStreamDestroy(stream);
}
