#include <otters/cuda/exception/cuda_assert.h>
#include <otters/cuda/container/buffer.cuh>
#include <otters/cuda/container/iterator.cuh>
#include <otters/cuda/bitmap/roaring.cuh>
#include <random>
#include <vector>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wextra"
#pragma clang diagnostic ignored "-Wreserved-id-macro"
#include <boost/timer/timer.hpp>
#pragma GCC diagnostic pop

#include <cuda.h>
#include <curand_kernel.h>

using namespace otters;

__global__ void generate_normal_kernel(curandState *state, int n, int *result)
{
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    int block_size = blockDim.x * gridDim.x;
    /* Copy state to local memory for efficiency */
    curandState localState = state[id];
    /* Generate pseudo-random normals */
    for(int i = id; i < n; i += block_size) {
        double generated = curand_normal_double(&localState);
            /* Check if within one standard deviaton */
        if(generated <= -3) generated = -3;
        if(generated >= 3) generated = 3;
        double normalized = (generated + 3) / 6;
        result[i] = ((int)trunc(normalized * 63)) % 64;
        //result[i] = i % 2;
    }
}

__global__ void setup_kernel(curandState *state)
{
    int id = threadIdx.x + blockIdx.x * 64;
    /* Each thread gets same seed, a different sequence
              number, no offset */
    curand_init(1234, id, 0, &state[id]);
}


int main() {
    constexpr auto NBLOCK = 16 * 16;
    constexpr auto NKEY = 64;
    constexpr auto chunk_length = GPURoaringBitmap<NKEY, DefaultRoaringTraits>::chunk_length;
    //constexpr auto BlockSize = GPURoaringBitmap<int32_t, int32_t, NKEY, DefaultRoaringTraits>::BlockSize;
    cuda_device_buffer<int32_t> device_bucket_ids(chunk_length * NBLOCK);
    srand(static_cast<unsigned>(time(nullptr)));

    curandState *devStates;
    OTTERS_CUDA_ERRORCHECK(cudaMalloc((void **)&devStates, 64 * 64 * sizeof(curandState)));
    setup_kernel<<<64, 64>>>(devStates);
    OTTERS_CUDA_ERRORCHECK(cudaDeviceSynchronize());
    generate_normal_kernel<<<64, 64>>>(devStates, chunk_length * NBLOCK, device_bucket_ids.get());
    OTTERS_CUDA_ERRORCHECK(cudaDeviceSynchronize());



    //cudaMemcpy(device_bucket_ids.get(), host_bucket_ids.data(),
    //        ChunkSize * NBLOCK * sizeof(int32_t), cudaMemcpyHostToDevice);

    {
        boost::timer::auto_cpu_timer t;
        GPURoaringBitmap<NKEY, DefaultRoaringTraits> roaring_bitmap(chunk_length * NBLOCK);
        roaring_bitmap.construct_bitmap(device_bucket_ids.get(), chunk_length * NBLOCK);
    }

    OTTERS_CUDA_ERRORCHECK( cudaDeviceSynchronize() );

    return 0;
}
