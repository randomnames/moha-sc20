#define CATCH_CONFIG_RUNNER
#include <catch2/catch.hpp>

#include <otters/common/utils/process.h>

#include <fmt/core.h>

#include <mpi.h>

using namespace otters;

static int g_argc;
static char** g_argv;

TEST_CASE("Dumb case") {
    int provided;
    MPI_Init_thread(&g_argc, &g_argv, MPI_THREAD_FUNNELED, &provided);
    assert(provided != MPI_THREAD_SINGLE);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    fmt::print("Hello process {}\n", rank);
    MPI_Finalize();
}

int main(int argc, char** argv) {
    auto status = Catch::Session().run(argc, argv);
    return status;
}
