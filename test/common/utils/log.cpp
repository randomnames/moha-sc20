#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#define OTTERS_LOG_LEVEL OTTERS_LOG_DEBUG
#include <otters/common/utils/log.h>

using namespace otters;

TEST_CASE() {
    otters::log(OTTERS_LOG_DEBUG, "Hello, World! I am {}", "groot!");
}
