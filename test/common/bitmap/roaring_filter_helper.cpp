#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <otters/common/bitmap/roaring_bitmap.h>
#include <otters/common/bitmap/roaring_filter_helper.h>
#include <otters/common/bitmap/binning.h>
#include <otters/common/containers/buffer.h>
#include <boost/dynamic_bitset.hpp>

using namespace otters;

// static auto chunk_size = 65536;

TEST_CASE("A 3D range can iterate over a hypertangular.") {
    DimensionRanges<DefaultRoaringTraits, 3> ranges({2,2,2}, {4,4,4}, {32,32,64});
    std::array<uint64_t, 1024> storage = {0};
    ranges.next_chunk(storage.data());
    
    size_t result = 0;
    for(auto it = storage.begin(); it != storage.end(); ++it) {
        // log(OTTERS_LOG_DEBUG, "r = {}", r.format());
        // auto start = r._start;
        // log(OTTERS_LOG_DEBUG, "r = {} {} {}", start / 25, (start % 25) / 5, start % 5);
        result += __builtin_popcountl(*it);
    }
    REQUIRE(result == 8);
}
/*
TEST_CASE("A 3D range can iterate over full hypertangular.") {
    DimensionRanges<DefaultRoaringTraits, 3> ranges({0,0,0}, {5,5,5}, {5,5,5});
    size_t result = 0;
    for(auto it = ranges.begin(); it.end() == false; ++it) {
        auto r = *it;
        result += r._size;
    }
    REQUIRE(result == 125);
}
*/

TEST_CASE("Groups of uncompressed bitmaps can be merged") {
    boost::dynamic_bitset<uint64_t> chunk1(65536);
    boost::dynamic_bitset<uint64_t> chunk2(65536);
    boost::dynamic_bitset<uint64_t> chunk3(65536);
    boost::dynamic_bitset<uint64_t> chunk4(65536);

    uint64_t chunk1_storage[1024];
    uint64_t chunk2_storage[1024];
    uint64_t chunk3_storage[1024];
    uint64_t chunk4_storage[1024];

    chunk1.flip();
    for(auto i = 0; i < 65536; i += 2)
        chunk2.set(i);
    for(auto i = 0; i < 65536; i += 4)
        chunk3.set(i);
    for(auto i = 0; i < 65536; i += 4)
        chunk4.set(i);

    boost::to_block_range(chunk1, chunk1_storage);
    boost::to_block_range(chunk2, chunk2_storage);
    boost::to_block_range(chunk3, chunk3_storage);
    boost::to_block_range(chunk4, chunk4_storage);

    std::array<RoaringRange<DefaultRoaringTraits>, 5> ranges;
    ranges[0]._type = RoaringRangeType::Run;
    ranges[1].set_uncompressed(chunk1_storage);
    ranges[2].set_uncompressed(chunk2_storage);
    ranges[3].set_uncompressed(chunk3_storage);
    ranges[4].set_uncompressed(chunk4_storage);
    auto result = merge_ranges_and_count_all_uncompressed(0, 65536, ranges);
    // (void) result;
    REQUIRE( result == 16384 );
}


TEST_CASE("A number of array ranges should be able to be merged and counted") {
    /*
    uint16_t chunk[4][4096] = {0};
    size_t chunk_sizes[4] = {0};

    for(auto i = 0; i < 4096; ++i) {
        chunk[0][chunk_sizes[0]++] = i;
    }
    for(auto i = 0; i < 4096; i += 2) {
        chunk[1][chunk_sizes[1]++] = i;
    }
    for(auto i = 0; i < 4096; i += 4) {
        chunk[2][chunk_sizes[2]++] = i;
    }
    for (auto i = 0; i < 4096; i += 8) {
        chunk[3][chunk_sizes[3]++] = i;
    }
    */
    /* FIXME: Finish this. */
}

const static int chunk_size = 65536;

auto from_dynamic_bitsets(std::vector<boost::dynamic_bitset<uint64_t>> bitsets) {
    EqualBinner<double> equal_binner((int) bitsets.size(), 0, 1);
    ParaRoaringBitmap<decltype(equal_binner), system_malloc_buffer> para_roaring1(equal_binner);

    for (auto k = 0u; k < bitsets[0].size() / chunk_size; ++k) {
        uint64_t range[1024];
        for (auto i = 0u; i < bitsets.size(); ++i) {
            boost::dynamic_bitset<uint64_t> temp(chunk_size);
            for (auto j = 0; j < chunk_size; ++j) {
                temp[j] = bitsets[i][k * chunk_size + j];
            }

            boost::to_block_range(temp, range);
            para_roaring1.add_chunk_from_bitmap(range);
        }
    }
    para_roaring1.set_size(bitsets[0].size());
    return para_roaring1;
}

auto get_bitsets(int num_of_buckets) {
    std::vector<boost::dynamic_bitset<uint64_t>> bitsets1;
    for(auto i = 0; i < num_of_buckets; ++i)
        bitsets1.emplace_back(chunk_size * 2);
    return bitsets1;
}

TEST_CASE("We can build a mask for a bunch of dimension ranges / attributes") {
    auto bitsets1 = get_bitsets(3);
    for(auto i = 0; i < chunk_size * 2; i += 2) {
        bitsets1[0].set(i);
    }
    for(auto i = 0; i < chunk_size * 2; i += 2 * 397) {
        bitsets1[1].set(i);
    }
    auto bitmap1 = from_dynamic_bitsets(bitsets1);

    auto bitsets2 = get_bitsets(3);
    for(auto i = 0; i < chunk_size * 2; i += 3) {
        bitsets2[0].set(i);
    }
    for(auto i = 0; i < chunk_size * 2; i += 2 * 397) {
        bitsets2[1].set(i);
    }
    auto bitmap2 = from_dynamic_bitsets(bitsets2);

    auto bitsets3 = get_bitsets(3);
    for (auto i = 0; i < chunk_size * 2; i += 2) {
        bitsets3[0].set(i);
    }
    for(auto i = 0; i < chunk_size * 2; i += 2 * 397) {
        bitsets3[1].set(i);
    }
    auto bitmap3 = from_dynamic_bitsets(bitsets3);

    DimensionRanges<DefaultRoaringTraits, 3> ranges({10,10,10}, {16,20,20}, {16,64,64});
    std::array<decltype(bitmap1)::OrIterator, 2> iters = {
        bitmap1.begin_or_iterator(0, 2),
        bitmap1.begin_or_iterator(0, 3)
    };

    uint64_t storage[1024];
    std::fill(storage, storage + 1024, 0);
    auto result = 0;
    next_filter_mask<typename decltype(bitmap1)::OrIterator, 3, 2>(storage, ranges, iters);
    for(auto i = 0; i < 1024; ++i) {
        result += __builtin_popcountl(storage[i]);
    }
    // log(OTTERS_LOG_DEBUG, "Here");
    std::fill(storage, storage + 1024, 0);
    next_filter_mask<typename decltype(bitmap1)::OrIterator, 3, 2>(storage, ranges, iters);
    for(auto i = 0; i < 1024; ++i) {
        result += __builtin_popcountl(storage[i]);
    }
    REQUIRE(result == 300);
}

TEST_CASE("We can compute support and means for a bunch of dimension ranges / attributes") {
    auto bitsets1 = get_bitsets(3);
    for(auto i = 0; i < chunk_size * 2; i += 2) {
        bitsets1[0].set(i);
    }
    for(auto i = 0; i < chunk_size * 2; i += 2 * 397) {
        bitsets1[1].set(i);
    }
    auto bitmap1 = from_dynamic_bitsets(bitsets1);

    auto bitsets2 = get_bitsets(3);
    for(auto i = 0; i < chunk_size * 2; i += 3) {
        bitsets2[0].set(i);
    }
    for(auto i = 0; i < chunk_size * 2; i += 2 * 397) {
        bitsets2[1].set(i);
    }
    auto bitmap2 = from_dynamic_bitsets(bitsets2);

    auto bitsets3 = get_bitsets(3);
    for (auto i = 0; i < chunk_size * 2; i += 2) {
        bitsets3[0].set(i);
    }
    for(auto i = 0; i < chunk_size * 2; i += 2 * 397) {
        bitsets3[1].set(i);
    }
    auto bitmap3 = from_dynamic_bitsets(bitsets3);

    DimensionRanges<DefaultRoaringTraits, 3> ranges({10,10,10}, {16,20,20}, {16,64,64});
    std::array<decltype(bitmap1)::OrIterator, 2> iters = {
        bitmap1.begin_or_iterator(0, 2),
        bitmap1.begin_or_iterator(0, 3)
    };

    EqualBinner<double> equal_binner((int) bitsets3.size(), 0, 1);
    auto result = roaring_compute_mean_support(ranges, iters, bitmap3, equal_binner, 0);
    log(OTTERS_LOG_DEBUG, "size = {}, mean = {}", result.size(), result.mean());

}
