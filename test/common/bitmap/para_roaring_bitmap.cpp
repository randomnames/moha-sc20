#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <otters/common/bitmap/roaring_bitmap.h>
#include <otters/common/bitmap/binning.h>
#include <otters/common/containers/buffer.h>
#include <boost/dynamic_bitset.hpp>

using namespace otters;

static auto chunk_size = 65536;

auto from_dynamic_bitset(boost::dynamic_bitset<uint64_t> bitset) {
    EqualBinner<double> equal_binner(1, 0, 1);
    ParaRoaringBitmap<decltype(equal_binner), system_malloc_buffer> para_roaring1;

    uint64_t range[1024];

    boost::to_block_range(bitset, range);
    para_roaring1.add_chunk_from_bitmap(range);
    return para_roaring1;
}

auto from_dynamic_bitsets(std::vector<boost::dynamic_bitset<uint64_t>> bitsets) {
    EqualBinner<double> equal_binner((int) bitsets.size(), 0, 1);
    ParaRoaringBitmap<decltype(equal_binner), system_malloc_buffer> para_roaring1(equal_binner);

    for(auto i = 0u; i < bitsets.size(); ++i) {
        uint64_t range[1024];

        boost::to_block_range(bitsets[i], range);
        para_roaring1.add_chunk_from_bitmap(range);
    }
    para_roaring1.set_size(chunk_size);
    return para_roaring1;
}

auto empty_bitmap() {
    ParaRoaringBitmap<EqualBinner<double>, system_malloc_buffer> para_roaring1;
    return para_roaring1;
}

TEST_CASE("append 0-run chunk works") {
    boost::dynamic_bitset<uint64_t> chunk(chunk_size);
    auto para_roaring1 = from_dynamic_bitset(chunk);


    REQUIRE((para_roaring1.offsets()[0]) == 0);
    REQUIRE((para_roaring1.types()[0]) == (uint8_t)RoaringChunkType::Array);
    REQUIRE((para_roaring1.offsets()[1]) == 0);
}

TEST_CASE("append array chunk works") {
    boost::dynamic_bitset<uint64_t> chunk(chunk_size);
    for(auto i = 2; i < 65536; i += 1337) {
        chunk.set(i);
    }
    auto para_roaring1 = from_dynamic_bitset(chunk);

    REQUIRE((para_roaring1.offsets()[0]) == 0);
    REQUIRE((para_roaring1.types()[0]) == (uint8_t)RoaringChunkType::Array);
    REQUIRE((para_roaring1.offsets()[1]) == 100);

    auto idx = 0;
    for(unsigned i = 2; i < 65536; i += 1337) {
        REQUIRE((para_roaring1.storage()[idx]) == i);
        ++idx;
    }
}

TEST_CASE("append 1-run running chunks works") {
    boost::dynamic_bitset<uint64_t> run_chunk(chunk_size);
    run_chunk.flip();
    auto para_roaring1 = from_dynamic_bitset(run_chunk);

    REQUIRE((para_roaring1.offsets()[0]) == 0);
    REQUIRE((para_roaring1.types()[0]) == (uint8_t)RoaringChunkType::Run);
    REQUIRE((para_roaring1.offsets()[1]) == 4);
    REQUIRE((para_roaring1.storage()[0]) == 0);
    REQUIRE((para_roaring1.storage()[1]) == 65535);
}

TEST_CASE("append 3-run running chunks works") {
    boost::dynamic_bitset<uint64_t> run_chunk(chunk_size);
    run_chunk.flip();
    run_chunk.reset(1024);
    run_chunk.reset(2048,1024);
    auto para_roaring1 = from_dynamic_bitset(run_chunk);

    REQUIRE((para_roaring1.offsets()[0]) == 0);
    REQUIRE((para_roaring1.types()[0]) == (uint8_t)RoaringChunkType::Run);
    REQUIRE((para_roaring1.offsets()[1]) == 12);
    REQUIRE((para_roaring1.storage()[0]) == 0);
    REQUIRE((para_roaring1.storage()[1]) == 1023);
    REQUIRE((para_roaring1.storage()[2]) == 1025);
    REQUIRE((para_roaring1.storage()[3]) == 1022);
    REQUIRE((para_roaring1.storage()[4]) == 3072);
    REQUIRE((para_roaring1.storage()[5]) == 65536 - 3072 - 1);
}

TEST_CASE("append long 3-run running chunks works") {
    boost::dynamic_bitset<uint64_t> run_chunk(chunk_size);
    run_chunk.flip();
    run_chunk.reset(10240);
    run_chunk.reset(20480,10240);
    auto para_roaring1 = from_dynamic_bitset(run_chunk);

    REQUIRE((para_roaring1.offsets()[0]) == 0);
    REQUIRE((para_roaring1.types()[0]) == (uint8_t)RoaringChunkType::Run);
    REQUIRE((para_roaring1.offsets()[1]) == 12);
    REQUIRE((para_roaring1.storage()[0]) == 0);
    REQUIRE((para_roaring1.storage()[1]) == 10239);
    REQUIRE((para_roaring1.storage()[2]) == 10241);
    REQUIRE((para_roaring1.storage()[3]) == 10238);
    REQUIRE((para_roaring1.storage()[4]) == 30720);
    REQUIRE((para_roaring1.storage()[5]) == 65536 - 30720 - 1);
}

TEST_CASE("and between two uncompressed chunks work") {
    auto target_chunk = empty_bitmap();
    boost::dynamic_bitset<uint64_t> chunk1(chunk_size);
    boost::dynamic_bitset<uint64_t> chunk2(chunk_size);
    for(auto i = 0; i < 65536; i += 7) {
        chunk1.set(i);
        if(i % 2 == 0)
            chunk2.set(i);
    }
    for(auto i = 7; i < 65536; i += 3) {
        chunk2.set(i);
        if(i % 2 == 1)
            chunk1.set(i);
    }
    auto bitmap1 = from_dynamic_bitset(chunk1);
    auto bitmap2 = from_dynamic_bitset(chunk2);
    target_chunk.chunk_and(bitmap1, 0, bitmap2, 0);
    REQUIRE((target_chunk.offsets()[0]) == 0);
    REQUIRE((target_chunk.types()[0]) == (uint8_t)RoaringChunkType::Uncompressed);
    REQUIRE((target_chunk.offsets()[1]) == 65536 / 8);
}

TEST_CASE("and between two array chunks work") {
    auto target_chunk = empty_bitmap();
    boost::dynamic_bitset<uint64_t> chunk1(chunk_size);
    boost::dynamic_bitset<uint64_t> chunk2(chunk_size);
    for(auto i = 0; i < 65536; i += 1337) {
        chunk1.set(i);
        if(i % 2 == 0)
            chunk2.set(i);
    }
    for(auto i = 7; i < 65536; i += 1337) {
        chunk2.set(i);
        if(i % 2 == 1)
            chunk1.set(i);
    }

    auto bitmap1 = from_dynamic_bitset(chunk1);
    auto bitmap2 = from_dynamic_bitset(chunk2);
    REQUIRE((bitmap1.types()[0]) == (uint8_t)RoaringChunkType::Array);
    REQUIRE((bitmap1.types()[0]) == (uint8_t)RoaringChunkType::Array);

    target_chunk.chunk_and(bitmap1, 0, bitmap2, 0);
    REQUIRE((target_chunk.offsets()[0]) == 0);
    REQUIRE((target_chunk.types()[0]) == (uint8_t)RoaringChunkType::Array);
    REQUIRE((target_chunk.offsets()[1]) == 100);
}

TEST_CASE("and between two run chunks work") {
    auto target_chunk = empty_bitmap();
    boost::dynamic_bitset<uint64_t> chunk1(chunk_size);
    boost::dynamic_bitset<uint64_t> chunk2(chunk_size);
    chunk1.flip();
    chunk1.reset(1024);
    chunk1.reset(2048,1024);

    chunk2.flip();
    chunk2.reset(1025);
    chunk2.reset(2046,1026);
    chunk2.reset(20480,10240);

    auto bitmap1 = from_dynamic_bitset(chunk1);
    auto bitmap2 = from_dynamic_bitset(chunk2);
    target_chunk.chunk_and(bitmap1, 0, bitmap2, 0);

    REQUIRE((bitmap1.types()[0]) == (uint8_t)RoaringChunkType::Run);
    assert((bitmap1.offsets()[0]) == 0);
    assert((bitmap1.offsets()[1]) == 12);
    REQUIRE((bitmap2.types()[0]) == (uint8_t)RoaringChunkType::Run);
    assert((bitmap2.offsets()[0]) == 0);
    assert((bitmap2.offsets()[1]) == 16);

    REQUIRE((target_chunk.offsets()[0]) == 0);
    REQUIRE((target_chunk.types()[0]) == (uint8_t)RoaringChunkType::Run);
    REQUIRE((target_chunk.offsets()[1]) == 16);
}

TEST_CASE("and between two run chunks work 2") {
    auto target_chunk = empty_bitmap();
    boost::dynamic_bitset<uint64_t> chunk1(chunk_size);
    boost::dynamic_bitset<uint64_t> chunk2(chunk_size);
    chunk1.set(2048,1024,true);
    chunk1.set(1024);
    chunk1.set(10248,1024,true);

    chunk2.set(1023,3,true);
    chunk2.set(2050, 1024, true);
    chunk2.set(10240, 4096,true);

    auto bitmap1 = from_dynamic_bitset(chunk1);
    auto bitmap2 = from_dynamic_bitset(chunk2);
    target_chunk.chunk_and(bitmap1, 0, bitmap2, 0);

    REQUIRE((bitmap1.types()[0]) == (uint8_t)RoaringChunkType::Run);
    REQUIRE((bitmap1.offsets()[0]) == 0);
    REQUIRE((bitmap1.offsets()[1]) == 12);
    REQUIRE((bitmap2.types()[0]) == (uint8_t)RoaringChunkType::Run);
    REQUIRE((bitmap2.offsets()[0]) == 0);
    REQUIRE((bitmap2.offsets()[1]) == 12);

    REQUIRE((target_chunk.offsets()[0]) == 0);
    REQUIRE((target_chunk.types()[0]) == (uint8_t)RoaringChunkType::Run);
    REQUIRE((target_chunk.offsets()[1]) == 12);
}

TEST_CASE("and between uncompressed and array works") {
    boost::dynamic_bitset<uint64_t> chunk1(chunk_size);
    boost::dynamic_bitset<uint64_t> chunk2(chunk_size);
    for(auto i = 0; i < 65536; i += 7) {
        chunk1.set(i);
        // if(i % 2 == 0)
        //     chunk2.set(i);
    }
    for(auto i = 0; i < 65536; i += 7 * 397) {
        chunk2.set(i);
    }

    auto bitmap1 = from_dynamic_bitset(chunk1);
    auto bitmap2 = from_dynamic_bitset(chunk2);
    auto target_chunk = empty_bitmap();
    target_chunk.chunk_and(bitmap1, 0, bitmap2, 0);

    REQUIRE((bitmap1.types()[0]) == (uint8_t)RoaringChunkType::Uncompressed);
    REQUIRE((bitmap2.types()[0]) == (uint8_t)RoaringChunkType::Array);
 

    REQUIRE((target_chunk.offsets()[0]) == 0);
    REQUIRE((target_chunk.types()[0]) == (uint8_t)RoaringChunkType::Array);
    REQUIRE((target_chunk.offsets()[1]) == 48);
}


TEST_CASE("and between uncompressed and run works") {
    boost::dynamic_bitset<uint64_t> chunk1(chunk_size);
    boost::dynamic_bitset<uint64_t> chunk2(chunk_size);
    for(auto i = 0; i < 65536; i += 7) {
        chunk1.set(i);
        // if(i % 2 == 0)
        //     chunk2.set(i);
    }
    
    chunk2.set(1023,3,true);
    chunk2.set(2050, 1024, true);
    chunk2.set(10240, 4096,true);

    auto bitmap1 = from_dynamic_bitset(chunk1);
    auto bitmap2 = from_dynamic_bitset(chunk2);
    auto target_chunk = empty_bitmap();
    target_chunk.chunk_and(bitmap1, 0, bitmap2, 0);

    REQUIRE((bitmap1.types()[0]) == (uint8_t)RoaringChunkType::Uncompressed);
    REQUIRE((bitmap2.types()[0]) == (uint8_t)RoaringChunkType::Run);
 

    REQUIRE((target_chunk.offsets()[0]) == 0);
    REQUIRE((target_chunk.types()[0]) == (uint8_t)RoaringChunkType::Uncompressed);
}

TEST_CASE("and between array and run works") {
    boost::dynamic_bitset<uint64_t> chunk1(chunk_size);
    boost::dynamic_bitset<uint64_t> chunk2(chunk_size);
    for(auto i = 0; i < 65536; i += 1337) {
        chunk1.set(i);
        // if(i % 2 == 0)
        //     chunk2.set(i);
    }
    
    chunk2.set(1023,3,true);
    chunk2.set(2050, 1024, true);
    chunk2.set(10240, 4096,true);

    auto bitmap1 = from_dynamic_bitset(chunk1);
    auto bitmap2 = from_dynamic_bitset(chunk2);
    auto target_chunk = empty_bitmap();
    target_chunk.chunk_and(bitmap1, 0, bitmap2, 0);

    REQUIRE((bitmap1.types()[0]) == (uint8_t)RoaringChunkType::Array);
    REQUIRE((bitmap2.types()[0]) == (uint8_t)RoaringChunkType::Run);
 

    REQUIRE((target_chunk.offsets()[0]) == 0);
    REQUIRE((target_chunk.types()[0]) == (uint8_t)RoaringChunkType::Array);
}

TEST_CASE("or between all chunks works") {
    std::vector<boost::dynamic_bitset<uint64_t>> bitsets;
    bitsets.emplace_back(chunk_size);
    bitsets.emplace_back(chunk_size);
    for(auto i = 0; i < 65536; i += 2) {
        bitsets[0].set(i);
        // if(i % 2 == 0)
        //     chunk2.set(i);
    }
    for(auto i = 0; i < 65536; i += 2 * 397) {
        bitsets[1].set(i);
    }

    auto bitmap1 = from_dynamic_bitsets(bitsets);

    auto iter = bitmap1.begin_or_iterator(0,2);
    REQUIRE(iter.min() == 0);
    REQUIRE(iter.max() == 65536);
    auto range = iter.range();
    size_t result = 0;
    for(auto i = 0; i < 1024; ++i) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-align"
        auto storage = (uint64_t*) range._storage;
#pragma clang diagnostic pop

        result += __builtin_popcountl(storage[i]);
    }
    REQUIRE(result == 65536 / 2);
    ++iter;
    REQUIRE(iter.end() == true);

    // REQUIRE((bitmap1.types()[0]) == (uint8_t)RoaringChunkType::Uncompressed);
    // REQUIRE((bitmap2.types()[0]) == (uint8_t)RoaringChunkType::Array);
 

    // REQUIRE((target_chunk.offsets()[0]) == 0);
    // REQUIRE((target_chunk.types()[0]) == (uint8_t)RoaringChunkType::Array);
    // REQUIRE((target_chunk.offsets()[1]) == 24);

}
