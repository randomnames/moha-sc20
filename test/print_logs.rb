#!/bin/ruby

total_procs = ARGV[1].to_i
prefix = ARGV[0]

(0..(total_procs - 1)).each do |idx|
    filename = "#{prefix}.#{idx}.log"
    puts "============ BEGIN #{filename} ==========="
    IO.foreach(filename) { |line|
        puts line
    }
    puts "============ END #{filename} ============"
    puts ""
end