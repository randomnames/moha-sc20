# Modern Heteregenous Analytics (MoHA, otters)

MoHA is a flexible in-situ query processing system designed to
perform complex and flexible analytics on GPU-generated simulation
results.

## Environment and dependencies

MoHA should compile with reasonably new C++/CUDA Compilers on Linux with support
for c++ 17 standard and device-side C++ 14 standard.

It is known to work with g++ 8.3/9 with CUDA 10.

MoHA requires the following dependencies:

* [fmt](https://github.com/fmtlib/fmt)
* [catch2](https://github.com/catchorg/Catch2)
* [roaring](https://github.com/RoaringBitmap/CRoaring)
* [boost](https://www.boost.org/)
* Reasonably new version of an MPI implementation.

The versions we have tested are:
* fmt 5.3.0
* catch2 2.7.1
* roaring 2019-03-05-2
* boost 1.72.0 / 1.69.0
* MPICH 3.3.2 / OpenMPI 4.0.1

## Bulid

MoHA does not rely on any depedency managers, though [vcpkg](https://github.com/Microsoft/vcpkg)
might lift you from the hell of manually install dependencies.

Simply use cmake to compile the examples and tests. If you want to use clang as a CUDA compiler,
you might need to [patch the cmake implementation](https://gitlab.kitware.com/cmake/cmake/issues/16586).

```
mkdir build
cmake .. -DCMAKE_BUILD_TYPE=Release
cmake --build . --target all -- -j
```

## Benchmarks

The two benchmarks can be found under `examples/`. For each benchmark, a single version,
an mpi version and a version with profiling instructions are compiled by default. Refer
to individual benchmark's code for configuration options.