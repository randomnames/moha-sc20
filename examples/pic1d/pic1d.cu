/*Demo 1D sheath PIC simulation with CUDA
GPU1: particle mover moved to the GPU
*/

#include <math.h>
#include <otters/common/utils/git_version.h>
#include <otters/cuda/exception/cuda_assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <otters/cuda/query/drivers/plain_save.cuh>
#include <otters/cuda/query/drivers/bitmap_save.cuh>
#include <otters/cuda/query/drivers/plain_csm.cuh>
#include <otters/cuda/query/drivers/plain_join_count.cuh>
#include <otters/cuda/query/drivers/bitmap_join_count.cuh>
#include <otters/cuda/query/drivers/bitmap_csm.cuh>
#include <otters/cuda/query/drivers/plain_copy.cuh>
#include <otters/cuda/query/drivers/plain_filter_oir.cuh>
#include <otters/cuda/query/drivers/bitmap_filter_oir.cuh>
#include <otters/cuda/query/drivers/bitmap_copy.cuh>
#include <otters/cuda/query/drivers/save_min_max.cuh>

#include <otters/common/utils/log.h>

#include <boost/timer/timer.hpp>
#include <boost/program_options.hpp>
#include <fmt/format.h>

#ifdef USE_MPI
#include <otters/cuda/query/drivers/mpi_bitmap_csm.cuh>
#include <otters/cuda/query/drivers/mpi_plain_csm.cuh>

#include <mpi.h>
#endif

/*constants*/
#define EPS_0 8.85418782e-12  	// F/m, vacuum permittivity
#define K	1.38065e-23			// J/K, Boltzmann constant
#define ME 9.10938215e-31		// kg, electron mass
#define QE 1.602176565e-19		// C, elementary charge
#define AMU  1.660538921e-27	// kg, atomic mass unit
#define EV_TO_K	11604.52		// 1eV in Kelvin, QE/K

/*simulation parameters, these could come from an input file*/
#define PLASMA_DEN	1e16		// plasma density to load
#define DX 1e-4					// cell spacing
#define DT 1e-11				// time step size
#define ELECTRON_TEMP 3.0		// electron temperature in eV
#define ION_TEMP 1.0			// ion temperature in eV

/*domain parameters, set here so can access from GPU*/
#define X0  0			/*mesh origin*/

const int THREADS_PER_BLOCK = 256;
/* Data structure to hold domain information*/
struct Domain
{

    Domain(int NC)
            : ni(NC+1), x0(X0), dx(DX), xl(NC*DX), xmax(X0 + xl) {
        /*allocate data structures, remember these need to be cleared first in C++*/
        phi = new double[ni];		//potential
        rho  = new double[ni];		//charge density
        ef  = new double[ni];		//electric field
        nde  = new float[ni];		//number density of electrons
        ndi	= new float[ni];		//number density of ions
    }

    ~Domain() {
        delete phi;
        delete rho;
        delete ef;
        delete nde;
        delete ndi;
    }

    Domain(const Domain&) = delete;
    Domain(Domain&&) = delete;
    Domain& operator=(const Domain&&) = delete;
    Domain& operator=(Domain&&) = delete;

    const int ni;
    const double x0;
    const double dx;
    const double xl;
    const double xmax;

    /*data structures*/
    double *phi;		/*potential*/
    double *ef;			/*electric field on the cpu*/
    double *rho;		/*charge density*/

    float *ndi;		/*ion density on the CPU*/
    float *nde;		/*electron density on the CPU*/
};

///* Data structure for particle storage **/
//struct Particle
//{
//    double x;			/*position*/
//    double v;			/*velocity*/
//    bool alive;			/*flag to avoid removing particles*/
//};

/* Data structure to hold species information*/
struct Species
{
    double mass;			/*particle mass in kg*/
    double charge;			/*particle charge in Coulomb*/
    double spwt;			/*species specific weight*/

    int np;					/*number of particles*/
    int np_alloc;			/*size of the allocated data array*/
    bool* cpu_part_alive;
    double* cpu_part_x;
    double* cpu_part_v;
    double* cpu_part_ef;
    bool* gpu_part_alive;
    double* gpu_part_x;
    double* gpu_part_v;
    double* gpu_part_ef;

    /* Particle *part; */			/*array holding particles on the CPU*/
    // Particle *part_gpu;		/*array holding particles on the GPU*/
    void alloc_space() {
        cpu_part_alive = new bool[np_alloc * 2];
        cpu_part_x = new double[np_alloc * 2];
        cpu_part_v = new double[np_alloc * 2];
        cpu_part_ef = new double[np_alloc * 2];
        OTTERS_CUDA_ERRORCHECK( cudaMalloc(&gpu_part_alive, sizeof(bool) * np_alloc * 2) );
        OTTERS_CUDA_ERRORCHECK( cudaMalloc(&gpu_part_x, sizeof(double) * np_alloc * 2) );
        OTTERS_CUDA_ERRORCHECK( cudaMalloc(&gpu_part_v, sizeof(double) * np_alloc * 2) );
        OTTERS_CUDA_ERRORCHECK( cudaMalloc(&gpu_part_ef, sizeof(double) * np_alloc * 2) );
    }
    void deallocate_space() {
        OTTERS_CUDA_ERRORCHECK( cudaFree(gpu_part_ef) );
        OTTERS_CUDA_ERRORCHECK( cudaFree(gpu_part_v) );
        OTTERS_CUDA_ERRORCHECK( cudaFree(gpu_part_x) );
        OTTERS_CUDA_ERRORCHECK( cudaFree(gpu_part_alive) );
        delete[] cpu_part_ef;
        delete[] cpu_part_v;
        delete[] cpu_part_x;
        delete[] cpu_part_alive;
    }
    void alloc_after(Species& species) {
        cpu_part_alive = species.cpu_part_alive + species.np_alloc;
        cpu_part_x = species.cpu_part_x + species.np_alloc;
        cpu_part_v = species.cpu_part_v + species.np_alloc;
        cpu_part_ef = species.cpu_part_ef + species.np_alloc;
        gpu_part_alive = species.gpu_part_alive + species.np_alloc;
        gpu_part_x = species.gpu_part_x + species.np_alloc;
        gpu_part_v = species.gpu_part_v + species.np_alloc;
        gpu_part_ef = species.gpu_part_ef + species.np_alloc;
    }

    void copy_to_gpu() {
        OTTERS_CUDA_ERRORCHECK( cudaMemcpy(gpu_part_alive, cpu_part_alive, sizeof(bool) * np_alloc, cudaMemcpyHostToDevice) );
        OTTERS_CUDA_ERRORCHECK( cudaMemcpy(gpu_part_x, cpu_part_x, sizeof(double) * np_alloc, cudaMemcpyHostToDevice) );
        OTTERS_CUDA_ERRORCHECK( cudaMemcpy(gpu_part_v, cpu_part_v, sizeof(double) * np_alloc, cudaMemcpyHostToDevice) );
        OTTERS_CUDA_ERRORCHECK( cudaMemcpy(gpu_part_ef, cpu_part_ef, sizeof(double) * np_alloc, cudaMemcpyHostToDevice) );
    }
};

/** FUNCTION PROTOTYPES **/
double rnd();
double SampleVel(double v_th);
void ScatterSpecies(Species *species, float *den, float *den_gpu);
void ComputeRho(Species *ions, Species *electrons, Domain& domain);
bool SolvePotential(double *phi, double *rho, Domain& domain);
bool SolvePotentialDirect(double *phi, double *rho);
void ComputeEF(double *phi, double *ef, double *ef_gpu);
void PushSpecies(Species *species, double *ef, Domain& domain);
void RewindSpecies(Species *species, double *ef);
void AddParticle(Species *species, double x, double v);
__host__ __device__ double XtoL(double pos);
void scatter(double lc, double value, double *field);
__host__ __device__ double gather(double lc, double *field);
void WriteResults(FILE* res_file, int ts, Domain& domain);

/*gpu versions of above functions to avoid needing compute_35*/
__device__ void dev_scatter(double lc, float value, float *field);

/* GLOBAL VARIABLES */
//FILE *file_res;



/***** HELPER FUNCTIONS *********************************************************/
/* random number generator
   for now using built-in but this is not adequate for real simulations*/
double rnd()
{
    return rand()/(double)RAND_MAX;
}

/* samples random velocity from Maxwellian distribution using Birdsall's method*/
double SampleVel(double v_th)
{
    const int M = 12;
    double sum = 0;
    for (int i=0;i<M;i++) sum+=rnd();

    return sqrt(0.5)*v_th*(sum-M/2.0)/sqrt(M/12.0);
}


/*GPU code to move particles*/
__global__ void scatterParticle(bool* gpu_part_alive, double* gpu_part_x, float *den, long N)
{
    /*get particle id*/
    long p = blockIdx.x*blockDim.x+threadIdx.x;
    if (p<N && gpu_part_alive[p])
    {
        double lc = XtoL(gpu_part_x[p]);
        dev_scatter(lc, 1.0, den);
    }
}

/*scatter particles of species to the mesh*/
void ScatterSpecies(Species *species, float *den, float *den_gpu, Domain& domain)
{
    /*initialize densities to zero*/
    OTTERS_CUDA_ERRORCHECK(cudaMemset(den_gpu,0,sizeof(float)*domain.ni));

    /*scatter particles to the mesh*/
    int nblocks = 1+species->np_alloc/THREADS_PER_BLOCK;
    scatterParticle<<<nblocks,THREADS_PER_BLOCK>>>(species->gpu_part_alive, species->gpu_part_x,den_gpu,species->np_alloc);

    /*copy density back to CPU*/
    OTTERS_CUDA_ERRORCHECK(cudaMemcpy(den,den_gpu,sizeof(float)*domain.ni,cudaMemcpyDeviceToHost));

    /*divide by cell volume*/
    for (int i=0;i<domain.ni;i++)
        den[i]*=species->spwt/domain.dx;

    /*only half cell at boundaries*/
    den[0] *=2.0;
    den[domain.ni-1] *= 2.0;
}

/*adds new particle to the species, returns pointer to the newly added data*/
void AddParticle(Species *species, double x, double v)
{
    /*abort the simulation if we ran out of space to store this particle*/
    if (species->np > species->np_alloc-1)
    {
        printf("Too many particles!\n"); exit(-1);
    }

    /*store position and velocity of this particle*/
    species->cpu_part_x[species->np] = x;
    species->cpu_part_v[species->np] = v;
    species->cpu_part_alive[species->np] = true;

    /*increment particle counter*/
    species->np++;
}

/*computes charge density by adding ion and electron data*/
void ComputeRho(Species *ions, Species *electrons, Domain& domain)
{
    double *rho = domain.rho;

    for (int i=0;i<domain.ni;i++)
        rho[i] = ions->charge*domain.ndi[i] + electrons->charge*domain.nde[i];
}

/*Thomas algorithm for a tri-diagonal matrix*/
bool SolvePotentialDirect(double *x, double *rho, Domain& domain)
{
    /*set coefficients, this should be pre-computed*/
    int ni = domain.ni;
    double dx2 = domain.dx*domain.dx;
    int i;
    double *a = new double[ni];
    double *b = new double[ni];
    double *c = new double[ni];

    /*central difference on internal nodes*/
    for (i=1;i<ni-1;i++)
    {
        a[i] = 1; b[i] = -2; c[i] = 1;
    }

    /*dirichlet b.c. on boundaries*/
    a[0]=0;b[0]=1;c[0]=0;
    a[ni-1]=0;b[ni-1]=1;c[ni-1]=0;

    /*multiply RHS*/
    for  (i=1;i<domain.ni-1;i++)
        x[i]=-rho[i]*dx2/EPS_0;

    x[0] = 0;
    x[ni-1] = 0;

    /* Modify the coefficients. */
    c[0] /= b[0];				/* Division by zero risk. */
    x[0] /= b[0];				/* Division by zero would imply a singular matrix. */
    for(i = 1; i < ni; i++)
    {
        double id = (b[i] - c[i-1] * a[i]);	/* Division by zero risk. */
        c[i] /= id;							/* Last value calculated is redundant. */
        x[i] = (x[i] - x[i-1] * a[i])/id;
    }

    /* Now back substitute. */
    for(i = ni - 2; i >= 0; i--)
        x[i] = x[i] - c[i] * x[i + 1];

    return true;
}

/* solves potential using the Gauss Seidel Method, returns true if converged*/
bool SolvePotential(double *phi, double *rho, Domain& domain)
{
    double L2;
    double dx2 = domain.dx*domain.dx;	/*precompute*/

    /*initialize boundaries*/
    phi[0]=phi[domain.ni-1]=0;

    /*solve potential, identical to lesson 2*/
    for (int solver_it=0;solver_it<40000;solver_it++)
    {
        /*Gauss Seidel method, phi[i-1]-2*phi[i]+phi[i+1] = -dx^2*rho[i]/eps_0*/
        for (int i=1;i<domain.ni-1;i++)
        {
            /*SOR*/
            double g = 0.5*(phi[i-1] + phi[i+1] + dx2*rho[i]/EPS_0);
            phi[i] = phi[i] + 1.4*(g-phi[i]);
        }

        /*check for convergence*/
        if (solver_it%25==0)
        {
            double sum = 0;
            for (int i=1;i<domain.ni-1;i++)
            {
                double R = -rho[i]/EPS_0 - (phi[i-1] - 2*phi[i] + phi[i+1])/dx2;
                sum+=R*R;
            }
            L2 = sqrt(sum)/domain.ni;
            if (L2<1e-4) {return true;}
        }
    }
    printf("Gauss-Seidel solver failed to converge, L2=%.3g!\n",L2);
    return false;
}

/* computes electric field by differentiating potential*/
void ComputeEF(double *phi, double *ef, double *ef_gpu, Domain& domain)
{
    for (int i=1;i<domain.ni-1;i++)
        ef[i] = -(phi[i+1]-phi[i-1])/(2*domain.dx);	//central difference

    /*one sided difference at boundaries*/
    ef[0] = -(phi[1]-phi[0])/domain.dx;
    ef[domain.ni-1] = -(phi[domain.ni-1]-phi[domain.ni-2])/domain.dx;

    /*copy to the gpu*/
    OTTERS_CUDA_ERRORCHECK(cudaMemcpy(ef_gpu,ef,domain.ni*sizeof(double),cudaMemcpyHostToDevice));
}

/*GPU code to move particles*/
__global__ void pushParticle(Species sp, double *ef, double qm, long N, double xmax)
{
    /*get particle id*/
    long p = blockIdx.x*blockDim.x+threadIdx.x;

    if (p<N && sp.gpu_part_alive[p])
    {

        /*compute particle node position*/
        double lc = XtoL(sp.gpu_part_x[p]);

        /*gather electric field onto particle position*/
        double part_ef = gather(lc,ef);

        /*advance velocity*/
        sp.gpu_part_v[p] += DT*qm*part_ef;

        /*advance position*/
        sp.gpu_part_x[p] += DT*sp.gpu_part_v[p];

        sp.gpu_part_ef[p] = part_ef;

        /*remove particles leaving the domain*/
        if (sp.gpu_part_x[p] < X0 || sp.gpu_part_x[p] >= xmax)
            sp.gpu_part_alive[p] = false;
    }
}

/* moves particles of a single species, returns wall charge*/
void PushSpecies(Species *species, double *ef, Domain& domain)
{
    /*precompute q/m*/
    double qm = species->charge / species->mass;

    /*loop over particles*/
    int nblocks = 1+species->np_alloc/THREADS_PER_BLOCK;
    pushParticle<<<nblocks,THREADS_PER_BLOCK>>>(*species, ef,qm,species->np_alloc, domain.xmax);
}



/*GPU code to rewind particles*/
__global__ void rewindParticle(Species sp, double *ef, double qm, long N)
{
    /*get particle id*/
    long p = blockIdx.x*blockDim.x+threadIdx.x;

    if (p<N && sp.gpu_part_alive[p])
    {
        /*compute particle node position*/
        double lc = XtoL(sp.gpu_part_x[p]);

        /*gather electric field onto particle position*/
        double part_ef = gather(lc,ef);
        sp.gpu_part_ef[p] = part_ef;

        /*advance velocity*/
        sp.gpu_part_v[p] -= 0.5*DT*qm*part_ef;
    }
}


/* rewinds particle velocities by -0.5DT*/
void RewindSpecies(Species *species, double *ef)
{
    /*precompute q/m*/
    double qm = species->charge / species->mass;

    /*loop over particles*/
    int nblocks = 1+species->np_alloc/THREADS_PER_BLOCK;
    rewindParticle<<<nblocks,THREADS_PER_BLOCK>>>(*species, ef,qm,species->np_alloc);
}


/* converts physical coordinate to logical*/
__host__ __device__ double XtoL(double pos)
{
    double li = (pos-0)/DX;
    return li;
}


/* scatters scalar value onto a field at logical coordinate lc*/
void scatter(double lc, double value, double *field)
{
    int i = (int)lc;
    double di = lc-i;

    field[i] += value*(1-di);
    field[i+1] += value*(di);
}

/* atomic scatter of scalar value onto a field at logical coordinate lc*/
__device__ void dev_scatter(double lc, float value, float *field)
{
    int i = (int)lc;
    float di = lc-i;

    atomicAdd(&(field[i]),value*(1-di));
    atomicAdd(&(field[i+1]),value*(di));
}

/* gathers field value at logical coordinate lc*/
__host__ __device__ double gather(double lc, double *field)
{
    int i = (int)lc;
    double di = lc-i;

    /*gather field value onto particle position*/
    double val = field[i]*(1-di) + field[i+1]*(di);
    return val;
}


/* writes new zone to the results file*/
void WriteResults(FILE* file_res, int ts, Domain& domain)
{
    fprintf(file_res,"ZONE I=%d T=ZONE_%06d\n",domain.ni,ts);
    for (int i=0;i<domain.ni;i++)
    {
        fprintf(file_res,"%g %g %g %g %g %g\n",i*domain.dx,
                domain.nde[i],domain.ndi[i],
                domain.rho[i],domain.phi[i],domain.ef[i]);
    }

    fflush(file_res);
}

/* --------- main -------------*/
int main(int argc, char** argv)
{
#ifdef USE_MPI
    int level = 0;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &level);
    if(level < MPI_THREAD_MULTIPLE) {
        std::cerr << "MPI_THREAD_MULTIPLE is not supported, exiting.\n";
        exit(1);
    }
    otters::MPINodeInfo mpi_info;
    otters::MPI_CHECK_ERROR( MPI_Comm_rank(MPI_COMM_WORLD, &mpi_info._rank) );
    otters::MPI_CHECK_ERROR( MPI_Comm_size(MPI_COMM_WORLD, &mpi_info._total_nodes) );
    mpi_info._total_nodes /= 2;

    if(mpi_info._rank == 0)
        otters::print_version();
#else
    otters::print_version();
#endif

    namespace po = boost::program_options;

    po::options_description desc("Allowed options:");
    desc.add_options()
                ("num_iters", po::value<int>()->required(), "number of iterations")
                ("num_cells", po::value<int>()->required(), "the number of the cells")
                ("problem_size", po::value<ssize_t>()->required(), "the number of ions/electrons")
                ("driver_type", po::value<std::string>()->required(), "the type of transfer")
                ("bitmap_range_prefix", po::value<std::string>(), "The prefix file for loading the bitmap range")
                ("cpu_workers", po::value<int>()->default_value(4), "number of cpu query workers.")
                ("prefix", po::value<std::string>()->default_value(""), "prefix for i/o.")
        // ("worker_count", po::value<int>()->default_value(2), "count of cpu workers")
        // ("prefix", po::value<std::string>()->default_value("prefix"), "prefix of the array")
            ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    auto num_iters = vm["num_iters"].as<int>();
    auto num_cells = vm["num_cells"].as<int>();
    auto num_ions = vm["problem_size"].as<ssize_t>();
    auto num_electrons  = vm["problem_size"].as<ssize_t>();
    auto cpu_workers = vm["cpu_workers"].as<int>();
    auto driver_type = vm["driver_type"].as<std::string>();

    std::unique_ptr<otters::QueryDriver<double, 3>> query_driver;

#ifdef USE_MPI
    auto prefix = vm["prefix"].as<std::string>() + "." + std::to_string(mpi_info._rank);
    auto bitmap_range_prefix = vm["bitmap_range_prefix"].as<std::string>() + "." + std::to_string(mpi_info._rank);
#else
    auto prefix = vm["prefix"].as<std::string>();
    auto bitmap_range_prefix = vm["bitmap_range_prefix"].as<std::string>();
#endif

#ifdef USE_MPI
    if(mpi_info._rank % 2 == 0) {
#endif
    /*get info on our GPU, defaulting to first one*/
    cudaDeviceProp prop;
    OTTERS_CUDA_ERRORCHECK(cudaGetDeviceProperties(&prop,0));
    printf("Found GPU '%s' with %g GiB of global memory, max %d threads per block, and %d multiprocessors\n",
            prop.name, prop.totalGlobalMem/(1024.0*1024.0*1024.0),
            prop.maxThreadsPerBlock,prop.multiProcessorCount);

    /*init CUDA*/
    OTTERS_CUDA_ERRORCHECK(cudaSetDevice(0));

    if(driver_type == "simulation_only" || driver_type == "none") {
        query_driver = std::make_unique<otters::NoneQueryDriver<double, 3>>();
    } else if (driver_type == "save_min_max") {
        query_driver = std::make_unique<otters::SaveMinMaxDriver<double, 3>>(bitmap_range_prefix);
    } else if (driver_type == "save_bitmap") {
        using Binner = otters::GPUDynamicEqualWidthBinner1<64, double>;
        std::array<Binner, 3> binners{
                Binner(bitmap_range_prefix + ".0"),
                Binner(bitmap_range_prefix + ".1"),
                Binner(bitmap_range_prefix + ".2"),
        };
        using CPUBinner = otters::EqualBinner<double>;
        std::array<CPUBinner, 3> cpu_binners{
                CPUBinner(64, 0, 1e9),
                CPUBinner(64, 0, 1e9),
                CPUBinner(64, 0, 1e9),
        };
        query_driver =
                std::make_unique<otters::BitmapSaveDriver<double, 3>>(prefix, std::move(binners), std::move(cpu_binners));
    } else if (driver_type == "save_plain") {
        query_driver = std::make_unique<otters::PlainSaveDriver<double, 3>>(prefix);
    } else if (driver_type == "bitmap_copy_only") {
        using Binner = otters::GPUDynamicEqualWidthBinner1<64, double>;
        std::array<Binner, 3> binners{
                Binner(bitmap_range_prefix + ".0"),
                Binner(bitmap_range_prefix + ".1"),
                Binner(bitmap_range_prefix + ".2"),
        };
        using CPUBinner = otters::EqualBinner<double>;
        std::array<CPUBinner, 3> cpu_binners{
                CPUBinner(64, 0, 1e9),
                CPUBinner(64, 0, 1e9),
                CPUBinner(64, 0, 1e9),
        };
        query_driver =
                std::make_unique<otters::BitmapCopyOnlyDriver<double, 3>>(std::move(binners), std::move(cpu_binners));
    } else if (driver_type == "plain_copy_only") {
        query_driver = std::make_unique<otters::PlainCopyOnlyDriver<double, 3>>();
    } else if (driver_type == "plain_join_count") {
        using CPUBinner = otters::CPUDynamicEqualWidthBinner1<double>;
        std::array<CPUBinner, 3> cpu_binners {
                CPUBinner(64, bitmap_range_prefix + ".0"),
                CPUBinner(64, bitmap_range_prefix + ".1"),
                CPUBinner(64, bitmap_range_prefix + ".2"),
        };
        using Driver = otters::PlainJoinCountDriver<double, 3, 3, CPUBinner>;
        query_driver = std::unique_ptr<Driver>(new Driver(
                cpu_workers,
                cpu_binners,
                64,
                prefix
        ));
    } else if (driver_type == "bitmap_join_count") {
        using CPUBinner = otters::CPUDynamicEqualWidthBinner1<double>;
        std::array<CPUBinner, 3> cpu_binners {
                CPUBinner(64, bitmap_range_prefix + ".0"),
                CPUBinner(64, bitmap_range_prefix + ".1"),
                CPUBinner(64, bitmap_range_prefix + ".2"),
        };
        using Driver = otters::BitmapJoinCountDriver<double, 3, CPUBinner>;
        using Binner = otters::GPUEqualWidthBinner<64, double>;

        std::array<Binner, 3> binners{
                Binner(0, 1e9),
                Binner(0, 1e9),
                Binner(0, 1e9),
        };

        query_driver = std::unique_ptr<Driver>(new Driver(
                cpu_workers,
                prefix,
                binners
        ));
    } else if (driver_type == "plain_csm") {
        using CPUBinner = otters::CPUDynamicEqualWidthBinner1<double>;
        std::array<CPUBinner, 3> cpu_binners {
                CPUBinner(64, bitmap_range_prefix + ".0"),
                CPUBinner(64, bitmap_range_prefix + ".1"),
                CPUBinner(64, bitmap_range_prefix + ".2"),
        };
#ifdef USE_MPI
        using Driver = otters::MPIPlainCSMDriver<double, 0, 3, CPUBinner>;
        query_driver = std::unique_ptr<Driver>(new Driver(
            mpi_info,
            cpu_workers,
            {},
            cpu_binners,
            64,
            64,
            prefix
        ));
#else
        using Driver = otters::PlainCSMDriver<double, 0, 3, CPUBinner>;
        query_driver = std::unique_ptr<Driver>(new Driver(
                cpu_workers,
                {},
                cpu_binners,
                64,
                64,
                prefix
        ));
#endif
    } else if (driver_type == "bitmap_csm") {
        using Binner = otters::GPUDynamicEqualWidthBinner1<64, double>;
        std::array<Binner, 3> binners{
                Binner(bitmap_range_prefix + ".0"),
                Binner(bitmap_range_prefix + ".1"),
                Binner(bitmap_range_prefix + ".2"),
        };
        using CPUBinner = otters::CPUDynamicEqualWidthBinner1<double>;
        std::array<CPUBinner, 3> cpu_binners{
                CPUBinner(64, bitmap_range_prefix + ".0"),
                CPUBinner(64, bitmap_range_prefix + ".1"),
                CPUBinner(64, bitmap_range_prefix + ".2"),
        };
        using GPUBitmap = otters::GPURoaringBitmap<64, otters::DefaultRoaringTraits>;

#ifdef USE_MPI
        using Driver = otters::MPIBitmapCSMDriver<double, 0, 3, GPUBitmap, Binner, CPUBinner>;
        query_driver = std::unique_ptr<Driver>(new Driver(
                mpi_info,
                cpu_workers,
                {},
                cpu_binners,
                64,
                64,
                prefix,
                binners
        ));
#else
        using Driver = otters::BitmapCSMDriver<double, 0, 3, GPUBitmap, Binner, CPUBinner>;
        query_driver = std::unique_ptr<Driver>(new Driver(
                cpu_workers,
                {},
                cpu_binners,
                64,
                64,
                prefix,
                binners
        ));
#endif

    } else {
        log(otters::OTTERS_LOG_ERROR, "Not supported driver: {}", driver_type);
        std::terminate();
    }



        Domain domain(num_cells);

        /*save pointers so we can write phi instead of domain.phi*/
        double* phi = domain.phi;
        double* rho = domain.rho;
        double* ef = domain.ef;
        float* nde = domain.nde;
        float* ndi = domain.ndi;

        /*also allocate GPU space for the ion and electron density*/
        float *nde_gpu, *ndi_gpu;
        OTTERS_CUDA_ERRORCHECK(cudaMalloc((void**)&nde_gpu, domain.ni * sizeof(float)));
        OTTERS_CUDA_ERRORCHECK(cudaMalloc((void**)&ndi_gpu, domain.ni * sizeof(float)));

        /*also allocate GPU space for the electric field*/
        double* ef_gpu;
        OTTERS_CUDA_ERRORCHECK(cudaMalloc((void**)&ef_gpu, domain.ni * sizeof(double)));

        /*clear data*/
        memset(phi, 0, sizeof(double) * domain.ni);

        /*variables to hold species data ions*/
        Species ions;
        Species electrons;

        /*set material data*/
        ions.mass = 16 * AMU;
        ions.charge = QE;
        ions.spwt = PLASMA_DEN * domain.xl / num_ions;
        ions.np = 0;
        ions.np_alloc = num_ions;
        ions.alloc_space();

        electrons.mass = ME; // electrons
        electrons.charge = -QE;
        electrons.spwt = PLASMA_DEN * domain.xl / num_electrons;
        electrons.np = 0;
        electrons.np_alloc = num_electrons;
        electrons.alloc_after(ions);

        /*randomize RNG*/
        auto seed = time(NULL);
        srand(seed);
        fmt::print("SEED={}\n", seed);

        /*load uniformly spaced ions and electrons*/
        double delta_ions = domain.xl / num_ions;
        double v_thi = sqrt(2 * K * ION_TEMP * EV_TO_K / ions.mass);
        for (int p = 0; p < num_ions; p++) {
            double x = domain.x0 + p * delta_ions;
            double v = SampleVel(v_thi);
            AddParticle(&ions, x, v);
        }

        /*now do the same for electrons*/
        double delta_electrons = domain.xl / num_electrons;
        double v_the = sqrt(2 * K * ELECTRON_TEMP * EV_TO_K / electrons.mass);
        for (int p = 0; p < num_electrons; p++) {
            double x = domain.x0 + p * delta_electrons;
            double v = SampleVel(v_the);
            AddParticle(&electrons, x, v);
        }

        /*copy particles to the GPU*/
        //    OTTERS_CUDA_ERRORCHECK(cudaMemcpy(ions.part_gpu,ions.part, num_ions *sizeof(Particle),cudaMemcpyHostToDevice));
        ions.copy_to_gpu();
        //    OTTERS_CUDA_ERRORCHECK(cudaMemcpy(electrons.part_gpu,electrons.part, num_ions *sizeof(Particle),cudaMemcpyHostToDevice));
        electrons.copy_to_gpu();

        /*compute number density*/
        ScatterSpecies(&ions, ndi, ndi_gpu, domain);
        ScatterSpecies(&electrons, nde, nde_gpu, domain);

        /*compute charge density and solve potential*/
        ComputeRho(&ions, &electrons, domain);
        SolvePotential(phi, rho, domain);
        ComputeEF(phi, ef, ef_gpu, domain);

        RewindSpecies(&ions, ef_gpu);
        RewindSpecies(&electrons, ef_gpu);

        /*OUTPUT*/
        //    file_res = fopen("results.dat","w");
        //    fprintf(file_res,"VARIABLES = x nde ndi rho phi ef\n");
        //    WriteResults(0);

        clock_t start = clock(); // grab starting clock time
#ifdef USE_MPI
        MPI_Barrier(MPI_COMM_WORLD);
        double start_time = MPI_Wtime();
#else
        boost::timer::auto_cpu_timer t("SIM_TIME=%w\n");
#endif
        otters::Timer timer("sim_only");
        /* MAIN LOOP*/
        for (int ts = 0; ts < num_iters; ts++) {
            //        fmt::print("Starting Timestep = {}", ts);
            /*compute number density*/
            timer.start();
            ScatterSpecies(&ions, ndi, ndi_gpu, domain);
            ScatterSpecies(&electrons, nde, nde_gpu, domain);

            ComputeRho(&ions, &electrons, domain);
            SolvePotential(phi, rho, domain);
            ComputeEF(phi, ef, ef_gpu, domain);

            /*move particles*/
            PushSpecies(&electrons, ef_gpu, domain);
            PushSpecies(&ions, ef_gpu, domain);
            timer.stop();

            query_driver->add_timestep_data(ts,
                    { ions.gpu_part_x, ions.gpu_part_v, ions.gpu_part_ef },
                    { num_ions * 2, num_ions * 2, num_ions * 2 });

            // /*write diagnostics*/
            // if (ts%25==0)
            // {
            //     /*max phi*/
            //     double max_phi = abs(phi[0]);
            //     for (int i=0;i<domain.ni;i++)
            //         if (abs(phi[i])>max_phi) max_phi=abs(phi[i]);

            //     printf("TS:%i\tnp_i:%d\tnp_e:%d\tdphi:%.3g\n",
            //             ts,ions.np,electrons.np,max_phi-phi[0]);
            // }

            /*save data*/
            //        if (ts%1000==0) WriteResults(ts);
        }
        query_driver->end_input();
        query_driver.reset(nullptr); /* force the destruction, refresh time. */

        clock_t end = clock();

        //    fclose(file_res);

        /*free up memory*/

        cudaFree(nde_gpu);
        cudaFree(ndi_gpu);
        cudaFree(ef_gpu);

        /*free particles*/
        /*
        delete ions.part;
        delete electrons.part;
        cudaFree(ions.part_gpu);
        cudaFree(electrons.part_gpu);
         */
        ions.deallocate_space();
        //    electrons.deallocate_space();

        printf("Time per time step: %.3g ms\n", 1000 * (end - start) / (double)(CLOCKS_PER_SEC * num_iters));

        /* call cudaDeviceReset for correct profiling data*/
        OTTERS_CUDA_ERRORCHECK(cudaDeviceReset());

#ifdef USE_MPI
        MPI_Barrier(MPI_COMM_WORLD);
        double end_time = MPI_Wtime();
        if(mpi_info._rank == 0) {
            fmt::print("SIM_TIME={}\n", end_time - start_time);
        }
    } else {
        if(driver_type == "plain_csm") {
        using CPUBinner = otters::CPUDynamicEqualWidthBinner1<double>;
        std::array<CPUBinner, 3> cpu_binners {
                CPUBinner(64, bitmap_range_prefix + ".0"),
                CPUBinner(64, bitmap_range_prefix + ".1"),
                CPUBinner(64, bitmap_range_prefix + ".2"),
        };
        using Driver = otters::MPIPlainCSMDriver<double, 0, 3, CPUBinner>;
        query_driver = std::unique_ptr<Driver>(new Driver(
            mpi_info,
            cpu_workers,
            {},
            cpu_binners,
            64,
            64,
            prefix
        ));
        } else if (driver_type == "bitmap_csm") {
            using Binner = otters::GPUDynamicEqualWidthBinner1<64, double>;
            std::array<Binner, 3> binners{
                    Binner(bitmap_range_prefix + ".0"),
                    Binner(bitmap_range_prefix + ".1"),
                    Binner(bitmap_range_prefix + ".2"),
            };
            using CPUBinner = otters::CPUDynamicEqualWidthBinner1<double>;
            std::array<CPUBinner, 3> cpu_binners{
                    CPUBinner(64, bitmap_range_prefix + ".0"),
                    CPUBinner(64, bitmap_range_prefix + ".1"),
                    CPUBinner(64, bitmap_range_prefix + ".2"),
            };
            using GPUBitmap = otters::GPURoaringBitmap<64, otters::DefaultRoaringTraits>;

            using Driver = otters::MPIBitmapCSMDriver<double, 0, 3, GPUBitmap, Binner, CPUBinner>;
            query_driver = std::unique_ptr<Driver>(new Driver(
                    mpi_info,
                    cpu_workers,
                    {},
                    cpu_binners,
                    64,
                    64,
                    prefix,
                    binners
            ));
        }

        MPI_Barrier(MPI_COMM_WORLD);
        query_driver.reset(nullptr); /* force the destruction, refresh time. */
        MPI_Barrier(MPI_COMM_WORLD);
    }
    MPI_Finalize();
#endif

    return 0;
}
