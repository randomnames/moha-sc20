/*

                 Copyright (c) 2010.
      Lawrence Livermore National Security, LLC.
Produced at the Lawrence Livermore National Laboratory.
                  LLNL-CODE-461231
                All rights reserved.

This file is part of LULESH, Version 1.0.
Please also read this link -- http://www.opensource.org/licenses/index.php

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the disclaimer below.

   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the disclaimer (as noted below)
     in the documentation and/or other materials provided with the
     distribution.

   * Neither the name of the LLNS/LLNL nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL LAWRENCE LIVERMORE NATIONAL SECURITY, LLC,
THE U.S. DEPARTMENT OF ENERGY OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Additional BSD Notice

1. This notice is required to be provided under our contract with the U.S.
   Department of Energy (DOE). This work was produced at Lawrence Livermore
   National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.

2. Neither the United States Government nor Lawrence Livermore National
   Security, LLC nor any of their employees, makes any warranty, express
   or implied, or assumes any liability or responsibility for the accuracy,
   completeness, or usefulness of any information, apparatus, product, or
   process disclosed, or represents that its use would not infringe
   privately-owned rights.

3. Also, reference herein to any specific commercial products, process, or
   services by trade name, trademark, manufacturer or otherwise does not
   necessarily constitute or imply its endorsement, recommendation, or
   favoring by the United States Government or Lawrence Livermore National
   Security, LLC. The views and opinions of authors expressed herein do not
   necessarily state or reflect those of the United States Government or
   Lawrence Livermore National Security, LLC, and shall not be used for
   advertising or product endorsement purposes.

*/
#include <otters/common/utils/git_version.h>
#include <otters/cuda/bitmap/query_driver.h>

#include <otters/common/bitmap/binning.h>
#include <otters/common/bitmap/roaring_bitmap.h>
#include <otters/cuda/bitmap/roaring.cuh>
#include <otters/cuda/bitmap/query_driver.h>
#include <otters/cuda/query/timestep.cuh>
#include <otters/cuda/query/drivers/plain_save.cuh>
#include <otters/cuda/query/drivers/bitmap_save.cuh>
#include <otters/cuda/query/drivers/plain_csm.cuh>
#include <otters/cuda/query/drivers/plain_join_count.cuh>
#include <otters/cuda/query/drivers/bitmap_join_count.cuh>
#include <otters/cuda/query/drivers/bitmap_csm.cuh>
#include <otters/cuda/query/drivers/plain_copy.cuh>
#include <otters/cuda/query/drivers/plain_filter_oir.cuh>
#include <otters/cuda/query/drivers/bitmap_filter_oir.cuh>
#include <otters/cuda/query/drivers/bitmap_copy.cuh>
#include <otters/cuda/query/drivers/save_min_max.cuh>
#include <otters/common/utils/log.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <sstream>

#include <util.h>
#include <sm_utils.inl>
#include <cuda.h>
#include <allocator.h>
#include "cuda_profiler_api.h"

#ifdef USE_MPI
#include <mpi.h>
#endif

#include <sys/time.h>
#include <unistd.h>

#include "lulesh.h"

#include <boost/program_options.hpp>

#include <fmt/format.h>

#include "lulesh-impl.h"

namespace po = boost::program_options;

int main(int argc, char *argv[])
{
  otters::print_version();
  po::options_description desc("Allowed options:");
  desc.add_options()
    ("help", "produce help message")
    ("num_iters", po::value<int>()->default_value(-1), "the number of iterations, default to -1.")
    ("problem_size", po::value<int>()->required(), "problem size")
    ("query_processor_type", po::value<std::string>()->default_value("none"),
        "types of query processor: gpu_only, direct_copy, generate_bitmap")
    ("bitmap_range_prefix", po::value<std::string>(), "The prefix file for loading the bitmap range")
    ("structured", po::bool_switch(), "use 3-d structured simulation")
    ("cpu_workers", po::value<int>()->default_value(4), "number of cpu query workers.")
    ("verify_gpu", po::bool_switch(), "verify the gpu result.")
    ("prefix", po::value<std::string>()->default_value(""), "prefix for i/o.")
    ("balance", po::value<int>()->default_value(1), "balance parameter of the simulation")
    ("cost", po::value<int>()->default_value(1), "cost parameter of the simulation")
  ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  int num_iters = vm["num_iters"].as<int>();

  std::unique_ptr<otters::QueryDriver<Real_t, 4>> query_driver;

  double bitmap_min = 0.0;
  double bitmap_max = 1.0e9;
  if (vm.count("bitmap_range") == 0) {
      std::cerr << "Warning: did not sepcify bitmap range, using default (0.0, 1.0e9)\n";
  } else {
      auto range = vm["bitmap_range"].as<std::vector<double>>();
      bitmap_min = range[0];
      bitmap_max = range[1];
  }

  bool structured = vm["structured"].as<bool>();
  int nx = vm["problem_size"].as<int>();
  int problem_size = nx*nx*nx;
  std::string prefix = vm["prefix"].as<std::string>();
  std::string bitmap_range_prefix = vm["bitmap_range_prefix"].as<std::string>();
  int balance = vm["balance"].as<int>();
  int cost = vm["cost"].as<int>();

/*
  if(vm["bitmap_type"].as<std::string>() == "none") {
      query_driver = std::make_unique<otters::NoneQueryDriver<Real_t>>();
  } else if (vm["bitmap_type"].as<std::string>() == "GPURoaring") {
      bool verify_gpu = vm["verify_gpu"].as<bool>();
      std::cerr << "verify=" << verify_gpu << "\n";
      query_driver = std::make_unique<otters::GPURoaringBitmapQueryDriver<Real_t>>
          (problem_size, bitmap_min, bitmap_max, verify_gpu);
  } else if (vm["bitmap_type"].as<std::string>() == "CPURoaring") {
      int bitmap_cpu_workers = vm["bitmap_cpu_workers"].as<int>();
      query_driver = std::make_unique<otters::CPURoaringBitmapQueryDriver<Real_t>>(
              problem_size, bitmap_min, bitmap_max, bitmap_cpu_workers);
  }
  */

  auto query_processor_type = vm["query_processor_type"].as<std::string>();
  int cpu_workers = vm["cpu_workers"].as<int>();

  if(query_processor_type == "plain_copy_only" || query_processor_type == "plain_copy_only2") {
      query_driver = std::make_unique<otters::PlainCopyOnlyDriver<Real_t, 4>>();
  } else if (query_processor_type == "simulation_only") {
      query_driver = std::make_unique<otters::NoneQueryDriver<Real_t, 4>>();
  } else if (query_processor_type == "save_plain") {
      query_driver = std::make_unique<otters::PlainSaveDriver<Real_t, 4>>(prefix);
  } else if (query_processor_type == "bitmap_copy_only" || query_processor_type == "bitmap_copy_only2") {
      using Binner = otters::GPUDynamicEqualWidthBinner1<64, Real_t>;
      std::array<Binner, 4> binners{
          Binner(bitmap_range_prefix + ".0"),
          Binner(bitmap_range_prefix + ".1"),
          Binner(bitmap_range_prefix + ".2"),
          Binner(bitmap_range_prefix + ".3")
      };
      using CPUBinner = otters::EqualBinner<Real_t>;
      std::array<CPUBinner, 4> cpu_binners{
          CPUBinner(64, 0, 1e9),
          CPUBinner(64, 0, 1e9),
          CPUBinner(64, 0, 1e9),
          CPUBinner(64, 0, 1e5),
      };
      query_driver =
              std::make_unique<otters::BitmapCopyOnlyDriver<Real_t, 4>>(std::move(binners), std::move(cpu_binners));

  } else if (query_processor_type == "save_bitmap") {
      using Binner = otters::GPUDynamicEqualWidthBinner1<64, Real_t>;
      std::array<Binner, 4> binners{
          Binner(bitmap_range_prefix + ".0"),
          Binner(bitmap_range_prefix + ".1"),
          Binner(bitmap_range_prefix + ".2"),
          Binner(bitmap_range_prefix + ".3")
      };
      using CPUBinner = otters::EqualBinner<Real_t>;
      std::array<CPUBinner, 4> cpu_binners{
          CPUBinner(64, 0, 1e9),
          CPUBinner(64, 0, 1e9),
          CPUBinner(64, 0, 1e9),
          CPUBinner(64, 0, 1e5),
      };
      query_driver =
              std::make_unique<otters::BitmapSaveDriver<Real_t, 4>>(prefix, std::move(binners), std::move(cpu_binners));
  } else if (query_processor_type == "plain_csm") {
      using CPUBinner = otters::CPUDynamicEqualWidthBinner1<Real_t>;
      std::array<CPUBinner, 4> cpu_binners {
        CPUBinner(64, bitmap_range_prefix + ".0"),
        CPUBinner(64, bitmap_range_prefix + ".1"),
        CPUBinner(64, bitmap_range_prefix + ".2"),
        CPUBinner(64, bitmap_range_prefix + ".3")
      };
      using Driver = otters::PlainCSMDriver<Real_t, 3, 4, CPUBinner>;
      query_driver = std::unique_ptr<Driver>(new Driver(
        cpu_workers,
        {nx,nx,nx},
        cpu_binners,
        64,
        64,
        prefix
      ));
  } else if (query_processor_type == "plain_join_count" || query_processor_type == "simulation_similarity") {
      using CPUBinner = otters::CPUDynamicEqualWidthBinner1<Real_t>;
      std::array<CPUBinner, 4> cpu_binners {
        CPUBinner(64, bitmap_range_prefix + ".0"),
        CPUBinner(64, bitmap_range_prefix + ".1"),
        CPUBinner(64, bitmap_range_prefix + ".2"),
        CPUBinner(64, bitmap_range_prefix + ".3")
      };
      using Driver = otters::PlainJoinCountDriver<Real_t, 3, 4, CPUBinner>;
      query_driver = std::unique_ptr<Driver>(new Driver(
        cpu_workers,
        cpu_binners,
        64,
        prefix
      ));
  } else if (query_processor_type == "plain_filter_oir") {
      using Driver = otters::PlainFilterOIRDriver<Real_t, 3, 4>;
      query_driver = std::unique_ptr<Driver>(new Driver(
              cpu_workers,
              {0,0,0},
              {nx,nx,nx},
              {nx,nx,nx},
              {otters::Range<double>{9e8,1e9}, otters::Range<double>{9e8,1e9}, otters::Range<double>{9e8,1e9}, otters::Range<double>{9e5,1e6}}
      ));
//      otters::log(otters::OTTERS_LOG_DEBUG, "Initialized driver.");
  } else if (query_processor_type == "bitmap_filter_oir") {
      using Driver = otters::BitmapFilterOIRDriver<Real_t, 3, 4>;
      using Binner = otters::GPUDynamicEqualWidthBinner1<64, Real_t>;
      std::array<Binner, 4> binners{
              Binner(bitmap_range_prefix + ".0"),
              Binner(bitmap_range_prefix + ".1"),
              Binner(bitmap_range_prefix + ".2"),
              Binner(bitmap_range_prefix + ".3")
      };
      using CPUBinner = otters::EqualBinner<Real_t>;
      std::array<CPUBinner, 4> cpu_binners{
              CPUBinner(64, 0, 1e9),
              CPUBinner(64, 0, 1e9),
              CPUBinner(64, 0, 1e9),
              CPUBinner(64, 0, 1e5),
      };
//      query_driver = std::make_unique<Driver>(
      query_driver.reset(new Driver(
              cpu_workers,
              {0,0,0},
              {0,0,0},
              {nx, nx, nx},
              {otters::Range<double>{9e8,1e9}, otters::Range<double>{9e8,1e9}, otters::Range<double>{9e8,1e9}, otters::Range<double>{9e5,1e6}},
              binners,
              cpu_binners,
              64));

  } else if (query_processor_type == "bitmap_csm") {
      using CPUBinner = otters::CPUDynamicEqualWidthBinner1<Real_t>;
      std::array<CPUBinner, 4> cpu_binners {
              CPUBinner(64, bitmap_range_prefix + ".0"),
              CPUBinner(64, bitmap_range_prefix + ".1"),
              CPUBinner(64, bitmap_range_prefix + ".2"),
              CPUBinner(64, bitmap_range_prefix + ".3")
      };
      using Binner = otters::GPUDynamicEqualWidthBinner1<64, Real_t>;
      std::array<Binner, 4> binners{
              Binner(bitmap_range_prefix + ".0"),
              Binner(bitmap_range_prefix + ".1"),
              Binner(bitmap_range_prefix + ".2"),
              Binner(bitmap_range_prefix + ".3")
      };

      using GPUBitmap = otters::GPURoaringBitmap<64, otters::DefaultRoaringTraits>;
      using Driver = otters::BitmapCSMDriver<Real_t, 3, 4, GPUBitmap, Binner, CPUBinner>;
      query_driver = std::unique_ptr<Driver>(new Driver(
              cpu_workers,
              {nx,nx,nx},
              cpu_binners,
              64,
              64,
              prefix,
              binners
      ));
  } else if (query_processor_type == "bitmap_join_count") {
      using CPUBinner = otters::CPUDynamicEqualWidthBinner1<Real_t>;
      std::array<CPUBinner, 4> cpu_binners {
              CPUBinner(64, bitmap_range_prefix + ".0"),
              CPUBinner(64, bitmap_range_prefix + ".1"),
              CPUBinner(64, bitmap_range_prefix + ".2"),
              CPUBinner(64, bitmap_range_prefix + ".3")
      };
      using Driver = otters::BitmapJoinCountDriver<Real_t, 4, CPUBinner>;
      using Binner = otters::GPUEqualWidthBinner<64, Real_t>;

      std::array<Binner, 4> binners{
              Binner(0, 1e9),
              Binner(0, 1e9),
              Binner(0, 1e9),
              Binner(0, 1e5),
      };

      query_driver = std::unique_ptr<Driver>(new Driver(
              cpu_workers,
              prefix,
              binners
      ));
  } else if (query_processor_type == "save_min_max2") {
      query_driver = std::make_unique<otters::SaveMinMaxDriver<Real_t, 4>>(bitmap_range_prefix);
  } /* else if (query_processor_type == "none") {
      query_driver = std::make_unique<otters::NoneQueryDriver<Real_t, 4>>(bitmap_range_prefix);
  } */ else {
      fmt::print("Driver does not exist.\n");
      exit(1);
  }

    Int_t numRanks ;
    Int_t myRank ;

#if USE_MPI
    Domain_member fieldData ;

  MPI_Init(&argc, &argv) ;
  MPI_Comm_size(MPI_COMM_WORLD, &numRanks) ;
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank) ;
#else
    numRanks = 1;
    myRank = 0;
#endif

    cuda_init(myRank);

    Domain *locDom ;

    // Set up the mesh and decompose. Assumes regular cubes for now
    Int_t col, row, plane, side;
    InitMeshDecomp(numRanks, myRank, &col, &row, &plane, &side);

    // TODO: change default nr to 11
    Int_t nr = 11;
//    Int_t balance = 1;
//    Int_t cost = 1;

    // TODO: modify this constructor to account for new fields
    // TODO: setup communication buffers
    locDom = NewDomain(argv, numRanks, col, row, plane, nx, side, structured, nr, balance, cost);

#if USE_MPI
    // copy to the host for mpi transfer
   locDom->h_nodalMass = locDom->nodalMass;

   fieldData = &Domain::get_nodalMass;

   // Initial domain boundary communication 
   CommRecv(*locDom, MSG_COMM_SBN, 1,
            locDom->sizeX + 1, locDom->sizeY + 1, locDom->sizeZ + 1,
            true, false) ;
   CommSend(*locDom, MSG_COMM_SBN, 1, &fieldData,
            locDom->sizeX + 1, locDom->sizeY + 1, locDom->sizeZ + 1,
            true, false) ;
   CommSBN(*locDom, 1, &fieldData) ;

   // copy back to the device
   locDom->nodalMass = locDom->h_nodalMass;

   // End initialization
   MPI_Barrier(MPI_COMM_WORLD);
#endif

    cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);

    /* timestep to solution */
    int its=0;

    if (myRank == 0) {
        if (structured)
            fmt::print("END_TIME={}\nNX={}\nPROBLEM_SIZE={}\n",locDom->stoptime,nx,nx*nx*nx);
        else
            fmt::print("END_TIME={}\nPROBLEM_SIZE={}\n",locDom->stoptime, locDom->numElem);
    }

    cudaProfilerStart();

#if USE_MPI
    double start = MPI_Wtime();
#else
    timeval start;
    gettimeofday(&start, NULL) ;
#endif
    otters::Timer sim_timer("sim_timer");
    while(locDom->time_h < locDom->stoptime)
    {
        sim_timer.start();
        // this has been moved after computation of volume forces to hide launch latencies
        //TimeIncrement(locDom) ;

        LagrangeLeapFrog(locDom) ;

        sim_timer.stop();

        auto* e_data = locDom->e.raw();
        auto* q_data = locDom->q.raw();
        auto* p_data = locDom->p.raw();
        auto* v_data = locDom->v.raw();

        auto size = nx*nx*nx;
        auto BlockSize = 512;

        query_driver->add_timestep_data(its, {e_data, q_data, p_data, v_data}, {size, size, size, size});

        //printf("cycle = %d, time = %e, dt=%e min = %e max = %e\n",
        //        its + 1,
        //        double(locDom->time_h),
        //        double(locDom->deltatime_h),
        //        result.min_val,
        //        result.max_val
        //);

        checkErrors(locDom,its,myRank);

#if LULESH_SHOW_PROGRESS
        if (myRank == 0)
	 printf("cycle = %d, time = %e, dt=%e\n", its+1, double(locDom->time_h), double(locDom->deltatime_h) ) ;
#endif
        its++;
        if (its == num_iters) break;
    }
    query_driver->end_input();

    // make sure GPU finished its work
    cudaDeviceSynchronize();

    query_driver.reset(nullptr);

    // Use reduced max elapsed time
    double elapsed_time;
#if USE_MPI
    elapsed_time = MPI_Wtime() - start;
#else
    timeval end;
    gettimeofday(&end, NULL) ;
    elapsed_time = (double)(end.tv_sec - start.tv_sec) + ((double)(end.tv_usec - start.tv_usec))/1000000 ;
#endif

    double elapsed_timeG;
#if USE_MPI
    MPI_Reduce(&elapsed_time, &elapsed_timeG, 1, MPI_DOUBLE,
              MPI_MAX, 0, MPI_COMM_WORLD);
#else
    elapsed_timeG = elapsed_time;
#endif

    cudaProfilerStop();

    if (myRank == 0)
        VerifyAndWriteFinalOutput(elapsed_timeG, *locDom, its, nx, numRanks, structured);

#ifdef SAMI
    DumpDomain(locDom) ;
#endif
    // cudaDeviceReset();

#if USE_MPI
    MPI_Finalize() ;
#endif

    return 0 ;
}

